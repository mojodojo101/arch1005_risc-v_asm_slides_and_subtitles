1
00:00:00,160 --> 00:00:04,240
so what can we take away from this code

2
00:00:02,320 --> 00:00:07,240
well the first thing is that this first

3
00:00:04,240 --> 00:00:09,040
use of BGE branch if greater than or

4
00:00:07,240 --> 00:00:10,599
equal might seem a little strange

5
00:00:09,040 --> 00:00:11,840
because that is certainly not what we

6
00:00:10,599 --> 00:00:13,200
put in the code we don't want to check

7
00:00:11,840 --> 00:00:15,400
if it's greater than or equal we want to

8
00:00:13,200 --> 00:00:17,640
check only if it's greater but because

9
00:00:15,400 --> 00:00:19,920
the code is already gotten past a branch

10
00:00:17,640 --> 00:00:22,320
if not equal by the time it gets down

11
00:00:19,920 --> 00:00:24,080
here it knows they can't be equal and

12
00:00:22,320 --> 00:00:26,400
therefore it's safe to use the branch if

13
00:00:24,080 --> 00:00:28,039
greater furthermore you see another

14
00:00:26,400 --> 00:00:29,759
branch if greater later on which is

15
00:00:28,039 --> 00:00:30,560
weird because you're checking for less

16
00:00:29,759 --> 00:00:32,239
than

17
00:00:30,560 --> 00:00:34,840
but it can get away with that because it

18
00:00:32,239 --> 00:00:37,200
just swaps the orders of the operands so

19
00:00:34,840 --> 00:00:40,559
here it was a5 greater than or equal to

20
00:00:37,200 --> 00:00:44,120
a4 and now you switch the order and it's

21
00:00:40,559 --> 00:00:46,719
is a4 greater than or equal to a5 which

22
00:00:44,120 --> 00:00:48,160
would actually be a less than because

23
00:00:46,719 --> 00:00:50,239
again the code knows that they're not

24
00:00:48,160 --> 00:00:52,359
equal at this point now I just want to

25
00:00:50,239 --> 00:00:54,359
show you the ghost of Christmas future

26
00:00:52,359 --> 00:00:56,480
and that is the fact that most reverse

27
00:00:54,359 --> 00:00:58,760
engineers do not look at code in

28
00:00:56,480 --> 00:01:00,680
straight line assembly they can see

29
00:00:58,760 --> 00:01:03,000
things in a much nicer form that is

30
00:01:00,680 --> 00:01:04,839
called a control flow graph so once you

31
00:01:03,000 --> 00:01:07,159
start dealing with lots of control flow

32
00:01:04,839 --> 00:01:09,840
and lots of nested ifs and eles and

33
00:01:07,159 --> 00:01:12,119
switches and ws and fours it is very

34
00:01:09,840 --> 00:01:13,960
helpful to have control flow graphs and

35
00:01:12,119 --> 00:01:15,880
that's exactly what tools like gidra

36
00:01:13,960 --> 00:01:17,799
will give you now that doesn't help you

37
00:01:15,880 --> 00:01:20,079
for now we're just expecting you to go

38
00:01:17,799 --> 00:01:21,880
through the code in GDB looking at

39
00:01:20,079 --> 00:01:24,680
straight line assembly but this is just

40
00:01:21,880 --> 00:01:26,240
to say things will get easier later on

41
00:01:24,680 --> 00:01:27,759
specifically if you're going down the

42
00:01:26,240 --> 00:01:29,640
reverse engineering path if you're like

43
00:01:27,759 --> 00:01:32,840
a developer then well you just got to

44
00:01:29,640 --> 00:01:34,600
use whatever tools are available okay

45
00:01:32,840 --> 00:01:37,280
picked up new assembly instructions we

46
00:01:34,600 --> 00:01:41,240
are now into these branch instructions

47
00:01:37,280 --> 00:01:43,640
b& branch if not equal BGE branch if

48
00:01:41,240 --> 00:01:46,040
greater than or equal putting those on

49
00:01:43,640 --> 00:01:48,240
our diagram we're going to pop up bne

50
00:01:46,040 --> 00:01:52,439
and BGE over here and start ourselves a

51
00:01:48,240 --> 00:01:52,439
nice little control flow area

