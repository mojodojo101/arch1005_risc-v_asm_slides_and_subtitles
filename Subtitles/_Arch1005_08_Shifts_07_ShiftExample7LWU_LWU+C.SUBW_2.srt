1
00:00:00,120 --> 00:00:03,520
now nothing brightens my day like a

2
00:00:01,680 --> 00:00:06,560
little bit of chaos magic so let's go

3
00:00:03,520 --> 00:00:08,480
ahead and do that and we did actually

4
00:00:06,560 --> 00:00:11,360
pick up a new compressed instruction

5
00:00:08,480 --> 00:00:15,000
here compressed subw so we already had

6
00:00:11,360 --> 00:00:17,760
the subw before that is the word size

7
00:00:15,000 --> 00:00:20,640
subtract register from register and so

8
00:00:17,760 --> 00:00:22,640
now we've got a compressed form cool so

9
00:00:20,640 --> 00:00:25,400
the takeaways from this example is that

10
00:00:22,640 --> 00:00:27,720
GCC really doesn't want to generate LW

11
00:00:25,400 --> 00:00:31,000
's for whatever reason and actually

12
00:00:27,720 --> 00:00:32,840
neither does clang so lwu that is load

13
00:00:31,000 --> 00:00:34,480
Word unsigned you'd think that you know

14
00:00:32,840 --> 00:00:36,719
utilization you could simply use you

15
00:00:34,480 --> 00:00:38,920
know an unsigned int and have some value

16
00:00:36,719 --> 00:00:40,960
read in and it would create an lwo Well

17
00:00:38,920 --> 00:00:43,559
turns out not to be the case they really

18
00:00:40,960 --> 00:00:45,600
seem to prefer the load words and then

19
00:00:43,559 --> 00:00:47,719
just using other assembly instructions

20
00:00:45,600 --> 00:00:52,120
that don't operate on the upper bits and

21
00:00:47,719 --> 00:00:53,640
that is equivalent of course to a lwu so

22
00:00:52,120 --> 00:00:55,640
what I ultimately found was that when I

23
00:00:53,640 --> 00:00:57,359
played this particular game I could get

24
00:00:55,640 --> 00:00:59,600
it to generate the

25
00:00:57,359 --> 00:01:01,320
lwu and the only reason I want to do

26
00:00:59,600 --> 00:01:04,360
that is is cuz I want to help you get

27
00:01:01,320 --> 00:01:07,360
this checked off your list and adding to

28
00:01:04,360 --> 00:01:10,479
our diagram we have the lwo here as the

29
00:01:07,360 --> 00:01:14,200
missing link between these store word

30
00:01:10,479 --> 00:01:16,119
load Word and the store half word load

31
00:01:14,200 --> 00:01:17,880
half word which also had a load half

32
00:01:16,119 --> 00:01:20,840
word unsigned and we had the load bite

33
00:01:17,880 --> 00:01:23,320
unsigned so these things all the same

34
00:01:20,840 --> 00:01:25,640
sort of pattern store word load Word

35
00:01:23,320 --> 00:01:27,479
load Word unsigned store half word load

36
00:01:25,640 --> 00:01:29,920
half word load half word unsigned and

37
00:01:27,479 --> 00:01:32,759
the same for bytes and also we pick

38
00:01:29,920 --> 00:01:34,840
picked up a compressed subtract W as we

39
00:01:32,759 --> 00:01:36,799
were here and then we've got our nice

40
00:01:34,840 --> 00:01:40,759
little filled in space that I was

41
00:01:36,799 --> 00:01:40,759
reserving just for that

