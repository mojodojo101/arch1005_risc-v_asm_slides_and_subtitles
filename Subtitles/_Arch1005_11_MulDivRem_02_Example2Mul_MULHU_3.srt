1
00:00:00,760 --> 00:00:04,799
okay so I recommended on this example

2
00:00:02,480 --> 00:00:06,680
that you literally make a code comment

3
00:00:04,799 --> 00:00:08,480
on every single line so you could

4
00:00:06,680 --> 00:00:10,400
understand what was going on so here's

5
00:00:08,480 --> 00:00:13,519
my quick walk through of my comments I

6
00:00:10,400 --> 00:00:15,240
made to myself so the ADI SP minus 16

7
00:00:13,519 --> 00:00:17,760
that's just allocation of Stack space as

8
00:00:15,240 --> 00:00:21,320
usual these three instructions the wepc

9
00:00:17,760 --> 00:00:24,080
ADI and LD that is just loading up some

10
00:00:21,320 --> 00:00:26,039
value from a hardcoded constant location

11
00:00:24,080 --> 00:00:27,599
somewhere after our code and if you

12
00:00:26,039 --> 00:00:29,759
looked in the debugger you would see

13
00:00:27,599 --> 00:00:31,640
that this read from offset zero from

14
00:00:29,759 --> 00:00:34,200
that or the load from offset zero from

15
00:00:31,640 --> 00:00:37,280
that is basically our Al low it is our

16
00:00:34,200 --> 00:00:39,440
dissected so it pulls dissected into a2

17
00:00:37,280 --> 00:00:42,360
it immediately stores it back out at SP

18
00:00:39,440 --> 00:00:44,360
plus 0 I'm going to interpret this as

19
00:00:42,360 --> 00:00:45,399
essentially this is the local variable a

20
00:00:44,360 --> 00:00:47,920
for its

21
00:00:45,399 --> 00:00:52,600
initialization and then it also reads

22
00:00:47,920 --> 00:00:55,039
from a3 + 8 and it loads that in to a3

23
00:00:52,600 --> 00:00:56,359
and so that is the a high that is if you

24
00:00:55,039 --> 00:00:58,840
looked in the debugger that is going to

25
00:00:56,359 --> 00:01:00,960
be a zero and so this is going along

26
00:00:58,840 --> 00:01:03,120
with the notion of well we're we just

27
00:01:00,960 --> 00:01:05,920
wrote dissected in our C source code but

28
00:01:03,120 --> 00:01:08,759
it's being stored into a 128bit variable

29
00:01:05,920 --> 00:01:11,640
so it's going to be upcast into a 128

30
00:01:08,759 --> 00:01:14,560
bit value and the upper 128 bits are

31
00:01:11,640 --> 00:01:16,759
zero so that a high was zero pulls it

32
00:01:14,560 --> 00:01:19,400
into the a3 and then immediately stores

33
00:01:16,759 --> 00:01:21,799
it back out to stack pointer plus 8 and

34
00:01:19,400 --> 00:01:25,320
I'm going to treat that as the upper uh

35
00:01:21,799 --> 00:01:26,880
64 bits of a a high and then because

36
00:01:25,320 --> 00:01:29,119
this is unoptimized assembly it

37
00:01:26,880 --> 00:01:32,680
immediately loads that value back in

38
00:01:29,119 --> 00:01:35,600
from SP plus 8 into the a2 register then

39
00:01:32,680 --> 00:01:38,079
it's going to do another wepc ADI and

40
00:01:35,600 --> 00:01:39,640
load to pull from a different location

41
00:01:38,079 --> 00:01:42,119
so that's not that location that's a

42
00:01:39,640 --> 00:01:44,000
different location and that is just the

43
00:01:42,119 --> 00:01:45,439
Delectable that is the hardcode constant

44
00:01:44,000 --> 00:01:48,960
delectable which we're going to call

45
00:01:45,439 --> 00:01:51,399
below and it pulls that in into a3 and

46
00:01:48,960 --> 00:01:54,920
then here is our first multiplication we

47
00:01:51,399 --> 00:02:00,240
have a2 which is the a high at this

48
00:01:54,920 --> 00:02:02,320
point a2 a high time a3 B low and and so

49
00:02:00,240 --> 00:02:04,719
this is going to give us the lower

50
00:02:02,320 --> 00:02:07,119
because it's a normal multiplication the

51
00:02:04,719 --> 00:02:09,560
lower 64 bits of a high which was really

52
00:02:07,119 --> 00:02:11,440
just 0 * below so the net result is

53
00:02:09,560 --> 00:02:14,000
really just going to be zero but in the

54
00:02:11,440 --> 00:02:16,160
context of that overall diagram we saw

55
00:02:14,000 --> 00:02:18,519
we knew that there were going to be 64

56
00:02:16,160 --> 00:02:20,599
bit multiplications uh that would yield

57
00:02:18,519 --> 00:02:22,599
a zero but we wanted to keep the you

58
00:02:20,599 --> 00:02:24,760
know the lower I'm just kind of

59
00:02:22,599 --> 00:02:26,879
visualizing there was the lower 64 bits

60
00:02:24,760 --> 00:02:28,680
of the one there's lower 64 bits of the

61
00:02:26,879 --> 00:02:30,120
one and then there's the upper 64 bits

62
00:02:28,680 --> 00:02:31,440
of the other if you can pick picture

63
00:02:30,120 --> 00:02:34,400
back to where that diagram would have

64
00:02:31,440 --> 00:02:36,040
been but anyways the net result of this

65
00:02:34,400 --> 00:02:38,480
is you can either think of it as the

66
00:02:36,040 --> 00:02:40,440
math of a high time B low and keeping

67
00:02:38,480 --> 00:02:43,640
the low 64 bits or you can say it's the

68
00:02:40,440 --> 00:02:46,959
literal zero at this point so a2 stores

69
00:02:43,640 --> 00:02:48,959
that result now it is loading up into a3

70
00:02:46,959 --> 00:02:50,760
from stack pointer plus Z we have to

71
00:02:48,959 --> 00:02:53,200
walk backwards and see where do we store

72
00:02:50,760 --> 00:02:57,440
a stack pointer plus Z right here so it

73
00:02:53,200 --> 00:03:00,640
is loading up a low into a3 and then it

74
00:02:57,440 --> 00:03:03,239
is loading an immediate to a1 and I

75
00:03:00,640 --> 00:03:06,360
think this is kind of the compiler just

76
00:03:03,239 --> 00:03:09,120
recognizing like hey I don't need to

77
00:03:06,360 --> 00:03:10,760
grab the a high from this delectable I

78
00:03:09,120 --> 00:03:12,799
know it's just going to be zero and so

79
00:03:10,760 --> 00:03:14,640
it's just uh you know instead of doing

80
00:03:12,799 --> 00:03:15,920
an extra like this is again you know

81
00:03:14,640 --> 00:03:17,519
they could have done the same thing up

82
00:03:15,920 --> 00:03:19,040
here I don't know why they did it down

83
00:03:17,519 --> 00:03:20,440
here but they just loaded an immediate

84
00:03:19,040 --> 00:03:23,519
because they recognized that the upper

85
00:03:20,440 --> 00:03:25,280
64 bits are just going to be zero and

86
00:03:23,519 --> 00:03:27,560
here it seems like well that's a little

87
00:03:25,280 --> 00:03:29,760
more efficient and so again it's just

88
00:03:27,560 --> 00:03:30,799
unoptimized code this was a little less

89
00:03:29,760 --> 00:03:32,599
efficient this is a little more

90
00:03:30,799 --> 00:03:35,000
efficient why they chose it to do one

91
00:03:32,599 --> 00:03:37,040
way one place and not the other I don't

92
00:03:35,000 --> 00:03:38,959
know but I believe that this is

93
00:03:37,040 --> 00:03:41,400
basically uh in the same way this is

94
00:03:38,959 --> 00:03:43,040
setting a low this is setting a high

95
00:03:41,400 --> 00:03:47,280
which it understood to be hardcoded to

96
00:03:43,040 --> 00:03:51,280
zero and so then it is doing a low so a3

97
00:03:47,280 --> 00:03:53,319
is a low a1 is B high a low time B high

98
00:03:51,280 --> 00:03:55,640
which is a hardcoded to zero and that

99
00:03:53,319 --> 00:03:59,519
gives you then the lower 64 bits of that

100
00:03:55,640 --> 00:04:01,480
going into a3 then it is going to add a3

101
00:03:59,519 --> 00:04:04,040
which which is this thing we just did

102
00:04:01,480 --> 00:04:07,280
right here so lower a low time B high

103
00:04:04,040 --> 00:04:10,040
lower a low time B high so a2 which is

104
00:04:07,280 --> 00:04:13,599
from earlier the a high time B low the a

105
00:04:10,040 --> 00:04:15,280
high time B low and so it's basically

106
00:04:13,599 --> 00:04:16,959
all of this math done together but

107
00:04:15,280 --> 00:04:19,120
really the that result is zero because

108
00:04:16,959 --> 00:04:22,280
both of those things led to zeros but

109
00:04:19,120 --> 00:04:24,400
you can see how it's doing the multiply

110
00:04:22,280 --> 00:04:27,080
multiply and then adding and these were

111
00:04:24,400 --> 00:04:28,919
sort of the the first addition of those

112
00:04:27,080 --> 00:04:33,520
two ads that are necessary to get the

113
00:04:28,919 --> 00:04:35,720
upper 64 bits okay so now it has got a1

114
00:04:33,520 --> 00:04:38,400
holding this math right there and really

115
00:04:35,720 --> 00:04:42,160
just a literal zero now it's going to

116
00:04:38,400 --> 00:04:45,120
load back the AL low from the stack

117
00:04:42,160 --> 00:04:48,000
pointer plus Z put it into a2 another

118
00:04:45,120 --> 00:04:50,560
wepc and it's going to get the below out

119
00:04:48,000 --> 00:04:52,880
of the area Beyond here because it

120
00:04:50,560 --> 00:04:54,160
didn't save a copy in a register anyway

121
00:04:52,880 --> 00:04:55,919
which it could have totally done because

122
00:04:54,160 --> 00:04:58,400
it has so many registers it could have

123
00:04:55,919 --> 00:05:01,400
but again this is unoptimized code so it

124
00:04:58,400 --> 00:05:03,039
pulled in a low it pulled in below and

125
00:05:01,400 --> 00:05:05,240
we know that the last sort of

126
00:05:03,039 --> 00:05:08,039
multiplication that's necessary to do

127
00:05:05,240 --> 00:05:10,360
the additions that we saw in our um in

128
00:05:08,039 --> 00:05:12,160
our diagram is you're going to have

129
00:05:10,360 --> 00:05:14,320
multiply to get the lower bits so the

130
00:05:12,160 --> 00:05:16,759
lower bits of a low times bow and

131
00:05:14,320 --> 00:05:21,479
multiply to get the higher bits of a low

132
00:05:16,759 --> 00:05:23,520
time bow so a2 a3 are just a low bow and

133
00:05:21,479 --> 00:05:26,759
so this one is going to store the high

134
00:05:23,520 --> 00:05:28,919
bits into a3 so at that point you now

135
00:05:26,759 --> 00:05:31,000
have like all the bits that you need to

136
00:05:28,919 --> 00:05:32,919
add to this this other thing and get the

137
00:05:31,000 --> 00:05:34,680
net result so there's a little bit of

138
00:05:32,919 --> 00:05:38,280
moving around that's going on so then

139
00:05:34,680 --> 00:05:40,639
it's going a0 into a4 what is a Zer go

140
00:05:38,280 --> 00:05:43,960
up here okay that's the result of this

141
00:05:40,639 --> 00:05:46,600
lower 64 bits so that goes into a4

142
00:05:43,960 --> 00:05:51,520
result of this higher 64 bits goes into

143
00:05:46,600 --> 00:05:55,520
a5 then it's adding a5 to a1 so a5 is

144
00:05:51,520 --> 00:05:58,720
the high bits a1 if we backtrack back to

145
00:05:55,520 --> 00:06:00,319
here we see that it is these it is the

146
00:05:58,720 --> 00:06:02,520
the lower 6 four bits from two

147
00:06:00,319 --> 00:06:05,080
multiplications but essentially it is

148
00:06:02,520 --> 00:06:07,440
that you know diagram that we have I

149
00:06:05,080 --> 00:06:11,160
guess I will go backwards and show it to

150
00:06:07,440 --> 00:06:12,919
you right we're doing this calculation

151
00:06:11,160 --> 00:06:14,680
plus this calculation we're keeping only

152
00:06:12,919 --> 00:06:17,000
the lower 64 bits we don't need these

153
00:06:14,680 --> 00:06:19,479
upper 64 bits so do this multiplication

154
00:06:17,000 --> 00:06:22,120
keep the lower 64 do this multiplication

155
00:06:19,479 --> 00:06:24,319
keep the lower 64 add them together and

156
00:06:22,120 --> 00:06:27,599
now it did this multiplication and it's

157
00:06:24,319 --> 00:06:29,720
adding the upper 64 to the sum of those

158
00:06:27,599 --> 00:06:31,759
two things and that'll give it the upper

159
00:06:29,720 --> 00:06:36,000
64 and then it's just going to have the

160
00:06:31,759 --> 00:06:38,880
lower 64 there so continuing

161
00:06:36,000 --> 00:06:40,680
on we are right here and we are doing

162
00:06:38,880 --> 00:06:42,919
the addition of all of that stuff

163
00:06:40,680 --> 00:06:46,720
together and that a3 is going to be our

164
00:06:42,919 --> 00:06:49,240
ultimate upper 64 bits of the 128bit

165
00:06:46,720 --> 00:06:53,240
result then just some moving around a3

166
00:06:49,240 --> 00:06:56,120
into a5 store the a4 value which was

167
00:06:53,240 --> 00:06:58,639
back from here it was this low times low

168
00:06:56,120 --> 00:07:01,080
so store the low times low back to the

169
00:06:58,639 --> 00:07:02,759
stack store the a5 pull them back in so

170
00:07:01,080 --> 00:07:05,879
this is just unoptimized moving around

171
00:07:02,759 --> 00:07:08,960
we wrote it out we read it back in we

172
00:07:05,879 --> 00:07:11,599
moved it between registers a4 to a2 a2

173
00:07:08,960 --> 00:07:14,440
to a0 and that means ultimately that is

174
00:07:11,599 --> 00:07:18,120
going to be our lower 64 bits of the

175
00:07:14,440 --> 00:07:21,840
return value and same thing a5 to a3 a3

176
00:07:18,120 --> 00:07:23,639
to a1 a1 is ultimately our upper 64 bits

177
00:07:21,840 --> 00:07:26,599
so that's why I went to the trouble of

178
00:07:23,639 --> 00:07:29,520
showing you that indication of you know

179
00:07:26,599 --> 00:07:32,080
here's our visualization of what kind of

180
00:07:29,520 --> 00:07:34,840
multiplication could happen for a 128

181
00:07:32,080 --> 00:07:38,240
time 128 where you only stored the lower

182
00:07:34,840 --> 00:07:40,840
128 this is what led to our three

183
00:07:38,240 --> 00:07:42,160
multiplies uh and really four multiplies

184
00:07:40,840 --> 00:07:44,280
because we have to do separate ones for

185
00:07:42,160 --> 00:07:46,520
lower bits and upper bits so hopefully

186
00:07:44,280 --> 00:07:48,840
if you wrote Every a comment on every

187
00:07:46,520 --> 00:07:49,919
single line and you know you recognize

188
00:07:48,840 --> 00:07:52,400
that there was this kind of thing going

189
00:07:49,919 --> 00:07:54,440
to be going on then you saw that exactly

190
00:07:52,400 --> 00:07:57,080
this is what was going on in the

191
00:07:54,440 --> 00:07:57,080
assembly

