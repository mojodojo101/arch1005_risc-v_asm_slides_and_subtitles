1
00:00:00,680 --> 00:00:04,319
understanding the stack is of critical

2
00:00:02,440 --> 00:00:06,279
importance when trying to understand

3
00:00:04,319 --> 00:00:08,840
what assembly is doing because it is

4
00:00:06,279 --> 00:00:11,799
going to make a lot of use of the stack

5
00:00:08,840 --> 00:00:14,360
so conceptually a stack not the stack

6
00:00:11,799 --> 00:00:17,320
but a stack in a data structures sense

7
00:00:14,360 --> 00:00:19,400
of the word is a last in first out or

8
00:00:17,320 --> 00:00:21,720
lifo data structure where the data is

9
00:00:19,400 --> 00:00:24,320
pushed onto the top of the stack and

10
00:00:21,720 --> 00:00:26,160
popped off of the top so basically it's

11
00:00:24,320 --> 00:00:28,080
like a stack of plates and you put

12
00:00:26,160 --> 00:00:29,759
things on top and you can't just like

13
00:00:28,080 --> 00:00:31,800
pull a plate out from the middle you

14
00:00:29,759 --> 00:00:33,600
have to take things off of the top you

15
00:00:31,800 --> 00:00:35,760
have to pop them off of the top of the

16
00:00:33,600 --> 00:00:37,840
stack before you can access things that

17
00:00:35,760 --> 00:00:40,480
are lower down in the stack so

18
00:00:37,840 --> 00:00:42,280
conceptually the stack as opposed to a

19
00:00:40,480 --> 00:00:45,120
stack now we're talking about the stack

20
00:00:42,280 --> 00:00:47,520
as used by assembly is a conceptual area

21
00:00:45,120 --> 00:00:49,680
of main memory which is RAM and it's

22
00:00:47,520 --> 00:00:51,520
designated by the OS when a program is

23
00:00:49,680 --> 00:00:53,239
starting and they designate it by

24
00:00:51,520 --> 00:00:54,960
basically setting a stack pointer and

25
00:00:53,239 --> 00:00:56,840
saying here is going to be the start of

26
00:00:54,960 --> 00:00:58,800
the stack you can start pushing things

27
00:00:56,840 --> 00:01:00,719
on and popping them off as you like

28
00:00:58,800 --> 00:01:02,519
because it's designated by the OS when a

29
00:01:00,719 --> 00:01:03,960
program is loaded it's going to be

30
00:01:02,519 --> 00:01:06,040
different depending on different

31
00:01:03,960 --> 00:01:08,360
operating systems virtualization

32
00:01:06,040 --> 00:01:10,080
firmware everyone can do their own thing

33
00:01:08,360 --> 00:01:12,439
that's up to the execution environment

34
00:01:10,080 --> 00:01:14,240
itself Additionally you can see them

35
00:01:12,439 --> 00:01:16,799
moving them around between program

36
00:01:14,240 --> 00:01:18,880
invocations due to security mechanisms

37
00:01:16,799 --> 00:01:21,200
like address Bas layout randomization

38
00:01:18,880 --> 00:01:22,439
and that's the concept that because

39
00:01:21,200 --> 00:01:24,479
sometimes it's beneficial for an

40
00:01:22,439 --> 00:01:26,159
attacker to know where something is in

41
00:01:24,479 --> 00:01:28,320
memory on the stack they're going to

42
00:01:26,159 --> 00:01:30,240
just move the stack around in all of the

43
00:01:28,320 --> 00:01:31,799
available memory to make it hard for an

44
00:01:30,240 --> 00:01:33,320
attacker to find something so the

45
00:01:31,799 --> 00:01:36,799
important thing to know about the stack

46
00:01:33,320 --> 00:01:39,360
is that it grows towards lower addresses

47
00:01:36,799 --> 00:01:41,680
so I always draw my diagrams with high

48
00:01:39,360 --> 00:01:43,960
addresses high and low addresses low

49
00:01:41,680 --> 00:01:46,360
this is basically just me keeping up the

50
00:01:43,960 --> 00:01:48,320
convention that was used in the CMU

51
00:01:46,360 --> 00:01:49,920
architecture book that I learned from so

52
00:01:48,320 --> 00:01:51,759
always assume high address is high low

53
00:01:49,920 --> 00:01:53,520
address is low if this is what a

54
00:01:51,759 --> 00:01:55,000
particular process's memory looks like

55
00:01:53,520 --> 00:01:57,159
it could have a mix of things it could

56
00:01:55,000 --> 00:01:59,600
have a stack a heap the actual assembly

57
00:01:57,159 --> 00:02:00,360
code global data and all sorts of other

58
00:01:59,600 --> 00:02:02,000
things

59
00:02:00,360 --> 00:02:04,719
but the key thing is that the stack

60
00:02:02,000 --> 00:02:07,479
grows towards low addresses normal

61
00:02:04,719 --> 00:02:10,280
memory wrs write from low to high but

62
00:02:07,479 --> 00:02:12,239
stack additions keep making the stack

63
00:02:10,280 --> 00:02:14,599
pointer go lower lower lower towards

64
00:02:12,239 --> 00:02:16,000
lower dresses so to emphasize this I'm

65
00:02:14,599 --> 00:02:18,440
going to pull out this completely

66
00:02:16,000 --> 00:02:20,720
obscure reference to a Nintendo

67
00:02:18,440 --> 00:02:23,000
Entertainment System original NES video

68
00:02:20,720 --> 00:02:25,239
game called Metal Storm in that game

69
00:02:23,000 --> 00:02:27,000
basically you play a little Mech Soldier

70
00:02:25,239 --> 00:02:29,200
and you can flip your orientation you

71
00:02:27,000 --> 00:02:30,920
can walk on the ceiling or you can walk

72
00:02:29,200 --> 00:02:33,599
on the floor and you have the ability to

73
00:02:30,920 --> 00:02:36,080
just flip up and down so the m308 Gunner

74
00:02:33,599 --> 00:02:38,160
needs to be very comfortable looking up

75
00:02:36,080 --> 00:02:40,319
looking down any direction but in the

76
00:02:38,160 --> 00:02:42,560
orientation that we're going to be using

77
00:02:40,319 --> 00:02:44,480
the stack grows towards low addresses

78
00:02:42,560 --> 00:02:46,080
high addresses high low addresses low

79
00:02:44,480 --> 00:02:47,800
and so from your perspective you can

80
00:02:46,080 --> 00:02:50,040
think of it like it's upside down and

81
00:02:47,800 --> 00:02:52,239
it's growing downwards towards lower

82
00:02:50,040 --> 00:02:54,080
addresses so you can think of it like

83
00:02:52,239 --> 00:02:56,000
that flipping it around the stack grows

84
00:02:54,080 --> 00:02:58,040
towards low addresses if it helps you

85
00:02:56,000 --> 00:03:00,040
can think about Ender Wigan from Ender's

86
00:02:58,040 --> 00:03:01,920
Game saying that he has to orient

87
00:03:00,040 --> 00:03:04,080
himself somehow and he chooses that you

88
00:03:01,920 --> 00:03:05,480
know the enemy's gate is down I'm

89
00:03:04,080 --> 00:03:08,040
telling you here the enemy's plate is

90
00:03:05,480 --> 00:03:10,760
down or you can trust the wise words of

91
00:03:08,040 --> 00:03:12,720
Weird Al and understand that everything

92
00:03:10,760 --> 00:03:16,000
you know is wrong black is white Up Is

93
00:03:12,720 --> 00:03:18,440
Down and short is long so Stacks grow

94
00:03:16,000 --> 00:03:20,840
towards low addresses all right so the

95
00:03:18,440 --> 00:03:24,560
stack is pointed to by the stack pointer

96
00:03:20,840 --> 00:03:26,120
SP also x2 and it points at the top of

97
00:03:24,560 --> 00:03:27,640
the stack and when we talk about the top

98
00:03:26,120 --> 00:03:30,040
of the stack we're referring to the

99
00:03:27,640 --> 00:03:32,080
lowest address the lowest memory address

100
00:03:30,040 --> 00:03:34,840
which is currently being used so not the

101
00:03:32,080 --> 00:03:37,280
address past the last data but the

102
00:03:34,840 --> 00:03:39,280
address of the last data the place where

103
00:03:37,280 --> 00:03:41,080
you will find some data and while RAM is

104
00:03:39,280 --> 00:03:43,040
RAM and if it was you know initialized

105
00:03:41,080 --> 00:03:45,120
or written to at any point in the past

106
00:03:43,040 --> 00:03:46,840
it will still have those values but for

107
00:03:45,120 --> 00:03:48,799
the purposes of programmers they should

108
00:03:46,840 --> 00:03:50,840
always be thinking of anything beyond

109
00:03:48,799 --> 00:03:53,120
the top of the stack is considered

110
00:03:50,840 --> 00:03:55,120
undefined or uninitialized and just

111
00:03:53,120 --> 00:03:57,920
basically should not be accessed should

112
00:03:55,120 --> 00:03:59,360
not be relied upon and you know accesses

113
00:03:57,920 --> 00:04:00,720
to that are fundamentally programed

114
00:03:59,360 --> 00:04:02,319
errors they're not supposed to access

115
00:04:00,720 --> 00:04:03,959
past the top of the stack and we

116
00:04:02,319 --> 00:04:05,519
actually talk about in the OST2

117
00:04:03,959 --> 00:04:08,000
vulnerabilities class how that can be

118
00:04:05,519 --> 00:04:10,439
used for exploits so what kind of things

119
00:04:08,000 --> 00:04:12,400
can you find on the stack you can find

120
00:04:10,439 --> 00:04:14,640
local variables and we'll be covering

121
00:04:12,400 --> 00:04:16,440
that shortly you can also find return

122
00:04:14,640 --> 00:04:18,639
addresses so we talked about the return

123
00:04:16,440 --> 00:04:20,400
address register but there's only one

124
00:04:18,639 --> 00:04:23,639
return address register for a given

125
00:04:20,400 --> 00:04:25,960
hart and so consequently IF function a

126
00:04:23,639 --> 00:04:27,600
calls function B sure it may store the

127
00:04:25,960 --> 00:04:29,639
return address in the return address

128
00:04:27,600 --> 00:04:32,080
register but what about if function B

129
00:04:29,639 --> 00:04:34,400
calls function C well it needs to

130
00:04:32,080 --> 00:04:36,919
clobber the value in the return address

131
00:04:34,400 --> 00:04:38,720
register and it needs to store that

132
00:04:36,919 --> 00:04:40,000
value somewhere before it clabs it

133
00:04:38,720 --> 00:04:42,280
otherwise you'll never be able to get

134
00:04:40,000 --> 00:04:44,880
back from B to a so return addresses get

135
00:04:42,280 --> 00:04:46,280
stored onto the stack once some function

136
00:04:44,880 --> 00:04:48,479
calls another function and needs to

137
00:04:46,280 --> 00:04:51,160
store off the previous value we can have

138
00:04:48,479 --> 00:04:52,639
save space for registers which we talked

139
00:04:51,160 --> 00:04:54,160
a little bit about but we didn't cover

140
00:04:52,639 --> 00:04:56,479
in depth yet there was a notion of

141
00:04:54,160 --> 00:04:58,800
Callie and call caller save registers

142
00:04:56,479 --> 00:05:00,360
where some registers a function can call

143
00:04:58,800 --> 00:05:02,560
another function and expect it'll be

144
00:05:00,360 --> 00:05:03,840
preserved but if that function that it

145
00:05:02,560 --> 00:05:05,800
called needs to fiddle with the

146
00:05:03,840 --> 00:05:08,000
registers they need to preserve it that

147
00:05:05,800 --> 00:05:10,440
would be a call E save register and

148
00:05:08,000 --> 00:05:12,160
other ones the caller needs to save them

149
00:05:10,440 --> 00:05:14,800
before it goes ahead and calls another

150
00:05:12,160 --> 00:05:16,639
function and that's the call E save

151
00:05:14,800 --> 00:05:18,680
register and the whole point is those

152
00:05:16,639 --> 00:05:20,880
registers are going to end up on the

153
00:05:18,680 --> 00:05:22,440
stack dynamically allocated memory can

154
00:05:20,880 --> 00:05:24,800
also appear on the stack and while

155
00:05:22,440 --> 00:05:27,520
you're probably familiar with the memory

156
00:05:24,800 --> 00:05:29,960
allocation function Malo there is a

157
00:05:27,520 --> 00:05:32,560
somewhat lesser known version called Al

158
00:05:29,960 --> 00:05:34,880
a and aloc a will actually allocate

159
00:05:32,560 --> 00:05:37,960
memory specifically on the stack instead

160
00:05:34,880 --> 00:05:40,560
of the Heap which is where malok uses

161
00:05:37,960 --> 00:05:42,319
sometimes if it's enabled at compilation

162
00:05:40,560 --> 00:05:44,039
time you'll see things like called the

163
00:05:42,319 --> 00:05:46,080
stack cookie also known as the stack

164
00:05:44,039 --> 00:05:48,280
Canary that is a software exploit

165
00:05:46,080 --> 00:05:50,600
mitigation and additionally you'll see

166
00:05:48,280 --> 00:05:52,960
arguments passed on the stack if you run

167
00:05:50,600 --> 00:05:54,560
out of registers and you need to pass

168
00:05:52,960 --> 00:05:56,400
too many arguments they can't all fit in

169
00:05:54,560 --> 00:05:59,160
the registers then they start getting

170
00:05:56,400 --> 00:06:01,240
passed in uh on the stack finally there

171
00:05:59,160 --> 00:06:03,400
is a a very rare case that you know

172
00:06:01,240 --> 00:06:06,400
compilers try to avoid and is a

173
00:06:03,400 --> 00:06:08,080
performance uh penalty where if the

174
00:06:06,400 --> 00:06:10,000
compiler needs to juggle so many

175
00:06:08,080 --> 00:06:12,039
registers that they they can't do all of

176
00:06:10,000 --> 00:06:14,599
their operations they need to hold too

177
00:06:12,039 --> 00:06:16,160
many values and the 32 general purpose

178
00:06:14,599 --> 00:06:18,160
registers are not enough then they're

179
00:06:16,160 --> 00:06:20,160
going to have to save or spill off some

180
00:06:18,160 --> 00:06:21,960
of those registers onto the stack so

181
00:06:20,160 --> 00:06:23,639
these are all various types of things

182
00:06:21,960 --> 00:06:25,280
that can show up in the stack we're not

183
00:06:23,639 --> 00:06:27,160
going to hit all of these in this class

184
00:06:25,280 --> 00:06:30,800
but certainly local variables return

185
00:06:27,160 --> 00:06:33,759
addresses Coler call col saved registers

186
00:06:30,800 --> 00:06:36,800
maybe I'll add an aloc a example later

187
00:06:33,759 --> 00:06:38,360
maybe a stack cookie uh example later

188
00:06:36,800 --> 00:06:41,360
but arguments pass between functions

189
00:06:38,360 --> 00:06:44,400
will explicitly forcibly try to see that

190
00:06:41,360 --> 00:06:45,880
and so forth so in general here is how

191
00:06:44,400 --> 00:06:47,280
you should think about what's going on

192
00:06:45,880 --> 00:06:48,840
with the stack when you have function

193
00:06:47,280 --> 00:06:50,680
calling another function calling another

194
00:06:48,840 --> 00:06:53,560
function so here we have some simple

195
00:06:50,680 --> 00:06:56,199
code main is calling foo with hard-coded

196
00:06:53,560 --> 00:06:58,240
parameter 7 fu is up here to find up

197
00:06:56,199 --> 00:07:00,560
here takes in that seven does multiply

198
00:06:58,240 --> 00:07:03,400
by five prints out the value and then

199
00:07:00,560 --> 00:07:05,840
takes that value and passes it to bar

200
00:07:03,400 --> 00:07:08,479
bar is up here multiplies it times three

201
00:07:05,840 --> 00:07:10,919
prints out the value and then returns

202
00:07:08,479 --> 00:07:13,080
and then foo returns from bar back to

203
00:07:10,919 --> 00:07:15,360
main and now you're back in main prints

204
00:07:13,080 --> 00:07:17,680
out the value seven that was passed and

205
00:07:15,360 --> 00:07:20,400
so forth so each of these invocations

206
00:07:17,680 --> 00:07:22,759
main calling fu fu calling bar these are

207
00:07:20,400 --> 00:07:24,120
each going to have a chunk of the stack

208
00:07:22,759 --> 00:07:26,639
that is reserved to them and that's

209
00:07:24,120 --> 00:07:28,599
called the stack frame so when main

210
00:07:26,639 --> 00:07:30,639
starts up main ultimately gets called by

211
00:07:28,599 --> 00:07:32,840
someone else there is some code that

212
00:07:30,639 --> 00:07:35,840
just is automatically added to compiled

213
00:07:32,840 --> 00:07:37,680
programs where basically the the C

214
00:07:35,840 --> 00:07:39,639
execution environment is going to have

215
00:07:37,680 --> 00:07:42,280
some start function that calls to main

216
00:07:39,639 --> 00:07:45,360
so someone calls to main at that point

217
00:07:42,280 --> 00:07:47,319
main gets its own stack frame that

218
00:07:45,360 --> 00:07:48,960
bottom of the stack is going to be sort

219
00:07:47,319 --> 00:07:50,199
of the bottom of the main frame or

220
00:07:48,960 --> 00:07:52,000
really the function that called it but

221
00:07:50,199 --> 00:07:53,400
we'll pretend that doesn't exist for now

222
00:07:52,000 --> 00:07:54,960
so the bottom of the stack is here at

223
00:07:53,400 --> 00:07:57,319
high addresses high address is high low

224
00:07:54,960 --> 00:07:59,960
address is low so bottom of the stack is

225
00:07:57,319 --> 00:08:01,919
there top of the stack is the the last

226
00:07:59,960 --> 00:08:03,599
value that was actually used in main's

227
00:08:01,919 --> 00:08:05,039
frame so any value that was stored there

228
00:08:03,599 --> 00:08:06,599
we're not going to focus on what values

229
00:08:05,039 --> 00:08:08,360
are stored there yet that'll be for

230
00:08:06,599 --> 00:08:11,039
later in the class but this is just a

231
00:08:08,360 --> 00:08:13,960
conceptual thing main gets a frame when

232
00:08:11,039 --> 00:08:16,400
main calls to foo foo gets a frame the

233
00:08:13,960 --> 00:08:19,560
stack top changes moving further towards

234
00:08:16,400 --> 00:08:21,319
low addresses when foo calls to Bar Bar

235
00:08:19,560 --> 00:08:23,560
gets a frame and again the stack top

236
00:08:21,319 --> 00:08:25,240
changes and so basically each of these

237
00:08:23,560 --> 00:08:28,360
are sort of reserved areas for each of

238
00:08:25,240 --> 00:08:31,280
those functions when bar returns then

239
00:08:28,360 --> 00:08:33,399
that area comes undefined uninitialized

240
00:08:31,280 --> 00:08:35,000
as far as we're concerned it will have

241
00:08:33,399 --> 00:08:36,680
actual values but we're supposed to

242
00:08:35,000 --> 00:08:38,360
treat it as if you know we should never

243
00:08:36,680 --> 00:08:40,719
ever touch it and it doesn't exist

244
00:08:38,360 --> 00:08:43,440
anymore so now there's only foo and main

245
00:08:40,719 --> 00:08:45,480
foo returns back to main and main

246
00:08:43,440 --> 00:08:46,920
returns back to whoever it called it so

247
00:08:45,480 --> 00:08:48,519
that's basically the way things work

248
00:08:46,920 --> 00:08:50,560
functions get called the stack moves

249
00:08:48,519 --> 00:08:51,839
towards low addresses functions return

250
00:08:50,560 --> 00:08:53,720
the stack moves back towards high

251
00:08:51,839 --> 00:08:55,959
addresses and when you're looking at the

252
00:08:53,720 --> 00:08:57,320
actual stack in RAM and you're looking

253
00:08:55,959 --> 00:08:59,200
at it with something like a debugger you

254
00:08:57,320 --> 00:09:01,800
have all concrete addresses concrete Val

255
00:08:59,200 --> 00:09:03,720
values you can sometimes think of it as

256
00:09:01,800 --> 00:09:05,600
if you know I'm running a 64-bit

257
00:09:03,720 --> 00:09:07,600
operating system and I can think of it

258
00:09:05,600 --> 00:09:09,880
like it's an array of a bunch of 64bit

259
00:09:07,600 --> 00:09:14,120
values where you know one has an address

260
00:09:09,880 --> 00:09:17,680
hex3 and then minus 8 is hex 28 Minus 8

261
00:09:14,120 --> 00:09:20,120
is hex 20 minus 8 is hex 18 so you could

262
00:09:17,680 --> 00:09:22,959
think of it like it's 64-bit chunks

263
00:09:20,120 --> 00:09:26,880
because the typical access size is going

264
00:09:22,959 --> 00:09:28,440
to be you know pointers or Longs or long

265
00:09:26,880 --> 00:09:30,920
Longs and things like that that are

266
00:09:28,440 --> 00:09:33,040
usually the same granularity as the

267
00:09:30,920 --> 00:09:34,200
pointer width so if pointers are 64 bits

268
00:09:33,040 --> 00:09:36,320
a lot of times you're going to be seeing

269
00:09:34,200 --> 00:09:38,160
accesses to 64-bit values whether

270
00:09:36,320 --> 00:09:40,839
they're pointers or just some other uh

271
00:09:38,160 --> 00:09:43,160
scaler type same thing 32-bit operating

272
00:09:40,839 --> 00:09:45,279
system maybe you most frequently see

273
00:09:43,160 --> 00:09:47,320
accesses in 32-bits but I want to be

274
00:09:45,279 --> 00:09:49,040
very clear that although you can think

275
00:09:47,320 --> 00:09:51,079
of it like it's just a big array of

276
00:09:49,040 --> 00:09:54,160
whatever your foundational pointer size

277
00:09:51,079 --> 00:09:57,680
is in reality you can have other uh

278
00:09:54,160 --> 00:10:00,880
scalar types of variables in C you know

279
00:09:57,680 --> 00:10:02,839
characters shorts in and those may not

280
00:10:00,880 --> 00:10:05,360
take up the full space so if you're in a

281
00:10:02,839 --> 00:10:07,160
32-bit operating system like this well

282
00:10:05,360 --> 00:10:08,480
you could have a short value and then

283
00:10:07,160 --> 00:10:11,200
another short value and then a character

284
00:10:08,480 --> 00:10:13,480
and then a character and so these are

285
00:10:11,200 --> 00:10:16,720
you know the short value is two bytes

286
00:10:13,480 --> 00:10:19,560
and so that means x30 minus 2 brings us

287
00:10:16,720 --> 00:10:22,800
down to 2 e character value that's 1 by

288
00:10:19,560 --> 00:10:25,120
2D minus 1 is 2 C so basically the the

289
00:10:22,800 --> 00:10:27,040
granularity of access on the stack is

290
00:10:25,120 --> 00:10:28,240
going to be bite level granularity so

291
00:10:27,040 --> 00:10:31,320
you shouldn't think of it like it's

292
00:10:28,240 --> 00:10:33,480
always must be 32 64 Etc it can be bite

293
00:10:31,320 --> 00:10:35,760
level granularity but it depends on

294
00:10:33,480 --> 00:10:37,720
what's most appropriate to the time

295
00:10:35,760 --> 00:10:39,839
sometimes it's easier to think of things

296
00:10:37,720 --> 00:10:42,600
as if everything's like this just 32bit

297
00:10:39,839 --> 00:10:44,160
32bit 32bit other times it's better to

298
00:10:42,600 --> 00:10:45,440
look at things more realistically

299
00:10:44,160 --> 00:10:48,440
especially when you're looking at local

300
00:10:45,440 --> 00:10:51,160
variables uh that may not have all the

301
00:10:48,440 --> 00:10:52,920
same you know 32 or 64- bit sizes all

302
00:10:51,160 --> 00:10:54,880
right and so the final point is just

303
00:10:52,920 --> 00:10:57,120
that our diagrams with high addresses

304
00:10:54,880 --> 00:10:59,639
high and low addresses low is not going

305
00:10:57,120 --> 00:11:02,040
to be what you see in debuggers buggers

306
00:10:59,639 --> 00:11:03,720
will generally have low addresses high

307
00:11:02,040 --> 00:11:05,600
and high addresses low so they'll start

308
00:11:03,720 --> 00:11:07,399
from some low address you know call it

309
00:11:05,600 --> 00:11:10,160
address zero and then they'll have you

310
00:11:07,399 --> 00:11:12,959
know let's say 16 bytes and then address

311
00:11:10,160 --> 00:11:15,839
hex 10 and 16 bytes address hex 20 16

312
00:11:12,959 --> 00:11:18,920
bytes so the key point of this Metal

313
00:11:15,839 --> 00:11:21,120
Storm game character here is to say you

314
00:11:18,920 --> 00:11:23,079
need to be comfortable reorienting

315
00:11:21,120 --> 00:11:24,959
yourself and we're going to have little

316
00:11:23,079 --> 00:11:27,040
games that are specifically to try to

317
00:11:24,959 --> 00:11:29,440
get you comfortable with reorienting

318
00:11:27,040 --> 00:11:31,079
yourself on demand our diagram are high

319
00:11:29,440 --> 00:11:33,360
address is high low address is low but

320
00:11:31,079 --> 00:11:35,440
you could go off and read someone else's

321
00:11:33,360 --> 00:11:37,279
data sheet someone else's document

322
00:11:35,440 --> 00:11:39,440
someone else's blog post and they could

323
00:11:37,279 --> 00:11:41,360
use a completely different orientation

324
00:11:39,440 --> 00:11:43,200
they might use it sideways as well they

325
00:11:41,360 --> 00:11:45,480
might put high addresses to the right

326
00:11:43,200 --> 00:11:47,320
low addresses to the left they might put

327
00:11:45,480 --> 00:11:50,000
it the opposite way and so the key thing

328
00:11:47,320 --> 00:11:52,040
is you have to be mentally prepared to

329
00:11:50,000 --> 00:11:54,360
for whatever you come into and be able

330
00:11:52,040 --> 00:11:56,240
to always reorient yourself to whatever

331
00:11:54,360 --> 00:11:58,120
you're reading from whoever but

332
00:11:56,240 --> 00:11:59,600
thankfully you know a lot of uh

333
00:11:58,120 --> 00:12:01,279
documentation out there and a lot of

334
00:11:59,600 --> 00:12:02,680
computer architecture books following

335
00:12:01,279 --> 00:12:04,360
the same convention that I'm following

336
00:12:02,680 --> 00:12:06,880
of high address is high low address is

337
00:12:04,360 --> 00:12:06,880
low

