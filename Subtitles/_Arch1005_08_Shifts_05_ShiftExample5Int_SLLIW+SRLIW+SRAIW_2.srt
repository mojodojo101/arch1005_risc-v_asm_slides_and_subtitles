1
00:00:00,080 --> 00:00:04,759
so the takeaways from this last example

2
00:00:02,720 --> 00:00:07,200
is that if we're using integers we're

3
00:00:04,759 --> 00:00:09,960
going to be getting W suffix things and

4
00:00:07,200 --> 00:00:11,840
if we're using eyes immediates that the

5
00:00:09,960 --> 00:00:14,000
compiler can know about then we're going

6
00:00:11,840 --> 00:00:15,480
to get be getting eye variants and if

7
00:00:14,000 --> 00:00:17,920
we're doing both of those things we're

8
00:00:15,480 --> 00:00:20,199
going to be getting IW variants and

9
00:00:17,920 --> 00:00:22,400
that's exactly what you saw here so

10
00:00:20,199 --> 00:00:27,080
finally collecting our last things silly

11
00:00:22,400 --> 00:00:30,640
w sirly w and sray W and we're so close

12
00:00:27,080 --> 00:00:32,279
to completing the RV64I instructions

13
00:00:30,640 --> 00:00:34,719
let's go ahead and make this a little

14
00:00:32,279 --> 00:00:38,960
bit more aesthetically appealing with

15
00:00:34,719 --> 00:00:41,000
our 4x3 so that's 12 plus another three

16
00:00:38,960 --> 00:00:42,920
here because these are actually two each

17
00:00:41,000 --> 00:00:45,120
of them so plus another three that

18
00:00:42,920 --> 00:00:48,760
brings us to 15 and the compressed

19
00:00:45,120 --> 00:00:54,680
variance brings us to 18 18 different

20
00:00:48,760 --> 00:00:54,680
variants of shifts that's ridiculous

