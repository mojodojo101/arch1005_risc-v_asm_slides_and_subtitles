1
00:00:00,520 --> 00:00:06,240
now for a couple of reminders going back

2
00:00:03,159 --> 00:00:08,840
to array local variable 2 that was the

3
00:00:06,240 --> 00:00:11,519
case where we basically took and changed

4
00:00:08,840 --> 00:00:14,960
this from being a six element long long

5
00:00:11,519 --> 00:00:17,160
array to being three followed by a b

6
00:00:14,960 --> 00:00:19,840
followed by another three element array

7
00:00:17,160 --> 00:00:22,320
and what we took away from that is that

8
00:00:19,840 --> 00:00:24,400
the compiler can go ahead and reorder

9
00:00:22,320 --> 00:00:27,480
these local variables if it likes they

10
00:00:24,400 --> 00:00:29,400
don't have to be exactly the order that

11
00:00:27,480 --> 00:00:32,119
they were declared in the C high Lev

12
00:00:29,400 --> 00:00:35,160
source code then previously the last

13
00:00:32,119 --> 00:00:37,840
example we had struct local variable and

14
00:00:35,160 --> 00:00:41,079
in that one we saw that the contents of

15
00:00:37,840 --> 00:00:44,640
the struct exactly matched the contents

16
00:00:41,079 --> 00:00:47,239
on the uh on the stack and so we have a

17
00:00:44,640 --> 00:00:49,520
question of need this necessarily be the

18
00:00:47,239 --> 00:00:52,199
case although we did see that that was

19
00:00:49,520 --> 00:00:54,320
the case exactly this upside down thing

20
00:00:52,199 --> 00:00:56,239
showed up exactly on the stack does it

21
00:00:54,320 --> 00:00:58,320
have to be the case so we're going to

22
00:00:56,239 --> 00:01:00,480
play the exact same game that we played

23
00:00:58,320 --> 00:01:02,719
with normal local variables we're going

24
00:01:00,480 --> 00:01:05,080
to change the definition of the struct

25
00:01:02,719 --> 00:01:07,159
we're going to break up that six element

26
00:01:05,080 --> 00:01:08,560
B we're going to make it three elements

27
00:01:07,159 --> 00:01:10,759
then we're going to follow it by a

28
00:01:08,560 --> 00:01:13,119
single scaler and then another three

29
00:01:10,759 --> 00:01:14,680
element array and just for good measure

30
00:01:13,119 --> 00:01:16,759
I'm going to throw in some extra shorts

31
00:01:14,680 --> 00:01:18,920
and characters just perhaps so that we

32
00:01:16,759 --> 00:01:21,560
can see a few extra assembly

33
00:01:18,920 --> 00:01:24,520
instructions so here is our new and more

34
00:01:21,560 --> 00:01:27,400
complicated struct local variable 2 we

35
00:01:24,520 --> 00:01:29,960
defined the my struct now as having the

36
00:01:27,400 --> 00:01:31,880
short the three element array single

37
00:01:29,960 --> 00:01:34,040
scaler three element array and extra

38
00:01:31,880 --> 00:01:36,079
stuff then we do a variety of

39
00:01:34,040 --> 00:01:38,680
initializations with values that are

40
00:01:36,079 --> 00:01:40,759
just there to help you see what's going

41
00:01:38,680 --> 00:01:42,920
on to the stack where so these are

42
00:01:40,759 --> 00:01:46,000
obviously just to help you out and

43
00:01:42,920 --> 00:01:50,560
figure out which area on the stack is B

44
00:01:46,000 --> 00:01:52,159
of zero which area is C which area is a2

45
00:01:50,560 --> 00:01:54,000
obviously it won't exactly help you

46
00:01:52,159 --> 00:01:55,960
differentiate B of zero from one from

47
00:01:54,000 --> 00:01:58,479
two but you could change that if you'd

48
00:01:55,960 --> 00:02:00,560
like to figure it out more concretely

49
00:01:58,479 --> 00:02:02,880
all right popping up our new assembly

50
00:02:00,560 --> 00:02:04,600
for this compilation and we get a few

51
00:02:02,880 --> 00:02:06,840
new assembly instructions but the text

52
00:02:04,600 --> 00:02:10,280
is getting a little small there so let's

53
00:02:06,840 --> 00:02:14,040
zoom on in and see that we have a new SB

54
00:02:10,280 --> 00:02:16,200
lhu and lbu so what are those assembly

55
00:02:14,040 --> 00:02:18,800
instructions now as a reminder in the

56
00:02:16,200 --> 00:02:21,720
previous section we saw LH load half

57
00:02:18,800 --> 00:02:24,760
word and it took an immediate added it

58
00:02:21,720 --> 00:02:27,000
to the register and then loaded the

59
00:02:24,760 --> 00:02:30,239
value from memory a half word value from

60
00:02:27,000 --> 00:02:33,400
memory into the destination register and

61
00:02:30,239 --> 00:02:35,440
ultimately sign extended it to 64 bits

62
00:02:33,400 --> 00:02:38,280
therefore it should be relatively easy

63
00:02:35,440 --> 00:02:41,760
for us to understand lhu because this is

64
00:02:38,280 --> 00:02:43,760
now load half word unsigned so unsigned

65
00:02:41,760 --> 00:02:45,599
from memory to register so the key

66
00:02:43,760 --> 00:02:47,680
difference is it's going to do zero

67
00:02:45,599 --> 00:02:50,319
extension instead of sign extension

68
00:02:47,680 --> 00:02:53,640
because it is treating that 16 bits as

69
00:02:50,319 --> 00:02:55,760
an unsigned value so same way that it's

70
00:02:53,640 --> 00:02:59,000
written and it's a load assembly

71
00:02:55,760 --> 00:03:00,959
instruction so know that loads collect

72
00:02:59,000 --> 00:03:03,480
memory from this side and loaded into a

73
00:03:00,959 --> 00:03:06,280
register so it loads the half word or

74
00:03:03,480 --> 00:03:10,280
16bit value from the memory address that

75
00:03:06,280 --> 00:03:12,879
is defined as rs1 plus immediate 12 the

76
00:03:10,280 --> 00:03:15,000
immediate 12 itself is still a signed

77
00:03:12,879 --> 00:03:17,519
value so that will be sign extended and

78
00:03:15,000 --> 00:03:20,440
added to the rs2 so you could be plus or

79
00:03:17,519 --> 00:03:22,319
minus calculate a memory address but

80
00:03:20,440 --> 00:03:24,040
then when you pull it in and store it in

81
00:03:22,319 --> 00:03:26,519
the destination it should be zero

82
00:03:24,040 --> 00:03:29,280
extended should treat just be treated as

83
00:03:26,519 --> 00:03:31,200
unsigned and just like with LH when

84
00:03:29,280 --> 00:03:33,080
you're running 32-bit code the zero

85
00:03:31,200 --> 00:03:35,560
extension to 32bits you're done and when

86
00:03:33,080 --> 00:03:38,360
you're running 64-bit zero extend it to

87
00:03:35,560 --> 00:03:40,439
64bits and you're done now I am a broken

88
00:03:38,360 --> 00:03:42,319
record and I thought about deleting this

89
00:03:40,439 --> 00:03:44,120
the portion where it says like most

90
00:03:42,319 --> 00:03:46,640
instructions this one is read from right

91
00:03:44,120 --> 00:03:48,519
to left but then I thought well what if

92
00:03:46,640 --> 00:03:50,360
someone just comes and looks at this

93
00:03:48,519 --> 00:03:53,439
thing in the non-animated form in the

94
00:03:50,360 --> 00:03:55,480
PDF exported form I want them to still

95
00:03:53,439 --> 00:03:57,720
understand what's going on so I kept

96
00:03:55,480 --> 00:03:59,720
these uh extra things that I've said for

97
00:03:57,720 --> 00:04:01,439
every single load in every single store

98
00:03:59,720 --> 00:04:04,079
throughout the class just because

99
00:04:01,439 --> 00:04:06,360
someone may see it in the exported class

100
00:04:04,079 --> 00:04:09,959
materials and also should always

101
00:04:06,360 --> 00:04:11,799
remember that reputation is the basis of

102
00:04:09,959 --> 00:04:14,840
memorization okay so now we've got a

103
00:04:11,799 --> 00:04:16,880
store stores store from the left to the

104
00:04:14,840 --> 00:04:20,160
right and this is going to take the

105
00:04:16,880 --> 00:04:22,840
bottom bite store bite take the bottom

106
00:04:20,160 --> 00:04:26,080
bite of rs2 the bottom 8 Bits and store

107
00:04:22,840 --> 00:04:28,800
it to memory where in memory well at rs1

108
00:04:26,080 --> 00:04:30,880
plus sign extended immediate 12 that's

109
00:04:28,800 --> 00:04:33,520
going to be a bite memory address and

110
00:04:30,880 --> 00:04:36,400
again it is stored from left to right

111
00:04:33,520 --> 00:04:38,960
and again parentheses means grab from

112
00:04:36,400 --> 00:04:41,240
memory okay and then there is load bite

113
00:04:38,960 --> 00:04:43,520
unsigned so just like load half word

114
00:04:41,240 --> 00:04:45,680
unsigned this is going to use zero

115
00:04:43,520 --> 00:04:47,560
extension rather than sign extension and

116
00:04:45,680 --> 00:04:49,720
it's loading a bite so it is only

117
00:04:47,560 --> 00:04:53,039
grabbing 8 Bits And then zero extending

118
00:04:49,720 --> 00:04:55,800
it to 32 and subsequently 64 bits so

119
00:04:53,039 --> 00:04:57,639
load we calculate a memory address the

120
00:04:55,800 --> 00:05:01,080
immediate is still a sign extended

121
00:04:57,639 --> 00:05:03,120
immediate so rs1 plus sign extended 12

122
00:05:01,080 --> 00:05:05,800
get that as a memory address and pluck

123
00:05:03,120 --> 00:05:09,120
out a bite and zero extended and stick

124
00:05:05,800 --> 00:05:13,280
it into Rd duplicate duplicate same

125
00:05:09,120 --> 00:05:15,919
things as always all right great so time

126
00:05:13,280 --> 00:05:18,280
for you to figure out where all of that

127
00:05:15,919 --> 00:05:20,960
stuff is stored on the stack and you do

128
00:05:18,280 --> 00:05:22,400
it like you always do it by stopping

129
00:05:20,960 --> 00:05:27,360
stepping through the assembly and

130
00:05:22,400 --> 00:05:27,360
drawing yourself a stack diagram

