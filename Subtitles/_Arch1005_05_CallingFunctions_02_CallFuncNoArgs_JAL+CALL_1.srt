1
00:00:00,080 --> 00:00:04,319
now we're going to get some practice

2
00:00:01,719 --> 00:00:06,080
calling functions main is going to call

3
00:00:04,319 --> 00:00:08,040
func but it's not going to pass any

4
00:00:06,080 --> 00:00:10,000
arguments yet we'll learn about argument

5
00:00:08,040 --> 00:00:12,639
passing examples later it's going to

6
00:00:10,000 --> 00:00:15,480
call func but all func does is return

7
00:00:12,639 --> 00:00:17,880
beef but we don't even use that return

8
00:00:15,480 --> 00:00:20,519
value we just return food so it's a

9
00:00:17,880 --> 00:00:22,439
whole B of whole bunch of pointlessness

10
00:00:20,519 --> 00:00:24,080
but it's optimization level zero so

11
00:00:22,439 --> 00:00:26,599
we'll get to see what the compiler

12
00:00:24,080 --> 00:00:28,400
generates for all that pointlessness so

13
00:00:26,599 --> 00:00:30,519
here is the code generated for that and

14
00:00:28,400 --> 00:00:33,680
we see that we have both a fun

15
00:00:30,519 --> 00:00:35,800
and a main so we've got the function at

16
00:00:33,680 --> 00:00:38,200
lower addresses and then main at higher

17
00:00:35,800 --> 00:00:40,680
addresses and it looks like we picked up

18
00:00:38,200 --> 00:00:45,360
a new assembly instruction jaw seems

19
00:00:40,680 --> 00:00:48,399
like Jaw is calling to func so we

20
00:00:45,360 --> 00:00:51,360
previously saw jll like dollar so as a

21
00:00:48,399 --> 00:00:53,320
reminder of what that did we had two

22
00:00:51,360 --> 00:00:55,640
effects the first effect is that it took

23
00:00:53,320 --> 00:00:58,280
a source register added the signed

24
00:00:55,640 --> 00:01:00,440
immediate 12 and changed the program

25
00:00:58,280 --> 00:01:02,840
counter the second effect

26
00:01:00,440 --> 00:01:05,159
is that it took the destination register

27
00:01:02,840 --> 00:01:07,720
whatever was specified and it placed the

28
00:01:05,159 --> 00:01:09,920
address after the jaw instruction into

29
00:01:07,720 --> 00:01:13,080
that and that of course would be PC plus

30
00:01:09,920 --> 00:01:15,400
4 cuz joller is a 4 byte non-compressed

31
00:01:13,080 --> 00:01:17,439
assembly instruction and more often than

32
00:01:15,400 --> 00:01:20,159
not the jaer would be used with a

33
00:01:17,439 --> 00:01:23,680
destination address of RA Because by the

34
00:01:20,159 --> 00:01:26,920
ABI convention ra is the return address

35
00:01:23,680 --> 00:01:28,920
so JW is the jump and link not jump and

36
00:01:26,920 --> 00:01:31,479
link register so why doesn't have the

37
00:01:28,920 --> 00:01:34,000
name register included because whereas

38
00:01:31,479 --> 00:01:36,360
jump and link register took a source

39
00:01:34,000 --> 00:01:39,079
register that added an immediate and put

40
00:01:36,360 --> 00:01:42,759
it into PC here there is no longer a

41
00:01:39,079 --> 00:01:45,640
source it is always going to be PC plus

42
00:01:42,759 --> 00:01:47,719
an immediate going back into PC and we

43
00:01:45,640 --> 00:01:49,560
also see that the immediate is increased

44
00:01:47,719 --> 00:01:51,600
in size previously it was a 12- bit

45
00:01:49,560 --> 00:01:55,240
immediate and now it is a 21 bit

46
00:01:51,600 --> 00:01:57,119
immediate so these are PC relative jumps

47
00:01:55,240 --> 00:01:58,840
whereas the other one could be relative

48
00:01:57,119 --> 00:02:01,799
to any sort of address that you could

49
00:01:58,840 --> 00:02:04,119
calculate like Jer this also sets the

50
00:02:01,799 --> 00:02:05,799
least significant bit of the address to

51
00:02:04,119 --> 00:02:08,800
zero so that it's going to be a multiple

52
00:02:05,799 --> 00:02:11,200
of two now effect two is the same thing

53
00:02:08,800 --> 00:02:13,640
as joller you take the address after the

54
00:02:11,200 --> 00:02:14,840
jaw which would be PC plus4 again we're

55
00:02:13,640 --> 00:02:17,680
not dealing with a compressed

56
00:02:14,840 --> 00:02:20,160
instruction PC plus4 into the

57
00:02:17,680 --> 00:02:22,720
destination register and more often than

58
00:02:20,160 --> 00:02:25,040
not that's going to be the ra the return

59
00:02:22,720 --> 00:02:26,400
address register and it uses that for

60
00:02:25,040 --> 00:02:28,280
the same reason that something like

61
00:02:26,400 --> 00:02:30,720
joller would use that so that you can

62
00:02:28,280 --> 00:02:34,840
get back to the add address after the

63
00:02:30,720 --> 00:02:36,879
jaw perhaps via a r right so jaw is sort

64
00:02:34,840 --> 00:02:39,159
of like a call and then Rett would be

65
00:02:36,879 --> 00:02:42,040
returning from the call back to a return

66
00:02:39,159 --> 00:02:45,040
address register that was set by the jaw

67
00:02:42,040 --> 00:02:48,360
instruction so jaw is very similar to

68
00:02:45,040 --> 00:02:50,599
jaer except that the jump is always PC

69
00:02:48,360 --> 00:02:52,760
relative so it is always implicit in

70
00:02:50,599 --> 00:02:55,200
this instruction that it is PC plus

71
00:02:52,760 --> 00:02:57,319
immediate not some source register plus

72
00:02:55,200 --> 00:02:59,480
immediate you can imagine that jaer

73
00:02:57,319 --> 00:03:00,319
might be more appropriate for something

74
00:02:59,480 --> 00:03:03,840
like

75
00:03:00,319 --> 00:03:06,000
uh C++ virtual functions or C usage of

76
00:03:03,840 --> 00:03:08,200
function pointers because you could

77
00:03:06,000 --> 00:03:09,959
calculate a address of a function

78
00:03:08,200 --> 00:03:12,319
pointer that you want to jump to just in

79
00:03:09,959 --> 00:03:13,799
time based on some input and you could

80
00:03:12,319 --> 00:03:16,640
say okay now I'm going to calculate that

81
00:03:13,799 --> 00:03:18,959
address put it into the RS and then jump

82
00:03:16,640 --> 00:03:21,280
there and then as previously mentioned

83
00:03:18,959 --> 00:03:23,840
the signed immediate is 21 bits instead

84
00:03:21,280 --> 00:03:26,599
of 12 that means you functionally get a

85
00:03:23,840 --> 00:03:29,080
plus or minus 1 Megabyte jump forward

86
00:03:26,599 --> 00:03:31,239
and jump backwards with a JW so Jal is

87
00:03:29,080 --> 00:03:34,400
great for jumping around internally

88
00:03:31,239 --> 00:03:36,480
within your own code and jaw is perhaps

89
00:03:34,400 --> 00:03:38,519
better for if you calculate an address

90
00:03:36,480 --> 00:03:41,439
not at compile time but on

91
00:03:38,519 --> 00:03:43,720
demand okay so that was jaw now you know

92
00:03:41,439 --> 00:03:47,120
what you need to know to go throw this

93
00:03:43,720 --> 00:03:50,000
into a debugger so here's our starting

94
00:03:47,120 --> 00:03:52,200
stack and it is time for you to stop and

95
00:03:50,000 --> 00:03:55,959
step through the assembly and draw a

96
00:03:52,200 --> 00:03:55,959
stack diagram

