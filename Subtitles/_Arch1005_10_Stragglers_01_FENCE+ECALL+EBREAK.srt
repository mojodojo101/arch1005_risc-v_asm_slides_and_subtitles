1
00:00:00,199 --> 00:00:04,279
so about this point in the class when

2
00:00:02,000 --> 00:00:06,560
I'm seeing how close we are I start to

3
00:00:04,279 --> 00:00:08,559
think stupid checklist why you know

4
00:00:06,560 --> 00:00:10,280
complete well it's because these

5
00:00:08,559 --> 00:00:12,320
particular assembly instructions are

6
00:00:10,280 --> 00:00:13,719
special and they do not show up in

7
00:00:12,320 --> 00:00:16,000
normal code you have to go out of your

8
00:00:13,719 --> 00:00:18,080
way to create them and so I'm just going

9
00:00:16,000 --> 00:00:20,359
to tell you what they are starting with

10
00:00:18,080 --> 00:00:22,279
fence fence is a memory fence and the

11
00:00:20,359 --> 00:00:25,560
fence instruction is used to order

12
00:00:22,279 --> 00:00:28,240
device IO and memory accesses as viewed

13
00:00:25,560 --> 00:00:30,480
by other RISC-V harts and external

14
00:00:28,240 --> 00:00:31,840
devices or co-processors

15
00:00:30,480 --> 00:00:33,879
another way of putting it is that a

16
00:00:31,840 --> 00:00:35,960
fence is going to be put into the code

17
00:00:33,879 --> 00:00:38,680
to make sure that everything before the

18
00:00:35,960 --> 00:00:41,760
fence in code has to have completed any

19
00:00:38,680 --> 00:00:43,520
of its device IO or memory accesses

20
00:00:41,760 --> 00:00:45,640
whether it's read or write you have to

21
00:00:43,520 --> 00:00:47,960
complete that stuff before you pass the

22
00:00:45,640 --> 00:00:49,960
fence and only then are you allowed to

23
00:00:47,960 --> 00:00:52,280
proceed and that makes sure that all the

24
00:00:49,960 --> 00:00:54,960
other hardware threads or harts are

25
00:00:52,280 --> 00:00:56,359
able to see stuff equally like they're

26
00:00:54,960 --> 00:00:58,840
not going to see different things

27
00:00:56,359 --> 00:01:01,000
depending on which one uh sees some read

28
00:00:58,840 --> 00:01:03,160
or write having occurred first now there

29
00:01:01,000 --> 00:01:04,680
is a more complicated form of fence that

30
00:01:03,160 --> 00:01:06,560
you can give in the assembly but we're

31
00:01:04,680 --> 00:01:08,360
just going to say you just use a fence

32
00:01:06,560 --> 00:01:11,159
instruction and it'll do what you want

33
00:01:08,360 --> 00:01:14,320
in that it places a fence on all device

34
00:01:11,159 --> 00:01:16,360
IO and all memory accesses so in the

35
00:01:14,320 --> 00:01:17,840
future we may cover that in a in another

36
00:01:16,360 --> 00:01:20,400
class where we talk about the different

37
00:01:17,840 --> 00:01:23,079
components but that's definitely not

38
00:01:20,400 --> 00:01:24,880
needed in this class and as a reminder

39
00:01:23,079 --> 00:01:27,000
from early on in the class we talked

40
00:01:24,880 --> 00:01:29,560
about a hart tohe hart chat we said a

41
00:01:27,000 --> 00:01:31,400
hart is a hardware thread meaning that

42
00:01:29,560 --> 00:01:34,040
if you have a core you can actually have

43
00:01:31,400 --> 00:01:35,560
multiple independent hardware threads

44
00:01:34,040 --> 00:01:37,200
that are going to basically have their

45
00:01:35,560 --> 00:01:40,119
own registers and they're going to look

46
00:01:37,200 --> 00:01:41,720
like uh their independent cores from the

47
00:01:40,119 --> 00:01:43,240
uh from the perspective of software

48
00:01:41,720 --> 00:01:45,600
that's trying to do you know

49
00:01:43,240 --> 00:01:48,719
multi-threading so the next instruction

50
00:01:45,600 --> 00:01:50,799
is e braak for environment break and

51
00:01:48,719 --> 00:01:52,840
there's both an e bra and a compressed e

52
00:01:50,799 --> 00:01:55,240
bra and this is how you do software

53
00:01:52,840 --> 00:01:56,880
break points so behind the scenes when

54
00:01:55,240 --> 00:01:58,759
you set a software breakpoint in

55
00:01:56,880 --> 00:02:00,799
something like GDB it's going to

56
00:01:58,759 --> 00:02:02,920
actually take the Loc that you specifi

57
00:02:00,799 --> 00:02:05,479
the break point should occur if it was a

58
00:02:02,920 --> 00:02:07,399
4 byte instruction it is going to remove

59
00:02:05,479 --> 00:02:09,360
the instruction and insert an e bra

60
00:02:07,399 --> 00:02:11,080
instruction if it was a 2 byte

61
00:02:09,360 --> 00:02:13,640
compressed instruction it's going to

62
00:02:11,080 --> 00:02:15,480
insert a compressed e instead as the

63
00:02:13,640 --> 00:02:17,800
code is running along eventually it will

64
00:02:15,480 --> 00:02:19,440
hit the e bra that will fire off the

65
00:02:17,800 --> 00:02:21,480
operating system will say like hey is

66
00:02:19,440 --> 00:02:23,120
there a debugger attached right now if

67
00:02:21,480 --> 00:02:25,200
so then they'll hand control to the

68
00:02:23,120 --> 00:02:27,120
debugger and the debugger will then you

69
00:02:25,200 --> 00:02:29,080
know stop and show you the context the

70
00:02:27,120 --> 00:02:30,959
registers whatever you want it to see

71
00:02:29,080 --> 00:02:32,840
and the debugger will show you that now

72
00:02:30,959 --> 00:02:34,760
in general debuggers will always hide

73
00:02:32,840 --> 00:02:37,000
the fact that they actually just removed

74
00:02:34,760 --> 00:02:39,599
the assembly instruction and installed a

75
00:02:37,000 --> 00:02:41,319
e bra or compressed e bra so if you were

76
00:02:39,599 --> 00:02:44,120
to just you know do from within the

77
00:02:41,319 --> 00:02:45,840
debugger a read memory of the address

78
00:02:44,120 --> 00:02:47,440
where you put a software break point

79
00:02:45,840 --> 00:02:49,080
it's going to show you the original

80
00:02:47,440 --> 00:02:51,879
instruction because that's what most

81
00:02:49,080 --> 00:02:53,720
people want to see but uh behind the

82
00:02:51,879 --> 00:02:56,000
scenes it actually got replaced and when

83
00:02:53,720 --> 00:02:57,920
you continue the code execution they're

84
00:02:56,000 --> 00:03:00,040
just going to execute the original

85
00:02:57,920 --> 00:03:02,319
instruction and make sure that the uh

86
00:03:00,040 --> 00:03:04,319
don't immediately break again and then

87
00:03:02,319 --> 00:03:06,879
continue on with the code execution so

88
00:03:04,319 --> 00:03:09,720
basically execute the real instruction

89
00:03:06,879 --> 00:03:11,080
replace the breakpoint back in the

90
00:03:09,720 --> 00:03:12,840
location where you want the breakpoint

91
00:03:11,080 --> 00:03:15,440
unless you've specifically said you know

92
00:03:12,840 --> 00:03:17,440
break once or uh you know just removed

93
00:03:15,440 --> 00:03:19,760
it as a temporary breakpoint so

94
00:03:17,440 --> 00:03:21,920
basically execute Put the Brak back in

95
00:03:19,760 --> 00:03:23,879
and continue on but if you want to see

96
00:03:21,920 --> 00:03:26,280
that this sort of subterfuge is

97
00:03:23,879 --> 00:03:28,439
occurring because of the debugger then

98
00:03:26,280 --> 00:03:30,040
uh I will give you a lab where you can

99
00:03:28,439 --> 00:03:31,799
go ahead and write a program that's

100
00:03:30,040 --> 00:03:34,799
going to actually read its own memory to

101
00:03:31,799 --> 00:03:35,680
be able to detect when e BRS or C braks

102
00:03:34,799 --> 00:03:39,080
have been

103
00:03:35,680 --> 00:03:43,200
inserted okay next and final instruction

104
00:03:39,080 --> 00:03:45,239
for RV32I ecall environment call this

105
00:03:43,200 --> 00:03:47,319
calls from a lower privileged execution

106
00:03:45,239 --> 00:03:48,879
environment to a higher privileged one

107
00:03:47,319 --> 00:03:50,519
so it could be calling from userspace

108
00:03:48,879 --> 00:03:52,680
to kernel space or kernelspace to

109
00:03:50,519 --> 00:03:54,799
hypervisor it could also be used within

110
00:03:52,680 --> 00:03:57,120
the context of firmware originally this

111
00:03:54,799 --> 00:03:59,519
instruction was called s call as in

112
00:03:57,120 --> 00:04:00,959
system call but then they decided Well

113
00:03:59,519 --> 00:04:02,959
it's it's more generic to call it

114
00:04:00,959 --> 00:04:04,200
environment call because it may not be

115
00:04:02,959 --> 00:04:06,400
just used for something like an

116
00:04:04,200 --> 00:04:08,920
operating system system call it could be

117
00:04:06,400 --> 00:04:11,159
used for like a hyper call so this will

118
00:04:08,920 --> 00:04:14,280
be appropriate to cover in architecture

119
00:04:11,159 --> 00:04:17,040
2005 and OST2 when we cover things like

120
00:04:14,280 --> 00:04:20,759
system calls and with that bada bing

121
00:04:17,040 --> 00:04:20,759
bada boom we have completed our

122
00:04:21,420 --> 00:04:26,759
[Music]

123
00:04:24,320 --> 00:04:29,919
checklist super awesome job collecting

124
00:04:26,759 --> 00:04:32,320
all the stars everyone all right and our

125
00:04:29,919 --> 00:04:35,759
final things on this diagram fence up

126
00:04:32,320 --> 00:04:38,240
here eall right there and E bra with the

127
00:04:35,759 --> 00:04:40,840
compressed e bra and with that we have

128
00:04:38,240 --> 00:04:43,600
learned a ton of assembly instructions

129
00:04:40,840 --> 00:04:46,000
55 regular instructions 16 pseudo

130
00:04:43,600 --> 00:04:48,919
instructions and 25 compressed

131
00:04:46,000 --> 00:04:51,320
instructions and while this diagram

132
00:04:48,919 --> 00:04:53,400
looks big and potentially intimidating

133
00:04:51,320 --> 00:04:55,960
you have to know that really there are

134
00:04:53,400 --> 00:04:57,800
some Blobs of things here right it's

135
00:04:55,960 --> 00:04:59,840
just a bunch of add and subtract

136
00:04:57,800 --> 00:05:01,320
variants and some pseudo instruction

137
00:04:59,840 --> 00:05:03,400
which behind the scenes are just adds

138
00:05:01,320 --> 00:05:05,000
and subtracts we've got a whole bunch of

139
00:05:03,400 --> 00:05:07,160
loads and stores and they're just

140
00:05:05,000 --> 00:05:10,240
different sizes loading and storing of

141
00:05:07,160 --> 00:05:12,360
double words words halfword and bites

142
00:05:10,240 --> 00:05:15,120
got a bunch of control flow stuff be it

143
00:05:12,360 --> 00:05:17,800
the branches down here or the jaws and

144
00:05:15,120 --> 00:05:20,160
calls and rs and things like that we've

145
00:05:17,800 --> 00:05:23,039
got many shifts more shifts than you can

146
00:05:20,160 --> 00:05:25,960
shake a stick at and we've got Boolean

147
00:05:23,039 --> 00:05:28,199
logic set if less than and a bunch of

148
00:05:25,960 --> 00:05:30,639
miscellaneous stuff here the fence the E

149
00:05:28,199 --> 00:05:32,560
call and the E bra so when you look at

150
00:05:30,639 --> 00:05:35,120
the picture like this it really isn't

151
00:05:32,560 --> 00:05:37,080
that bad right we got adds and subtracts

152
00:05:35,120 --> 00:05:38,960
which everybody knows you just need to

153
00:05:37,080 --> 00:05:42,280
remember what the different forms are in

154
00:05:38,960 --> 00:05:45,199
terms of immediates versus registers for

155
00:05:42,280 --> 00:05:48,039
the arguments or word size and yeah so

156
00:05:45,199 --> 00:05:49,479
just in general it's not too bad so in

157
00:05:48,039 --> 00:05:51,800
this class we said that we were only

158
00:05:49,479 --> 00:05:54,440
going to cover these things and that was

159
00:05:51,800 --> 00:05:56,720
the 32-bit and 64-bit base and we did

160
00:05:54,440 --> 00:05:58,120
cover that we collected all the stars we

161
00:05:56,720 --> 00:05:59,680
said we're not going to cover all the

162
00:05:58,120 --> 00:06:01,880
compressed things but after after you

163
00:05:59,680 --> 00:06:03,919
learn how to read the fun manual later

164
00:06:01,880 --> 00:06:05,600
on in the class you can go off and find

165
00:06:03,919 --> 00:06:07,599
all the compressed instructions that we

166
00:06:05,600 --> 00:06:09,680
didn't cover thus far and so we're

167
00:06:07,599 --> 00:06:12,440
pretty much ready to move on to the m

168
00:06:09,680 --> 00:06:14,080
extension for multiply and divide but

169
00:06:12,440 --> 00:06:16,120
again at the very beginning of the class

170
00:06:14,080 --> 00:06:18,000
I said that everything boils down to

171
00:06:16,120 --> 00:06:21,800
these themes loads and stores ads and

172
00:06:18,000 --> 00:06:23,919
subtracts ansor exors and so forth and

173
00:06:21,800 --> 00:06:26,560
if we break all of those categories up

174
00:06:23,919 --> 00:06:28,680
into the actual instructions you can see

175
00:06:26,560 --> 00:06:31,560
there's the loads and the stores there's

176
00:06:28,680 --> 00:06:34,240
the ads and subtracts there's the binary

177
00:06:31,560 --> 00:06:36,880
Boolean operations got the shifts we've

178
00:06:34,240 --> 00:06:41,479
got the salt for set if less than

179
00:06:36,880 --> 00:06:44,800
branches joller and JW and Lou LUI and

180
00:06:41,479 --> 00:06:46,280
we PC so this is you know a lot less

181
00:06:44,800 --> 00:06:48,080
intimidating what you realize there's

182
00:06:46,280 --> 00:06:49,759
just a couple you know permutations you

183
00:06:48,080 --> 00:06:52,240
just need to be able to say okay load

184
00:06:49,759 --> 00:06:54,319
bite and load bite unsigned load half

185
00:06:52,240 --> 00:06:56,759
word and load half word onsign and load

186
00:06:54,319 --> 00:06:59,000
dword and there's no unsign variant and

187
00:06:56,759 --> 00:07:02,039
so forth so we definitely learned a lot

188
00:06:59,000 --> 00:07:05,919
of stuff but it all is much simpler once

189
00:07:02,039 --> 00:07:05,919
you realize what all is there

