1
00:00:00,399 --> 00:00:04,319
now I always like to get into it as

2
00:00:02,360 --> 00:00:08,599
quickly as possible so here is your

3
00:00:04,319 --> 00:00:11,160
first RISC-V instruction knop or noop

4
00:00:08,599 --> 00:00:13,880
no operation no registers no values no

5
00:00:11,160 --> 00:00:16,800
nothing a no op is generally just there

6
00:00:13,880 --> 00:00:19,000
in code to pad or align bytes to make

7
00:00:16,800 --> 00:00:20,840
them go on some particular bite boundary

8
00:00:19,000 --> 00:00:23,680
for instance multiples of two multiples

9
00:00:20,840 --> 00:00:25,039
of four multiples of 16 and so forth and

10
00:00:23,680 --> 00:00:27,400
sometimes on some architectures it's

11
00:00:25,039 --> 00:00:29,080
there to delay time but that's not how

12
00:00:27,400 --> 00:00:30,720
it's used on this architecture now to

13
00:00:29,080 --> 00:00:32,599
introduce you to the inventions I'll be

14
00:00:30,720 --> 00:00:34,360
using every time I introduce a new

15
00:00:32,599 --> 00:00:36,879
assembly instruction we'll collect a

16
00:00:34,360 --> 00:00:39,440
nice little star here and then I'll also

17
00:00:36,879 --> 00:00:42,600
indicate which ISA it is part of this

18
00:00:39,440 --> 00:00:46,120
case I say RV32I but I want you to

19
00:00:42,600 --> 00:00:50,840
remember that RV32I instructions are

20
00:00:46,120 --> 00:00:53,559
actually included in the RV64I base ISA

21
00:00:50,840 --> 00:00:55,920
so anytime I put RV32I I don't think

22
00:00:53,559 --> 00:00:58,680
that is exclusively only available for

23
00:00:55,920 --> 00:01:02,680
32bit code it will also be available for

24
00:00:58,680 --> 00:01:04,479
RV64I by ISA compiled code AS will be

25
00:01:02,680 --> 00:01:07,560
used in this class but I've got some

26
00:01:04,479 --> 00:01:10,080
news for you some knop news knop is not

27
00:01:07,560 --> 00:01:13,119
a real instruction it's a pseudo

28
00:01:10,080 --> 00:01:14,880
instruction or instruction alias pseudo

29
00:01:13,119 --> 00:01:18,240
instructions Walk Among Us open your

30
00:01:14,880 --> 00:01:20,560
eyes sheeple so in this class I'm going

31
00:01:18,240 --> 00:01:24,400
to be using the convention of

32
00:01:20,560 --> 00:01:26,759
referencing the 1984 series V for

33
00:01:24,400 --> 00:01:29,400
visitors so I've never actually seen

34
00:01:26,759 --> 00:01:32,079
this series but my wife has and she said

35
00:01:29,400 --> 00:01:33,840
that it gave her nightmares as a kid so

36
00:01:32,079 --> 00:01:35,600
the basic premise of the series is that

37
00:01:33,840 --> 00:01:38,200
some aliens come and they look like

38
00:01:35,600 --> 00:01:40,240
humans but beneath their skin they're

39
00:01:38,200 --> 00:01:42,119
actually reptiles who are not

40
00:01:40,240 --> 00:01:43,920
particularly friendly but they try to

41
00:01:42,119 --> 00:01:45,920
act like they're friendly ironically

42
00:01:43,920 --> 00:01:48,600
enough I think this is where the actual

43
00:01:45,920 --> 00:01:51,799
real conspiracy theory that there are

44
00:01:48,600 --> 00:01:53,680
actual Elites in power in the world are

45
00:01:51,799 --> 00:01:56,039
actually reptiles beneath their skin

46
00:01:53,680 --> 00:01:59,280
feel like it's a not particularly

47
00:01:56,039 --> 00:02:01,759
creative uh you know reappropriation of

48
00:01:59,280 --> 00:02:04,240
the conspiracy from this particular TV

49
00:02:01,759 --> 00:02:07,000
show anyway so every time we encounter a

50
00:02:04,240 --> 00:02:09,679
pseudo instruction we will have some V

51
00:02:07,000 --> 00:02:12,520
creatures so behind the scenes whenever

52
00:02:09,679 --> 00:02:14,480
you see a disassembler showing you a op

53
00:02:12,520 --> 00:02:16,319
or if you put into an assembler hey I

54
00:02:14,480 --> 00:02:18,120
would like to use a op most of the time

55
00:02:16,319 --> 00:02:20,480
what it's actually going to be using is

56
00:02:18,120 --> 00:02:22,959
the ADDI assembly instruction with ad

57
00:02:20,480 --> 00:02:24,879
ey x0 x0 and Zer there are many

58
00:02:22,959 --> 00:02:26,640
different sequences that can do nothing

59
00:02:24,879 --> 00:02:28,640
but this is sort of the recommended

60
00:02:26,640 --> 00:02:30,519
version so before we get into what that

61
00:02:28,640 --> 00:02:32,519
ADDI is I want to introduce some

62
00:02:30,519 --> 00:02:34,440
conventions I'm going to be using for

63
00:02:32,519 --> 00:02:37,920
writing assembly instructions so if you

64
00:02:34,440 --> 00:02:40,400
see something like italic Rd that is a

65
00:02:37,920 --> 00:02:43,319
destination general purpose register if

66
00:02:40,400 --> 00:02:45,319
you see RS that is a source general

67
00:02:43,319 --> 00:02:47,239
purpose register if the particular

68
00:02:45,319 --> 00:02:49,400
assembly instruction takes only a single

69
00:02:47,239 --> 00:02:51,319
source on the other hand if it takes

70
00:02:49,400 --> 00:02:54,480
multiple source registers you will have

71
00:02:51,319 --> 00:02:57,080
an rs1 for the first source register and

72
00:02:54,480 --> 00:02:59,080
rs2 for the second general purpose

73
00:02:57,080 --> 00:03:00,319
source register and I explicitly call

74
00:02:59,080 --> 00:03:02,000
out that all of these these are general

75
00:03:00,319 --> 00:03:04,599
purpose registers meaning that you

76
00:03:02,000 --> 00:03:06,799
cannot use the PC or program counter

77
00:03:04,599 --> 00:03:09,120
register for assembly instructions that

78
00:03:06,799 --> 00:03:13,440
have these indications we also see

79
00:03:09,120 --> 00:03:16,640
things like IMM for immediate and N for

80
00:03:13,440 --> 00:03:19,799
an nbit signed immediate in this case so

81
00:03:16,640 --> 00:03:23,360
if I say IMM n it's going to mean for

82
00:03:19,799 --> 00:03:25,440
instance imm2 that is a signed 12bit

83
00:03:23,360 --> 00:03:28,000
immediate it's going to be sign extended

84
00:03:25,440 --> 00:03:31,480
up to whatever the X length size is when

85
00:03:28,000 --> 00:03:33,360
it's ultimately used be 32 64 128 bits

86
00:03:31,480 --> 00:03:35,120
there's only going to be a couple of

87
00:03:33,360 --> 00:03:37,280
exceptions to this later on when we talk

88
00:03:35,120 --> 00:03:39,799
about shift instructions so most of the

89
00:03:37,280 --> 00:03:43,280
time any time you see me saying imm2 for

90
00:03:39,799 --> 00:03:45,200
instance say it's a 12bit signed uh

91
00:03:43,280 --> 00:03:47,319
immediate and immediate is again just a

92
00:03:45,200 --> 00:03:49,799
special word for a constant that is

93
00:03:47,319 --> 00:03:54,159
baked into an assembly stream if you see

94
00:03:49,799 --> 00:03:55,720
U IMM n that means it is a unsigned and

95
00:03:54,159 --> 00:03:57,680
bit immediate so that's not going to be

96
00:03:55,720 --> 00:03:59,760
sign extended it's going to be zero

97
00:03:57,680 --> 00:04:01,640
extended you'll actually see throughout

98
00:03:59,760 --> 00:04:04,000
the assembly instructions that the bias

99
00:04:01,640 --> 00:04:06,480
is very much to always be using signed

100
00:04:04,000 --> 00:04:08,360
stuff and usually if they're going to

101
00:04:06,480 --> 00:04:10,079
use unsigned it's going to be a separate

102
00:04:08,360 --> 00:04:12,120
assembly instruction that will have a u

103
00:04:10,079 --> 00:04:13,799
somewhere in its name to indicate hey by

104
00:04:12,120 --> 00:04:15,280
the way this is the unsigned version of

105
00:04:13,799 --> 00:04:17,320
this assembly instruction and

106
00:04:15,280 --> 00:04:19,280
consequently you'll also see unsigned

107
00:04:17,320 --> 00:04:20,959
immediates used in the notation there's

108
00:04:19,280 --> 00:04:24,160
a small modification of that there's the

109
00:04:20,959 --> 00:04:26,720
NZ for nonzero nonzero signed we could

110
00:04:24,160 --> 00:04:28,240
also have an nzu IMM I don't remember

111
00:04:26,720 --> 00:04:30,280
for sure if we see any of those in this

112
00:04:28,240 --> 00:04:32,080
class but basically that would be

113
00:04:30,280 --> 00:04:34,280
nonzero unsigned version of this so

114
00:04:32,080 --> 00:04:36,080
again an immediate it's n Bits long and

115
00:04:34,280 --> 00:04:38,160
in this case it is specifically saying

116
00:04:36,080 --> 00:04:40,360
it shall not be equal to zero it is

117
00:04:38,160 --> 00:04:42,320
reserved or otherwise unacceptable for

118
00:04:40,360 --> 00:04:44,520
it to be zero and then the final form

119
00:04:42,320 --> 00:04:46,840
has an uppercase U to indicate that it

120
00:04:44,520 --> 00:04:48,919
is setting the upper n Bits of an

121
00:04:46,840 --> 00:04:51,520
immediate so most commonly this is going

122
00:04:48,919 --> 00:04:54,039
to be the upper 20 bits to go along with

123
00:04:51,520 --> 00:04:56,759
the other things doing the lower 12 bits

124
00:04:54,039 --> 00:04:59,400
and so upper 20 plus lower 12 equals a

125
00:04:56,759 --> 00:05:01,440
32bit immediate but this can be upper n

126
00:04:59,400 --> 00:05:04,680
could could be 17 it could be some other

127
00:05:01,440 --> 00:05:06,560
value and so the U capital u indicates

128
00:05:04,680 --> 00:05:09,400
it's setting the upper bits not just the

129
00:05:06,560 --> 00:05:11,600
lower bits so RISC-V immediates again

130
00:05:09,400 --> 00:05:13,120
immediate is just a constant value that

131
00:05:11,600 --> 00:05:15,120
is hard-coded into the assembly

132
00:05:13,120 --> 00:05:17,759
instruction it's just a special name for

133
00:05:15,120 --> 00:05:19,800
that type of constant and in Risk 5 if

134
00:05:17,759 --> 00:05:22,199
you see assembly instructions that end

135
00:05:19,800 --> 00:05:24,440
in an I that is usually indicating that

136
00:05:22,199 --> 00:05:26,840
it's a form that takes an immediate so

137
00:05:24,440 --> 00:05:28,960
add I is not adding two registers

138
00:05:26,840 --> 00:05:31,479
together it's adding a register to an

139
00:05:28,960 --> 00:05:33,680
immediate now immediates if they're n

140
00:05:31,479 --> 00:05:35,800
bit most commonly 12 bit they're going

141
00:05:33,680 --> 00:05:39,759
to be signed values that are s extended

142
00:05:35,800 --> 00:05:41,680
to xll bits be at 3264 128 before

143
00:05:39,759 --> 00:05:45,759
they're actually used so the possible

144
00:05:41,680 --> 00:05:49,039
12-bit values are from zero up to FFF

145
00:05:45,759 --> 00:05:50,600
and those as signed values would be zero

146
00:05:49,039 --> 00:05:52,880
one and then all the way up to the

147
00:05:50,600 --> 00:05:55,720
maximum positive value which is 7 FF

148
00:05:52,880 --> 00:05:59,479
which is 247 and the negative values are

149
00:05:55,720 --> 00:06:03,199
netive -1 which is FFF all the way to -2

150
00:05:59,479 --> 00:06:05,680
2048 which is 880 so that's our possible

151
00:06:03,199 --> 00:06:09,160
values for 12 bits again those would be

152
00:06:05,680 --> 00:06:11,440
sign extended up so if it was FFF 3 FS

153
00:06:09,160 --> 00:06:14,000
it would be sign extended to 8 FS if it

154
00:06:11,440 --> 00:06:16,280
was a 32-bit value so I want to give you

155
00:06:14,000 --> 00:06:18,280
a little bit of an intuition behind why

156
00:06:16,280 --> 00:06:20,800
things like 12-bit immediates are very

157
00:06:18,280 --> 00:06:22,560
common in Risk 5 assembly instructions

158
00:06:20,800 --> 00:06:24,840
so we start with the premise that the

159
00:06:22,560 --> 00:06:28,120
assembly instructions themselves for RV

160
00:06:24,840 --> 00:06:30,360
32i and 64i are going to be encoded in

161
00:06:28,120 --> 00:06:32,240
32 bits and if it's compressed then it's

162
00:06:30,360 --> 00:06:34,479
going to be 16 bits so that means you

163
00:06:32,240 --> 00:06:36,919
have a very small number of bits in

164
00:06:34,479 --> 00:06:39,080
which to encode actual immediates so

165
00:06:36,919 --> 00:06:40,639
they don't have any notion of like 32

166
00:06:39,080 --> 00:06:42,720
bits for the assembly instruction and

167
00:06:40,639 --> 00:06:44,599
then another 32 bits for the immediate

168
00:06:42,720 --> 00:06:47,199
the immediates are actually encoded

169
00:06:44,599 --> 00:06:49,199
directly into 32 bits for the assembly

170
00:06:47,199 --> 00:06:51,160
instruction so I don't want to get into

171
00:06:49,199 --> 00:06:53,199
all the nitty-gritty details about

172
00:06:51,160 --> 00:06:56,199
assembly instruction and coding just to

173
00:06:53,199 --> 00:06:58,560
say that the bits are broken up in very

174
00:06:56,199 --> 00:07:01,160
specific ways and if we looked at the

175
00:06:58,560 --> 00:07:04,240
actual details we would see that for ad

176
00:07:01,160 --> 00:07:06,520
ey for instance the 12bit immediate that

177
00:07:04,240 --> 00:07:09,840
it can take the 12bit signed immediate

178
00:07:06,520 --> 00:07:11,919
is encoded in bits 20 through 31 so

179
00:07:09,840 --> 00:07:14,440
basically they set aside these 12 bits

180
00:07:11,919 --> 00:07:17,319
and they say these are going to be the

181
00:07:14,440 --> 00:07:19,639
12- bit values zero through FFF that are

182
00:07:17,319 --> 00:07:21,919
available for use in this ADI assembly

183
00:07:19,639 --> 00:07:24,280
instruction so it's very often 12 bits

184
00:07:21,919 --> 00:07:26,639
it's very often smaller than that for

185
00:07:24,280 --> 00:07:28,479
the compressed versions but this just is

186
00:07:26,639 --> 00:07:30,520
to give you a little bit of an intuition

187
00:07:28,479 --> 00:07:32,560
of why you can't just have you know

188
00:07:30,520 --> 00:07:35,280
32-bit immediates it's because

189
00:07:32,560 --> 00:07:37,560
everything is all encoded in the 32-bit

190
00:07:35,280 --> 00:07:39,720
assembly instruction itself so back to

191
00:07:37,560 --> 00:07:41,919
the lies I tell don't trust me I said

192
00:07:39,720 --> 00:07:44,360
noop was your first assembly instruction

193
00:07:41,919 --> 00:07:46,440
but it's actually an Insidious pseudo

194
00:07:44,360 --> 00:07:49,120
instruction so what is the actual

195
00:07:46,440 --> 00:07:51,840
instruction that backs it it is AD I

196
00:07:49,120 --> 00:07:53,199
this is the add immediate to register

197
00:07:51,840 --> 00:07:55,520
this is our first real assembly

198
00:07:53,199 --> 00:07:57,879
instruction ADDI has a destination

199
00:07:55,520 --> 00:08:00,520
register here a source register and a

200
00:07:57,879 --> 00:08:03,000
12bit signed IM immediate so what

201
00:08:00,520 --> 00:08:06,039
operation does this do takes the RS the

202
00:08:03,000 --> 00:08:08,879
source register adds the sign extended

203
00:08:06,039 --> 00:08:11,120
12bit immediate and places the result in

204
00:08:08,879 --> 00:08:12,960
the destination register so for instance

205
00:08:11,120 --> 00:08:17,120
for no up we said that that is actually

206
00:08:12,960 --> 00:08:19,800
encoded by ad i x0 x0 0 and so that

207
00:08:17,120 --> 00:08:22,280
means you're taking the zero register x0

208
00:08:19,800 --> 00:08:23,599
the mnemonic for that is zero register

209
00:08:22,280 --> 00:08:26,199
the zero register which is always

210
00:08:23,599 --> 00:08:28,400
hardcoded to zero read from that get a

211
00:08:26,199 --> 00:08:30,400
zero add zero Store it back into the

212
00:08:28,400 --> 00:08:32,039
zero register but of course no rights to

213
00:08:30,400 --> 00:08:33,800
the zero register ever make any

214
00:08:32,039 --> 00:08:35,800
difference because the zero register is

215
00:08:33,800 --> 00:08:38,599
hard-coded to zero so that's a whole

216
00:08:35,800 --> 00:08:40,560
bunch of no actual real operation

217
00:08:38,599 --> 00:08:43,159
occurring now one miscellaneous thing

218
00:08:40,560 --> 00:08:45,240
that I want to say is that the immediate

219
00:08:43,159 --> 00:08:47,440
12s even though they're they're signed

220
00:08:45,240 --> 00:08:50,200
numbers but they will always be shown in

221
00:08:47,440 --> 00:08:51,560
decimal in GDB as we debug things as of

222
00:08:50,200 --> 00:08:53,839
right now there doesn't appear to be any

223
00:08:51,560 --> 00:08:55,480
way to configure it to display and hex

224
00:08:53,839 --> 00:08:59,839
so for instance here's some other

225
00:08:55,480 --> 00:09:01,720
examples we could have ADI x1 x2 133 37

226
00:08:59,839 --> 00:09:04,800
what is that doing it's doing this is

227
00:09:01,720 --> 00:09:08,800
the source x2 plus so whatever the value

228
00:09:04,800 --> 00:09:10,440
is stored in x2 plus 1337 and store the

229
00:09:08,800 --> 00:09:12,920
result back into the destination

230
00:09:10,440 --> 00:09:16,480
register x1 this is an okay assembly

231
00:09:12,920 --> 00:09:18,320
instruction because 1337 can be encoded

232
00:09:16,480 --> 00:09:22,399
within the 12-bit range we could have an

233
00:09:18,320 --> 00:09:24,839
alternative at I spsp -48 this is also

234
00:09:22,399 --> 00:09:29,240
okay because NE 48 was in within that

235
00:09:24,839 --> 00:09:31,959
12bit range so stack pointer minus 48 or

236
00:09:29,240 --> 00:09:34,000
plus -48 and store the result back into

237
00:09:31,959 --> 00:09:36,040
the stacko this assembly instruction on

238
00:09:34,000 --> 00:09:37,399
the other hand is not an actual valid

239
00:09:36,040 --> 00:09:39,160
assembly instruction you could not

240
00:09:37,399 --> 00:09:41,079
encode this you could not put this into

241
00:09:39,160 --> 00:09:42,920
the assembly stream because you cannot

242
00:09:41,079 --> 00:09:46,079
encode the immediate

243
00:09:42,920 --> 00:09:48,600
31337 into 12 bits it's just beyond the

244
00:09:46,079 --> 00:09:51,000
range the maximum positive value was

245
00:09:48,600 --> 00:09:53,560
2047 so this is all the nitty-gritty

246
00:09:51,000 --> 00:09:56,959
details that the manual has about how

247
00:09:53,560 --> 00:09:59,120
all of the general purpose RV32I base

248
00:09:56,959 --> 00:10:01,040
ISA instructions are but again I said

249
00:09:59,120 --> 00:10:03,079
said I don't want to deal with this

250
00:10:01,040 --> 00:10:05,519
nitty-gritty of instruction coding what

251
00:10:03,079 --> 00:10:07,839
I really want is just a list of the RV

252
00:10:05,519 --> 00:10:09,200
32i base instructions and there we have

253
00:10:07,839 --> 00:10:10,959
it and this is the list that we're going

254
00:10:09,200 --> 00:10:12,959
to be operating off of for the rest of

255
00:10:10,959 --> 00:10:14,200
the class and there is our first

256
00:10:12,959 --> 00:10:16,160
assembly instruction that we've

257
00:10:14,200 --> 00:10:19,600
collected give yourself a pretty little

258
00:10:16,160 --> 00:10:21,360
Star ad I is the first of 40 assembly

259
00:10:19,600 --> 00:10:22,839
instructions from the base instruction

260
00:10:21,360 --> 00:10:24,920
set that we'll be collecting throughout

261
00:10:22,839 --> 00:10:26,519
this class now I have an alternative way

262
00:10:24,920 --> 00:10:28,480
that I'm going to display this where I

263
00:10:26,519 --> 00:10:30,240
keep track of the pseudo instructions

264
00:10:28,480 --> 00:10:33,200
because that previous diagram was only

265
00:10:30,240 --> 00:10:35,680
the actual Baseline instructions so we

266
00:10:33,200 --> 00:10:37,959
have our sneaky pseudo instructions

267
00:10:35,680 --> 00:10:40,160
which start off yellow end up green

268
00:10:37,959 --> 00:10:43,839
because they're actually lizards behind

269
00:10:40,160 --> 00:10:45,519
the scenes so noop is actually an ADDI

270
00:10:43,839 --> 00:10:47,440
behind the scenes it's presenting as a

271
00:10:45,519 --> 00:10:50,279
noop but it's actually just an add

272
00:10:47,440 --> 00:10:51,560
immediate the zero immediate so this is

273
00:10:50,279 --> 00:10:53,760
how we're going to be representing and

274
00:10:51,560 --> 00:10:55,440
building ourselves up a nice diagram

275
00:10:53,760 --> 00:10:57,079
like this this is where we're going this

276
00:10:55,440 --> 00:10:58,040
is all the stuff we're going to learn

277
00:10:57,079 --> 00:11:01,720
this is

278
00:10:58,040 --> 00:11:03,480
madness this is a RISC-V assembly class

279
00:11:01,720 --> 00:11:05,120
but actually this is not Madness because

280
00:11:03,480 --> 00:11:07,360
it is actually much simpler than it

281
00:11:05,120 --> 00:11:09,760
looks all of that stuff is really just

282
00:11:07,360 --> 00:11:12,200
adds and subtracts all of that stuff is

283
00:11:09,760 --> 00:11:14,839
really just loads and stores we've got

284
00:11:12,200 --> 00:11:16,760
some control flow instructions over here

285
00:11:14,839 --> 00:11:18,839
we've got shift instructions over here

286
00:11:16,760 --> 00:11:21,760
it's a big block of shifts we've got

287
00:11:18,839 --> 00:11:23,360
Boolean logic we've got this set if

288
00:11:21,760 --> 00:11:25,120
these things that say if something is

289
00:11:23,360 --> 00:11:27,519
true less than greater than then set a

290
00:11:25,120 --> 00:11:29,639
register to one and then we've got some

291
00:11:27,519 --> 00:11:31,240
miscellaneous so you can see this all

292
00:11:29,639 --> 00:11:33,000
you know yes there's many different

293
00:11:31,240 --> 00:11:34,360
variations of ads and subs and

294
00:11:33,000 --> 00:11:36,120
compressed versions and pseudo

295
00:11:34,360 --> 00:11:38,160
instruction versions but really it's

296
00:11:36,120 --> 00:11:41,120
just add and subtract you understand add

297
00:11:38,160 --> 00:11:42,920
and subtract right so not a big deal and

298
00:11:41,120 --> 00:11:45,760
this is where we are currently in our

299
00:11:42,920 --> 00:11:45,760
journey

