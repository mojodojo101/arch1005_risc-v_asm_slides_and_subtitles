1
00:00:00,120 --> 00:00:05,200
all right now I'm going to assume that

2
00:00:01,719 --> 00:00:08,000
you ran this code from slome user local

3
00:00:05,200 --> 00:00:09,679
variable by o0 no cookie it has to be

4
00:00:08,000 --> 00:00:12,440
exactly that or you won't see the same

5
00:00:09,679 --> 00:00:15,759
thing as me but that's okay if you

6
00:00:12,440 --> 00:00:17,960
stepped through main + 32 this is what

7
00:00:15,759 --> 00:00:19,880
your stack would look like first we have

8
00:00:17,960 --> 00:00:22,279
the character pointer B which I

9
00:00:19,880 --> 00:00:25,800
intentionally gave the address of a the

10
00:00:22,279 --> 00:00:27,880
address of a being 8 E4 right because we

11
00:00:25,800 --> 00:00:30,480
always see those inss get the upper four

12
00:00:27,880 --> 00:00:32,680
bytes so 8 E4 that is exactly the

13
00:00:30,480 --> 00:00:35,920
pointer that we have here so that was so

14
00:00:32,680 --> 00:00:37,600
we could reference B of 0 and B of3

15
00:00:35,920 --> 00:00:40,280
because in so doing I found that I could

16
00:00:37,600 --> 00:00:42,520
get it to generate an lb instruction but

17
00:00:40,280 --> 00:00:44,840
also we see on the stack here that it

18
00:00:42,520 --> 00:00:47,199
has chosen to take the arguments into

19
00:00:44,840 --> 00:00:49,360
main and store them on the stack it put

20
00:00:47,199 --> 00:00:50,760
the argc which was one assuming that you

21
00:00:49,360 --> 00:00:54,680
have no additional command line

22
00:00:50,760 --> 00:00:57,800
arguments argc is one and the Char star

23
00:00:54,680 --> 00:01:00,600
start argv is right here so the pointer

24
00:00:57,800 --> 00:01:03,280
to a pointer is right right here so if

25
00:01:00,600 --> 00:01:05,239
we go up here that turns out to be

26
00:01:03,280 --> 00:01:06,840
pointing at the same place as our frame

27
00:01:05,239 --> 00:01:09,119
pointer actually so that is where our

28
00:01:06,840 --> 00:01:11,040
frame pointer pointed on entry into main

29
00:01:09,119 --> 00:01:15,200
we know because our saved frame pointer

30
00:01:11,040 --> 00:01:17,799
is right here so if that is a copy of

31
00:01:15,200 --> 00:01:20,799
argv then that means what we're looking

32
00:01:17,799 --> 00:01:22,680
at right now should be arv of zero the

33
00:01:20,799 --> 00:01:25,280
zero withth element of you know

34
00:01:22,680 --> 00:01:26,880
sometimes you can think of arv is a Char

35
00:01:25,280 --> 00:01:29,439
star star pointer I type that because

36
00:01:26,880 --> 00:01:31,920
I'm literally too lazy to type the array

37
00:01:29,439 --> 00:01:33,640
symbol one less character but in

38
00:01:31,920 --> 00:01:36,280
practice when you actually dreference a

39
00:01:33,640 --> 00:01:38,920
value you use things like rv0 arv1 and

40
00:01:36,280 --> 00:01:41,840
so forth so anyways if this is argv of

41
00:01:38,920 --> 00:01:44,439
zero the zeroth entry then it is a

42
00:01:41,840 --> 00:01:46,799
pointer to a character array so if we go

43
00:01:44,439 --> 00:01:49,759
and we dreference that we will see that

44
00:01:46,799 --> 00:01:51,920
the argv of Zer is appropriately by

45
00:01:49,759 --> 00:01:53,880
convention the name of the executable

46
00:01:51,920 --> 00:01:55,840
itself the full path where the

47
00:01:53,880 --> 00:01:57,399
executable was launched from and we can

48
00:01:55,840 --> 00:02:00,719
of course see this in the debugger as

49
00:01:57,399 --> 00:02:05,600
well if we examined the

50
00:02:00,719 --> 00:02:08,000
argv uh address as a uh two giant words

51
00:02:05,600 --> 00:02:09,759
two 64-bit values we would have seen the

52
00:02:08,000 --> 00:02:12,160
first arv of Zer and then we would have

53
00:02:09,759 --> 00:02:15,440
seen a null Terminator for this

54
00:02:12,160 --> 00:02:18,440
array going to the arv of Z examining it

55
00:02:15,440 --> 00:02:21,239
as a string then we see this full string

56
00:02:18,440 --> 00:02:23,640
here so what are our takeaways from this

57
00:02:21,239 --> 00:02:25,800
stack well first the frame pointer has

58
00:02:23,640 --> 00:02:28,040
been pointing at RGV upon entry and we

59
00:02:25,800 --> 00:02:30,080
didn't even realize it this whole time

60
00:02:28,040 --> 00:02:32,599
but also the compiler for what ever

61
00:02:30,080 --> 00:02:36,080
reason seems to be storing copies of the

62
00:02:32,599 --> 00:02:37,720
arguments onto the stack and these are

63
00:02:36,080 --> 00:02:39,920
literally not even used in this code

64
00:02:37,720 --> 00:02:42,200
this code does not touch argc this code

65
00:02:39,920 --> 00:02:43,840
does not touch argv but yet for some

66
00:02:42,200 --> 00:02:45,879
reason the compiler decided to put it

67
00:02:43,840 --> 00:02:47,959
there and the other takeaways from this

68
00:02:45,879 --> 00:02:50,840
is that taking that local variable a

69
00:02:47,959 --> 00:02:53,239
taking its address and casting it to a

70
00:02:50,840 --> 00:02:55,080
character pointer address uh character

71
00:02:53,239 --> 00:02:57,760
pointer and then dereferencing that

72
00:02:55,080 --> 00:03:00,480
pointer by accessing b of0 b of3

73
00:02:57,760 --> 00:03:02,040
individual bytes out of this integer our

74
00:03:00,480 --> 00:03:04,599
salad bad integer I haven't made

75
00:03:02,040 --> 00:03:07,000
reference to the salad bad joke yet the

76
00:03:04,599 --> 00:03:09,239
salad bad integer if we grab the bite

77
00:03:07,000 --> 00:03:12,560
zero and the bite three that will lead

78
00:03:09,239 --> 00:03:14,760
to these load bite assembly instructions

79
00:03:12,560 --> 00:03:18,000
additionally because we're returning an

80
00:03:14,760 --> 00:03:20,920
INT the RV64I code which we're running

81
00:03:18,000 --> 00:03:23,799
is going to use the subw instruction to

82
00:03:20,920 --> 00:03:26,840
do integer sized subtractions when it

83
00:03:23,799 --> 00:03:28,480
takes these bytes and turns them into

84
00:03:26,840 --> 00:03:30,920
integers and does the subtraction on

85
00:03:28,480 --> 00:03:33,239
that so some new assembly instructions

86
00:03:30,920 --> 00:03:35,640
that much sought after lb which we were

87
00:03:33,239 --> 00:03:39,040
missing amongst the loads in the stores

88
00:03:35,640 --> 00:03:41,480
from before and also we picked up a subw

89
00:03:39,040 --> 00:03:43,920
like the ad W that we had already seen

90
00:03:41,480 --> 00:03:47,439
it's just a word siiz register

91
00:03:43,920 --> 00:03:50,799
subtraction used by 64bit code and on

92
00:03:47,439 --> 00:03:54,239
our diagram that would be subw there and

93
00:03:50,799 --> 00:03:54,239
load bite there

