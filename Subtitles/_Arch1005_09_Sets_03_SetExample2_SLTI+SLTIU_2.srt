1
00:00:00,280 --> 00:00:05,000
so what are the takeaways from the set

2
00:00:02,200 --> 00:00:06,960
example 2 and set example 3 well if you

3
00:00:05,000 --> 00:00:08,559
let the compiler know that there is a

4
00:00:06,960 --> 00:00:10,200
particular immediate not something

5
00:00:08,559 --> 00:00:13,040
that's dynamically figured out at

6
00:00:10,200 --> 00:00:15,160
runtime and if you do something that's

7
00:00:13,040 --> 00:00:17,600
going to cause some sort of set less

8
00:00:15,160 --> 00:00:19,080
than or a salt assembly instruction then

9
00:00:17,600 --> 00:00:21,600
the use of it immediate will just

10
00:00:19,080 --> 00:00:23,400
generate the salty or salty U depending

11
00:00:21,600 --> 00:00:26,199
on the sign whether it's signed or

12
00:00:23,400 --> 00:00:27,840
unsigned comparison is appropriate and

13
00:00:26,199 --> 00:00:30,560
basically it'll generate those instead

14
00:00:27,840 --> 00:00:33,000
of a salt or salt U when it doesn't know

15
00:00:30,560 --> 00:00:35,000
what the value is going to be but really

16
00:00:33,000 --> 00:00:37,440
we just want to fill in our list we've

17
00:00:35,000 --> 00:00:40,079
got salty down here and then boom that

18
00:00:37,440 --> 00:00:43,280
column is completed and we've got salty

19
00:00:40,079 --> 00:00:45,320
U up here and that column is almost

20
00:00:43,280 --> 00:00:47,239
completed we just seem to have three

21
00:00:45,320 --> 00:00:51,440
stragglers that we'll cover in the next

22
00:00:47,239 --> 00:00:53,879
section so salty and salty U and the

23
00:00:51,440 --> 00:00:55,800
reason again we say they're salt is

24
00:00:53,879 --> 00:00:58,359
because if you can pronounce it with ass

25
00:00:55,800 --> 00:01:00,320
salt it must be a set if less than and

26
00:00:58,359 --> 00:01:03,359
if you can't pronounce it with assault

27
00:01:00,320 --> 00:01:05,000
like silly sirly these things you know

28
00:01:03,359 --> 00:01:06,640
if you're just skimming the code if you

29
00:01:05,000 --> 00:01:08,640
try to read these if they don't read as

30
00:01:06,640 --> 00:01:11,560
a salt then you know you're looking at a

31
00:01:08,640 --> 00:01:13,320
shift otherwise it's a set if less than

32
00:01:11,560 --> 00:01:15,119
there's no other real places that you're

33
00:01:13,320 --> 00:01:17,200
going to encounter those s's there's of

34
00:01:15,119 --> 00:01:18,840
course the subtract but should be pretty

35
00:01:17,200 --> 00:01:21,680
obvious that's a subtract and then

36
00:01:18,840 --> 00:01:24,720
there's the permutations on set bite

37
00:01:21,680 --> 00:01:26,720
half word word or double word but those

38
00:01:24,720 --> 00:01:29,560
are s uh two-letter

39
00:01:26,720 --> 00:01:31,759
instructions so should be easier now to

40
00:01:29,560 --> 00:01:34,520
figure F out when you're seeing a set if

41
00:01:31,759 --> 00:01:36,240
less than a salt compared to the other

42
00:01:34,520 --> 00:01:39,600
variant instructions that we can see

43
00:01:36,240 --> 00:01:39,600
when we're skimming code

