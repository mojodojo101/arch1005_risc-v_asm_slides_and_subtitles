1
00:00:00,120 --> 00:00:05,520
hunting for more Boolean assembly

2
00:00:02,240 --> 00:00:07,799
instructions we have Boolean because 2

3
00:00:05,520 --> 00:00:09,599
and here we have just an or operation

4
00:00:07,799 --> 00:00:12,120
now the big difference about this is

5
00:00:09,599 --> 00:00:14,160
that we are using optimization level too

6
00:00:12,120 --> 00:00:16,680
so we're playing our typical trick of

7
00:00:14,160 --> 00:00:18,840
using some arguments coming in from the

8
00:00:16,680 --> 00:00:20,920
command line and making it so that

9
00:00:18,840 --> 00:00:22,359
essentially the compiler can't optimize

10
00:00:20,920 --> 00:00:24,359
down everything because they don't know

11
00:00:22,359 --> 00:00:26,279
what the I value is going to be so they

12
00:00:24,359 --> 00:00:28,039
need to generate assembly that can take

13
00:00:26,279 --> 00:00:30,560
any sort of I value coming off the

14
00:00:28,039 --> 00:00:33,360
command line or it with J which is a

15
00:00:30,560 --> 00:00:35,600
hardcoded constant 22 and get the result

16
00:00:33,360 --> 00:00:37,760
K before returning it here's the

17
00:00:35,600 --> 00:00:42,320
resulting assembly and we have some new

18
00:00:37,760 --> 00:00:44,480
assembly instructions or I and Zex B so

19
00:00:42,320 --> 00:00:47,280
what is or I well it is just a bitwise

20
00:00:44,480 --> 00:00:50,320
or with an immediate just like the xori

21
00:00:47,280 --> 00:00:54,039
that we saw a moment ago so or two

22
00:00:50,320 --> 00:00:56,359
operand source and immediate 12 take the

23
00:00:54,039 --> 00:00:57,760
source bitwise or it with the immediate

24
00:00:56,359 --> 00:01:00,000
which is going to be aign extended

25
00:00:57,760 --> 00:01:02,359
immediate and store the result into the

26
00:01:00,000 --> 00:01:04,640
destination so nothing super special

27
00:01:02,359 --> 00:01:08,159
about this just have a new operation

28
00:01:04,640 --> 00:01:11,680
here we've got or a5 A52 2001 A Space

29
00:01:08,159 --> 00:01:16,200
Odyssey and so if you did that then this

30
00:01:11,680 --> 00:01:18,560
2001 is 71 so or one in one you get one

31
00:01:16,200 --> 00:01:20,960
zero and zero you get zero one and zero

32
00:01:18,560 --> 00:01:23,759
you get one and so forth so then that

33
00:01:20,960 --> 00:01:26,560
result is staba daa

34
00:01:23,759 --> 00:01:29,079
D I guess I didn't go for any particular

35
00:01:26,560 --> 00:01:33,240
value there I just wanted the 2001 joke

36
00:01:29,079 --> 00:01:35,920
then we have X to be zero extend bite in

37
00:01:33,240 --> 00:01:37,680
register to double word register so you

38
00:01:35,920 --> 00:01:40,680
take the source register you grab the

39
00:01:37,680 --> 00:01:42,520
bottom bits and you zero extend it to 64

40
00:01:40,680 --> 00:01:44,719
bits or another way of thinking of it is

41
00:01:42,520 --> 00:01:46,960
you're truncating it and then just

42
00:01:44,719 --> 00:01:49,040
throwing all the zeros at the top bits

43
00:01:46,960 --> 00:01:51,439
other than the bottom bits eight other

44
00:01:49,040 --> 00:01:54,640
than the bottom eight bits that is the

45
00:01:51,439 --> 00:01:57,280
thing is that if you go and try to find

46
00:01:54,640 --> 00:01:59,079
this Zex B in the unprivileged

47
00:01:57,280 --> 00:02:02,079
specification that we've been using thus

48
00:01:59,079 --> 00:02:04,880
far yeah ain't going to find it and you

49
00:02:02,079 --> 00:02:07,640
might find this this B standard for bit

50
00:02:04,880 --> 00:02:09,759
manipulation extension and then you

51
00:02:07,640 --> 00:02:12,720
might go looking for the RISC-V bit

52
00:02:09,759 --> 00:02:16,040
manipulation ISA extension and find the

53
00:02:12,720 --> 00:02:19,040
not yet ratified version 1.0 that's in

54
00:02:16,040 --> 00:02:23,200
public review from 2021 and there you

55
00:02:19,040 --> 00:02:25,640
will find Zex H but you won't find Zex B

56
00:02:23,200 --> 00:02:27,800
so what you need to do is travel back in

57
00:02:25,640 --> 00:02:31,840
time to version

58
00:02:27,800 --> 00:02:36,040
.93 and there you will find zxb Zex H

59
00:02:31,840 --> 00:02:38,000
Zex W and sex W so yes uh there's an

60
00:02:36,040 --> 00:02:39,840
older version I'm not exactly sure why

61
00:02:38,000 --> 00:02:41,519
it's not in the newer version of the

62
00:02:39,840 --> 00:02:43,200
spec maybe they're throwing it away

63
00:02:41,519 --> 00:02:46,080
maybe they're not I don't know I just

64
00:02:43,200 --> 00:02:48,120
know there's also this link to the benu

65
00:02:46,080 --> 00:02:50,360
tills uh mailing list where they said

66
00:02:48,120 --> 00:02:52,680
back in 2020 that they were adding this

67
00:02:50,360 --> 00:02:54,760
support either way it is not yet a

68
00:02:52,680 --> 00:02:56,680
ratified thing so I said in this class

69
00:02:54,760 --> 00:02:58,840
that we're only going to cover this base

70
00:02:56,680 --> 00:03:01,440
and these extensions but then all of a

71
00:02:58,840 --> 00:03:03,360
sudden I slap down down a zbb draft

72
00:03:01,440 --> 00:03:05,280
extension for bit

73
00:03:03,360 --> 00:03:06,959
manipulation except we're not actually

74
00:03:05,280 --> 00:03:09,360
really going to cover that that's not

75
00:03:06,959 --> 00:03:12,080
interesting we want to use the chaos

76
00:03:09,360 --> 00:03:16,959
magic to get rid of the Zex and so we

77
00:03:12,080 --> 00:03:20,360
shall boom wiped away and left with Andy

78
00:03:16,959 --> 00:03:22,840
nice friendly Andy Orie and Andy so the

79
00:03:20,360 --> 00:03:25,239
Zex is one of those terrible pseudo

80
00:03:22,840 --> 00:03:28,040
instructions that haunt our nightmares

81
00:03:25,239 --> 00:03:29,920
and it's really just an and ey register

82
00:03:28,040 --> 00:03:33,400
destination register source and a

83
00:03:29,920 --> 00:03:35,959
hardcoded 255 why 255 we'll see in a

84
00:03:33,400 --> 00:03:38,640
second it's implicitly going to zero

85
00:03:35,959 --> 00:03:41,239
extend this to 64 bits how well let's

86
00:03:38,640 --> 00:03:44,840
see an example of anding with an

87
00:03:41,239 --> 00:03:48,080
immediate 255 so Andy just like other

88
00:03:44,840 --> 00:03:49,959
things like Ori and zor and a quick

89
00:03:48,080 --> 00:03:52,640
interlude let me go ahead and introduce

90
00:03:49,959 --> 00:03:54,120
you to my Elite Photoshop skills

91
00:03:52,640 --> 00:03:55,959
actually made an Apple preview not

92
00:03:54,120 --> 00:03:58,720
Photoshop yes I demand you look at my

93
00:03:55,959 --> 00:04:01,400
awesome Andy boot okay moving on Andy

94
00:03:58,720 --> 00:04:02,959
take the immediate sign extended bitwise

95
00:04:01,400 --> 00:04:06,560
and with source place it into

96
00:04:02,959 --> 00:04:10,599
destination so if we took some a5 value

97
00:04:06,560 --> 00:04:15,160
like staba dudes and we ended with 255

98
00:04:10,599 --> 00:04:18,160
which is a 12bit immediate so it is 0

99
00:04:15,160 --> 00:04:20,720
FF then we are essentially going to have

100
00:04:18,160 --> 00:04:24,600
only the bottom two hex nibbles

101
00:04:20,720 --> 00:04:27,400
preserved so only the D and the S so one

102
00:04:24,600 --> 00:04:31,120
and one that's one Etc because all of

103
00:04:27,400 --> 00:04:33,600
these up upper zero bits are all going

104
00:04:31,120 --> 00:04:35,919
to result in zeros when they're run

105
00:04:33,600 --> 00:04:39,639
through the and operation so ending with

106
00:04:35,919 --> 00:04:41,440
255 ending with hex FF because it's a 12

107
00:04:39,639 --> 00:04:43,400
bit immediate that's not going to S

108
00:04:41,440 --> 00:04:46,360
extend the upper bits it's just going to

109
00:04:43,400 --> 00:04:49,039
be FF and so you essentially truncate

110
00:04:46,360 --> 00:04:51,240
down to a single bite all the upper bits

111
00:04:49,039 --> 00:04:53,039
are zero and you can call that zero

112
00:04:51,240 --> 00:04:55,039
extension if you want I tend to think of

113
00:04:53,039 --> 00:04:58,560
it like just truncation of a large value

114
00:04:55,039 --> 00:05:00,960
to a small value but yeah it's the and I

115
00:04:58,560 --> 00:05:03,199
behind the scenes not a zero extend not

116
00:05:00,960 --> 00:05:06,520
a Zex there's no such thing as Zex

117
00:05:03,199 --> 00:05:08,800
there's only and I so now you can go

118
00:05:06,520 --> 00:05:10,880
ahead and go look at the assembly step

119
00:05:08,800 --> 00:05:13,479
through check your understanding draw a

120
00:05:10,880 --> 00:05:17,400
stack diagram if you're so inclined and

121
00:05:13,479 --> 00:05:17,400
then come back and let's continue

