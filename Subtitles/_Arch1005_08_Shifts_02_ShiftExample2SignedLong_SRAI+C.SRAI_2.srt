1
00:00:00,199 --> 00:00:04,120
well the presence of Scarlet Witch means

2
00:00:02,120 --> 00:00:07,319
there must be something to see and what

3
00:00:04,120 --> 00:00:09,320
there is to see is a compressed sray so

4
00:00:07,319 --> 00:00:11,840
the takeaway from this code is that

5
00:00:09,320 --> 00:00:13,599
logical shift right shift right logical

6
00:00:11,840 --> 00:00:16,800
by immediate is associated with the

7
00:00:13,599 --> 00:00:19,160
right shift operator on unsigned values

8
00:00:16,800 --> 00:00:21,560
so that's what we F saw before Surly was

9
00:00:19,160 --> 00:00:24,480
here when we were using unsigned values

10
00:00:21,560 --> 00:00:27,160
but if we're using signed values then an

11
00:00:24,480 --> 00:00:29,000
arithmetic shift WR or sray is going to

12
00:00:27,160 --> 00:00:31,080
be used when you have the right shift

13
00:00:29,000 --> 00:00:33,719
operator on the other hand we can infer

14
00:00:31,080 --> 00:00:35,920
that the left shift doesn't care so

15
00:00:33,719 --> 00:00:37,480
we're using silly regardless of whether

16
00:00:35,920 --> 00:00:40,160
it's signed or

17
00:00:37,480 --> 00:00:43,239
unsigned so here's our new instruction

18
00:00:40,160 --> 00:00:45,480
sray and sray so these are the only

19
00:00:43,239 --> 00:00:47,800
three instructions that you'll find in

20
00:00:45,480 --> 00:00:50,640
the RV64I that have the exact same

21
00:00:47,800 --> 00:00:52,120
names as you find over in the RV32I

22
00:00:50,640 --> 00:00:54,199
because everything else you're just

23
00:00:52,120 --> 00:00:56,359
using the 32-bit version and it's

24
00:00:54,199 --> 00:00:58,160
behaving the correct way based on either

25
00:00:56,359 --> 00:01:01,039
you know the size that's being specified

26
00:00:58,160 --> 00:01:03,519
for loading and storing or based on just

27
00:01:01,039 --> 00:01:05,960
using x-rs or things like offsets that

28
00:01:03,519 --> 00:01:08,960
don't really care on our diagram it

29
00:01:05,960 --> 00:01:12,960
looks like this sray two instructions

30
00:01:08,960 --> 00:01:12,960
plus the compressed sray

