1
00:00:00,640 --> 00:00:06,520
and if you stepped through main plus 104

2
00:00:03,679 --> 00:00:10,040
the last store you should have seen this

3
00:00:06,520 --> 00:00:12,719
so we have a again set to A's down here

4
00:00:10,040 --> 00:00:15,799
at the lowest address B and the key

5
00:00:12,719 --> 00:00:17,600
thing is we see B CD this has not been

6
00:00:15,799 --> 00:00:21,600
reordered seems to still be in the same

7
00:00:17,600 --> 00:00:24,080
order indeed a2 e and f everything seems

8
00:00:21,600 --> 00:00:26,480
to be in exactly the same order so our

9
00:00:24,080 --> 00:00:29,119
takeaway from this is that all struct

10
00:00:26,480 --> 00:00:31,000
members order is and actually must be

11
00:00:29,119 --> 00:00:33,360
preserved On the Stack the reason I'm

12
00:00:31,000 --> 00:00:35,480
now confident enough to say must be not

13
00:00:33,360 --> 00:00:37,480
just because I've seen two examples but

14
00:00:35,480 --> 00:00:40,200
because we think of things like the use

15
00:00:37,480 --> 00:00:42,440
of the mem copy if you had a local

16
00:00:40,200 --> 00:00:45,520
variable struct and you wanted to do a

17
00:00:42,440 --> 00:00:48,520
mem copy from foo to bar and you set a

18
00:00:45,520 --> 00:00:50,840
size of my struct type if the compiler

19
00:00:48,520 --> 00:00:53,320
did not respect the ordering and

20
00:00:50,840 --> 00:00:54,800
guarantee the Ordering of structs well

21
00:00:53,320 --> 00:00:56,559
then something like that wouldn't work

22
00:00:54,800 --> 00:00:58,280
but you perhaps know from your C

23
00:00:56,559 --> 00:01:01,519
experience that something like that

24
00:00:58,280 --> 00:01:03,320
always does work so the compiler shall

25
00:01:01,519 --> 00:01:05,560
always keep things exactly in the same

26
00:01:03,320 --> 00:01:07,920
order so local variables don't have to

27
00:01:05,560 --> 00:01:10,520
be into any particular order but strs

28
00:01:07,920 --> 00:01:12,600
definitely do so take a struct flip it

29
00:01:10,520 --> 00:01:14,840
upside down for our diagram and that's

30
00:01:12,600 --> 00:01:18,880
what you can expect to see okay picked

31
00:01:14,840 --> 00:01:23,159
up some more instructions lbu load bite

32
00:01:18,880 --> 00:01:25,079
unsigned lhu load halfword unsigned 16

33
00:01:23,159 --> 00:01:27,680
bits treated as unsigned so use zero

34
00:01:25,079 --> 00:01:30,960
extend to don't sign extend and store

35
00:01:27,680 --> 00:01:32,600
bite so we note that there is a load

36
00:01:30,960 --> 00:01:35,799
bite but we didn't see that for whatever

37
00:01:32,600 --> 00:01:38,439
reason GCC really seems to like lbu the

38
00:01:35,799 --> 00:01:40,439
unsigned version rather than the signed

39
00:01:38,439 --> 00:01:42,200
version but we can perhaps play some

40
00:01:40,439 --> 00:01:45,119
games later on to force it to generate

41
00:01:42,200 --> 00:01:49,560
an lb for us until then let's pick up

42
00:01:45,119 --> 00:01:52,759
our new instructions lhu SB and lbu our

43
00:01:49,560 --> 00:01:54,759
conspicuous absence of lb right here and

44
00:01:52,759 --> 00:01:56,280
something seems like it goes right there

45
00:01:54,759 --> 00:02:00,159
we don't know what yet but we're going

46
00:01:56,280 --> 00:02:00,159
to keep trucking and find out

