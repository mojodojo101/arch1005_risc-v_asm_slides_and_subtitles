1
00:00:00,359 --> 00:00:05,799
okay Kylo wants more RTFM and who am I

2
00:00:03,040 --> 00:00:07,600
to refuse so one of the first things

3
00:00:05,799 --> 00:00:09,840
that we saw in the manual is that we

4
00:00:07,600 --> 00:00:11,599
immediately run into these instruction

5
00:00:09,840 --> 00:00:13,519
encodings and like I said I don't really

6
00:00:11,599 --> 00:00:16,119
know why they would put that upfront

7
00:00:13,519 --> 00:00:18,480
ahead of the things like uh the actual

8
00:00:16,119 --> 00:00:20,519
assembly instructions themselves that

9
00:00:18,480 --> 00:00:22,560
doesn't seem like necessary information

10
00:00:20,519 --> 00:00:24,519
at least for us software and firmware

11
00:00:22,560 --> 00:00:26,240
type people but maybe hardware

12
00:00:24,519 --> 00:00:28,400
Architects want to know that I'm not

13
00:00:26,240 --> 00:00:30,519
sure so they show us stuff like this the

14
00:00:28,400 --> 00:00:33,000
r type encoding the I type encoding the

15
00:00:30,519 --> 00:00:35,040
S Type encoding but they also don't

16
00:00:33,000 --> 00:00:37,360
actually say anywhere as far as I can

17
00:00:35,040 --> 00:00:40,120
tell in the entire manual where those

18
00:00:37,360 --> 00:00:41,559
names come from you can infer it but why

19
00:00:40,120 --> 00:00:43,440
should you have to infer it that's what

20
00:00:41,559 --> 00:00:46,440
the documentation is for it's supposed

21
00:00:43,440 --> 00:00:49,160
to tell you what it means so once again

22
00:00:46,440 --> 00:00:51,120
we play the inference game we go and we

23
00:00:49,160 --> 00:00:54,359
find a different location in the manual

24
00:00:51,120 --> 00:00:56,559
talking about the compressed 16bit RVC

25
00:00:54,359 --> 00:00:59,559
instruction formats and within that

26
00:00:56,559 --> 00:01:01,840
context we see things like CR meaning

27
00:00:59,559 --> 00:01:05,239
the register encoding compressed

28
00:01:01,840 --> 00:01:07,920
register encoding CI for immediate CL

29
00:01:05,239 --> 00:01:10,640
for load CS for store ca for arithmetic

30
00:01:07,920 --> 00:01:13,119
branch jump and so we can take these

31
00:01:10,640 --> 00:01:16,320
names and inferences of these letters

32
00:01:13,119 --> 00:01:18,960
and apply them back to the r i SB U and

33
00:01:16,320 --> 00:01:21,680
J type en codings so we would infer that

34
00:01:18,960 --> 00:01:23,960
the r type has to do with register so

35
00:01:21,680 --> 00:01:25,759
register type en coding and indeed if we

36
00:01:23,960 --> 00:01:28,560
look across all of these possible

37
00:01:25,759 --> 00:01:31,920
encoding types we see that the r type is

38
00:01:28,560 --> 00:01:34,479
the only one that has three fullsize

39
00:01:31,920 --> 00:01:37,360
registers specified we have a

40
00:01:34,479 --> 00:01:40,000
destination we have a source and we have

41
00:01:37,360 --> 00:01:43,079
a source two so three possible register

42
00:01:40,000 --> 00:01:45,200
values in contrast we have the immediate

43
00:01:43,079 --> 00:01:47,399
type encoding where you've got your

44
00:01:45,200 --> 00:01:49,000
12bit immediate and I said earlier in

45
00:01:47,399 --> 00:01:52,439
the class that's one of the most common

46
00:01:49,000 --> 00:01:55,200
sizes 12 bit immediates 12bit immediate

47
00:01:52,439 --> 00:01:56,880
source one and destination and so we

48
00:01:55,200 --> 00:02:00,000
could see this being used for instance

49
00:01:56,880 --> 00:02:02,000
for the ADDI or the exor I because

50
00:02:00,000 --> 00:02:04,280
you'll have a 12-bit immediate a source

51
00:02:02,000 --> 00:02:06,600
for a register that is being exord or

52
00:02:04,280 --> 00:02:09,560
added to the immediate and then a

53
00:02:06,600 --> 00:02:12,680
destination where it's stored the S type

54
00:02:09,560 --> 00:02:16,000
for store instructions like store word

55
00:02:12,680 --> 00:02:18,120
store halfword store bite the B type for

56
00:02:16,000 --> 00:02:20,599
branched instructions like branch if

57
00:02:18,120 --> 00:02:24,760
equal branch if not equal branch if less

58
00:02:20,599 --> 00:02:28,040
than the J for jump type so this can be

59
00:02:24,760 --> 00:02:30,400
used for the jaw instruction and then

60
00:02:28,040 --> 00:02:33,280
there's one more that we couldn't in for

61
00:02:30,400 --> 00:02:35,440
the uh what the name stood for based on

62
00:02:33,280 --> 00:02:37,120
the compressed versions but based on

63
00:02:35,440 --> 00:02:39,519
where we see this used elsewhere in the

64
00:02:37,120 --> 00:02:43,239
code we can infer that the U type is for

65
00:02:39,519 --> 00:02:47,040
upper so upper meaning like Lui LUI

66
00:02:43,239 --> 00:02:49,680
for load upper immediate or a wepc for

67
00:02:47,040 --> 00:02:51,360
add upper immediate 2 PC those

68
00:02:49,680 --> 00:02:54,120
situations where you're going to have 20

69
00:02:51,360 --> 00:02:56,200
bits specified as a upper 20 bits of an

70
00:02:54,120 --> 00:02:58,480
immediate but it's also worth saying for

71
00:02:56,200 --> 00:03:02,120
instance that this U type is used for

72
00:02:58,480 --> 00:03:05,000
jwar so jump is used for jaw but U type

73
00:03:02,120 --> 00:03:07,080
is used for jaer again not well not

74
00:03:05,000 --> 00:03:10,440
again I'm saying this down here so the

75
00:03:07,080 --> 00:03:12,879
upper for load immediate and wepc but I

76
00:03:10,440 --> 00:03:14,840
don't think it's actually necessary to

77
00:03:12,879 --> 00:03:17,159
memorize you know oh this instruction is

78
00:03:14,840 --> 00:03:18,640
U type that instruction is B type now

79
00:03:17,159 --> 00:03:21,519
it's not important to memorize that but

80
00:03:18,640 --> 00:03:24,040
it is useful to get a sense of there's a

81
00:03:21,519 --> 00:03:26,239
very limited number of instruction

82
00:03:24,040 --> 00:03:27,959
encodings and so this makes it easier

83
00:03:26,239 --> 00:03:31,680
for the hardware to like pluck apart you

84
00:03:27,959 --> 00:03:34,400
know here's some uh you know 32 bits and

85
00:03:31,680 --> 00:03:36,360
how am I supposed to interpret this and

86
00:03:34,400 --> 00:03:38,200
so sometimes for certain up codes it

87
00:03:36,360 --> 00:03:40,120
could say okay well I know now based on

88
00:03:38,200 --> 00:03:42,640
this op code that it's and maybe based

89
00:03:40,120 --> 00:03:44,239
on this op code and this minor op code

90
00:03:42,640 --> 00:03:46,000
as we'll talk about in a little bit I

91
00:03:44,239 --> 00:03:47,480
know now okay well these bits are going

92
00:03:46,000 --> 00:03:49,080
to be a destination register and these

93
00:03:47,480 --> 00:03:50,439
bits are going to be a source or these

94
00:03:49,080 --> 00:03:53,000
bits are destination and this isn't

95
00:03:50,439 --> 00:03:54,599
immediate so instruction decoding can be

96
00:03:53,000 --> 00:03:57,040
made more simple by the fact that

97
00:03:54,599 --> 00:04:00,680
there's this very limited number of

98
00:03:57,040 --> 00:04:00,680
different instruction encodings

