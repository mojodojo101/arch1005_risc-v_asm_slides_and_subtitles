1
00:00:00,040 --> 00:00:04,520
all right and if you stepped through

2
00:00:01,680 --> 00:00:06,600
func 3 +4 which again is not the last

3
00:00:04,520 --> 00:00:08,440
store but rather the last movement of

4
00:00:06,600 --> 00:00:10,639
the frame pointer this is what you

5
00:00:08,440 --> 00:00:12,840
should have seen and indeed you start to

6
00:00:10,639 --> 00:00:14,599
see what looks like a pattern here and

7
00:00:12,840 --> 00:00:17,160
it appears to be the extension of the

8
00:00:14,599 --> 00:00:19,640
pattern we learned before so we've got

9
00:00:17,160 --> 00:00:21,480
the main which stores off the way to

10
00:00:19,640 --> 00:00:23,240
return back out of main and the frame

11
00:00:21,480 --> 00:00:25,640
pointer as it existed at the time that

12
00:00:23,240 --> 00:00:28,119
you got into main we've got main's frame

13
00:00:25,640 --> 00:00:30,880
we've got func's frame we've got func

14
00:00:28,119 --> 00:00:32,320
2's frame and we've got func 3's frame

15
00:00:30,880 --> 00:00:35,320
and the key difference again being that

16
00:00:32,320 --> 00:00:37,160
func 3 is a leaf function it calls no

17
00:00:35,320 --> 00:00:38,559
other functions and therefore it doesn't

18
00:00:37,160 --> 00:00:40,360
look like it needs to save off the

19
00:00:38,559 --> 00:00:42,680
return address it can just keep the

20
00:00:40,360 --> 00:00:45,719
return address in the ra and ultimately

21
00:00:42,680 --> 00:00:47,760
return back so our takeaways from this

22
00:00:45,719 --> 00:00:49,760
is that the pattern holds the return

23
00:00:47,760 --> 00:00:52,399
address is not saved to the stack by Lea

24
00:00:49,760 --> 00:00:55,199
functions but it is for interior

25
00:00:52,399 --> 00:00:57,239
functions now I want to actually kind of

26
00:00:55,199 --> 00:00:59,680
show you what's really going on with the

27
00:00:57,239 --> 00:01:02,320
frame pointer usage so let's start here

28
00:00:59,680 --> 00:01:04,720
where here somewhere inside of func 3

29
00:01:02,320 --> 00:01:07,520
the stack frames have grown to the point

30
00:01:04,720 --> 00:01:09,759
where func free has a frame frame

31
00:01:07,520 --> 00:01:12,640
pointer points here and even back in the

32
00:01:09,759 --> 00:01:14,680
call one function no args we saw that it

33
00:01:12,640 --> 00:01:17,360
looked like the convention was frame

34
00:01:14,680 --> 00:01:20,079
pointer minus 8 to get the saved one for

35
00:01:17,360 --> 00:01:21,400
leaf node and frame pointer minus 10 to

36
00:01:20,079 --> 00:01:24,119
get the saved one for other ones so

37
00:01:21,400 --> 00:01:27,720
let's see that concretely frame pointer

38
00:01:24,119 --> 00:01:29,320
minus 8 gives us a saved frame pointer

39
00:01:27,720 --> 00:01:33,200
this is a pointer so where does it point

40
00:01:29,320 --> 00:01:36,240
it points to 8 F0 so this points up to

41
00:01:33,200 --> 00:01:38,119
here 8 F0 that's the previous location

42
00:01:36,240 --> 00:01:42,560
of the frame pointer before it was moved

43
00:01:38,119 --> 00:01:44,960
down and this if we do a minus hex1

44
00:01:42,560 --> 00:01:48,119
because it's an interior node not a leaf

45
00:01:44,960 --> 00:01:50,240
node minus xx10 gets us down to here

46
00:01:48,119 --> 00:01:53,960
another saved frame pointer where does

47
00:01:50,240 --> 00:01:56,840
that point points up at 900 then let's

48
00:01:53,960 --> 00:01:59,960
do another minus hex1 that brings us

49
00:01:56,840 --> 00:02:02,640
down to 8 F0 we find another saved frame

50
00:01:59,960 --> 00:02:07,200
pointer where does that point points up

51
00:02:02,640 --> 00:02:10,679
here at 910 we do another minus hex uh

52
00:02:07,200 --> 00:02:12,800
minus hex1 that points down here it has

53
00:02:10,679 --> 00:02:14,519
a saved frame pointer where does that

54
00:02:12,800 --> 00:02:17,440
point

55
00:02:14,519 --> 00:02:19,440
a98 is where the frame pointer pointed

56
00:02:17,440 --> 00:02:21,599
when we first got into main so this

57
00:02:19,440 --> 00:02:23,920
points all the way back up to there and

58
00:02:21,599 --> 00:02:26,760
this you're now starting to see how the

59
00:02:23,920 --> 00:02:28,040
link list exists it's not exactly as

60
00:02:26,760 --> 00:02:29,360
clean as it is on some other

61
00:02:28,040 --> 00:02:31,080
architectures where the frame pointer

62
00:02:29,360 --> 00:02:33,160
literally points at the Save frame

63
00:02:31,080 --> 00:02:35,120
pointer and furthermore because of these

64
00:02:33,160 --> 00:02:36,760
conventions about sometimes the return

65
00:02:35,120 --> 00:02:38,640
address is saved on the stack and

66
00:02:36,760 --> 00:02:40,920
sometimes it's not we've got this

67
00:02:38,640 --> 00:02:43,360
discrepancy of the leaf node versus the

68
00:02:40,920 --> 00:02:45,680
interior nodes but still you can see the

69
00:02:43,360 --> 00:02:48,400
general idea of how something like a

70
00:02:45,680 --> 00:02:51,319
debugger could walk backwards and figure

71
00:02:48,400 --> 00:02:55,040
out what frame we're in so GDB has a

72
00:02:51,319 --> 00:02:56,239
command BT for back trace the call stack

73
00:02:55,040 --> 00:02:58,239
and it would show you something like

74
00:02:56,239 --> 00:03:00,159
this if you're sitting there in func 3

75
00:02:58,239 --> 00:03:03,040
and you run BT it's saying hey hey

76
00:03:00,159 --> 00:03:05,040
you're in func 3 now but if you returned

77
00:03:03,040 --> 00:03:07,000
you would return back to func 2 and func

78
00:03:05,040 --> 00:03:09,000
2 would return back to func and func

79
00:03:07,000 --> 00:03:10,879
would return to main or another way of

80
00:03:09,000 --> 00:03:13,360
saying it is that main called func

81
00:03:10,879 --> 00:03:15,360
called func 2 called func 3 so how does

82
00:03:13,360 --> 00:03:17,000
a debugger figure this out well the

83
00:03:15,360 --> 00:03:19,400
first way that it figures out that it is

84
00:03:17,000 --> 00:03:21,159
currently in func 3 is simply looking at

85
00:03:19,400 --> 00:03:23,000
the program counter and saying here's

86
00:03:21,159 --> 00:03:25,000
the program counter is it between the

87
00:03:23,000 --> 00:03:26,360
boundaries of some particular function

88
00:03:25,000 --> 00:03:28,519
based on the symbols that have been

89
00:03:26,360 --> 00:03:30,680
compiled into the executable yes looks

90
00:03:28,519 --> 00:03:33,239
like it is it's with in the bounds of

91
00:03:30,680 --> 00:03:35,200
func but then how does it know that it

92
00:03:33,239 --> 00:03:37,760
would return back to func 2 well if

93
00:03:35,200 --> 00:03:40,280
you're currently in func 3 then the ra

94
00:03:37,760 --> 00:03:43,120
register should hold where you're going

95
00:03:40,280 --> 00:03:47,239
to return back to and the ra register

96
00:03:43,120 --> 00:03:50,920
holds 5 648 and that is indeed what the

97
00:03:47,239 --> 00:03:53,280
BT command says it says FES 648 is where

98
00:03:50,920 --> 00:03:56,079
you would return back to to get to func

99
00:03:53,280 --> 00:03:58,640
2 then how does it know that func 2

100
00:03:56,079 --> 00:04:01,200
returns back to func well there now it

101
00:03:58,640 --> 00:04:03,360
starts to use the linked list that is on

102
00:04:01,200 --> 00:04:06,040
the stack and it goes and it searches

103
00:04:03,360 --> 00:04:09,200
and it finds the first Saved return

104
00:04:06,040 --> 00:04:12,040
address on the stack and this is FES 664

105
00:04:09,200 --> 00:04:15,159
and so it's saying func at address fives

106
00:04:12,040 --> 00:04:17,280
664 is where you would return to if you

107
00:04:15,159 --> 00:04:19,239
were returning from func 2 and same

108
00:04:17,280 --> 00:04:21,199
thing again how do you get back to main

109
00:04:19,239 --> 00:04:23,320
it goes it searches the link list it

110
00:04:21,199 --> 00:04:26,680
finds the next saved return address

111
00:04:23,320 --> 00:04:28,759
which is 680 and it knows 680 is inside

112
00:04:26,680 --> 00:04:30,600
of main that is where you would return

113
00:04:28,759 --> 00:04:32,240
back to now of course it could keep

114
00:04:30,600 --> 00:04:34,440
searching and it could say like okay

115
00:04:32,240 --> 00:04:36,199
well main is going to return back to

116
00:04:34,440 --> 00:04:38,400
whatever this thing points to which is

117
00:04:36,199 --> 00:04:39,840
something like start but in general they

118
00:04:38,400 --> 00:04:41,919
don't show that because in general

119
00:04:39,840 --> 00:04:44,440
programmers don't want to see where

120
00:04:41,919 --> 00:04:46,759
their main function returns back into

121
00:04:44,440 --> 00:04:48,800
system included code you just say I

122
00:04:46,759 --> 00:04:50,840
wrote code from main and that's all I

123
00:04:48,800 --> 00:04:52,520
really care about so yeah this is how

124
00:04:50,840 --> 00:04:54,680
something like the back Trace command

125
00:04:52,520 --> 00:04:57,039
can utilize this linked list walk

126
00:04:54,680 --> 00:04:58,720
backwards find things on the stack like

127
00:04:57,039 --> 00:05:01,080
saved return addresses save frame

128
00:04:58,720 --> 00:05:03,080
pointers and so forth now earlier in the

129
00:05:01,080 --> 00:05:04,720
class I gave you this example of what's

130
00:05:03,080 --> 00:05:06,680
the frame pointer and what's its point

131
00:05:04,720 --> 00:05:08,360
when we first encountered it and I said

132
00:05:06,680 --> 00:05:10,400
it's going to matter once you start

133
00:05:08,360 --> 00:05:13,160
having many functions that there's going

134
00:05:10,400 --> 00:05:15,960
to be a linked list and I gave you a

135
00:05:13,160 --> 00:05:17,720
very sketch outline now we know things

136
00:05:15,960 --> 00:05:19,240
about local variables now we know things

137
00:05:17,720 --> 00:05:21,479
about return addresses now we know

138
00:05:19,240 --> 00:05:23,720
things about frame pointers so I can

139
00:05:21,479 --> 00:05:25,720
fill in this code just by sort of

140
00:05:23,720 --> 00:05:28,319
eyeballing it and understanding what's

141
00:05:25,720 --> 00:05:31,800
going on so the first thing main would

142
00:05:28,319 --> 00:05:33,600
do is it would all at space for a frame

143
00:05:31,800 --> 00:05:36,160
and the compiler would allocate space

144
00:05:33,600 --> 00:05:37,960
appropriate based on what it knows about

145
00:05:36,160 --> 00:05:39,440
what it needs to save for return

146
00:05:37,960 --> 00:05:41,520
addresses is it going to call another

147
00:05:39,440 --> 00:05:44,039
function yes it is okay it needs space

148
00:05:41,520 --> 00:05:46,039
for return address is it going to save

149
00:05:44,039 --> 00:05:48,039
the frame pointer yes it is it was

150
00:05:46,039 --> 00:05:50,360
compiled with saving frame pointers so

151
00:05:48,039 --> 00:05:52,280
it allocates space for that and does it

152
00:05:50,360 --> 00:05:54,639
have local variables yes it does so it's

153
00:05:52,280 --> 00:05:56,880
going to allocate space for that so

154
00:05:54,639 --> 00:05:58,440
space is allocated I'll just point out

155
00:05:56,880 --> 00:06:00,520
that because there is a single local

156
00:05:58,440 --> 00:06:02,520
variable we've seen the thus far that

157
00:06:00,520 --> 00:06:06,280
you know space for local variables is

158
00:06:02,520 --> 00:06:08,560
allocated in HEX 10 sized chunks there's

159
00:06:06,280 --> 00:06:10,479
only an INT so therefore necessarily

160
00:06:08,560 --> 00:06:11,960
there's going to be over allocation

161
00:06:10,479 --> 00:06:14,560
there's going to be uninitialized space

162
00:06:11,960 --> 00:06:15,919
for alignment and so when you get in

163
00:06:14,560 --> 00:06:17,520
here the first thing that this code

164
00:06:15,919 --> 00:06:19,360
would do based on just you know that

165
00:06:17,520 --> 00:06:21,319
previous assembly for that other code we

166
00:06:19,360 --> 00:06:23,199
know the first thing it does is that

167
00:06:21,319 --> 00:06:25,120
it's going to allocate space then it's

168
00:06:23,199 --> 00:06:26,800
going to save the return address then

169
00:06:25,120 --> 00:06:28,440
it's going to save the frame pointer

170
00:06:26,800 --> 00:06:30,639
which will point up to wherever the

171
00:06:28,440 --> 00:06:32,280
frame pointer current points and then

172
00:06:30,639 --> 00:06:35,000
it's going to move the frame pointer

173
00:06:32,280 --> 00:06:37,639
Down based on that diagram we just saw

174
00:06:35,000 --> 00:06:39,080
we get the sense that the frame pointer

175
00:06:37,639 --> 00:06:40,759
is going to be if there's a return

176
00:06:39,080 --> 00:06:43,240
address it's going to be frame pointer

177
00:06:40,759 --> 00:06:44,680
minus 10 to get the save frame pointer

178
00:06:43,240 --> 00:06:46,919
so essentially it's kind of like the

179
00:06:44,680 --> 00:06:50,240
frame pointer is pointing just outside

180
00:06:46,919 --> 00:06:52,639
of main's frame so it gets moved down to

181
00:06:50,240 --> 00:06:54,599
here there was the space reserved for

182
00:06:52,639 --> 00:06:56,840
INT C it's not actually going to be set

183
00:06:54,599 --> 00:06:58,440
to anything yet until this foo returns

184
00:06:56,840 --> 00:06:59,599
but we'll just place it on our diagram

185
00:06:58,440 --> 00:07:02,160
to say yes there's going to going to be

186
00:06:59,599 --> 00:07:04,319
an Inc so stuff in this Frame return

187
00:07:02,160 --> 00:07:07,199
address frame pointer local variables

188
00:07:04,319 --> 00:07:10,680
and padding due to needing to have

189
00:07:07,199 --> 00:07:13,000
alignment of hex boundaries then when

190
00:07:10,680 --> 00:07:15,080
main calls into fu fu is going to do the

191
00:07:13,000 --> 00:07:16,319
same thing it's going to allocate space

192
00:07:15,080 --> 00:07:17,879
there's going to be too much space

193
00:07:16,319 --> 00:07:20,240
because there's only a single local

194
00:07:17,879 --> 00:07:21,759
variable B it's going to save the turn

195
00:07:20,240 --> 00:07:23,560
addess because it calls to another

196
00:07:21,759 --> 00:07:25,520
function bar it's going to save the

197
00:07:23,560 --> 00:07:27,240
frame pointer which points at wherever

198
00:07:25,520 --> 00:07:28,840
the frame pointer currently points it's

199
00:07:27,240 --> 00:07:30,199
going to move the frame pointer down the

200
00:07:28,840 --> 00:07:32,879
frame pointer was kind of pointing

201
00:07:30,199 --> 00:07:34,720
outside of foo's frame and then there

202
00:07:32,879 --> 00:07:36,639
was that space for INT B and in this

203
00:07:34,720 --> 00:07:38,440
case b is set to something upon

204
00:07:36,639 --> 00:07:40,919
initialization so there would be an

205
00:07:38,440 --> 00:07:43,440
actual value in there foo calls to Bar

206
00:07:40,919 --> 00:07:45,479
Bar allocates stack frame space there's

207
00:07:43,440 --> 00:07:48,080
going to be some uninitialized alignment

208
00:07:45,479 --> 00:07:50,240
space both for the local variable a and

209
00:07:48,080 --> 00:07:52,240
because we now know that leaf node

210
00:07:50,240 --> 00:07:53,800
functions don't bother saving the return

211
00:07:52,240 --> 00:07:56,720
address but they still have to keep

212
00:07:53,800 --> 00:07:58,840
things aligned to x16 so save only the

213
00:07:56,720 --> 00:08:02,159
frame pointer move the frame pointer

214
00:07:58,840 --> 00:08:04,159
down and allocate space for INT a do the

215
00:08:02,159 --> 00:08:07,199
ultimate calculation based on the input

216
00:08:04,159 --> 00:08:09,639
arguments set the value to a and then

217
00:08:07,199 --> 00:08:12,759
print something and it's time to return

218
00:08:09,639 --> 00:08:14,800
so what happens upon return well the

219
00:08:12,759 --> 00:08:16,919
thing is going to move the frame pointer

220
00:08:14,800 --> 00:08:19,120
back up to where it used to be and then

221
00:08:16,919 --> 00:08:21,159
it's just going to deallocate all the

222
00:08:19,120 --> 00:08:23,520
stuff for the frame by just changing the

223
00:08:21,159 --> 00:08:25,240
stack pointer back to where it used to

224
00:08:23,520 --> 00:08:26,759
point which is right here I should have

225
00:08:25,240 --> 00:08:29,759
perhaps included the stack pointer on

226
00:08:26,759 --> 00:08:32,120
this diagram but oh well so then

227
00:08:29,759 --> 00:08:33,800
bar just returned to fu fu is going to

228
00:08:32,120 --> 00:08:35,680
say okay well I'm just going to return

229
00:08:33,800 --> 00:08:37,599
immediately as well back to main so what

230
00:08:35,680 --> 00:08:39,320
does it do moves the frame pointer up to

231
00:08:37,599 --> 00:08:41,399
where the save frame pointer says it

232
00:08:39,320 --> 00:08:43,599
used to point and then it just

233
00:08:41,399 --> 00:08:45,839
deallocates all the stack Space by

234
00:08:43,599 --> 00:08:48,360
adding to the stack pointer and once

235
00:08:45,839 --> 00:08:50,440
more main just got the return value

236
00:08:48,360 --> 00:08:52,240
from foo it's going to store it into C so

237
00:08:50,440 --> 00:08:55,040
now C would have an actual value based

238
00:08:52,240 --> 00:08:56,640
on this calculation but last thing it

239
00:08:55,040 --> 00:08:58,320
does moves the frame pointer up to

240
00:08:56,640 --> 00:09:01,480
wherever it used to point and

241
00:08:58,320 --> 00:09:04,240
deallocates the face so that is now

242
00:09:01,480 --> 00:09:06,440
actual detailed look at what's going on

243
00:09:04,240 --> 00:09:08,120
with frame pointers we now know stuff

244
00:09:06,440 --> 00:09:09,839
about what's on the stack we now know

245
00:09:08,120 --> 00:09:14,360
about how frame pointers used create a

246
00:09:09,839 --> 00:09:14,360
link list and that's cool

