1
00:00:01,240 --> 00:00:05,600
now the registers of an architecture are

2
00:00:03,159 --> 00:00:08,360
its most fundamental unit of memory

3
00:00:05,600 --> 00:00:10,719
storage so registers are small memory

4
00:00:08,360 --> 00:00:12,280
storage areas built onto the processor

5
00:00:10,719 --> 00:00:14,559
they're volatile memory so when the

6
00:00:12,280 --> 00:00:16,480
power goes away the register values go

7
00:00:14,559 --> 00:00:18,960
away but they're actually built out of

8
00:00:16,480 --> 00:00:23,119
transistors on the actual silicon itself

9
00:00:18,960 --> 00:00:24,840
so RV32I and 64i have 32 general

10
00:00:23,119 --> 00:00:26,920
purpose registers plus the program

11
00:00:24,840 --> 00:00:27,960
counter which is not general purpose

12
00:00:26,920 --> 00:00:29,640
because it points at the next

13
00:00:27,960 --> 00:00:31,720
instruction execute as we already

14
00:00:29,640 --> 00:00:35,559
mentioned before we also mentioned that

15
00:00:31,720 --> 00:00:37,200
what sets RV32E aside from RV32I is

16
00:00:35,559 --> 00:00:39,520
that it's for embedded systems and it

17
00:00:37,200 --> 00:00:42,520
only has 16 general purpose registers

18
00:00:39,520 --> 00:00:45,760
instead of 32 now RISC-V has this notion

19
00:00:42,520 --> 00:00:49,320
of an XLEN as the length in bits for

20
00:00:45,760 --> 00:00:52,520
registers for a given ISA so for RV32I

21
00:00:49,320 --> 00:00:55,520
32e the XLEN is 32 so if you're

22
00:00:52,520 --> 00:00:57,879
compiling for those base ISAs then these

23
00:00:55,520 --> 00:01:01,039
registers will all be 32 bits wide if

24
00:00:57,879 --> 00:01:05,239
you're compiling for RV64I the XLEN

25
00:01:01,039 --> 00:01:07,560
will be 64 and for RV128I the XLEN is 128

26
00:01:05,239 --> 00:01:11,119
so these represent the 32 general

27
00:01:07,560 --> 00:01:13,560
purpose registers x0 to x31 and the one

28
00:01:11,119 --> 00:01:15,640
special purpose register PC for program

29
00:01:13,560 --> 00:01:18,439
counter so they show it as X length

30
00:01:15,640 --> 00:01:22,360
bents with the zeroth bit on the right

31
00:01:18,439 --> 00:01:24,280
and the xll minus one bit on the left

32
00:01:22,360 --> 00:01:25,520
and so different you know documentation

33
00:01:24,280 --> 00:01:27,960
different spec sheets different

34
00:01:25,520 --> 00:01:30,159
architectures will'll all show their uh

35
00:01:27,960 --> 00:01:32,200
their bit ordering differently so this

36
00:01:30,159 --> 00:01:33,720
is just so you know that they are using

37
00:01:32,200 --> 00:01:35,320
least significant bit to the right most

38
00:01:33,720 --> 00:01:38,119
significant bit to the left and they're

39
00:01:35,320 --> 00:01:41,360
defining it in terms of E length which

40
00:01:38,119 --> 00:01:43,439
could be 32 64 or 128 depending on the

41
00:01:41,360 --> 00:01:46,600
ISA now let's talk about

42
00:01:43,439 --> 00:01:48,399
a the application binary interface which

43
00:01:46,600 --> 00:01:50,399
defines how data structures or

44
00:01:48,399 --> 00:01:52,280
computational routines are accessed in

45
00:01:50,399 --> 00:01:55,159
machine code what we're going to really

46
00:01:52,280 --> 00:01:57,840
care about from the ABI is how it

47
00:01:55,159 --> 00:01:59,880
defines things like calling C functions

48
00:01:57,840 --> 00:02:01,680
so that's specifically called calling

49
00:01:59,880 --> 00:02:03,439
conventions additionally we like the

50
00:02:01,680 --> 00:02:05,399
RISC 5 abi because it gives us

51
00:02:03,439 --> 00:02:08,000
alternative and usually more intuitive

52
00:02:05,399 --> 00:02:10,640
names for registers instead of just x0

53
00:02:08,000 --> 00:02:13,040
through x31 so let's talk about ABI

54
00:02:10,640 --> 00:02:16,360
mnemonics like Johnny mnemonic so the

55
00:02:13,040 --> 00:02:18,840
ABI mnemonic is the more humanfriendly

56
00:02:16,360 --> 00:02:21,800
name for the different registers so the

57
00:02:18,840 --> 00:02:24,760
x0 register has an ABI name of the

58
00:02:21,800 --> 00:02:27,440
zero register the x1 has a thing called

59
00:02:24,760 --> 00:02:29,000
ra which stands for return address x2 SP

60
00:02:27,440 --> 00:02:31,080
for stack pointer and so forth we'll

61
00:02:29,000 --> 00:02:32,879
talk about each of the more in a second

62
00:02:31,080 --> 00:02:34,640
but this is the actual table from the

63
00:02:32,879 --> 00:02:36,400
documentation which you would consult if

64
00:02:34,640 --> 00:02:38,800
you're trying to figure out you know oh

65
00:02:36,400 --> 00:02:40,680
something says that it's S1 what is the

66
00:02:38,800 --> 00:02:42,560
actual register behind the scenes so

67
00:02:40,680 --> 00:02:44,400
we're going to use this sort of coloring

68
00:02:42,560 --> 00:02:45,920
convention where if you see something in

69
00:02:44,400 --> 00:02:47,519
green that means we're going to actually

70
00:02:45,920 --> 00:02:49,200
see it in this class and if you see

71
00:02:47,519 --> 00:02:50,720
something in red that means we're not

72
00:02:49,200 --> 00:02:53,519
going to see it in this class so first

73
00:02:50,720 --> 00:02:55,720
of all the x0 or zero register if you

74
00:02:53,519 --> 00:02:57,400
read from the x0 register you're always

75
00:02:55,720 --> 00:02:59,840
going to get a zero this is basically a

76
00:02:57,400 --> 00:03:01,879
register which is hardcoded to zero cons

77
00:02:59,840 --> 00:03:03,519
quently if you write to the x0 register

78
00:03:01,879 --> 00:03:05,519
it doesn't do anything because the

79
00:03:03,519 --> 00:03:07,239
register is hardcoded to zero it's

80
00:03:05,519 --> 00:03:08,599
always set to zero and this is seen on

81
00:03:07,239 --> 00:03:11,920
you know some other architectures

82
00:03:08,599 --> 00:03:14,599
besides RISC-V this is in myips or spark

83
00:03:11,920 --> 00:03:16,879
for instance x1 register is also known

84
00:03:14,599 --> 00:03:18,840
as ra which stands for return address

85
00:03:16,879 --> 00:03:20,599
register this is also called the link

86
00:03:18,840 --> 00:03:23,840
register on some other RISC-based

87
00:03:20,599 --> 00:03:27,360
architectures like uh arm for instance

88
00:03:23,840 --> 00:03:29,799
so that ra is your link to the Past it's

89
00:03:27,360 --> 00:03:31,720
frequently used when you call a function

90
00:03:29,799 --> 00:03:33,519
and then that ra the return address is

91
00:03:31,720 --> 00:03:35,640
the link to the past wherever you called

92
00:03:33,519 --> 00:03:38,239
from how do you get back to where you

93
00:03:35,640 --> 00:03:40,519
came from that's ra as we'll see later

94
00:03:38,239 --> 00:03:43,280
the instructions that actually call into

95
00:03:40,519 --> 00:03:45,040
functions don't require that you set the

96
00:03:43,280 --> 00:03:47,000
return address to ra you could actually

97
00:03:45,040 --> 00:03:48,799
use other general purpose registers but

98
00:03:47,000 --> 00:03:50,319
again we're talking about Abi Abi

99
00:03:48,799 --> 00:03:52,680
mnemonics and these are basically the

100
00:03:50,319 --> 00:03:54,920
conventions that they expect executable

101
00:03:52,680 --> 00:03:57,079
code to follow in this case for Linux

102
00:03:54,920 --> 00:03:59,200
systems or rather I should perhaps say

103
00:03:57,079 --> 00:04:01,439
for Elf based systems elf being the

104
00:03:59,200 --> 00:04:03,599
executable in linking format which is

105
00:04:01,439 --> 00:04:07,159
used by Linux but is also used by other

106
00:04:03,599 --> 00:04:09,079
Unix type systems x2 is the SP or stack

107
00:04:07,159 --> 00:04:11,560
pointer register and this holds the

108
00:04:09,079 --> 00:04:13,879
address which is a pointer this holds

109
00:04:11,560 --> 00:04:16,280
the address for the top of the stack and

110
00:04:13,879 --> 00:04:18,680
so it's basically pointing at the last

111
00:04:16,280 --> 00:04:20,199
memory on the stack that is in use and

112
00:04:18,680 --> 00:04:22,880
we'll talk more about the stack later

113
00:04:20,199 --> 00:04:24,000
now x3 the GP or global pointer is

114
00:04:22,880 --> 00:04:26,560
something we're not actually going to

115
00:04:24,000 --> 00:04:28,000
see in this class I'm told but have not

116
00:04:26,560 --> 00:04:29,960
actually confirmed that on Linux it

117
00:04:28,000 --> 00:04:32,240
holds a pointer to the data section

118
00:04:29,960 --> 00:04:34,600
where globals are stored I did a very

119
00:04:32,240 --> 00:04:36,759
quick and simple test where I made a

120
00:04:34,600 --> 00:04:38,360
global and I said okay compile this

121
00:04:36,759 --> 00:04:41,360
thing use a global do I see it using the

122
00:04:38,360 --> 00:04:43,080
x3 or GP register and I didn't so that's

123
00:04:41,360 --> 00:04:44,840
again I haven't actually confirmed this

124
00:04:43,080 --> 00:04:46,680
perhaps in more complicated code you

125
00:04:44,840 --> 00:04:49,400
will actually see this ABI convention

126
00:04:46,680 --> 00:04:51,199
being used perhaps GCC doesn't use it I

127
00:04:49,400 --> 00:04:53,520
don't know for sure I'm just reporting

128
00:04:51,199 --> 00:04:55,560
on what I found it is ostensibly used

129
00:04:53,520 --> 00:04:57,680
for another nemonic that we not actually

130
00:04:55,560 --> 00:05:00,759
going to see in this class is x4 being

131
00:04:57,680 --> 00:05:02,360
used as TP or the thread pointer and on

132
00:05:00,759 --> 00:05:04,720
Linux this is supposed to hold a pointer

133
00:05:02,360 --> 00:05:06,600
to the thread local storage but if you

134
00:05:04,720 --> 00:05:08,520
don't have a multi-threaded app it'll

135
00:05:06,600 --> 00:05:10,120
just be used as a temporary register

136
00:05:08,520 --> 00:05:12,160
again I didn't make an app that has

137
00:05:10,120 --> 00:05:13,919
thread local storage that's sort of a

138
00:05:12,160 --> 00:05:16,600
special thing we have all sort of

139
00:05:13,919 --> 00:05:18,039
non-threaded very simple uh applications

140
00:05:16,600 --> 00:05:20,000
in this class though of course you can

141
00:05:18,039 --> 00:05:22,360
go off and try to experiment with that

142
00:05:20,000 --> 00:05:24,720
yourself and see if you can see the x4

143
00:05:22,360 --> 00:05:26,960
TP register being used for this purpose

144
00:05:24,720 --> 00:05:30,400
back to conventions we will see x5

145
00:05:26,960 --> 00:05:32,520
through X7 are also called t0 through T2

146
00:05:30,400 --> 00:05:34,560
so this is temporary 0 temporary 1

147
00:05:32,520 --> 00:05:37,400
temporary 2 and these are temporary

148
00:05:34,560 --> 00:05:40,840
registers and they're caller saved that

149
00:05:37,400 --> 00:05:42,479
uh that document before this one would

150
00:05:40,840 --> 00:05:44,560
tell you whether or not things are

151
00:05:42,479 --> 00:05:46,319
supposed to be caller saved or collie

152
00:05:44,560 --> 00:05:48,680
saved and we'll talk more about what it

153
00:05:46,319 --> 00:05:50,080
means to be caller or call E saved later

154
00:05:48,680 --> 00:05:51,800
on in the class when we talk about the

155
00:05:50,080 --> 00:05:53,800
calling conventions but the basic point

156
00:05:51,800 --> 00:05:55,240
is temporary registers are treated as

157
00:05:53,800 --> 00:05:57,000
temporary so if you're going to call a

158
00:05:55,240 --> 00:05:58,680
function you should assume that the

159
00:05:57,000 --> 00:06:00,160
function you just called could smash

160
00:05:58,680 --> 00:06:02,199
these register values vales and

161
00:06:00,160 --> 00:06:04,120
consequently you the caller need to go

162
00:06:02,199 --> 00:06:06,160
ahead and save those before you call

163
00:06:04,120 --> 00:06:09,560
into a function if you need their values

164
00:06:06,160 --> 00:06:11,479
preserved across call s0 and S1 the

165
00:06:09,560 --> 00:06:14,160
saved registers are supposed to be call

166
00:06:11,479 --> 00:06:16,599
E saved registers so as opposed to these

167
00:06:14,160 --> 00:06:18,919
caller saved the call E saved means you

168
00:06:16,599 --> 00:06:21,400
can go ahead and put values into these

169
00:06:18,919 --> 00:06:23,280
and you can call a function and the call

170
00:06:21,400 --> 00:06:25,440
E should not smash them if the call E

171
00:06:23,280 --> 00:06:27,080
needs to use them it's the col's

172
00:06:25,440 --> 00:06:28,919
responsibility to go ahead and save

173
00:06:27,080 --> 00:06:31,080
these registers now one of those in

174
00:06:28,919 --> 00:06:35,039
particular actually has a third name so

175
00:06:31,080 --> 00:06:37,479
X8 equal to s0 equal to FP the frame

176
00:06:35,039 --> 00:06:39,160
pointer register this is used to store

177
00:06:37,479 --> 00:06:41,680
pointer to the base of the current

178
00:06:39,160 --> 00:06:43,639
function's stack frame if frame pointers

179
00:06:41,680 --> 00:06:46,720
are used so frame pointers are not

180
00:06:43,639 --> 00:06:48,280
required but basically if the concept of

181
00:06:46,720 --> 00:06:50,280
frame pointers is being used in the

182
00:06:48,280 --> 00:06:52,560
compiled code then essentially each

183
00:06:50,280 --> 00:06:55,360
function has what's called a frame of

184
00:06:52,560 --> 00:06:57,800
the stack that is for its storage of its

185
00:06:55,360 --> 00:06:59,360
data and the frame pointer would point

186
00:06:57,800 --> 00:07:00,720
near that it's not exactly point

187
00:06:59,360 --> 00:07:02,440
pointing at the base of the stack on

188
00:07:00,720 --> 00:07:05,919
RISC-V as we'll see later on in the

189
00:07:02,440 --> 00:07:08,599
class then X10 through X17 are the a0

190
00:07:05,919 --> 00:07:11,160
through A7 arguments to functions so you

191
00:07:08,599 --> 00:07:13,120
can actually see there are up to eight

192
00:07:11,160 --> 00:07:15,400
possible arguments that can be passed in

193
00:07:13,120 --> 00:07:17,840
registers to a function that is being

194
00:07:15,400 --> 00:07:21,599
called and then we have all of our extra

195
00:07:17,840 --> 00:07:25,240
ones we've got x18 to x27 which is S2

196
00:07:21,599 --> 00:07:28,639
Callie saved registers S2 to S11 and

197
00:07:25,240 --> 00:07:30,960
then we have some more temps T3 to T6

198
00:07:28,639 --> 00:07:33,520
and so you might be asking yourself why

199
00:07:30,960 --> 00:07:35,800
do we have two disjoint regions for

200
00:07:33,520 --> 00:07:39,240
temporary registers and call E save

201
00:07:35,800 --> 00:07:44,120
registers we've got t0 and T2 appear at

202
00:07:39,240 --> 00:07:46,759
x5 to X7 and then T3 to T6 at x28 to x21

203
00:07:44,120 --> 00:07:49,639
likewise we've got these disjoint s0 and

204
00:07:46,759 --> 00:07:53,639
S1 and S2 to S11 so why are these

205
00:07:49,639 --> 00:07:57,919
disjoint well the answer is RV32E so we

206
00:07:53,639 --> 00:08:00,919
said RV32E is going to cut down from 32

207
00:07:57,919 --> 00:08:03,000
general purpose registers to only 16

208
00:08:00,919 --> 00:08:06,240
general purpose registers so that would

209
00:08:03,000 --> 00:08:09,199
look a little something like this bam

210
00:08:06,240 --> 00:08:12,159
love it excellent animations you know

211
00:08:09,199 --> 00:08:15,680
yeah so basically RV32E lets you cut

212
00:08:12,159 --> 00:08:17,560
off all of those upper 16 registers but

213
00:08:15,680 --> 00:08:19,360
still all of these conventions would

214
00:08:17,560 --> 00:08:21,159
apply and this makes it so that you can

215
00:08:19,360 --> 00:08:23,080
still have temporary registers some of

216
00:08:21,159 --> 00:08:27,560
them are going to be used the same way

217
00:08:23,080 --> 00:08:30,120
in RV32E versus 32i t0 through T2 s0

218
00:08:27,560 --> 00:08:33,519
through S1 the arguments now get slashed

219
00:08:30,120 --> 00:08:36,680
down from a0 to A7 now you can only have

220
00:08:33,519 --> 00:08:40,640
upwards of six arguments a0 to a5 and an

221
00:08:36,680 --> 00:08:42,240
RV32E ABI based uh compiled code but

222
00:08:40,640 --> 00:08:43,519
still for functions that don't take a

223
00:08:42,240 --> 00:08:45,839
lot of arguments things are going to

224
00:08:43,519 --> 00:08:48,680
look basically all the same so looking

225
00:08:45,839 --> 00:08:52,000
at that in a similar way you know RV32I

226
00:08:48,680 --> 00:08:56,320
has 32 general purpose registers rv32 e

227
00:08:52,000 --> 00:08:59,000
has only 16 and boom love that animation

228
00:08:56,320 --> 00:09:00,360
skill Zeno so yeah basically you just

229
00:08:59,000 --> 00:09:02,640
cut cut it down and again the

230
00:09:00,360 --> 00:09:05,040
justification for that is that they said

231
00:09:02,640 --> 00:09:07,560
in small RV32I core designs the upper

232
00:09:05,040 --> 00:09:09,839
16 registers consume around one quar of

233
00:09:07,560 --> 00:09:12,800
the total area of the core so the actual

234
00:09:09,839 --> 00:09:14,839
silicon fabrication space uh physical

235
00:09:12,800 --> 00:09:17,519
space and consequently the removal saves

236
00:09:14,839 --> 00:09:20,519
around 25% of the core area and

237
00:09:17,519 --> 00:09:22,839
reduction of Core Power so you can save

238
00:09:20,519 --> 00:09:25,079
space and you can save power if you have

239
00:09:22,839 --> 00:09:26,600
an embedded system that doesn't need all

240
00:09:25,079 --> 00:09:29,519
that complexity doesn't need all of

241
00:09:26,600 --> 00:09:31,240
those uh juggling of registers it can

242
00:09:29,519 --> 00:09:33,440
basically perhaps it'll use a little bit

243
00:09:31,240 --> 00:09:34,920
more you know at the upper end perhaps

244
00:09:33,440 --> 00:09:37,200
it's going to use a little bit more

245
00:09:34,920 --> 00:09:38,920
assembly instructions if if really they

246
00:09:37,200 --> 00:09:41,440
can't juggle everything in the available

247
00:09:38,920 --> 00:09:43,079
registers but um that is probably going

248
00:09:41,440 --> 00:09:44,480
to be an acceptable trade-off for people

249
00:09:43,079 --> 00:09:46,519
who are trying to keep the power

250
00:09:44,480 --> 00:09:48,560
reduction small for something that needs

251
00:09:46,519 --> 00:09:50,480
to run a long time in an embedded system

252
00:09:48,560 --> 00:09:52,480
so the final word about compilers is

253
00:09:50,480 --> 00:09:54,920
that compilers have their own biases

254
00:09:52,480 --> 00:09:57,959
created by the programmers who make the

255
00:09:54,920 --> 00:09:59,839
compilers and so although you know we

256
00:09:57,959 --> 00:10:01,680
have this notion of temporary registers

257
00:09:59,839 --> 00:10:04,160
for instance you'll see as you look at

258
00:10:01,680 --> 00:10:06,920
the code that GCC emits throughout the

259
00:10:04,160 --> 00:10:08,920
class granted it is unoptimized code

260
00:10:06,920 --> 00:10:11,360
most of the time but it really seems to

261
00:10:08,920 --> 00:10:14,120
prefer using the argument registers as

262
00:10:11,360 --> 00:10:16,640
temporary storage values rather than the

263
00:10:14,120 --> 00:10:18,680
temp ones and that's just down to a GCC

264
00:10:16,640 --> 00:10:20,160
ISM we could see different behaviors if

265
00:10:18,680 --> 00:10:22,040
we looked at a different compiler such

266
00:10:20,160 --> 00:10:23,959
as clang so I just want to say don't

267
00:10:22,040 --> 00:10:26,360
treat the ABI rules like they're the

268
00:10:23,959 --> 00:10:28,480
absolute truth of how a compiler will or

269
00:10:26,360 --> 00:10:30,720
must behave and that if you don't see

270
00:10:28,480 --> 00:10:32,320
things be behing exactly the way the ABI

271
00:10:30,720 --> 00:10:34,040
says that you know there's something

272
00:10:32,320 --> 00:10:36,240
wrong or you're not understanding it's

273
00:10:34,040 --> 00:10:38,920
just compiler makers will generally

274
00:10:36,240 --> 00:10:42,839
abide by the ABI but in certain cases

275
00:10:38,920 --> 00:10:42,839
they will deviate slightly

