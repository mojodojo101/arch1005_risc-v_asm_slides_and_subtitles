1
00:00:00,160 --> 00:00:04,000
okay we've seen calling multiple

2
00:00:01,760 --> 00:00:07,000
functions in a row let's start to deal

3
00:00:04,000 --> 00:00:08,679
with calling functions with arguments so

4
00:00:07,000 --> 00:00:11,400
this time we're going to pass a single

5
00:00:08,679 --> 00:00:14,599
argument a single hard-coded long long

6
00:00:11,400 --> 00:00:16,320
value to the function func and it's just

7
00:00:14,599 --> 00:00:19,600
going to immediately return that back to

8
00:00:16,320 --> 00:00:21,720
us although the return value is an INT

9
00:00:19,600 --> 00:00:23,519
not a long long so necessarily that's

10
00:00:21,720 --> 00:00:25,359
going to get truncated down if we

11
00:00:23,519 --> 00:00:28,080
compile this with optimization level

12
00:00:25,359 --> 00:00:30,080
zero we get this and so there's no new

13
00:00:28,080 --> 00:00:32,640
assembly instructions it's just going to

14
00:00:30,080 --> 00:00:34,239
be reading and understanding and seeing

15
00:00:32,640 --> 00:00:36,480
what's placed where on the stack and

16
00:00:34,239 --> 00:00:38,879
which registers and so forth so once

17
00:00:36,480 --> 00:00:41,960
again it's time to set things up to make

18
00:00:38,879 --> 00:00:44,280
yourself a nice little stack diagram go

19
00:00:41,960 --> 00:00:48,760
ahead and stop step through the assembly

20
00:00:44,280 --> 00:00:48,760
and draw a stack diagram

