1
00:00:00,240 --> 00:00:06,879
now this example is basically a accident

2
00:00:04,319 --> 00:00:09,480
so I was in the local variable section

3
00:00:06,879 --> 00:00:11,599
and I wanted to find the load bite

4
00:00:09,480 --> 00:00:13,400
assembly instruction I was creating

5
00:00:11,599 --> 00:00:15,400
things to try to find that and I came up

6
00:00:13,400 --> 00:00:17,320
with this and I found it but then as I

7
00:00:15,400 --> 00:00:19,760
was writing up the stack diagram for

8
00:00:17,320 --> 00:00:22,160
that I was like oh crap there's the argc

9
00:00:19,760 --> 00:00:24,199
and argv I can't cover this until we

10
00:00:22,160 --> 00:00:25,720
cover calling functions and arguments

11
00:00:24,199 --> 00:00:27,240
and you have to be able to understand

12
00:00:25,720 --> 00:00:29,000
that there's two arguments coming into

13
00:00:27,240 --> 00:00:31,439
main so that you understand what a Zer

14
00:00:29,000 --> 00:00:34,200
and a1 are when you see them used in

15
00:00:31,439 --> 00:00:36,040
main what I just realized as I was

16
00:00:34,200 --> 00:00:38,760
going to record this is that neither

17
00:00:36,040 --> 00:00:40,879
argc nor argv are actually used in this

18
00:00:38,760 --> 00:00:42,960
code there was literally no point for

19
00:00:40,879 --> 00:00:44,640
them to be here I could have deleted

20
00:00:42,960 --> 00:00:46,640
them and put this back in the section

21
00:00:44,640 --> 00:00:49,800
but I had already reordered thing

22
00:00:46,640 --> 00:00:51,280
renumbered the little stars and assembly

23
00:00:49,800 --> 00:00:54,399
instructions and it was going to be

24
00:00:51,280 --> 00:00:56,320
super annoying to renumber them back so

25
00:00:54,399 --> 00:00:59,359
I left it where it was but you're going

26
00:00:56,320 --> 00:01:02,519
to get an inadvertent introduction to

27
00:00:59,359 --> 00:01:05,199
the argc and arv usage when passed into

28
00:01:02,519 --> 00:01:08,720
main here's our compilation no stack

29
00:01:05,199 --> 00:01:12,840
protector as before and here is our load

30
00:01:08,720 --> 00:01:15,360
B load bite and subw so two new assembly

31
00:01:12,840 --> 00:01:18,799
instructions let's cover those first so

32
00:01:15,360 --> 00:01:21,880
here's subw subtract word register from

33
00:01:18,799 --> 00:01:24,479
word register so the W suffix is your

34
00:01:21,880 --> 00:01:26,640
hint that you're looking at an RV64I

35
00:01:24,479 --> 00:01:29,240
assembly instruction that wants to

36
00:01:26,640 --> 00:01:31,000
operate on word registers 32-bit

37
00:01:29,240 --> 00:01:33,520
registers right rather than its native

38
00:01:31,000 --> 00:01:37,000
64-bit register so this is written as

39
00:01:33,520 --> 00:01:39,799
subw source one source 2 destination and

40
00:01:37,000 --> 00:01:42,680
it takes the bottom 32bits of source one

41
00:01:39,799 --> 00:01:45,240
minus the bottom 32bits of source 2 and

42
00:01:42,680 --> 00:01:48,360
stores it into the destination register

43
00:01:45,240 --> 00:01:51,200
that 32bit result so the subtraction is

44
00:01:48,360 --> 00:01:53,280
operated on as a 32-bit result gets the

45
00:01:51,200 --> 00:01:56,479
result and then sign extends it up to

46
00:01:53,280 --> 00:01:58,520
64bits and as I said this is what RV 64

47
00:01:56,479 --> 00:02:01,960
I code uses when it wants to operate on

48
00:01:58,520 --> 00:02:04,680
32-bit or word size variables such as

49
00:02:01,960 --> 00:02:07,039
integers then we have the lb the thing

50
00:02:04,680 --> 00:02:09,239
we were looking for before load bite not

51
00:02:07,039 --> 00:02:12,040
load bite unsigned from memory to

52
00:02:09,239 --> 00:02:15,040
register so this is now going to do

53
00:02:12,040 --> 00:02:17,319
source plus immediate 12 which is going

54
00:02:15,040 --> 00:02:19,840
to be sign extended as usual because

55
00:02:17,319 --> 00:02:22,319
this is a signed immediate source plus

56
00:02:19,840 --> 00:02:25,720
sign extended immediate 12 treat that as

57
00:02:22,319 --> 00:02:29,120
a bite address and then sign extend the

58
00:02:25,720 --> 00:02:31,560
result to the 32-bit register or 64-bit

59
00:02:29,120 --> 00:02:33,879
if you're running 64-bit code so we are

60
00:02:31,560 --> 00:02:36,160
looking at a sign extended result rather

61
00:02:33,879 --> 00:02:38,760
than a zero extended we saw load bite

62
00:02:36,160 --> 00:02:41,159
unsigned before that's zero extended

63
00:02:38,760 --> 00:02:44,200
this is load bite which is signed and

64
00:02:41,159 --> 00:02:47,440
signed extended for whatever reason GCC

65
00:02:44,200 --> 00:02:49,000
really prefers to use lbu rather than lb

66
00:02:47,440 --> 00:02:51,080
and I have to go out of my way to create

67
00:02:49,000 --> 00:02:53,400
an example that I could get it to create

68
00:02:51,080 --> 00:02:55,560
the lb with now it's time to draw your

69
00:02:53,400 --> 00:02:58,040
stack diagram but the very important

70
00:02:55,560 --> 00:03:00,800
hint is that you are looking at a main

71
00:02:58,040 --> 00:03:04,680
that is taking two arguments a integer

72
00:03:00,800 --> 00:03:06,440
argc and a pointer pointer argv so a

73
00:03:04,680 --> 00:03:08,879
character pointer pointer so it's a

74
00:03:06,440 --> 00:03:11,480
pointer that points at something that

75
00:03:08,879 --> 00:03:14,680
points at an array of character pointers

76
00:03:11,480 --> 00:03:16,480
terminated by a null Terminator so stop

77
00:03:14,680 --> 00:03:19,519
step through the assembly be very

78
00:03:16,480 --> 00:03:21,519
careful with what there is around uh go

79
00:03:19,519 --> 00:03:23,599
ahead and you know do extra D references

80
00:03:21,519 --> 00:03:26,599
and stuff if you get confused over what

81
00:03:23,599 --> 00:03:31,239
anything is but draw that stack diagram

82
00:03:26,599 --> 00:03:31,239
and come back and see how it should look

