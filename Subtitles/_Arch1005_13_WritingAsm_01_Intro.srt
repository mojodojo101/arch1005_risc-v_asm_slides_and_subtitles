1
00:00:00,080 --> 00:00:03,399
now before I talk about how we're going

2
00:00:01,839 --> 00:00:05,480
to write assembly in this class

3
00:00:03,399 --> 00:00:08,000
specifically I want to just briefly

4
00:00:05,480 --> 00:00:10,320
touch on the situations in which someone

5
00:00:08,000 --> 00:00:12,160
might need to write assembly for

6
00:00:10,320 --> 00:00:14,360
instance it's very common when writing

7
00:00:12,160 --> 00:00:16,359
firmware operating systems or operating

8
00:00:14,360 --> 00:00:18,800
system drivers just in general if you're

9
00:00:16,359 --> 00:00:21,320
writing privileged code you will often

10
00:00:18,800 --> 00:00:23,960
times be accessing hardware that can

11
00:00:21,320 --> 00:00:26,480
only be accessed with special privileged

12
00:00:23,960 --> 00:00:28,320
instructions so unprivileged code has

13
00:00:26,480 --> 00:00:30,480
one set of instructions and privileged

14
00:00:28,320 --> 00:00:32,279
code can do some more things

15
00:00:30,480 --> 00:00:35,079
another situation is if you were making

16
00:00:32,279 --> 00:00:37,520
a cryptographic library if your hardware

17
00:00:35,079 --> 00:00:39,480
had access to cryptographic acceleration

18
00:00:37,520 --> 00:00:41,960
instructions then you would want to use

19
00:00:39,480 --> 00:00:43,680
those special purpose instructions of

20
00:00:41,960 --> 00:00:45,520
course in the context of security when

21
00:00:43,680 --> 00:00:47,800
writing an exploit for a memory

22
00:00:45,520 --> 00:00:50,239
corruption vulnerability you often times

23
00:00:47,800 --> 00:00:52,960
need extremely precise control over

24
00:00:50,239 --> 00:00:54,960
exactly what you write where uh how you

25
00:00:52,960 --> 00:00:57,239
order and organize your memory so that

26
00:00:54,960 --> 00:01:00,079
you can cause the vulnerability to be

27
00:00:57,239 --> 00:01:02,680
exploited or maybe you just are writing

28
00:01:00,079 --> 00:01:04,239
extremely optimized code and you want

29
00:01:02,680 --> 00:01:06,600
full control over the code for

30
00:01:04,239 --> 00:01:08,759
optimization purposes for the purpose of

31
00:01:06,600 --> 00:01:10,680
this class being able to write assembly

32
00:01:08,759 --> 00:01:12,840
is a great way for you to learn the

33
00:01:10,680 --> 00:01:15,159
assembly language because that allows

34
00:01:12,840 --> 00:01:16,880
you to experiment with and play with

35
00:01:15,159 --> 00:01:18,880
things that we haven't necessarily

36
00:01:16,880 --> 00:01:21,079
covered in class or just play with the

37
00:01:18,880 --> 00:01:23,240
instructions we have covered in class so

38
00:01:21,079 --> 00:01:25,400
that you make sure you truly understand

39
00:01:23,240 --> 00:01:27,040
exactly what they're doing the problem

40
00:01:25,400 --> 00:01:29,600
with this though is that getting the

41
00:01:27,040 --> 00:01:31,920
syntax correct for a given assembler is

42
00:01:29,600 --> 00:01:34,600
of often times annoying so I will later

43
00:01:31,920 --> 00:01:37,880
on give you a very specific example that

44
00:01:34,600 --> 00:01:39,840
you can use to get started in this class

45
00:01:37,880 --> 00:01:41,680
now there's going to be two general ways

46
00:01:39,840 --> 00:01:43,479
that a programmer might write assembly

47
00:01:41,680 --> 00:01:45,880
the first is Standalone assembly and the

48
00:01:43,479 --> 00:01:47,600
second is inline assembly so for

49
00:01:45,880 --> 00:01:50,360
Standalone assembly you write the

50
00:01:47,600 --> 00:01:53,000
assembly into a standalone file that

51
00:01:50,360 --> 00:01:55,960
file is then assembled by an assembler

52
00:01:53,000 --> 00:01:57,880
application into an object file and that

53
00:01:55,960 --> 00:02:00,039
object file is subsequently linked with

54
00:01:57,880 --> 00:02:02,320
the final binary and other compiled

55
00:02:00,039 --> 00:02:04,520
objects so you may have some C source

56
00:02:02,320 --> 00:02:06,439
code that code is compiled and

57
00:02:04,520 --> 00:02:09,000
ultimately creating an object file of

58
00:02:06,439 --> 00:02:10,560
its own and then at the end the Linker

59
00:02:09,000 --> 00:02:12,000
will link together all the various

60
00:02:10,560 --> 00:02:14,360
object files to create the final

61
00:02:12,000 --> 00:02:16,040
executable one nice thing about using a

62
00:02:14,360 --> 00:02:18,280
standalone assembler is that it will

63
00:02:16,040 --> 00:02:21,160
often have helpful capabilities and

64
00:02:18,280 --> 00:02:24,080
extra syntax sugar which allows a

65
00:02:21,160 --> 00:02:26,400
programmer to write code more easily a

66
00:02:24,080 --> 00:02:28,920
lot of the time these syntaxes are not

67
00:02:26,400 --> 00:02:30,840
exactly what would be valid within the

68
00:02:28,920 --> 00:02:32,800
context of the ass L language but

69
00:02:30,840 --> 00:02:34,519
they're more like shortcuts or short

70
00:02:32,800 --> 00:02:36,720
hand that makes it easier for the

71
00:02:34,519 --> 00:02:38,959
programmer to write code an example of

72
00:02:36,720 --> 00:02:41,040
this would be for instance jumping to a

73
00:02:38,959 --> 00:02:43,840
label through some sort of control flow

74
00:02:41,040 --> 00:02:46,360
instruction intrinsically the

75
00:02:43,840 --> 00:02:48,720
instruction that goes to this location

76
00:02:46,360 --> 00:02:51,080
would require calculating some sort of

77
00:02:48,720 --> 00:02:54,000
offset and rather than the programmer

78
00:02:51,080 --> 00:02:55,920
having to you know look at the code and

79
00:02:54,000 --> 00:02:57,319
add up the number of bytes in all the

80
00:02:55,920 --> 00:02:59,200
code between the source and the

81
00:02:57,319 --> 00:03:01,680
destination and calculate the offset

82
00:02:59,200 --> 00:03:04,159
that way basically the assembler will

83
00:03:01,680 --> 00:03:06,040
just handle that for the programmer in

84
00:03:04,159 --> 00:03:07,959
contrast to Standalone assembly where

85
00:03:06,040 --> 00:03:10,480
you write all of your assembly off to

86
00:03:07,959 --> 00:03:12,840
the side in its own file inline assembly

87
00:03:10,480 --> 00:03:15,840
is where you place assembly directly in

88
00:03:12,840 --> 00:03:18,879
line with your other C or C++ code so

89
00:03:15,840 --> 00:03:21,360
GCC for instance supports a Syntax for

90
00:03:18,879 --> 00:03:24,680
inline assembly that conforms to what

91
00:03:21,360 --> 00:03:26,879
the G assembler or gas is expecting and

92
00:03:24,680 --> 00:03:28,799
a final point about writing assembly is

93
00:03:26,879 --> 00:03:31,400
that sometimes it's actually beneficial

94
00:03:28,799 --> 00:03:34,159
to write raw bites rather than writing

95
00:03:31,400 --> 00:03:36,159
assembly this is often the case when you

96
00:03:34,159 --> 00:03:37,720
for instance don't want to fight with

97
00:03:36,159 --> 00:03:40,200
the assembler try you don't want to

98
00:03:37,720 --> 00:03:42,519
figure out what is the syntax that this

99
00:03:40,200 --> 00:03:44,200
particular assembler expects you know

100
00:03:42,519 --> 00:03:46,920
exactly which assembly instruction you

101
00:03:44,200 --> 00:03:49,640
want to create and you hopefully now

102
00:03:46,920 --> 00:03:52,239
will know how to encode that instruction

103
00:03:49,640 --> 00:03:54,560
by reading the fun manual and so you can

104
00:03:52,239 --> 00:03:57,680
just do a shortcut around you know

105
00:03:54,560 --> 00:03:59,760
whatever syntax games the assembler is

106
00:03:57,680 --> 00:04:01,560
trying to play and just directly

107
00:03:59,760 --> 00:04:04,079
calculate the exact bytes for the

108
00:04:01,560 --> 00:04:05,879
instruction you want in the past I found

109
00:04:04,079 --> 00:04:07,879
this to be necessary when I'm dealing

110
00:04:05,879 --> 00:04:10,040
with rare privileged assembly

111
00:04:07,879 --> 00:04:12,159
instructions that have some sort of

112
00:04:10,040 --> 00:04:13,959
weird form and I can't figure out how

113
00:04:12,159 --> 00:04:16,600
the assembler wants me to write that

114
00:04:13,959 --> 00:04:18,000
weird form I just write the raw bites

115
00:04:16,600 --> 00:04:20,359
and that gives me exactly the

116
00:04:18,000 --> 00:04:23,120
instruction I expect writing raw bites

117
00:04:20,359 --> 00:04:24,759
is also a good way to confirm that you

118
00:04:23,120 --> 00:04:26,919
understand instruction in coding and

119
00:04:24,759 --> 00:04:28,440
decoding because if you write certain

120
00:04:26,919 --> 00:04:30,800
bytes based on how you think an

121
00:04:28,440 --> 00:04:32,199
instruction could be encoded and you

122
00:04:30,800 --> 00:04:34,280
then go off and look at it in a

123
00:04:32,199 --> 00:04:36,320
disassembler or debugger and you don't

124
00:04:34,280 --> 00:04:37,880
see the instruction you expect that

125
00:04:36,320 --> 00:04:39,919
means there's something you don't

126
00:04:37,880 --> 00:04:41,919
understand about how the encoding format

127
00:04:39,919 --> 00:04:44,560
works and you need to go back and read

128
00:04:41,919 --> 00:04:46,560
the fun manual some more so now let's go

129
00:04:44,560 --> 00:04:50,280
see how we're going to write assembly

130
00:04:46,560 --> 00:04:50,280
for this class specifically

