1
00:00:00,320 --> 00:00:04,920
hello and welcome to the open security

2
00:00:02,159 --> 00:00:07,359
training architecture 1005 RISC-V

3
00:00:04,920 --> 00:00:10,040
assembly I'm Xeno Kovah and I'll be your

4
00:00:07,359 --> 00:00:12,799
instructor for this class so about this

5
00:00:10,040 --> 00:00:15,360
class we are going to be doing a top-down

6
00:00:12,799 --> 00:00:17,760
class in which I assume that you know a

7
00:00:15,360 --> 00:00:19,640
high-level language like C which is one of

8
00:00:17,760 --> 00:00:22,880
the lowest level high-level languages you

9
00:00:19,640 --> 00:00:26,279
can use and we're going to use C source

10
00:00:22,880 --> 00:00:27,880
code compile it generate RISC-V assembly

11
00:00:26,279 --> 00:00:31,840
and learn about the new assembly

12
00:00:27,880 --> 00:00:35,079
instructions as we see new elements of C

13
00:00:31,840 --> 00:00:37,360
code introduced in our new examples

14
00:00:35,079 --> 00:00:39,000
this top-down approach is contrasted

15
00:00:37,360 --> 00:00:40,680
with a bottom up approach where I would

16
00:00:39,000 --> 00:00:42,719
just teach you individual assembly

17
00:00:40,680 --> 00:00:45,280
instructions like this is an add this is

18
00:00:42,719 --> 00:00:48,120
a subtract and build up small programs

19
00:00:45,280 --> 00:00:50,399
from that I feel it's better to teach

20
00:00:48,120 --> 00:00:52,520
based on something you already know like

21
00:00:50,399 --> 00:00:54,280
the C programming language so that

22
00:00:52,520 --> 00:00:56,520
fundamentally your brain has something

23
00:00:54,280 --> 00:00:59,600
to hang this new knowledge off of you

24
00:00:56,520 --> 00:01:02,440
can say oh I know that calls in C

25
00:00:59,600 --> 00:01:05,640
program programming correspond to JALs

26
00:01:02,440 --> 00:01:07,159
in assembly language for RISC-V the

27
00:01:05,640 --> 00:01:09,159
other point is that this class is

28
00:01:07,159 --> 00:01:11,320
intended to expose you to the most

29
00:01:09,159 --> 00:01:14,560
common assembly instructions in both

30
00:01:11,320 --> 00:01:17,040
32-bit and 64-bit RISC-V so it is

31
00:01:14,560 --> 00:01:19,520
intentionally not going to cover some of

32
00:01:17,040 --> 00:01:22,240
the other aspects such as floating

33
00:01:19,520 --> 00:01:24,520
points vector extensions privileged

34
00:01:22,240 --> 00:01:26,960
assembly instructions transitions from

35
00:01:24,520 --> 00:01:28,880
userspace to kernelspace and so forth

36
00:01:26,960 --> 00:01:30,439
those are topics that would be reserved

37
00:01:28,880 --> 00:01:33,799
for future classes

38
00:01:30,439 --> 00:01:36,880
such as a future architecture 2005 for

39
00:01:33,799 --> 00:01:38,720
OS internals on RISC-V systems now the

40
00:01:36,880 --> 00:01:42,640
miscellaneous that I'm obliged to tell

41
00:01:38,720 --> 00:01:45,159
you up front is that this RISC V is not

42
00:01:42,640 --> 00:01:47,000
pronounced RISC V it's pronounced RISC-V

43
00:01:45,159 --> 00:01:49,600
and that's because this is the fifth

44
00:01:47,000 --> 00:01:52,479
iteration of the Berkeley reduced

45
00:01:49,600 --> 00:01:54,680
instruction set computer or RISC

46
00:01:52,479 --> 00:01:58,119
architecture and I think we all remember

47
00:01:54,680 --> 00:02:00,439
in hackers where Kate Libby told us that

48
00:01:58,119 --> 00:02:03,680
RISC architecture is going to change

49
00:02:00,439 --> 00:02:06,399
everything and Dade said yeah RISC is

50
00:02:03,680 --> 00:02:08,879
good so if you're curious about the

51
00:02:06,399 --> 00:02:10,840
genealogy of RISC-V and what RISC 1

52
00:02:08,879 --> 00:02:12,200
through 4 are you can go ahead and check

53
00:02:10,840 --> 00:02:14,319
that link at the bottom of the screen

54
00:02:12,200 --> 00:02:16,040
which will be posted after the video

55
00:02:14,319 --> 00:02:18,599
you'll see that by 1990 they were

56
00:02:16,040 --> 00:02:20,840
already on RISC 4 because they had

57
00:02:18,599 --> 00:02:23,200
started the RISC architecture back in

58
00:02:20,840 --> 00:02:25,280
the80s now perhaps you've heard of RISC

59
00:02:23,200 --> 00:02:27,239
5 but you don't know where exactly it's

60
00:02:25,280 --> 00:02:29,319
used these days but you're curious

61
00:02:27,239 --> 00:02:31,040
enough to take this class and so let me

62
00:02:29,319 --> 00:02:32,680
give you a couple of anecdotes about

63
00:02:31,040 --> 00:02:34,840
different places it's used and some of

64
00:02:32,680 --> 00:02:37,800
the players using it right now the first

65
00:02:34,840 --> 00:02:39,680
and most important one would be sci5

66
00:02:37,800 --> 00:02:42,519
this is a company that is founded by

67
00:02:39,680 --> 00:02:45,280
some of the core RISC-V Architects and

68
00:02:42,519 --> 00:02:48,080
consequently its whole goal is to push

69
00:02:45,280 --> 00:02:51,360
RISC-V forward as a useful industry

70
00:02:48,080 --> 00:02:54,000
usable technology to that end they focus

71
00:02:51,360 --> 00:02:56,040
on a important element of silicon

72
00:02:54,000 --> 00:02:59,760
ecosystems which is developing

73
00:02:56,040 --> 00:03:01,480
intellectual property or IPS so you

74
00:02:59,760 --> 00:03:03,319
create an IP and then you sell it to

75
00:03:01,480 --> 00:03:05,640
another company and then they can sort

76
00:03:03,319 --> 00:03:07,519
of just plug in play and put that thing

77
00:03:05,640 --> 00:03:09,959
into their existing silicon

78
00:03:07,519 --> 00:03:12,319
manufacturing so some particular subc

79
00:03:09,959 --> 00:03:14,400
core of a system on a chip could be a

80
00:03:12,319 --> 00:03:17,680
RISC-V core while other cores could

81
00:03:14,400 --> 00:03:19,640
use other Technologies and so sci5 is

82
00:03:17,680 --> 00:03:21,680
focused on spreading RISC-V

83
00:03:19,640 --> 00:03:23,720
technology through intellectual property

84
00:03:21,680 --> 00:03:25,640
development licensing selling and so

85
00:03:23,720 --> 00:03:27,640
forth although I should also say that

86
00:03:25,640 --> 00:03:29,319
they are one of the primary companies

87
00:03:27,640 --> 00:03:32,239
that's focusing on trying to show that

88
00:03:29,319 --> 00:03:34,360
RISC can work as a more high performance

89
00:03:32,239 --> 00:03:36,319
computing platform as well not just

90
00:03:34,360 --> 00:03:39,159
embedded systems but all the way up

91
00:03:36,319 --> 00:03:42,360
through mobile phones or small computers

92
00:03:39,159 --> 00:03:44,560
but today as I write this class in 2024

93
00:03:42,360 --> 00:03:47,040
the place where RISC-V is most prevalent

94
00:03:44,560 --> 00:03:49,239
is at the embedded system at the lower

95
00:03:47,040 --> 00:03:51,480
end of the architecture and so we can

96
00:03:49,239 --> 00:03:53,879
see embedded system makers like Western

97
00:03:51,480 --> 00:03:56,920
Digital who are makers of hard drives

98
00:03:53,879 --> 00:03:58,920
and solid state drives they use RISC-V

99
00:03:56,920 --> 00:04:00,920
for their hard drive controllers the

100
00:03:58,920 --> 00:04:03,040
little chip that's on the hard drive

101
00:04:00,920 --> 00:04:05,000
that tells it what to do and this is

102
00:04:03,040 --> 00:04:07,519
relevant to Firmware security people

103
00:04:05,000 --> 00:04:10,040
like myself because I remember back in

104
00:04:07,519 --> 00:04:12,000
2013 when there started to be security

105
00:04:10,040 --> 00:04:14,159
researchers who were finding

106
00:04:12,000 --> 00:04:16,440
vulnerabilities by which they could have

107
00:04:14,159 --> 00:04:19,160
arbitrary code execution on Western

108
00:04:16,440 --> 00:04:21,840
Digital drives firmware and indeed later

109
00:04:19,160 --> 00:04:24,880
on based on the leaks by Edward Snowden

110
00:04:21,840 --> 00:04:27,479
in 2013 it was recognized that places

111
00:04:24,880 --> 00:04:30,080
like the NSA may already have hard drive

112
00:04:27,479 --> 00:04:33,080
firmware infection capability and also

113
00:04:30,080 --> 00:04:35,560
later on in 2015 kasperski showed

114
00:04:33,080 --> 00:04:37,600
documentation of actual inth wild hard

115
00:04:35,560 --> 00:04:39,199
drive firmware infection capabilities

116
00:04:37,600 --> 00:04:42,400
that of course were heavily implied to

117
00:04:39,199 --> 00:04:43,639
be nsas so in 2013 the researchers would

118
00:04:42,400 --> 00:04:45,320
have been looking at hard drive

119
00:04:43,639 --> 00:04:47,840
controllers from Western Digital that

120
00:04:45,320 --> 00:04:49,440
were using arm-based systems but today

121
00:04:47,840 --> 00:04:50,960
if they wanted to do the same sort of

122
00:04:49,440 --> 00:04:53,240
research they would need to learn

123
00:04:50,960 --> 00:04:55,440
something about RISC-V similarly

124
00:04:53,240 --> 00:04:58,240
competitors like Seagate are also

125
00:04:55,440 --> 00:05:00,560
invested in the RISC P ecosystem and are

126
00:04:58,240 --> 00:05:02,560
using that for their controllers on

127
00:05:00,560 --> 00:05:04,479
their hard drives and solid state drives

128
00:05:02,560 --> 00:05:06,080
as well there's this awesome link here

129
00:05:04,479 --> 00:05:08,160
when I was just searching around for

130
00:05:06,080 --> 00:05:10,919
RISC-V things talking about the new

131
00:05:08,160 --> 00:05:12,560
Hammer technology heat assisted magnetic

132
00:05:10,919 --> 00:05:14,680
recording that's increasing the

133
00:05:12,560 --> 00:05:17,000
densities on hard drives and it's got a

134
00:05:14,680 --> 00:05:19,280
whole bunch of awesome words in the

135
00:05:17,000 --> 00:05:22,199
article like plasmonic writer

136
00:05:19,280 --> 00:05:25,360
nanophotonic laser photonic funnel and

137
00:05:22,199 --> 00:05:28,360
Quantum antenna Technologies so perhaps

138
00:05:25,360 --> 00:05:31,319
RISC-V is the least exciting of those

139
00:05:28,360 --> 00:05:33,360
keywords but RISC-V is definitely there

140
00:05:31,319 --> 00:05:35,919
now appropo to my current firmware

141
00:05:33,360 --> 00:05:38,039
security interests we have expressive

142
00:05:35,919 --> 00:05:39,759
which is a maker of Wi-Fi and Bluetooth

143
00:05:38,039 --> 00:05:43,199
chips Bluetooth being my current

144
00:05:39,759 --> 00:05:44,680
research focus and they decided in 2022

145
00:05:43,199 --> 00:05:47,160
that they were only going to be using

146
00:05:44,680 --> 00:05:48,840
RISC-V for all their new chips and so

147
00:05:47,160 --> 00:05:50,560
they already have chips that are out and

148
00:05:48,840 --> 00:05:52,520
they said we like it well enough that

149
00:05:50,560 --> 00:05:55,759
we're just going to keep going with all

150
00:05:52,520 --> 00:05:58,680
RISC-V from now on in the automotive

151
00:05:55,759 --> 00:06:01,680
sector there's Renaissance as a maker of

152
00:05:58,680 --> 00:06:04,120
microcontroller used by vehicles and in

153
00:06:01,680 --> 00:06:06,199
2021 they announced a partnership with

154
00:06:04,120 --> 00:06:08,039
sci5 and they said they were going to

155
00:06:06,199 --> 00:06:11,199
work on Automotive Systems that were

156
00:06:08,039 --> 00:06:13,080
RISC 5based and in 2023 they did release

157
00:06:11,199 --> 00:06:14,919
their first RISC 5based platform

158
00:06:13,080 --> 00:06:16,479
although when we look at their page for

159
00:06:14,919 --> 00:06:19,000
the current chips that are out they say

160
00:06:16,479 --> 00:06:21,639
that they are focused on targeting motor

161
00:06:19,000 --> 00:06:23,680
control applications and it's for

162
00:06:21,639 --> 00:06:26,080
efficient handling of brushless DC

163
00:06:23,680 --> 00:06:28,199
motors so that's an interesting use case

164
00:06:26,080 --> 00:06:30,759
I wasn't even aware of and the final

165
00:06:28,199 --> 00:06:33,840
anecdote is that Bosch infinion Nordic

166
00:06:30,759 --> 00:06:36,440
nxp and Qualcomm have invested in a

167
00:06:33,840 --> 00:06:40,360
joint company in 2023 it went through

168
00:06:36,440 --> 00:06:43,960
the regulatory approvals and in 2024 it

169
00:06:40,360 --> 00:06:46,599
was named quintus and so this jointly

170
00:06:43,960 --> 00:06:48,560
invested company is for the explicit

171
00:06:46,599 --> 00:06:50,800
purpose of furthering their goals of

172
00:06:48,560 --> 00:06:53,319
using RISC-V and creating more

173
00:06:50,800 --> 00:06:55,400
standardization in the ecosystem so that

174
00:06:53,319 --> 00:06:57,599
they can all benefit from the RISC-V

175
00:06:55,400 --> 00:06:59,680
Technologies without fragmentation that

176
00:06:57,599 --> 00:07:01,759
can occur when you have an open source

177
00:06:59,680 --> 00:07:03,280
instruction set architecture that people

178
00:07:01,759 --> 00:07:05,479
can just customize to their hart's

179
00:07:03,280 --> 00:07:07,879
content so those are some places that

180
00:07:05,479 --> 00:07:09,720
RISC-V is used already today but why

181
00:07:07,879 --> 00:07:11,560
should you learn about RISC-V why is it

182
00:07:09,720 --> 00:07:13,560
a technology that's going to be worth

183
00:07:11,560 --> 00:07:15,120
knowing in the future why is it not just

184
00:07:13,560 --> 00:07:18,199
going to be relegated to the Dustbin of

185
00:07:15,120 --> 00:07:20,199
History well in my humble opinion RISC

186
00:07:18,199 --> 00:07:22,039
five is important to know about because

187
00:07:20,199 --> 00:07:24,440
there is a fundamental economic

188
00:07:22,039 --> 00:07:27,000
incentive that will lead to more and

189
00:07:24,440 --> 00:07:29,360
more silicon makers adopting it and it

190
00:07:27,000 --> 00:07:32,199
will displace more and more Al

191
00:07:29,360 --> 00:07:34,039
alternative RISC architectures myips is

192
00:07:32,199 --> 00:07:36,400
a good example because myips has been on

193
00:07:34,039 --> 00:07:39,319
the downswing for a while holding on

194
00:07:36,400 --> 00:07:42,120
primarily in some networking spaces but

195
00:07:39,319 --> 00:07:43,919
arm of course is the big player in

196
00:07:42,120 --> 00:07:46,000
RISC-based architectures and especially

197
00:07:43,919 --> 00:07:48,840
embedded systems and they are definitely

198
00:07:46,000 --> 00:07:51,800
feeling the heat from RISC-V and so the

199
00:07:48,840 --> 00:07:54,400
core issue is that arm is a company that

200
00:07:51,800 --> 00:07:56,440
makes its money through licensing so

201
00:07:54,400 --> 00:07:58,120
they don't actually build the Silicon

202
00:07:56,440 --> 00:08:00,039
they license out the intellectual

203
00:07:58,120 --> 00:08:02,159
property to other companies ianes to

204
00:08:00,039 --> 00:08:04,800
build the Silicon fundamentally if you

205
00:08:02,159 --> 00:08:07,000
want to build a chip that is based on

206
00:08:04,800 --> 00:08:08,520
arms instruction set architectures

207
00:08:07,000 --> 00:08:12,479
you're going to be paying a licensing

208
00:08:08,520 --> 00:08:15,120
fee so companies necessarily will say

209
00:08:12,479 --> 00:08:17,159
well if I can use RISC-V and not pay

210
00:08:15,120 --> 00:08:20,080
a licensing fee why should I pay you a

211
00:08:17,159 --> 00:08:22,720
licensing fee arm and arm is quite

212
00:08:20,080 --> 00:08:24,960
concerned about this fact it makes RISC

213
00:08:22,720 --> 00:08:27,159
5 a fundamental threat to its core

214
00:08:24,960 --> 00:08:29,479
business model and indeed when you tell

215
00:08:27,159 --> 00:08:31,759
silicon makers hey you can still sell

216
00:08:29,479 --> 00:08:34,240
chips but just not pay the arm licensing

217
00:08:31,759 --> 00:08:36,800
fees that you're paying today well they

218
00:08:34,240 --> 00:08:39,399
say you had my curiosity but now you

219
00:08:36,800 --> 00:08:42,159
have my attention another fundamental

220
00:08:39,399 --> 00:08:44,120
reason why RISC-V isn't worth knowing is

221
00:08:42,159 --> 00:08:46,519
because we've actually seen this sort of

222
00:08:44,120 --> 00:08:49,240
business Dynamic play out before in the

223
00:08:46,519 --> 00:08:52,360
context of Linux and other unix-based

224
00:08:49,240 --> 00:08:56,600
operating systems previously there were

225
00:08:52,360 --> 00:08:58,800
things like ax hpu or Solaris which were

226
00:08:56,600 --> 00:08:59,760
fundamentally proprietary Unix

227
00:08:58,800 --> 00:09:01,399
derivative

228
00:08:59,760 --> 00:09:03,000
where you would have to pay some company

229
00:09:01,399 --> 00:09:05,560
for the privilege of using that

230
00:09:03,000 --> 00:09:07,279
operating system Linux came in and it

231
00:09:05,560 --> 00:09:09,600
fundamentally changed the economic

232
00:09:07,279 --> 00:09:11,920
models as companies saw they could do

233
00:09:09,600 --> 00:09:14,240
the same thing with Linux but not have

234
00:09:11,920 --> 00:09:15,839
to pay anyone for it other than of

235
00:09:14,240 --> 00:09:17,880
course their system administrators

236
00:09:15,839 --> 00:09:20,519
programmers but those are the same sort

237
00:09:17,880 --> 00:09:23,959
of people that they were paying anyways

238
00:09:20,519 --> 00:09:26,360
so in that sense RISC-V is the Linux of

239
00:09:23,959 --> 00:09:29,200
instruction set architectures it is a

240
00:09:26,360 --> 00:09:32,000
open-source instruction set architecture

241
00:09:29,200 --> 00:09:34,120
and consequently it benefits from all of

242
00:09:32,000 --> 00:09:36,279
the advantages of open- source things

243
00:09:34,120 --> 00:09:38,240
but if RISC-V is the Linux of

244
00:09:36,279 --> 00:09:39,839
instructions set architectures I

245
00:09:38,240 --> 00:09:42,240
personally like to think of open

246
00:09:39,839 --> 00:09:44,880
security training 2 as the Linux of

247
00:09:42,240 --> 00:09:47,360
training platforms it's open source and

248
00:09:44,880 --> 00:09:49,519
fundamentally I think longterm that

249
00:09:47,360 --> 00:09:51,959
benefit is going to eat some other

250
00:09:49,519 --> 00:09:53,959
platforms anyways throughout the rest of

251
00:09:51,959 --> 00:09:56,040
this class there is an optional book

252
00:09:53,959 --> 00:09:58,000
there will be only scant references to

253
00:09:56,040 --> 00:10:00,320
it so it is definitely not required

254
00:09:58,000 --> 00:10:02,760
reading but but if you'd like to pick up

255
00:10:00,320 --> 00:10:05,320
the RISC-V reader and open architecture

256
00:10:02,760 --> 00:10:07,720
Atlas by Patterson and Waterman those

257
00:10:05,320 --> 00:10:10,680
are some of those architects of the RISC

258
00:10:07,720 --> 00:10:12,600
5 ISA and there is at least a couple of

259
00:10:10,680 --> 00:10:14,680
diagrams in the class that come from

260
00:10:12,600 --> 00:10:17,399
that book that are fundamentally more

261
00:10:14,680 --> 00:10:19,760
valuable than what you find in the open

262
00:10:17,399 --> 00:10:21,480
documentation but let's finally get into

263
00:10:19,760 --> 00:10:22,720
what we're going to learn in this class

264
00:10:21,480 --> 00:10:24,800
in this class we're going to see

265
00:10:22,720 --> 00:10:27,240
high-level source code like this hello

266
00:10:24,800 --> 00:10:29,399
OST2 and it could look like this here's

267
00:10:27,240 --> 00:10:33,519
an example of what that code comp piles

268
00:10:29,399 --> 00:10:35,839
down to with GCC 7.3 on JS Linux that is

269
00:10:33,519 --> 00:10:38,079
a JavaScript based Linux that runs

270
00:10:35,839 --> 00:10:40,360
entirely in the browser use and it

271
00:10:38,079 --> 00:10:42,120
actually does have support for RISC-V so

272
00:10:40,360 --> 00:10:44,519
if you compiled it with that you'd get

273
00:10:42,120 --> 00:10:46,880
this if you used compiler Explorer at

274
00:10:44,519 --> 00:10:49,440
God bolt. org then you would see this

275
00:10:46,880 --> 00:10:52,320
code instead but that alternatively

276
00:10:49,440 --> 00:10:55,240
could be represented by compilation with

277
00:10:52,320 --> 00:10:57,079
clay in compiler Explorer and compiler

278
00:10:55,240 --> 00:10:58,800
Explorer can show us what GCC might

279
00:10:57,079 --> 00:11:00,760
compile what clang might compile so all

280
00:10:58,800 --> 00:11:02,680
sort sorts of different options but

281
00:11:00,760 --> 00:11:04,279
fundamentally it's going to boil down to

282
00:11:02,680 --> 00:11:06,240
this in our class we're going to be

283
00:11:04,279 --> 00:11:10,360
using GCC

284
00:11:06,240 --> 00:11:12,560
1.4.0 inside of qemu a emulation

285
00:11:10,360 --> 00:11:15,800
environment where it will be supporting

286
00:11:12,560 --> 00:11:19,480
RISC-V 64-bit and so if we compiled our

287
00:11:15,800 --> 00:11:21,720
hello OST2 with optimization for size it

288
00:11:19,480 --> 00:11:23,639
would boil down to these assembly

289
00:11:21,720 --> 00:11:25,880
instructions but I want you to take

290
00:11:23,639 --> 00:11:27,399
hart there's actually not that many

291
00:11:25,880 --> 00:11:29,680
assembly instructions that you really

292
00:11:27,399 --> 00:11:32,120
need to know in order to understand the

293
00:11:29,680 --> 00:11:34,279
majority of code that's out there by one

294
00:11:32,120 --> 00:11:36,600
estimation so I've cited it here at the

295
00:11:34,279 --> 00:11:39,920
bottom of the page just 20 Instructions

296
00:11:36,600 --> 00:11:43,880
make up 91% of the code for 32-bit RISC

297
00:11:39,920 --> 00:11:46,040
5 and 86% of the code for 64-bit and

298
00:11:43,880 --> 00:11:48,720
indeed because they did this statistical

299
00:11:46,040 --> 00:11:51,600
gathering over the spec 2006

300
00:11:48,720 --> 00:11:53,399
benchmarking programs this actually is

301
00:11:51,600 --> 00:11:55,000
over representing some things like

302
00:11:53,399 --> 00:11:56,839
floating point that we don't cover in

303
00:11:55,000 --> 00:11:58,440
this class it's showing up up here as

304
00:11:56,839 --> 00:12:00,160
one of the most frequent things but

305
00:11:58,440 --> 00:12:02,200
that's just because these are

306
00:12:00,160 --> 00:12:03,920
benchmarking tools so I wanted to see

307
00:12:02,200 --> 00:12:05,680
something a little more realistic to

308
00:12:03,920 --> 00:12:07,680
what we're going to see in the class so

309
00:12:05,680 --> 00:12:09,839
I went ahead and collected Statistics

310
00:12:07,680 --> 00:12:12,600
over our final lab in this class the

311
00:12:09,839 --> 00:12:15,880
Carnegie melon binary bomb compiled for

312
00:12:12,600 --> 00:12:18,720
RISC-V and this is what I saw total we

313
00:12:15,880 --> 00:12:20,920
have 53 instructions but you can see

314
00:12:18,720 --> 00:12:23,120
that the frequency of a few of these

315
00:12:20,920 --> 00:12:25,639
down at the tail end of the graph is

316
00:12:23,120 --> 00:12:27,760
extremely small you may not actually see

317
00:12:25,639 --> 00:12:29,920
those those may be in there just as

318
00:12:27,760 --> 00:12:31,720
extra code that's compiled filed in as a

319
00:12:29,920 --> 00:12:35,079
consequence of using the standard C

320
00:12:31,720 --> 00:12:37,600
libraries for instance so 53 for total

321
00:12:35,079 --> 00:12:40,040
possible understanding only 25

322
00:12:37,600 --> 00:12:42,800
instructions to get the top 90% of the

323
00:12:40,040 --> 00:12:45,279
code and only 10 instructions to get the

324
00:12:42,800 --> 00:12:48,560
top 2/3

325
00:12:45,279 --> 00:12:51,000
67.5% so 10 instructions gets you 2/3 of

326
00:12:48,560 --> 00:12:53,199
the code and you've already seen seven

327
00:12:51,000 --> 00:12:55,800
instructions in just the hello world so

328
00:12:53,199 --> 00:12:58,320
we're going to be learning the RISC-V

329
00:12:55,800 --> 00:13:01,079
base integer ISA instruction set

330
00:12:58,320 --> 00:13:03,600
architecture RV32I that has 40

331
00:13:01,079 --> 00:13:05,720
instructions but it's an easy 40 in that

332
00:13:03,600 --> 00:13:08,360
it's mostly variance on the same thing

333
00:13:05,720 --> 00:13:11,680
there's actually let's say I want to say

334
00:13:08,360 --> 00:13:13,120
15 of those are just shifts so then

335
00:13:11,680 --> 00:13:17,600
we're also going to learn the base

336
00:13:13,120 --> 00:13:19,959
64-bit ISA RV64I and that just adds 15

337
00:13:17,600 --> 00:13:21,760
instructions above and beyond the 32-bit

338
00:13:19,959 --> 00:13:23,920
and those are just variants on the same

339
00:13:21,760 --> 00:13:26,000
theme but they're using 32-bit registers

340
00:13:23,920 --> 00:13:27,519
while you're in 64-bit mode and that's

341
00:13:26,000 --> 00:13:29,519
the only real difference functionally

342
00:13:27,519 --> 00:13:32,160
they're exactly the same and like I said

343
00:13:29,519 --> 00:13:34,360
you've already seen seven instructions

344
00:13:32,160 --> 00:13:37,360
just looking at the simplest version of

345
00:13:34,360 --> 00:13:38,680
hello OST2 so all in all all of these

346
00:13:37,360 --> 00:13:41,040
instructions in this class are going to

347
00:13:38,680 --> 00:13:43,639
boil down to these nine themes loads and

348
00:13:41,040 --> 00:13:46,920
stores ads and subtracts Boolean

349
00:13:43,639 --> 00:13:49,639
operations with and or an exor shift

350
00:13:46,920 --> 00:13:52,480
left shift right setting a register one

351
00:13:49,639 --> 00:13:54,440
if it's less than some other values

352
00:13:52,480 --> 00:13:55,920
branching if equal not equal less than

353
00:13:54,440 --> 00:13:57,800
or greater than so just branch

354
00:13:55,920 --> 00:14:00,440
instructions to go somewhere based on

355
00:13:57,800 --> 00:14:02,880
some condition jumping and linking so

356
00:14:00,440 --> 00:14:04,800
that's an important uh characteristic

357
00:14:02,880 --> 00:14:06,600
the link element of jumping somewhere

358
00:14:04,800 --> 00:14:09,040
linking to a register for how to get

359
00:14:06,600 --> 00:14:11,759
back and then loading 20 bit upper

360
00:14:09,040 --> 00:14:14,040
immediates and adding 208 bit upper

361
00:14:11,759 --> 00:14:15,399
immediates to the program counter so not

362
00:14:14,040 --> 00:14:17,079
all of that needs to make sense yet

363
00:14:15,399 --> 00:14:19,600
we'll get into all of it but this is

364
00:14:17,079 --> 00:14:22,480
just to say even if it's 40 instructions

365
00:14:19,600 --> 00:14:24,680
or 55 if you count the 64-bit stuff this

366
00:14:22,480 --> 00:14:26,600
is still really just these nine themes

367
00:14:24,680 --> 00:14:29,040
and stuff like add and subtract you know

368
00:14:26,600 --> 00:14:30,680
load and store that's going to be simple

369
00:14:29,040 --> 00:14:32,800
so there's not really going to be a lot

370
00:14:30,680 --> 00:14:35,639
of complexity here and that's one of the

371
00:14:32,800 --> 00:14:37,519
benefits of RISC architectures simpler

372
00:14:35,639 --> 00:14:39,279
instructions that don't do a whole lot

373
00:14:37,519 --> 00:14:41,440
of things sometimes you might need to

374
00:14:39,279 --> 00:14:43,639
use more instructions to do something

375
00:14:41,440 --> 00:14:46,480
than you would on a complex architecture

376
00:14:43,639 --> 00:14:49,120
like Intel but it's more instructions

377
00:14:46,480 --> 00:14:50,680
that are each simpler rather than more

378
00:14:49,120 --> 00:14:53,040
instructions that are each more

379
00:14:50,680 --> 00:14:55,959
complicated to do more things in one

380
00:14:53,040 --> 00:14:57,920
instruction so the outcome of this class

381
00:14:55,959 --> 00:15:00,720
is that by the end of the class I expect

382
00:14:57,920 --> 00:15:02,279
you to be able to know all the RISC-V

383
00:15:00,720 --> 00:15:04,720
general purpose registers and their

384
00:15:02,279 --> 00:15:06,959
alternative ABI or application binary

385
00:15:04,720 --> 00:15:09,720
interface names those are the more human

386
00:15:06,959 --> 00:15:11,800
friendly names understand how data like

387
00:15:09,720 --> 00:15:15,040
local variables frame pointers and

388
00:15:11,800 --> 00:15:17,160
return addresses are stored on the stack

389
00:15:15,040 --> 00:15:18,720
understand function calling conventions

390
00:15:17,160 --> 00:15:21,519
so what are the conventions for when we

391
00:15:18,720 --> 00:15:23,639
call into and return out of a function

392
00:15:21,519 --> 00:15:25,920
be comfortable writing some C source

393
00:15:23,639 --> 00:15:28,319
code and compiling it down and reading

394
00:15:25,920 --> 00:15:30,480
the consequent assembly language that

395
00:15:28,319 --> 00:15:32,759
you would see if you looked at it in a

396
00:15:30,480 --> 00:15:34,959
debugger and I of course want you to

397
00:15:32,759 --> 00:15:36,959
feel comfortable reading the fun manual

398
00:15:34,959 --> 00:15:38,920
on your own because I would be doing you

399
00:15:36,959 --> 00:15:41,120
a disservice if I didn't give you the

400
00:15:38,920 --> 00:15:43,120
tools to continue to explore on your own

401
00:15:41,120 --> 00:15:44,839
without my help after the class so I

402
00:15:43,120 --> 00:15:46,800
really want to teach you to fish in this

403
00:15:44,839 --> 00:15:49,519
class and make sure that you can read

404
00:15:46,800 --> 00:15:51,279
the fun manual and finally you should be

405
00:15:49,519 --> 00:15:53,839
able to read assembly well enough to

406
00:15:51,279 --> 00:15:56,360
determine the expected inputs and

407
00:15:53,839 --> 00:15:58,240
influence the control flow of the opaque

408
00:15:56,360 --> 00:16:00,680
binary that we're going to use as our

409
00:15:58,240 --> 00:16:03,600
final lab which is the famous or

410
00:16:00,680 --> 00:16:06,319
inFAMOUS Carnegie melon binary bomb it's

411
00:16:03,600 --> 00:16:08,839
just an opaque binary where it just says

412
00:16:06,319 --> 00:16:10,480
hey hi I'm the bomb you put some input

413
00:16:08,839 --> 00:16:12,680
in and if you don't give the right input

414
00:16:10,480 --> 00:16:15,319
it blows up you want to avoid the

415
00:16:12,680 --> 00:16:16,959
blowing up so you need to read the code

416
00:16:15,319 --> 00:16:19,199
and figure out what it expects out of

417
00:16:16,959 --> 00:16:21,319
you to not blow up so yeah that's the

418
00:16:19,199 --> 00:16:23,160
outcomes of this class all of that will

419
00:16:21,319 --> 00:16:26,040
be yours so I hope you're excited to

420
00:16:23,160 --> 00:16:29,040
learn more about RISC-V assembly let's

421
00:16:26,040 --> 00:16:29,040
go

