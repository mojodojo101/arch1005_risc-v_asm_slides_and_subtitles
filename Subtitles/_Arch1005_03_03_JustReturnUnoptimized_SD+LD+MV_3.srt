1
00:00:00,040 --> 00:00:04,200
so I just want to point out a little

2
00:00:01,360 --> 00:00:06,160
fearful symmetry in the just return code

3
00:00:04,200 --> 00:00:09,120
and that is the fact that we have an add

4
00:00:06,160 --> 00:00:11,719
I of -16 to the stack pointer at the

5
00:00:09,120 --> 00:00:13,639
beginning to allocate stack space that

6
00:00:11,719 --> 00:00:16,720
is allocation of the stack frame for

7
00:00:13,639 --> 00:00:19,480
main and the ADDI of positive 16 at the

8
00:00:16,720 --> 00:00:21,800
end to get rid of the stack space so

9
00:00:19,480 --> 00:00:24,320
stack moved down to allocate space for

10
00:00:21,800 --> 00:00:26,599
main stack moved back up to get rid of

11
00:00:24,320 --> 00:00:29,160
it perfectly balanced as all things

12
00:00:26,599 --> 00:00:31,800
should be and then also we see the

13
00:00:29,160 --> 00:00:34,559
manipulation of the frame pointer the s0

14
00:00:31,800 --> 00:00:36,200
so I would like it if this used the FP

15
00:00:34,559 --> 00:00:38,680
instead of the s0 but you're going to

16
00:00:36,200 --> 00:00:40,920
have to get used to remembering that s0

17
00:00:38,680 --> 00:00:44,280
could actually be the frame pointer so

18
00:00:40,920 --> 00:00:47,559
we have the store of the existing frame

19
00:00:44,280 --> 00:00:49,440
pointer to SP plus 8 so store off and

20
00:00:47,559 --> 00:00:53,239
save the existing frame pointer at the

21
00:00:49,440 --> 00:00:55,760
beginning and upon exit load up and get

22
00:00:53,239 --> 00:00:57,680
the previous saved version of the frame

23
00:00:55,760 --> 00:01:00,079
pointer back into the frame pointer

24
00:00:57,680 --> 00:01:02,000
register as it's exiting out and so

25
00:01:00,079 --> 00:01:04,479
these this is sort of the what we call

26
00:01:02,000 --> 00:01:06,960
the function prologue it's just doing

27
00:01:04,479 --> 00:01:08,920
standard stuff to allocate space store

28
00:01:06,960 --> 00:01:11,000
frame pointers and move the frame

29
00:01:08,920 --> 00:01:13,520
pointer down and then the function

30
00:01:11,000 --> 00:01:15,479
epilog is just move the frame pointer

31
00:01:13,520 --> 00:01:17,680
back up to where it was deallocate that

32
00:01:15,479 --> 00:01:19,759
stack space and return back to wherever

33
00:01:17,680 --> 00:01:22,280
we came from now at this point I will

34
00:01:19,759 --> 00:01:25,000
just make an observation that if we look

35
00:01:22,280 --> 00:01:28,560
at the addresses of our assembly we can

36
00:01:25,000 --> 00:01:31,600
see that from the ADDI to the SD is two

37
00:01:28,560 --> 00:01:34,600
bytes so this is address 628 this is

38
00:01:31,600 --> 00:01:36,520
address 62a and that is two bytes

39
00:01:34,600 --> 00:01:38,240
between them so there's a lot of these

40
00:01:36,520 --> 00:01:40,520
addresses that are only offset by two

41
00:01:38,240 --> 00:01:42,320
bytes and having one assembly

42
00:01:40,520 --> 00:01:44,240
instruction start two bytes after the

43
00:01:42,320 --> 00:01:45,640
next assembly instruction is a pretty

44
00:01:44,240 --> 00:01:47,560
good hint that there's probably

45
00:01:45,640 --> 00:01:50,200
compressed assembly instructions being

46
00:01:47,560 --> 00:01:52,600
used behind the scenes so at this point

47
00:01:50,200 --> 00:01:56,399
we could invoke the awesome chaos magic

48
00:01:52,600 --> 00:01:58,960
of Scarlet Witch but this turns out to

49
00:01:56,399 --> 00:02:00,759
have a whole big old Rabbit Hole worth

50
00:01:58,960 --> 00:02:02,479
of stuff that I don't want to deal with

51
00:02:00,759 --> 00:02:04,119
right now because that would make this

52
00:02:02,479 --> 00:02:06,000
video too long and this lesson too

53
00:02:04,119 --> 00:02:08,599
complex so I'm going to put the

54
00:02:06,000 --> 00:02:10,160
complexity of understanding the actual

55
00:02:08,599 --> 00:02:12,560
compressed assembly instructions that

56
00:02:10,160 --> 00:02:14,360
GCC happened to emit going to cover that

57
00:02:12,560 --> 00:02:17,440
a little bit later and we'll come back

58
00:02:14,360 --> 00:02:21,239
to it but for now instead I'm going to

59
00:02:17,440 --> 00:02:23,480
force a recompilation of this assembly

60
00:02:21,239 --> 00:02:25,840
specifically excluding compressed

61
00:02:23,480 --> 00:02:29,040
assembly instructions the way I do that

62
00:02:25,840 --> 00:02:33,200
is that I use this- M architecture M

63
00:02:29,040 --> 00:02:35,800
Arch March equals RV64I which is what I

64
00:02:33,200 --> 00:02:38,640
really want and then D so I just found

65
00:02:35,800 --> 00:02:40,319
that I have to give RV 64 ID otherwise

66
00:02:38,640 --> 00:02:42,200
it won't compile it was saying something

67
00:02:40,319 --> 00:02:43,920
like you don't have the D but it's

68
00:02:42,200 --> 00:02:45,640
needed for a calling convention

69
00:02:43,920 --> 00:02:49,680
something like that so whatever all I

70
00:02:45,640 --> 00:02:52,239
really care is that this is not RV 64 GC

71
00:02:49,680 --> 00:02:55,319
I don't want the inclusion of the

72
00:02:52,239 --> 00:02:58,560
compressed assembly instructions so if I

73
00:02:55,319 --> 00:03:03,319
compile it this way then I get the exact

74
00:02:58,560 --> 00:03:05,720
same code at I SD ADI Li etc etc so the

75
00:03:03,319 --> 00:03:07,840
code is all exactly the same the only

76
00:03:05,720 --> 00:03:10,120
difference is the fact that now every

77
00:03:07,840 --> 00:03:12,440
assembly instruction is encoded in its 4

78
00:03:10,120 --> 00:03:13,840
byte form because we have not told the

79
00:03:12,440 --> 00:03:16,200
compiler that it's okay to use

80
00:03:13,840 --> 00:03:18,560
compressed 2 byte assembly instructions

81
00:03:16,200 --> 00:03:22,159
and with that I will now actually invoke

82
00:03:18,560 --> 00:03:22,159
Scarlet Witch's power no more

83
00:03:22,640 --> 00:03:27,120
aliases and I go ahead and blow those

84
00:03:25,000 --> 00:03:30,040
away and what I see is that although we

85
00:03:27,120 --> 00:03:32,799
still have the SD and the LD that move

86
00:03:30,040 --> 00:03:35,120
assembly instruction just went away so

87
00:03:32,799 --> 00:03:38,360
indeed it is shocking it is outrageous

88
00:03:35,120 --> 00:03:40,720
the move is actually just an ADDI with

89
00:03:38,360 --> 00:03:43,640
a register source register destination

90
00:03:40,720 --> 00:03:46,120
and an immediate of zero so basically

91
00:03:43,640 --> 00:03:49,080
add zero to your source and move it and

92
00:03:46,120 --> 00:03:50,480
store it back into that destination so

93
00:03:49,080 --> 00:03:51,799
there's one little bit of Monkey

94
00:03:50,480 --> 00:03:54,360
Business which I want to call your

95
00:03:51,799 --> 00:03:55,840
attention to because you may have read

96
00:03:54,360 --> 00:03:58,599
this assembly and you may have been

97
00:03:55,840 --> 00:04:01,439
giving it the side eye and said hey

98
00:03:58,599 --> 00:04:03,439
couldn't the compiler have just moved 65

99
00:04:01,439 --> 00:04:06,400
directly into a0 instead of moving it

100
00:04:03,439 --> 00:04:09,720
into a5 and then moving a5 and a z that

101
00:04:06,400 --> 00:04:12,480
seems very inefficient that seems very

102
00:04:09,720 --> 00:04:13,599
unoptimal why would they do that and yes

103
00:04:12,480 --> 00:04:16,199
they could have done that they could

104
00:04:13,599 --> 00:04:19,120
have moved 65 directly into a z which is

105
00:04:16,199 --> 00:04:21,239
why this is unoptimized code yes we are

106
00:04:19,120 --> 00:04:24,479
explicitly telling it to be unoptimized

107
00:04:21,239 --> 00:04:27,360
we are giving the- o0 saying set your

108
00:04:24,479 --> 00:04:30,000
optimization level to zero because when

109
00:04:27,360 --> 00:04:31,240
we do that we get nice simple code

110
00:04:30,000 --> 00:04:34,080
that's easy to understand the

111
00:04:31,240 --> 00:04:36,360
correspondences back to the highle

112
00:04:34,080 --> 00:04:38,840
language and indeed when we tell that to

113
00:04:36,360 --> 00:04:41,560
the compiler unoptimized code generated

114
00:04:38,840 --> 00:04:44,600
by Koller is basically a very rote

115
00:04:41,560 --> 00:04:47,720
replace some branch of a pars abstract

116
00:04:44,600 --> 00:04:50,120
syntax tree with some specific assembly

117
00:04:47,720 --> 00:04:51,479
you know y so got a chunk of the tree

118
00:04:50,120 --> 00:04:52,840
you replace it with assembly another

119
00:04:51,479 --> 00:04:55,479
chunk of the tree replace it with

120
00:04:52,840 --> 00:04:57,880
assembly and that leads to unnecessary

121
00:04:55,479 --> 00:04:59,919
inclusions that needs leads to just

122
00:04:57,880 --> 00:05:01,960
inefficient code that's why it's the

123
00:04:59,919 --> 00:05:04,240
unoptimized version It's just simple

124
00:05:01,960 --> 00:05:06,120
wrote and it allows us to see patterns a

125
00:05:04,240 --> 00:05:08,280
lot of the times directly between

126
00:05:06,120 --> 00:05:10,199
statement one in the C source code and

127
00:05:08,280 --> 00:05:13,000
you know assembly instruction two3

128
00:05:10,199 --> 00:05:15,320
statement 2 assembly instruction 345 and

129
00:05:13,000 --> 00:05:17,840
so forth so anyways I'm just bringing

130
00:05:15,320 --> 00:05:20,199
this up because inevitably whenever we

131
00:05:17,840 --> 00:05:22,080
start out with very unoptimized

132
00:05:20,199 --> 00:05:24,120
explicitly intentionally unoptimized

133
00:05:22,080 --> 00:05:25,960
code people always ask the question like

134
00:05:24,120 --> 00:05:28,080
why did it do this and I just want to

135
00:05:25,960 --> 00:05:30,000
kind of head you off at the pass for the

136
00:05:28,080 --> 00:05:31,440
rest of the class when you you see some

137
00:05:30,000 --> 00:05:33,560
weird code that doesn't make sense and

138
00:05:31,440 --> 00:05:35,680
you say like why did it do this you got

139
00:05:33,560 --> 00:05:38,319
to go look and say did we do it as

140
00:05:35,680 --> 00:05:40,280
unoptimized code and if so then that's

141
00:05:38,319 --> 00:05:42,840
just you know the way the compiler heris

142
00:05:40,280 --> 00:05:45,000
STS all broke down it said when I was

143
00:05:42,840 --> 00:05:47,000
parsing the code I just did something

144
00:05:45,000 --> 00:05:48,840
very simple whatever their Baseline is

145
00:05:47,000 --> 00:05:51,360
without optimization and that often

146
00:05:48,840 --> 00:05:53,680
leads to weird inefficiencies okay final

147
00:05:51,360 --> 00:05:55,880
takeaways from just return optimization

148
00:05:53,680 --> 00:05:57,800
level zero first of all Zeno hid the

149
00:05:55,880 --> 00:05:59,919
compressed instructions from us for some

150
00:05:57,800 --> 00:06:02,160
reason don't know why yet but we'll get

151
00:05:59,919 --> 00:06:03,919
back to it later and then also there's

152
00:06:02,160 --> 00:06:05,720
an interesting thing that even when

153
00:06:03,919 --> 00:06:07,560
there are no variables there's no local

154
00:06:05,720 --> 00:06:10,360
variables here there's no saved

155
00:06:07,560 --> 00:06:13,840
registers anything like that GCC seems

156
00:06:10,360 --> 00:06:15,919
to be over allocating hex1 16 bytes

157
00:06:13,840 --> 00:06:19,000
worth of space when the only thing it

158
00:06:15,919 --> 00:06:23,000
actually writes to the stack is hex 8 a

159
00:06:19,000 --> 00:06:25,599
stack pointer so we see the ad I of -16

160
00:06:23,000 --> 00:06:28,120
that is the allocation of 16 bytes this

161
00:06:25,599 --> 00:06:30,680
adding the 16 back later on that is the

162
00:06:28,120 --> 00:06:34,280
deallocation of 16 bites but the only

163
00:06:30,680 --> 00:06:37,120
thing it ever stores into the stack is

164
00:06:34,280 --> 00:06:40,599
the frame pointer this s0o register so

165
00:06:37,120 --> 00:06:42,919
it stores a 64-bit but it allocated 128

166
00:06:40,599 --> 00:06:45,160
bits worth of space so it's only using

167
00:06:42,919 --> 00:06:47,319
half the space why did it do that well

168
00:06:45,160 --> 00:06:49,680
unfortunately that's a mystery and the

169
00:06:47,319 --> 00:06:51,639
Mystery Machine goes rolling by here and

170
00:06:49,680 --> 00:06:54,680
it's going to introduce us to this

171
00:06:51,639 --> 00:06:56,639
mystery listy of a variety of mysteries

172
00:06:54,680 --> 00:06:58,240
that we will come back to later so

173
00:06:56,639 --> 00:07:00,879
that's the first thing in our mystery

174
00:06:58,240 --> 00:07:03,080
list why is GCC over allocating space

175
00:07:00,879 --> 00:07:04,840
for Save frame pointers we don't know

176
00:07:03,080 --> 00:07:06,879
right now but we'll come back to it once

177
00:07:04,840 --> 00:07:08,960
we learn a little bit more so in this

178
00:07:06,879 --> 00:07:12,520
example we picked up two new assembly

179
00:07:08,960 --> 00:07:15,080
instructions load dword and store dword

180
00:07:12,520 --> 00:07:17,280
again dwords are those 64-bit values You

181
00:07:15,080 --> 00:07:19,960
couldn't possibly have that down in the

182
00:07:17,280 --> 00:07:22,400
32-bit uh assembly instructions because

183
00:07:19,960 --> 00:07:24,080
they have 32-bit registers and so it

184
00:07:22,400 --> 00:07:27,199
wouldn't be making sense for them to

185
00:07:24,080 --> 00:07:30,000
load 64-bit values out of memory into

186
00:07:27,199 --> 00:07:31,879
registers that are only 32bits large and

187
00:07:30,000 --> 00:07:34,199
putting our picture back together we

188
00:07:31,879 --> 00:07:35,840
have the introduction of this move which

189
00:07:34,199 --> 00:07:38,720
is just another one of those Insidious

190
00:07:35,840 --> 00:07:41,400
pseudo instructions it's just an add ey

191
00:07:38,720 --> 00:07:45,000
but we also pop up the store dword and

192
00:07:41,400 --> 00:07:45,000
the load dword

