1
00:00:00,160 --> 00:00:05,319
so if you stepped through the code all

2
00:00:02,360 --> 00:00:07,160
the way past main plus 60 the last store

3
00:00:05,319 --> 00:00:09,200
to the stack you should have seen

4
00:00:07,160 --> 00:00:11,480
something like this we've got the sort

5
00:00:09,200 --> 00:00:13,440
of uninitialized alignment things that

6
00:00:11,480 --> 00:00:17,279
we tend to expect when we don't have

7
00:00:13,440 --> 00:00:20,119
exactly multiples of X10 for our local

8
00:00:17,279 --> 00:00:22,359
variables or we always have seen it thus

9
00:00:20,119 --> 00:00:25,480
far for the stack point or for the frame

10
00:00:22,359 --> 00:00:28,640
pointer rather so what went where well

11
00:00:25,480 --> 00:00:32,399
it looks like short a babe went right

12
00:00:28,640 --> 00:00:35,480
here at the lowest address and we had b

13
00:00:32,399 --> 00:00:37,800
of0 b of one B of one got set equal to

14
00:00:35,480 --> 00:00:40,120
babe but because B of one is an INT that

15
00:00:37,800 --> 00:00:41,360
necessarily would lead to sign extension

16
00:00:40,120 --> 00:00:43,680
so we would see that the most

17
00:00:41,360 --> 00:00:45,719
significant bit of babe is a one and

18
00:00:43,680 --> 00:00:48,079
that sign extends all ones which leads

19
00:00:45,719 --> 00:00:51,600
to hex FS for the upper bits of the

20
00:00:48,079 --> 00:00:54,960
32-bit value ffffff babe then we saw

21
00:00:51,600 --> 00:00:57,680
that b of4 had some math done on it and

22
00:00:54,960 --> 00:00:59,920
so this is just the result of that math

23
00:00:57,680 --> 00:01:02,640
and C was set to a hardcoded constant of

24
00:00:59,920 --> 00:01:04,400
of bboa bled blood again so again this

25
00:01:02,640 --> 00:01:06,960
is what you should see if you went

26
00:01:04,400 --> 00:01:09,240
through the code so the takeaway from

27
00:01:06,960 --> 00:01:11,920
this example is that it appears to be

28
00:01:09,240 --> 00:01:14,560
that all of the declared order of these

29
00:01:11,920 --> 00:01:17,360
things is stored exactly in the order of

30
00:01:14,560 --> 00:01:19,799
Declaration just you know upside down

31
00:01:17,360 --> 00:01:22,360
according to our diagram now also what

32
00:01:19,799 --> 00:01:24,079
we see here is uninitialized things for

33
00:01:22,360 --> 00:01:26,000
you know if we haven't actually touch

34
00:01:24,079 --> 00:01:27,720
those but there is one additional thing

35
00:01:26,000 --> 00:01:31,200
that we can see here and that is the

36
00:01:27,720 --> 00:01:35,200
fact that this short a which is only 16

37
00:01:31,200 --> 00:01:38,200
bits got itself a whole 8 bytes worth of

38
00:01:35,200 --> 00:01:40,840
space and so all this upper bytes are

39
00:01:38,200 --> 00:01:42,399
just uninitialized so you could imagine

40
00:01:40,840 --> 00:01:43,880
that all of this stuff could have been

41
00:01:42,399 --> 00:01:47,399
you know compressed together you could

42
00:01:43,880 --> 00:01:49,439
have had a followed by B of zero and

43
00:01:47,399 --> 00:01:51,200
then maybe you know one bite of unused

44
00:01:49,439 --> 00:01:53,000
space and then maybe B of one would have

45
00:01:51,200 --> 00:01:55,159
started here you could imagine different

46
00:01:53,000 --> 00:01:57,240
ways of actually organizing this stuff

47
00:01:55,159 --> 00:01:58,799
and putting it all together and we'll

48
00:01:57,240 --> 00:02:00,640
you know come back and talk about that a

49
00:01:58,799 --> 00:02:02,880
little bit more a little bit bit later

50
00:02:00,640 --> 00:02:06,119
but for now let's keep track of what we

51
00:02:02,880 --> 00:02:10,959
just collected and that was the LH load

52
00:02:06,119 --> 00:02:15,560
halfword sh store half word add IW so

53
00:02:10,959 --> 00:02:19,519
add immediate to a word sized register

54
00:02:15,560 --> 00:02:22,480
and add W add two word sized registers

55
00:02:19,519 --> 00:02:25,400
together putting it like this we got our

56
00:02:22,480 --> 00:02:28,080
halfword and half word store and load

57
00:02:25,400 --> 00:02:31,840
we've got our adiw with the compressed

58
00:02:28,080 --> 00:02:34,959
adiw we've got our W with the compressed

59
00:02:31,840 --> 00:02:38,840
ADW and we picked up a compressed LUI

60
00:02:34,959 --> 00:02:38,840
to go along with our LUI over here

