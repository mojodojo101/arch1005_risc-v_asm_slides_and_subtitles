1
00:00:01,160 --> 00:00:05,759
let's learn about the RISC-V ISA family

2
00:00:03,520 --> 00:00:07,799
now what an ISA is actually matters

3
00:00:05,759 --> 00:00:09,960
quite a lot in this class and so I

4
00:00:07,799 --> 00:00:11,599
prefer this definition from the arm

5
00:00:09,960 --> 00:00:13,519
glossary rather than the Wikipedia

6
00:00:11,599 --> 00:00:16,000
definition an instruction set

7
00:00:13,519 --> 00:00:17,800
architecture or ISA is part of the

8
00:00:16,000 --> 00:00:20,519
abstract model of a computer that

9
00:00:17,800 --> 00:00:22,800
defines how the CPU is controlled by the

10
00:00:20,519 --> 00:00:24,359
software so this has to do with the

11
00:00:22,800 --> 00:00:26,119
assembly language and basically the

12
00:00:24,359 --> 00:00:28,080
assembly language is how the CPU is

13
00:00:26,119 --> 00:00:30,720
controlled by the software now from the

14
00:00:28,080 --> 00:00:34,000
RISC documentation it says a RISC-V ISA

15
00:00:30,720 --> 00:00:36,760
is defined as a base integer ISA which

16
00:00:34,000 --> 00:00:39,559
must be present in any implementation

17
00:00:36,760 --> 00:00:41,320
plus optional extensions to the base ISA

18
00:00:39,559 --> 00:00:44,280
it says although it is convenient to

19
00:00:41,320 --> 00:00:46,760
speak of the RISC-V ISA RISC-V is

20
00:00:44,280 --> 00:00:48,879
actually a family of related ISAs of

21
00:00:46,760 --> 00:00:50,879
which there are currently four base ISAs

22
00:00:48,879 --> 00:00:53,440
so that's my emphasis risk 5 is

23
00:00:50,879 --> 00:00:56,800
actually a family of related ISAs so

24
00:00:53,440 --> 00:00:56,800
let's learn about the ISA

25
00:00:58,920 --> 00:01:03,719
family

26
00:01:00,140 --> 00:01:06,880
[Music]

27
00:01:03,719 --> 00:01:10,880
the ISA family so here we go we've

28
00:01:06,880 --> 00:01:15,520
got the RV64I 64-bit integer RV32I

29
00:01:10,880 --> 00:01:19,640
32bit integer RV128I 128bit integer and

30
00:01:15,520 --> 00:01:22,799
RV32E 32bit embedded so let's learn

31
00:01:19,640 --> 00:01:27,079
more about each of them and the RV 32bit

32
00:01:22,799 --> 00:01:28,840
I integer ISA you have 32bit pointers

33
00:01:27,079 --> 00:01:31,600
and all the registers and pointers are

34
00:01:28,840 --> 00:01:33,799
32 bits wide and there are 32 general

35
00:01:31,600 --> 00:01:35,920
purpose registers and a single special

36
00:01:33,799 --> 00:01:37,320
purpose program counter register that

37
00:01:35,920 --> 00:01:39,680
points at the next instruction to

38
00:01:37,320 --> 00:01:43,040
execute and there are 40 instructions in

39
00:01:39,680 --> 00:01:44,439
this RV 32bit I ISA so these are the

40
00:01:43,040 --> 00:01:46,159
instructions we're going to be learning

41
00:01:44,439 --> 00:01:48,680
about throughout the class you know they

42
00:01:46,159 --> 00:01:50,479
actually break up into different groups

43
00:01:48,680 --> 00:01:52,960
so here we've got some loads here we've

44
00:01:50,479 --> 00:01:55,240
got some stores got some arithmetic

45
00:01:52,960 --> 00:01:56,560
Boolean operations and so forth so we'll

46
00:01:55,240 --> 00:02:00,560
learn about all that throughout the rest

47
00:01:56,560 --> 00:02:03,360
of the class in the RV32E for embedded

48
00:02:00,560 --> 00:02:05,479
ISA you still have 32-bit registers and

49
00:02:03,360 --> 00:02:07,960
pointers but the key thing is that they

50
00:02:05,479 --> 00:02:10,560
cut it down from having 32 general

51
00:02:07,960 --> 00:02:12,160
purpose registers to having only 16 now

52
00:02:10,560 --> 00:02:13,920
at the time of writing and recording

53
00:02:12,160 --> 00:02:16,040
this class this is not yet actually

54
00:02:13,920 --> 00:02:18,000
ratified but it's generally understood

55
00:02:16,040 --> 00:02:20,800
that it'll behave basically the same as

56
00:02:18,000 --> 00:02:23,800
RISC-V 32-bit I and then they just have

57
00:02:20,800 --> 00:02:25,280
less registers now why is that well they

58
00:02:23,800 --> 00:02:27,120
include their motivation in the

59
00:02:25,280 --> 00:02:31,239
documentation it says we have found that

60
00:02:27,120 --> 00:02:33,800
an small RV32I core design the upper 16

61
00:02:31,239 --> 00:02:36,120
registers consume around one4 of the

62
00:02:33,800 --> 00:02:38,680
total area of the core meaning the

63
00:02:36,120 --> 00:02:40,879
physical silicon D and excluding

64
00:02:38,680 --> 00:02:43,840
memories thus their removal saves around

65
00:02:40,879 --> 00:02:45,959
25% core area with corresponding Core

66
00:02:43,840 --> 00:02:47,640
Power reduction so basically if you get

67
00:02:45,959 --> 00:02:50,840
rid of some registers then you're going

68
00:02:47,640 --> 00:02:52,840
to save power and space for the actual

69
00:02:50,840 --> 00:02:54,720
silicon fabrication and space is at a

70
00:02:52,840 --> 00:02:56,440
premium because the smaller the chip the

71
00:02:54,720 --> 00:02:58,680
more of them you can get on a silicon

72
00:02:56,440 --> 00:03:02,080
wafer and that means you have increased

73
00:02:58,680 --> 00:03:05,920
yields and increase profits the RV

74
00:03:02,080 --> 00:03:08,239
64i is the 64-bit integer ISA and here

75
00:03:05,920 --> 00:03:10,879
the registers and pointers are all 64

76
00:03:08,239 --> 00:03:12,440
bits wide and there are still only 32

77
00:03:10,879 --> 00:03:16,040
general purpose registers and the

78
00:03:12,440 --> 00:03:20,239
program counter now RV64I is a super

79
00:03:16,040 --> 00:03:23,360
set of the RV32I RV64I includes all of

80
00:03:20,239 --> 00:03:25,560
the instructions from RV32I and then it

81
00:03:23,360 --> 00:03:26,959
adds 15 new instructions which are

82
00:03:25,560 --> 00:03:29,319
mostly for when you're dealing with

83
00:03:26,959 --> 00:03:31,680
32-bit operations but you happen to have

84
00:03:29,319 --> 00:03:33,280
6 forbit registers so in this class the

85
00:03:31,680 --> 00:03:36,599
lab environment that we're using the

86
00:03:33,280 --> 00:03:40,200
docker container actually is running qmu

87
00:03:36,599 --> 00:03:42,519
that is only capable of executing RV64I

88
00:03:40,200 --> 00:03:44,920
binaries and so although it can use RV

89
00:03:42,519 --> 00:03:47,280
32i instructions the binaries themselves

90
00:03:44,920 --> 00:03:50,439
are compiled for 64-bit it assumes

91
00:03:47,280 --> 00:03:52,959
64-bit registers and so forth so I may

92
00:03:50,439 --> 00:03:54,879
fix this I may add 32-bit support at

93
00:03:52,959 --> 00:03:56,599
some point in the future but when this

94
00:03:54,879 --> 00:03:58,879
class was first made we only supported

95
00:03:56,599 --> 00:04:01,200
RV64I and you should assume that you're

96
00:03:58,879 --> 00:04:05,519
always dealing with the base 40 from RV

97
00:04:01,200 --> 00:04:08,120
32i he base 40 right here plus the 15

98
00:04:05,519 --> 00:04:10,560
that are added by RV64I and you can see

99
00:04:08,120 --> 00:04:13,120
in the documentation it says RV 64 base

100
00:04:10,560 --> 00:04:15,480
instruction set in addition to the RV

101
00:04:13,120 --> 00:04:17,479
32i so here's our extra stuff there

102
00:04:15,480 --> 00:04:19,199
going to be a whole bunch of w suffixed

103
00:04:17,479 --> 00:04:20,639
things which is going to be our hint

104
00:04:19,199 --> 00:04:23,680
that it's something that deals with

105
00:04:20,639 --> 00:04:26,320
32-bit sizes word sizes instead of

106
00:04:23,680 --> 00:04:31,639
64-bit sizes and then finally the last

107
00:04:26,320 --> 00:04:33,639
base ISA is the RV 128bit I RV128I and

108
00:04:31,639 --> 00:04:36,600
here you move up to having

109
00:04:33,639 --> 00:04:38,960
128bit wide registers and pointers but

110
00:04:36,600 --> 00:04:42,880
you still retain only 32 general purpose

111
00:04:38,960 --> 00:04:44,639
registers now like RV32E embedded this

112
00:04:42,880 --> 00:04:46,639
is not actually ratified at the time of

113
00:04:44,639 --> 00:04:48,520
writing and they gave some motivation

114
00:04:46,639 --> 00:04:51,039
justification and essentially comes down

115
00:04:48,520 --> 00:04:52,880
to supercomputers and the recognition

116
00:04:51,039 --> 00:04:55,720
that sooner or later they're going to

117
00:04:52,880 --> 00:04:57,440
need more than 64 bits of space they're

118
00:04:55,720 --> 00:04:59,680
guesstimating you know perhaps by the

119
00:04:57,440 --> 00:05:01,400
2030s and so consequently they just just

120
00:04:59,680 --> 00:05:03,720
want to recognize that they're probably

121
00:05:01,400 --> 00:05:05,840
going to have to move up to 128 bits of

122
00:05:03,720 --> 00:05:08,320
space and rather than doing you know

123
00:05:05,840 --> 00:05:10,520
subsets like other architectures when

124
00:05:08,320 --> 00:05:12,639
they you know try to move past 32 to

125
00:05:10,520 --> 00:05:14,759
64bit they put little slices you know

126
00:05:12,639 --> 00:05:16,560
first you have 48 bit then you have 52

127
00:05:14,759 --> 00:05:18,639
bit well they just say like hey look

128
00:05:16,560 --> 00:05:20,600
let's just move up to 128 bit when we

129
00:05:18,639 --> 00:05:22,600
get there now as the RISC-V document

130
00:05:20,600 --> 00:05:25,400
said a few slides ago you can also have

131
00:05:22,600 --> 00:05:27,440
numerous extensions to the base Isis and

132
00:05:25,400 --> 00:05:29,919
indeed most of these extensions are

133
00:05:27,440 --> 00:05:32,560
frequently used to create what they call

134
00:05:29,919 --> 00:05:36,160
the general purpose ISAs so they have RV 32G

135
00:05:32,560 --> 00:05:38,919
and RV64G for the general purpose

136
00:05:36,160 --> 00:05:41,120
and that means you include also the you

137
00:05:38,919 --> 00:05:42,560
start with the base I ISA for integer

138
00:05:41,120 --> 00:05:47,560
operations and then you include

139
00:05:42,560 --> 00:05:49,280
extensions M A FD zisar and zeny we'll

140
00:05:47,560 --> 00:05:51,759
talk about those in a second but the

141
00:05:49,280 --> 00:05:54,199
basic nice thing here about RISC-V is

142
00:05:51,759 --> 00:05:56,919
that theoretically you could get away

143
00:05:54,199 --> 00:05:59,759
with a computer that only uses the base

144
00:05:56,919 --> 00:06:01,440
I ISA and then you know you do emulation

145
00:05:59,759 --> 00:06:03,400
and libraries and whatever to achieve

146
00:06:01,440 --> 00:06:05,960
something like multiplication so the key

147
00:06:03,400 --> 00:06:07,840
thing is that the Silicon makers can

148
00:06:05,960 --> 00:06:10,319
only include those extensions that are

149
00:06:07,840 --> 00:06:12,000
actually required for their purposes now

150
00:06:10,319 --> 00:06:13,360
if they're building a general purpose

151
00:06:12,000 --> 00:06:17,120
computer then they're probably going to

152
00:06:13,360 --> 00:06:19,520
do this RV 32G or 64g so here's our ISO

153
00:06:17,120 --> 00:06:21,840
family extensions and sorry to change

154
00:06:19,520 --> 00:06:26,639
decades on you here but um we've got our

155
00:06:21,840 --> 00:06:28,720
RV32I 128i 64i and 32e that we just

156
00:06:26,639 --> 00:06:31,560
talked about and then we have the M

157
00:06:28,720 --> 00:06:35,039
extension a extension F extension D

158
00:06:31,560 --> 00:06:38,080
extension ziser and zeny the Adams

159
00:06:35,039 --> 00:06:41,199
Family extension I mean sorry ISA family

160
00:06:38,080 --> 00:06:43,560
extensions the first extension is M for

161
00:06:41,199 --> 00:06:44,919
integer multiplication and division and

162
00:06:43,560 --> 00:06:47,039
they separate the multiply and divide

163
00:06:44,919 --> 00:06:48,880
out from the base to simplify low-end

164
00:06:47,039 --> 00:06:50,720
implementations basically something that

165
00:06:48,880 --> 00:06:52,520
doesn't actually need multiplication or

166
00:06:50,720 --> 00:06:54,080
for applications where integer multiply

167
00:06:52,520 --> 00:06:56,360
and divide operations are either

168
00:06:54,080 --> 00:06:58,440
infrequent or better handled in attached

169
00:06:56,360 --> 00:07:01,080
accelerators so they're big into the

170
00:06:58,440 --> 00:07:03,199
idea of acceler ators in Risk 5 which is

171
00:07:01,080 --> 00:07:04,639
the notion that maybe you build and it's

172
00:07:03,199 --> 00:07:06,639
a notion that goes along with current

173
00:07:04,639 --> 00:07:08,280
silicon practices maybe you build an

174
00:07:06,639 --> 00:07:10,520
accelerator off to the side that's like

175
00:07:08,280 --> 00:07:12,919
dedicated silicon specifically for

176
00:07:10,520 --> 00:07:14,960
particular types of operations such as

177
00:07:12,919 --> 00:07:17,360
you know audio video processing things

178
00:07:14,960 --> 00:07:19,520
like that neural Nets and so forth and

179
00:07:17,360 --> 00:07:21,759
so in those situations maybe your base

180
00:07:19,520 --> 00:07:23,520
CPU doesn't have multiplication but

181
00:07:21,759 --> 00:07:25,800
there's some other accelerator off to

182
00:07:23,520 --> 00:07:27,560
the side for other purposes and you use

183
00:07:25,800 --> 00:07:30,000
that for multiplication so this is

184
00:07:27,560 --> 00:07:33,039
covered in Chapter 7 of the

185
00:07:30,000 --> 00:07:34,919
documentation then we have a for Atomic

186
00:07:33,039 --> 00:07:36,919
instructions the standard atomic

187
00:07:34,919 --> 00:07:38,840
instruction extension named a contains

188
00:07:36,919 --> 00:07:40,919
instructions that automically read

189
00:07:38,840 --> 00:07:42,720
modify write memory to support

190
00:07:40,919 --> 00:07:45,080
synchronization between multiple RISC

191
00:07:42,720 --> 00:07:47,360
five harts running in the same memory

192
00:07:45,080 --> 00:07:49,680
space so first atomic if you're not

193
00:07:47,360 --> 00:07:52,080
familiar it's this notion of an

194
00:07:49,680 --> 00:07:53,919
uninterruptible sequence so you have

195
00:07:52,080 --> 00:07:56,240
situations in computers whenever you

196
00:07:53,919 --> 00:07:58,639
have parallelism where potentially you

197
00:07:56,240 --> 00:08:00,479
know one thread of execution could

198
00:07:58,639 --> 00:08:03,159
modify some some memory and then another

199
00:08:00,479 --> 00:08:04,840
thread of execution could read it like

200
00:08:03,159 --> 00:08:06,599
before or after the modification that's

201
00:08:04,840 --> 00:08:08,240
where we have race conditions and

202
00:08:06,599 --> 00:08:09,720
consequently the two threads would not

203
00:08:08,240 --> 00:08:11,400
see the same thing when you actually

204
00:08:09,720 --> 00:08:13,599
want them to see the same thing so

205
00:08:11,400 --> 00:08:15,599
Atomic operations say let's make sure

206
00:08:13,599 --> 00:08:17,280
that this can't be interrupted so that

207
00:08:15,599 --> 00:08:19,120
when this operation completes when the

208
00:08:17,280 --> 00:08:20,960
right is complete then everyone sees the

209
00:08:19,120 --> 00:08:24,039
same thing and that's where they're

210
00:08:20,960 --> 00:08:26,759
referencing harts so what is a hart a

211
00:08:24,039 --> 00:08:28,879
hart is a hardware thread in Risk five

212
00:08:26,759 --> 00:08:31,080
terminology so the way they Define it a

213
00:08:28,879 --> 00:08:32,760
component is termed a core if it

214
00:08:31,080 --> 00:08:35,159
contains an independent instruction

215
00:08:32,760 --> 00:08:37,360
fetch unit and a RISC-V compatible core

216
00:08:35,159 --> 00:08:39,680
might support multiple RISC-V compatible

217
00:08:37,360 --> 00:08:42,560
hardware threads or harts through

218
00:08:39,680 --> 00:08:44,440
multi-threading so you may be familiar

219
00:08:42,560 --> 00:08:46,120
with this in the context of Intel

220
00:08:44,440 --> 00:08:48,080
systems for instance there's this

221
00:08:46,120 --> 00:08:50,320
concept of simultaneous multi-threading

222
00:08:48,080 --> 00:08:52,959
smt and on Intel systems you may have

223
00:08:50,320 --> 00:08:55,560
heard that each of your cores actually

224
00:08:52,959 --> 00:08:57,360
has two threads of execution to it they

225
00:08:55,560 --> 00:08:59,160
call it hyperthreading but it's

226
00:08:57,360 --> 00:09:01,040
basically this notion that an individual

227
00:08:59,160 --> 00:09:03,320
ual core could actually have like more

228
00:09:01,040 --> 00:09:05,440
registers could have enough registers in

229
00:09:03,320 --> 00:09:07,800
other state in order to make it so that

230
00:09:05,440 --> 00:09:10,480
to software and operating systems and

231
00:09:07,800 --> 00:09:13,680
things like that the core looks like it

232
00:09:10,480 --> 00:09:16,079
was two cores and so if it was doing

233
00:09:13,680 --> 00:09:18,560
threading from a software perspective

234
00:09:16,079 --> 00:09:20,279
it's actually able to you know context

235
00:09:18,560 --> 00:09:21,760
switch between two different

236
00:09:20,279 --> 00:09:23,640
environments that have completely

237
00:09:21,760 --> 00:09:25,440
different sets of registers so Intel

238
00:09:23,640 --> 00:09:27,680
systems and hyperthreading is the most

239
00:09:25,440 --> 00:09:29,680
basic where you have two threads two

240
00:09:27,680 --> 00:09:31,800
hardware threads and and other

241
00:09:29,680 --> 00:09:33,680
architectures like IBM power 9 can

242
00:09:31,800 --> 00:09:35,880
actually support up to eight threads per

243
00:09:33,680 --> 00:09:38,120
core but the key point of a hart or

244
00:09:35,880 --> 00:09:40,519
hardware thread is just it's something

245
00:09:38,120 --> 00:09:43,000
that to software looks indistinguishable

246
00:09:40,519 --> 00:09:44,959
from having sort of multiple cores and

247
00:09:43,000 --> 00:09:47,240
so software can take advantage of this

248
00:09:44,959 --> 00:09:50,200
and contact switch between the two then

249
00:09:47,240 --> 00:09:52,079
we have the ziser extension which stands

250
00:09:50,200 --> 00:09:55,079
for control and Status register

251
00:09:52,079 --> 00:09:57,760
instructions csrs RISC-V defines a

252
00:09:55,079 --> 00:09:59,800
separate address space with 496 control

253
00:09:57,760 --> 00:10:02,120
and Status registers so associated with

254
00:09:59,800 --> 00:10:04,079
each hart so again it's you know per

255
00:10:02,120 --> 00:10:06,560
hart so if the device has multiple

256
00:10:04,079 --> 00:10:08,200
harts per core then it's going to have

257
00:10:06,560 --> 00:10:09,839
you know multiple of these control and

258
00:10:08,200 --> 00:10:11,320
Status registers per core and that's why

259
00:10:09,839 --> 00:10:13,160
I say it's not just like the general

260
00:10:11,320 --> 00:10:15,839
purpose registers you have to have the

261
00:10:13,160 --> 00:10:18,959
full extra register state for each

262
00:10:15,839 --> 00:10:20,519
individual hart so chapter 9 defines

263
00:10:18,959 --> 00:10:22,720
the control and Status register

264
00:10:20,519 --> 00:10:24,200
instructions and it says while csrs are

265
00:10:22,720 --> 00:10:25,959
primarily used in privileged

266
00:10:24,200 --> 00:10:28,160
architecture which is not the context of

267
00:10:25,959 --> 00:10:30,720
this class there are several uses in

268
00:10:28,160 --> 00:10:33,079
unprivileged code including for counters

269
00:10:30,720 --> 00:10:34,519
and timers and for floating point status

270
00:10:33,079 --> 00:10:36,079
counters and timers are no longer

271
00:10:34,519 --> 00:10:38,240
considered mandatory parts of the

272
00:10:36,079 --> 00:10:40,880
standard base ISA so the CSR

273
00:10:38,240 --> 00:10:43,600
instructions have been moved out to a

274
00:10:40,880 --> 00:10:45,320
separate chapter so basically a ziser at

275
00:10:43,600 --> 00:10:46,839
some point you know they said like okay

276
00:10:45,320 --> 00:10:48,320
we'll have the control registers as part

277
00:10:46,839 --> 00:10:50,120
of the Bas ISA and then eventually they

278
00:10:48,320 --> 00:10:51,680
said nah that's not required anymore

279
00:10:50,120 --> 00:10:53,839
because we don't need these counters and

280
00:10:51,680 --> 00:10:55,959
floating point status stuff so let's

281
00:10:53,839 --> 00:10:57,839
move it out have it be an optional thing

282
00:10:55,959 --> 00:10:59,519
now it's optional doesn't have to be

283
00:10:57,839 --> 00:11:01,839
included you could have something that

284
00:10:59,519 --> 00:11:03,399
completely doesn't use it at all but if

285
00:11:01,839 --> 00:11:05,600
it's going to do things like floating

286
00:11:03,399 --> 00:11:06,720
point status and counters and timers

287
00:11:05,600 --> 00:11:08,920
then it's probably going to include

288
00:11:06,720 --> 00:11:11,040
those because these are prerequisites

289
00:11:08,920 --> 00:11:12,519
for that so ziser is very much the kind

290
00:11:11,040 --> 00:11:14,920
of thing that is more appropriate for

291
00:11:12,519 --> 00:11:16,320
some future architecture 2005 class

292
00:11:14,920 --> 00:11:19,480
that's more analogous to our

293
00:11:16,320 --> 00:11:22,480
architecture 2001 in 2001 we covered the

294
00:11:19,480 --> 00:11:25,519
privileged and you know OS internals

295
00:11:22,480 --> 00:11:27,800
elements of x86 architecture so 2005

296
00:11:25,519 --> 00:11:30,440
would be the same thing for RISC-V Z

297
00:11:27,800 --> 00:11:32,839
feny is another extension and this

298
00:11:30,440 --> 00:11:34,519
extension includes the fence eye

299
00:11:32,839 --> 00:11:36,279
instruction that provides explicit

300
00:11:34,519 --> 00:11:38,120
synchronization between rights to

301
00:11:36,279 --> 00:11:40,040
instruction memory and instruction

302
00:11:38,120 --> 00:11:41,320
fetches on the Same hart currently this

303
00:11:40,040 --> 00:11:43,160
instruction is the only standard

304
00:11:41,320 --> 00:11:44,680
mechanism to ensure that stores visible

305
00:11:43,160 --> 00:11:46,800
to the hart will be visible to its

306
00:11:44,680 --> 00:11:48,800
instruction fetches so the basic idea

307
00:11:46,800 --> 00:11:50,560
here is if you're going to be

308
00:11:48,800 --> 00:11:53,000
overwriting your own code and then

309
00:11:50,560 --> 00:11:54,639
fetching it and actually executing it

310
00:11:53,000 --> 00:11:56,720
like you might do with just in time

311
00:11:54,639 --> 00:11:59,040
compilation or like some malware does

312
00:11:56,720 --> 00:12:00,959
with polymorphic malware you have to use

313
00:11:59,040 --> 00:12:02,639
the fence eye instruction to say hey by

314
00:12:00,959 --> 00:12:04,079
the way I just did it right now make

315
00:12:02,639 --> 00:12:05,959
sure everything's synchronized so that

316
00:12:04,079 --> 00:12:08,079
when I read it back to execute an

317
00:12:05,959 --> 00:12:09,680
instruction that I actually see what was

318
00:12:08,079 --> 00:12:11,639
just written without the use of this

319
00:12:09,680 --> 00:12:13,279
fence eye instruction you would read

320
00:12:11,639 --> 00:12:15,320
back from memory but you would not see

321
00:12:13,279 --> 00:12:17,360
what was just actually written now fence

322
00:12:15,320 --> 00:12:20,199
eye should not be confused with fence

323
00:12:17,360 --> 00:12:22,320
which is part of the rv32 base is set so

324
00:12:20,199 --> 00:12:25,320
fence just has to do with General

325
00:12:22,320 --> 00:12:27,279
reading and writing and making sure that

326
00:12:25,320 --> 00:12:29,279
there's memory synchronization whereas

327
00:12:27,279 --> 00:12:31,440
fence ey is specifically memory

328
00:12:29,279 --> 00:12:33,639
synchronization for fetching

329
00:12:31,440 --> 00:12:35,120
instructions then we've got the F

330
00:12:33,639 --> 00:12:37,839
extensions which have to do with

331
00:12:35,120 --> 00:12:39,959
floating points and so this is the

332
00:12:37,839 --> 00:12:42,880
single Precision floating point that is

333
00:12:39,959 --> 00:12:44,959
compliant with these i e standards and

334
00:12:42,880 --> 00:12:49,000
also you can't have this F extension

335
00:12:44,959 --> 00:12:50,760
unless you also have the ziser or Z CSR

336
00:12:49,000 --> 00:12:52,959
because it depends on that and then the

337
00:12:50,760 --> 00:12:55,160
D extension is just double Precision so

338
00:12:52,959 --> 00:12:56,839
f for single Precision floating point D

339
00:12:55,160 --> 00:12:58,839
for double Precision floating point

340
00:12:56,839 --> 00:13:00,519
again conforming to some a e stand

341
00:12:58,839 --> 00:13:02,079
standard but we're not really covering

342
00:13:00,519 --> 00:13:03,680
floating point in this class so we're

343
00:13:02,079 --> 00:13:06,360
not actually going to care about this

344
00:13:03,680 --> 00:13:12,720
and again the D depends on f f depends

345
00:13:06,360 --> 00:13:16,399
on ziser so rv32 G is going to be rv32 I

346
00:13:12,720 --> 00:13:18,519
plus ziser z feny a for Atomic M for

347
00:13:16,399 --> 00:13:20,399
multiply f for single Precision floating

348
00:13:18,519 --> 00:13:25,040
point and D for double Precision

349
00:13:20,399 --> 00:13:27,839
floating point that is RV 32G a base ISA

350
00:13:25,040 --> 00:13:31,160
plus some set of extensions now what is

351
00:13:27,839 --> 00:13:32,920
our GCC use in our lab if we run GCC D-

352
00:13:31,160 --> 00:13:36,320
verbose and we look through all this

353
00:13:32,920 --> 00:13:40,800
Goble we eventually see this with Arch

354
00:13:36,320 --> 00:13:44,279
RV 64 GC so what is RV 64 GC well

355
00:13:40,800 --> 00:13:47,560
replace the RV32I with an rv32 i plus

356
00:13:44,279 --> 00:13:50,560
RV64I then get all the rest of that for

357
00:13:47,560 --> 00:13:53,000
G but then the C implies that we have

358
00:13:50,560 --> 00:13:55,240
another extension the C extension what

359
00:13:53,000 --> 00:13:58,240
is the C extension well that is

360
00:13:55,240 --> 00:14:00,519
compressed instructions so a standard

361
00:13:58,240 --> 00:14:03,680
assembly instruction in Risk 5 is

362
00:14:00,519 --> 00:14:06,079
encoded in 32 bits but if you use the

363
00:14:03,680 --> 00:14:08,959
compressed instructions then you will

364
00:14:06,079 --> 00:14:10,880
have 16 bits instead of 32 bits encoding

365
00:14:08,959 --> 00:14:12,399
the assembly instruction so this is

366
00:14:10,880 --> 00:14:15,240
going to be commonly used when you're

367
00:14:12,399 --> 00:14:17,560
trying to get smaller code size now you

368
00:14:15,240 --> 00:14:20,120
may be familiar with other architectures

369
00:14:17,560 --> 00:14:23,519
like arm has a notion of thumb and myips

370
00:14:20,120 --> 00:14:25,480
has micro myips these are other ISA uh

371
00:14:23,519 --> 00:14:27,199
approaches to compressing their code so

372
00:14:25,480 --> 00:14:29,720
that you can take high frequency code

373
00:14:27,199 --> 00:14:32,639
and have it take up less space CIS is

374
00:14:29,720 --> 00:14:34,920
say like x86 don't need to have a

375
00:14:32,639 --> 00:14:37,399
separate sort of compressed mode because

376
00:14:34,920 --> 00:14:39,519
they have variable length en coding so

377
00:14:37,399 --> 00:14:42,279
an x86 assembly instruction could be one

378
00:14:39,519 --> 00:14:44,480
bite 2 by 3 by four byte 5 byte 6 byte

379
00:14:42,279 --> 00:14:46,040
all the way up to 15 bytes so they can

380
00:14:44,480 --> 00:14:48,440
be really long or they can be really

381
00:14:46,040 --> 00:14:49,680
short and as long as they uh you know

382
00:14:48,440 --> 00:14:51,480
are using some of these short

383
00:14:49,680 --> 00:14:54,959
instructions then they'll get better

384
00:14:51,480 --> 00:14:57,560
code size so chapter 16 to cover the

385
00:14:54,959 --> 00:14:59,440
compressed instructions so in this class

386
00:14:57,560 --> 00:15:03,480
although this is what the compiler is

387
00:14:59,440 --> 00:15:04,880
compiling for RV64G C we're only going

388
00:15:03,480 --> 00:15:09,519
to cover the

389
00:15:04,880 --> 00:15:12,040
following so we cover the RV32I and 64i

390
00:15:09,519 --> 00:15:14,079
those 55 instructions the M

391
00:15:12,040 --> 00:15:16,560
multiplication and division and

392
00:15:14,079 --> 00:15:17,800
remainder and the C for compressed

393
00:15:16,560 --> 00:15:19,199
instructions and we're not going to

394
00:15:17,800 --> 00:15:21,079
cover every single compressed

395
00:15:19,199 --> 00:15:22,800
instruction Under the Sun we'll just

396
00:15:21,079 --> 00:15:24,959
cover them as they come up in the

397
00:15:22,800 --> 00:15:29,240
context of Alternatives being used for

398
00:15:24,959 --> 00:15:29,240
our 32 and 64-bit instructions

