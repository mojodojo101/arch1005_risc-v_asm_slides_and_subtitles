1
00:00:00,080 --> 00:00:04,759
so the takeaway from example four is

2
00:00:01,959 --> 00:00:07,520
just that by hiding the input operands

3
00:00:04,759 --> 00:00:09,040
the compiler can't know a priori that oh

4
00:00:07,520 --> 00:00:12,240
yeah that hardcoded constant is actually

5
00:00:09,040 --> 00:00:15,280
an unsigned value and so it has to honor

6
00:00:12,240 --> 00:00:17,600
the signedness of your variable so now

7
00:00:15,280 --> 00:00:20,800
it generates signed divisions signed

8
00:00:17,600 --> 00:00:22,680
divisions by a word-sized variable and

9
00:00:20,800 --> 00:00:27,720
again it's really all about collecting

10
00:00:22,680 --> 00:00:31,240
those variables div REM div W remw we're

11
00:00:27,720 --> 00:00:35,559
so close just two new multi mply save

12
00:00:31,240 --> 00:00:35,559
the high for us to learn about

