1
00:00:00,560 --> 00:00:05,040
continuing our inference game we just

2
00:00:02,800 --> 00:00:07,160
saw what happens when we have two ins

3
00:00:05,040 --> 00:00:10,120
which are 32-bit variables so let's go

4
00:00:07,160 --> 00:00:12,880
ahead and try two 64-bit variables here

5
00:00:10,120 --> 00:00:16,240
I have leafless bold face and affected

6
00:00:12,880 --> 00:00:19,119
oddballs as our new two local variables

7
00:00:16,240 --> 00:00:20,600
compile that and you'll get this code

8
00:00:19,119 --> 00:00:22,640
and it looks like we've got a new

9
00:00:20,600 --> 00:00:24,680
assembly instruction to learn about and

10
00:00:22,640 --> 00:00:26,840
also I changed it to now show us the

11
00:00:24,680 --> 00:00:28,880
full absolute addresses that it has at

12
00:00:26,840 --> 00:00:30,720
runtime assuming that address Bas layout

13
00:00:28,880 --> 00:00:32,719
randomization is disable abled as it is

14
00:00:30,720 --> 00:00:34,879
in our lab VM and the reason I'm showing

15
00:00:32,719 --> 00:00:36,719
you the longer addresses rather than the

16
00:00:34,879 --> 00:00:38,719
shortened truncated versions that I've

17
00:00:36,719 --> 00:00:40,480
been using thus far is because we can

18
00:00:38,719 --> 00:00:43,239
see there's some sort of calculation

19
00:00:40,480 --> 00:00:45,680
going on and the disassembler is telling

20
00:00:43,239 --> 00:00:48,280
us that this line whatever is going on

21
00:00:45,680 --> 00:00:52,120
seems to be calculating an address 5es

22
00:00:48,280 --> 00:00:53,559
660 and this one 5es 668 so that

23
00:00:52,120 --> 00:00:56,160
probably has something to do with this

24
00:00:53,559 --> 00:00:58,000
new at this new assembly instruction

25
00:00:56,160 --> 00:01:02,719
that we'll cover here in a second so

26
00:00:58,000 --> 00:01:04,439
this is ADD upper immediate to PC and

27
00:01:02,719 --> 00:01:09,159
store to register and I'm going to

28
00:01:04,439 --> 00:01:10,240
pronounce that like a PC we PC the

29
00:01:09,159 --> 00:01:12,439
French

30
00:01:10,240 --> 00:01:16,040
wepc all right so

31
00:01:12,439 --> 00:01:19,560
wepc this adds this upper 20 bit

32
00:01:16,040 --> 00:01:21,360
immediate to the program counter PC

33
00:01:19,560 --> 00:01:23,520
that's just hardcoded this add upper

34
00:01:21,360 --> 00:01:28,240
immediate to PC that's part of its name

35
00:01:23,520 --> 00:01:30,400
so wi PC is going to add 20 to PC and

36
00:01:28,240 --> 00:01:32,720
store it into register

37
00:01:30,400 --> 00:01:34,640
destination okay so I just said that it

38
00:01:32,720 --> 00:01:37,720
takes the encoded immediate as if it

39
00:01:34,640 --> 00:01:40,040
were the upper 20 bits of a 32-bit value

40
00:01:37,720 --> 00:01:42,640
with the bottom 12 bits being assumed to

41
00:01:40,040 --> 00:01:45,439
be zero and it adds that to the program

42
00:01:42,640 --> 00:01:49,079
counter and stores in TD I want to point

43
00:01:45,439 --> 00:01:51,560
out that the PC value here is the value

44
00:01:49,079 --> 00:01:54,960
of PC at the time that it's pointing at

45
00:01:51,560 --> 00:01:57,119
the wepc instruction to execute so

46
00:01:54,960 --> 00:02:00,320
unlike things like Jer that we saw

47
00:01:57,119 --> 00:02:03,600
before jer jer store unlike Jer that we

48
00:02:00,320 --> 00:02:06,560
saw before it is going to have the PC of

49
00:02:03,600 --> 00:02:08,759
the instruction itself not the PC plus4

50
00:02:06,560 --> 00:02:12,200
of the next assembly instruction so

51
00:02:08,759 --> 00:02:14,800
therefore wepc is good for calculating

52
00:02:12,200 --> 00:02:17,360
PC relative addresses so if you want to

53
00:02:14,800 --> 00:02:19,800
find some data and you are able to

54
00:02:17,360 --> 00:02:22,280
encode its address as wherever this

55
00:02:19,800 --> 00:02:25,599
assembly instruction is plus or minus

56
00:02:22,280 --> 00:02:27,959
something then wepc can help you find

57
00:02:25,599 --> 00:02:30,360
that data and indeed that is exactly

58
00:02:27,959 --> 00:02:33,720
what we see going on here what happening

59
00:02:30,360 --> 00:02:36,040
is it's doing we PC and it's doing zero

60
00:02:33,720 --> 00:02:39,720
plus PC so whatever this assembly

61
00:02:36,040 --> 00:02:42,599
instructions address is here it's FES

62
00:02:39,720 --> 00:02:44,239
62e that now is the address that is

63
00:02:42,599 --> 00:02:48,280
going to be in

64
00:02:44,239 --> 00:02:51,159
a5 so then the next part this add I is

65
00:02:48,280 --> 00:02:52,680
adding 50 to that so basically you'll

66
00:02:51,159 --> 00:02:56,080
have to run it through a calculator

67
00:02:52,680 --> 00:03:01,000
yourself or take my word for it but 62 e

68
00:02:56,080 --> 00:03:03,680
plus decimal 50 equals heximal 5 5

69
00:03:01,000 --> 00:03:05,239
660 so the net result of these two

70
00:03:03,680 --> 00:03:08,599
assembly instructions is that it

71
00:03:05,239 --> 00:03:11,319
calculated the address all fives 660

72
00:03:08,599 --> 00:03:14,000
placed it into the register a5 and now

73
00:03:11,319 --> 00:03:17,680
it is going to do a load double word

74
00:03:14,000 --> 00:03:20,959
from a5 plus offset zero and load that

75
00:03:17,680 --> 00:03:22,720
into a5 so what could it be loading well

76
00:03:20,959 --> 00:03:24,640
when we skim through this assembly we

77
00:03:22,720 --> 00:03:27,519
don't see any references to leafless

78
00:03:24,640 --> 00:03:29,840
bold face or affected oddballs so in

79
00:03:27,519 --> 00:03:31,680
your debugger you could go and just look

80
00:03:29,840 --> 00:03:33,480
at what that is or you could just step

81
00:03:31,680 --> 00:03:36,080
past the load and see which value gets

82
00:03:33,480 --> 00:03:39,200
loaded into a5 but basically we've got

83
00:03:36,080 --> 00:03:42,280
two of these sequences wepc of zero to

84
00:03:39,200 --> 00:03:45,400
get the address add I to offset from the

85
00:03:42,280 --> 00:03:46,920
address and then load dword so one way

86
00:03:45,400 --> 00:03:49,360
or another those are probably going to

87
00:03:46,920 --> 00:03:51,879
be references to this data of these

88
00:03:49,360 --> 00:03:55,040
local variables because they're too big

89
00:03:51,879 --> 00:03:57,680
and we can't use the uh load upper

90
00:03:55,040 --> 00:04:00,280
immediate the LUI or the ADDI those

91
00:03:57,680 --> 00:04:02,159
can only get us 32-bit values so to get

92
00:04:00,280 --> 00:04:04,480
these 64-bit values it looks like we're

93
00:04:02,159 --> 00:04:06,680
going to pluck them out of some memory

94
00:04:04,480 --> 00:04:09,239
that is stored just past the end of this

95
00:04:06,680 --> 00:04:11,799
assembly we can't actually see it 660

96
00:04:09,239 --> 00:04:13,840
668 well those are just sort of right

97
00:04:11,799 --> 00:04:15,720
past the end of this function so we

98
00:04:13,840 --> 00:04:18,600
can't see it here but you can see it in

99
00:04:15,720 --> 00:04:20,680
your debugger so go ahead this is what

100
00:04:18,600 --> 00:04:23,320
your Stack's going to look like and go

101
00:04:20,680 --> 00:04:25,320
take a look in your debugger at what the

102
00:04:23,320 --> 00:04:28,400
net result of running this two long

103
00:04:25,320 --> 00:04:30,120
Longs is going to be go ahead and stop

104
00:04:28,400 --> 00:04:32,960
draw yourself a stack diag

105
00:04:30,120 --> 00:04:37,840
and see how that differs with two long

106
00:04:32,960 --> 00:04:37,840
Longs compared to two ins

