1
00:00:00,120 --> 00:00:03,760
now we have to pick the next assembly

2
00:00:01,959 --> 00:00:07,520
instruction to look at and I'm going to

3
00:00:03,760 --> 00:00:10,160
choose the CI forn because it sounds

4
00:00:07,520 --> 00:00:12,759
similar to the CI that we just looked at

5
00:00:10,160 --> 00:00:14,639
so there is a diagram in the optional

6
00:00:12,759 --> 00:00:16,680
book for this class which I find

7
00:00:14,639 --> 00:00:19,160
extremely helpful at figuring out what

8
00:00:16,680 --> 00:00:20,640
the names are of things so we've got

9
00:00:19,160 --> 00:00:23,359
this

10
00:00:20,640 --> 00:00:26,160
c4n and we want to figure out what is

11
00:00:23,359 --> 00:00:28,039
this well we basically take that and we

12
00:00:26,160 --> 00:00:30,880
line it up here and we go down the line

13
00:00:28,039 --> 00:00:34,879
and say does it equal one of these rows

14
00:00:30,880 --> 00:00:37,399
so the first thing is c. add I is one

15
00:00:34,879 --> 00:00:40,520
option and this underscore means nothing

16
00:00:37,399 --> 00:00:43,680
so you can have a C add that is a valid

17
00:00:40,520 --> 00:00:46,120
compressed uh instruction and C add I is

18
00:00:43,680 --> 00:00:47,399
also a valid compressed instruction but

19
00:00:46,120 --> 00:00:51,280
that is not what we're looking for we're

20
00:00:47,399 --> 00:00:56,320
looking for C add i4n so moving down C

21
00:00:51,280 --> 00:01:00,440
add I 16 SP nope that's not it and C add

22
00:00:56,320 --> 00:01:02,879
I for SPN Bingo that is exactly what we

23
00:01:00,440 --> 00:01:05,680
want so that also tells us the name what

24
00:01:02,879 --> 00:01:09,520
does this thing stand for compressed add

25
00:01:05,680 --> 00:01:11,360
immediate time 4 to stack pointer

26
00:01:09,520 --> 00:01:13,000
non-destructive all right let's break

27
00:01:11,360 --> 00:01:16,240
all that up in a little bit but

28
00:01:13,000 --> 00:01:21,720
basically the ci4 SPN will ultimately

29
00:01:16,240 --> 00:01:24,000
expand out to just this ad I s0 sp16 so

30
00:01:21,720 --> 00:01:25,799
it expands to this it's just the

31
00:01:24,000 --> 00:01:27,400
compressed form now if you don't have

32
00:01:25,799 --> 00:01:29,360
the book which is fine you can just

33
00:01:27,400 --> 00:01:30,360
search and find all the descriptions and

34
00:01:29,360 --> 00:01:32,600
definitions

35
00:01:30,360 --> 00:01:34,360
in the manual of course the one downside

36
00:01:32,600 --> 00:01:36,640
of the manual is that at least as far as

37
00:01:34,360 --> 00:01:38,880
I've seen thus far it doesn't seem to

38
00:01:36,640 --> 00:01:40,880
actually spell out to the instruction NM

39
00:01:38,880 --> 00:01:43,759
monics that way the way that diagram

40
00:01:40,880 --> 00:01:45,520
does in the book and that means that

41
00:01:43,759 --> 00:01:46,920
essentially as far as I can tell right

42
00:01:45,520 --> 00:01:49,719
now if you just looked in the manual you

43
00:01:46,920 --> 00:01:53,640
would never really fully understand that

44
00:01:49,719 --> 00:01:56,759
ci4 SPN stands for add immediate times

45
00:01:53,640 --> 00:01:59,280
for stack pointer non-destructive like

46
00:01:56,759 --> 00:02:01,079
there are hints of that in here but it's

47
00:01:59,280 --> 00:02:02,759
not just all spelled out like that so

48
00:02:01,079 --> 00:02:05,280
that is why I like that diagram from the

49
00:02:02,759 --> 00:02:07,119
book okay so let's break it down now

50
00:02:05,280 --> 00:02:08,959
it's compressed add instruction times 4

51
00:02:07,119 --> 00:02:11,640
to stack poter non-destructive if the

52
00:02:08,959 --> 00:02:13,640
instruction is immediate times 4 as it

53
00:02:11,640 --> 00:02:17,080
says right here and if I'm reading an

54
00:02:13,640 --> 00:02:21,200
assembly instruction that has s0 SP and

55
00:02:17,080 --> 00:02:23,560
16 we are left with some ambiguity of is

56
00:02:21,200 --> 00:02:25,920
this immediate multiplied times 4 or is

57
00:02:23,560 --> 00:02:27,800
a smaller immediate of four already

58
00:02:25,920 --> 00:02:29,840
multiplied by four and then the

59
00:02:27,800 --> 00:02:32,040
disassembler is just telling us 16

60
00:02:29,840 --> 00:02:34,239
because it already calculated the 4 * 4

61
00:02:32,040 --> 00:02:36,080
to get 16 so that's ambiguous and we

62
00:02:34,239 --> 00:02:37,519
kind of have to dig into the actual

63
00:02:36,080 --> 00:02:39,680
encoding and definitions and

64
00:02:37,519 --> 00:02:42,080
documentation I don't really want to

65
00:02:39,680 --> 00:02:43,680
cover that this early in the class but

66
00:02:42,080 --> 00:02:45,080
unfortunately in order to understand

67
00:02:43,680 --> 00:02:47,200
what's going on with these compressed

68
00:02:45,080 --> 00:02:49,599
instructions we kind of have to okay so

69
00:02:47,200 --> 00:02:55,760
I said that this particular thing ci4

70
00:02:49,599 --> 00:02:57,120
SPN s0 sp16 expands to EDI s0 sp6 so

71
00:02:55,760 --> 00:02:58,800
that ADDI does not have a

72
00:02:57,120 --> 00:03:01,319
multiplication and so if I'm saying it's

73
00:02:58,800 --> 00:03:03,519
16 well that means it must be 16 is the

74
00:03:01,319 --> 00:03:07,120
ultimate value that's added to stack

75
00:03:03,519 --> 00:03:09,840
pointer not 16 * 4 but that still leaves

76
00:03:07,120 --> 00:03:12,200
us with ambiguity in terms of is the

77
00:03:09,840 --> 00:03:15,040
actual 16 encoded into the assembly

78
00:03:12,200 --> 00:03:16,480
instruction or is the disassembler again

79
00:03:15,040 --> 00:03:18,200
you know hiding the fact that there's a

80
00:03:16,480 --> 00:03:21,120
four there and then it's multiplying it

81
00:03:18,200 --> 00:03:23,080
times four and giving us a 16 to display

82
00:03:21,120 --> 00:03:25,040
for the human to make a ice and again we

83
00:03:23,080 --> 00:03:27,799
can only really figure that out if we

84
00:03:25,040 --> 00:03:30,200
drill down and actually dig into how the

85
00:03:27,799 --> 00:03:33,120
cad ey is actually

86
00:03:30,200 --> 00:03:35,040
encoded so let's go ahead and zoom in on

87
00:03:33,120 --> 00:03:36,959
this there is an element here that is

88
00:03:35,040 --> 00:03:38,840
going to recur in other compressed

89
00:03:36,959 --> 00:03:41,720
instructions and that is this portion

90
00:03:38,840 --> 00:03:44,040
here it says that this is scaled by four

91
00:03:41,720 --> 00:03:46,480
and that's what it means by that

92
00:03:44,040 --> 00:03:49,400
immediate time 4 so it's saying it adds

93
00:03:46,480 --> 00:03:51,879
a zero extended nonzero immediate scaled

94
00:03:49,400 --> 00:03:54,959
by four to the stack pointer now that

95
00:03:51,879 --> 00:03:58,239
nonzero unsigned or zero extended

96
00:03:54,959 --> 00:04:00,000
immediate is these eight bits right here

97
00:03:58,239 --> 00:04:01,840
but if we look carefully at these eight

98
00:04:00,000 --> 00:04:04,519
bits we can see that while it tells us

99
00:04:01,840 --> 00:04:07,239
you know bit n and bit 8 and bit seven

100
00:04:04,519 --> 00:04:09,760
and bit six it is not actually telling

101
00:04:07,239 --> 00:04:12,480
us all possible bit values we fill this

102
00:04:09,760 --> 00:04:19,239
in from this so bit five comes first bit

103
00:04:12,480 --> 00:04:22,680
four next bit 9 8 7 6 23 but there is no

104
00:04:19,239 --> 00:04:25,840
mention and there is no room for bit one

105
00:04:22,680 --> 00:04:27,600
and bit zero so if we break this out and

106
00:04:25,840 --> 00:04:29,759
we do our little decoding here to figure

107
00:04:27,600 --> 00:04:31,840
out you know what this ultimate 10 bit

108
00:04:29,759 --> 00:04:34,199
value is going to look like it's 10 bit

109
00:04:31,840 --> 00:04:37,000
because it has immediate 9 is specified

110
00:04:34,199 --> 00:04:39,720
here bit n is specified and so if

111
00:04:37,000 --> 00:04:41,960
there's up to bit 9 index 9 that means

112
00:04:39,720 --> 00:04:44,280
it must be a 10 bit value overall so

113
00:04:41,960 --> 00:04:46,400
this 10 bit value has eight of the bits

114
00:04:44,280 --> 00:04:49,320
specified but the bottom two bits are

115
00:04:46,400 --> 00:04:51,440
left unspecified uncoded in here and

116
00:04:49,320 --> 00:04:54,520
this is what this scaled by X really

117
00:04:51,440 --> 00:04:55,880
means it said it was scaled by four and

118
00:04:54,520 --> 00:04:58,759
that means that actually it's going to

119
00:04:55,880 --> 00:05:01,120
leave the least significant two bits as

120
00:04:58,759 --> 00:05:03,479
implicitly zero Z and the value you're

121
00:05:01,120 --> 00:05:06,240
actually encoding with 8 Bits is a 10bit

122
00:05:03,479 --> 00:05:08,680
value that is multiples of four so

123
00:05:06,240 --> 00:05:11,680
scaled by X really means you can only

124
00:05:08,680 --> 00:05:14,280
encode multiples of X so let's see that

125
00:05:11,680 --> 00:05:17,560
concretely okay here is our code and if

126
00:05:14,280 --> 00:05:20,199
we use the disassemble SLR option it'll

127
00:05:17,560 --> 00:05:22,280
give us the actual upcode bytes that are

128
00:05:20,199 --> 00:05:25,800
used to encode the instructions we're

129
00:05:22,280 --> 00:05:29,360
going to focus on the cad forn and that

130
00:05:25,800 --> 00:05:31,000
shows us that it has bytes 00 and 08 now

131
00:05:29,360 --> 00:05:33,000
this point we don't know if the

132
00:05:31,000 --> 00:05:34,759
disassembler is actually showing this

133
00:05:33,000 --> 00:05:38,039
little Endy in order or big Endy in

134
00:05:34,759 --> 00:05:40,880
order so to remove any ambiguity we go

135
00:05:38,039 --> 00:05:44,919
ahead and do a hex display so we display

136
00:05:40,880 --> 00:05:47,880
memory as one halfword 16bit value

137
00:05:44,919 --> 00:05:51,479
specified in hexadecimal format so if we

138
00:05:47,880 --> 00:05:54,240
do that we get this a 16bit value where

139
00:05:51,479 --> 00:05:56,600
the most significant bite is 08 and the

140
00:05:54,240 --> 00:05:58,520
least significant bite is 0 so there's

141
00:05:56,600 --> 00:06:00,199
no ambiguity there it's always sort of

142
00:05:58,520 --> 00:06:01,840
big endian order once once you see a

143
00:06:00,199 --> 00:06:03,919
value like this all right so go ahead

144
00:06:01,840 --> 00:06:07,759
and fill in that

145
00:06:03,919 --> 00:06:10,280
080 in HEX so zero these four bits are

146
00:06:07,759 --> 00:06:12,919
the most significant zero these four

147
00:06:10,280 --> 00:06:15,160
bits are the eight zero and zero so we

148
00:06:12,919 --> 00:06:17,360
fill in all those bits we transfer them

149
00:06:15,160 --> 00:06:20,199
down into immediate bit four and five

150
00:06:17,360 --> 00:06:22,360
and 9 and 8 and so forth and so putting

151
00:06:20,199 --> 00:06:24,759
all those bits through what we get is

152
00:06:22,360 --> 00:06:27,599
bit zero is hardcoded to zero bit one is

153
00:06:24,759 --> 00:06:29,479
hardcoded to zero and then bit two comes

154
00:06:27,599 --> 00:06:32,240
from this zero bit three comes from that

155
00:06:29,479 --> 00:06:35,680
not zero and bit four is the one that

156
00:06:32,240 --> 00:06:37,960
came from this x nibble 8 so that one

157
00:06:35,680 --> 00:06:40,680
transfers through and that ultimately

158
00:06:37,960 --> 00:06:43,599
leaves us with this 10 bit value 10 bit

159
00:06:40,680 --> 00:06:45,560
value encoded with only 8 Bits and the

160
00:06:43,599 --> 00:06:48,960
least significant two bits hard-coded to

161
00:06:45,560 --> 00:06:51,319
zero if we interpret those binary digits

162
00:06:48,960 --> 00:06:53,880
as hexadecimal we get the least

163
00:06:51,319 --> 00:06:58,039
significant nibble four bits of Zer is

164
00:06:53,880 --> 00:07:02,000
zero four bits of 0000 1 is 1 and upper

165
00:06:58,039 --> 00:07:04,120
two bits of 00 is0 that gives us hex 10

166
00:07:02,000 --> 00:07:06,720
which is decimal 16 so this is the

167
00:07:04,120 --> 00:07:08,400
actual 16 that we see encoded in the

168
00:07:06,720 --> 00:07:09,919
assembly instruction here and you know

169
00:07:08,400 --> 00:07:12,520
how is it actually encoded behind the

170
00:07:09,919 --> 00:07:14,720
scenes you could say this is encoded as

171
00:07:12,520 --> 00:07:16,800
for like if you ignore the fact that

172
00:07:14,720 --> 00:07:19,319
there are these two implicit things the

173
00:07:16,800 --> 00:07:21,479
actual you know value here is four in

174
00:07:19,319 --> 00:07:23,599
some sense but you can also see that

175
00:07:21,479 --> 00:07:27,479
it's not like just straight up encoded

176
00:07:23,599 --> 00:07:29,560
as a four there's no like 0 0 1 0 0 Z

177
00:07:27,479 --> 00:07:31,680
because all these bits are all scattered

178
00:07:29,560 --> 00:07:34,039
and rearranged in their order so

179
00:07:31,680 --> 00:07:37,000
basically I would just treat it like

180
00:07:34,039 --> 00:07:38,759
look it's encoded as a 10 bit value the

181
00:07:37,000 --> 00:07:41,240
bottom two bits because it says scaled

182
00:07:38,759 --> 00:07:42,560
by four are actually zero and zero and

183
00:07:41,240 --> 00:07:44,520
you know just when you look at the

184
00:07:42,560 --> 00:07:46,840
encoding you can see there is no zeroth

185
00:07:44,520 --> 00:07:50,560
bit there is no oneth bit so ultimately

186
00:07:46,840 --> 00:07:52,720
it is just encoding decimal 16 X10 and

187
00:07:50,560 --> 00:07:54,360
so we'll see this scaled by X thing

188
00:07:52,720 --> 00:07:56,319
occur multiple times and whenever you

189
00:07:54,360 --> 00:07:58,240
see a scaled by X you can just treat it

190
00:07:56,319 --> 00:07:59,560
as well if they say scaled by two that

191
00:07:58,240 --> 00:08:01,800
means the least significant bit bit is

192
00:07:59,560 --> 00:08:03,639
zero scaled by four two bits are zero

193
00:08:01,800 --> 00:08:05,879
scaled by eight three bits are zero

194
00:08:03,639 --> 00:08:07,800
scaled by 16 four bits are zero and so

195
00:08:05,879 --> 00:08:10,039
forth but if you look at the actual you

196
00:08:07,800 --> 00:08:12,360
know description it'll ultimately become

197
00:08:10,039 --> 00:08:14,400
unambiguous of course this is why we

198
00:08:12,360 --> 00:08:16,319
wanted to leave reading the fun manual

199
00:08:14,400 --> 00:08:18,000
till later on and we will come back to

200
00:08:16,319 --> 00:08:20,039
it again but we're going to just keep

201
00:08:18,000 --> 00:08:22,199
trucking with these couple of compressed

202
00:08:20,039 --> 00:08:24,159
instructions just for now just because

203
00:08:22,199 --> 00:08:26,919
they're going to keep coming up as we

204
00:08:24,159 --> 00:08:28,960
use Scarlet Witch's power to blast away

205
00:08:26,919 --> 00:08:31,039
the aliases instruction aliases we're

206
00:08:28,960 --> 00:08:32,880
going to keep seeing these so we kind of

207
00:08:31,039 --> 00:08:35,159
need to cover them early on in the class

208
00:08:32,880 --> 00:08:38,080
but back to covering what exactly this

209
00:08:35,159 --> 00:08:40,000
instruction does so we've got the zero

210
00:08:38,080 --> 00:08:41,640
extended nonzero immediate so it's

211
00:08:40,000 --> 00:08:44,120
telling you you can't use you can't

212
00:08:41,640 --> 00:08:46,000
encode a zero here otherwise that's

213
00:08:44,120 --> 00:08:49,880
going to be considered invalid and it's

214
00:08:46,000 --> 00:08:53,399
also unsigned or zero extended so it is

215
00:08:49,880 --> 00:08:54,959
zero extended before you add it to the

216
00:08:53,399 --> 00:08:57,360
uh register before you add it to the

217
00:08:54,959 --> 00:09:00,519
stack pointer register and before it's

218
00:08:57,360 --> 00:09:02,200
placed into the destination register

219
00:09:00,519 --> 00:09:04,560
but there's also that element at the end

220
00:09:02,200 --> 00:09:07,360
of here where it says non-destructive so

221
00:09:04,560 --> 00:09:09,160
what does non-destructive mean well it

222
00:09:07,360 --> 00:09:11,240
means that it's adding to the stack

223
00:09:09,160 --> 00:09:13,360
pointer and it's actually hardcoded that

224
00:09:11,240 --> 00:09:16,320
it adds to the stack pointer before

225
00:09:13,360 --> 00:09:18,160
putting it into the destination register

226
00:09:16,320 --> 00:09:20,160
but it is not like adding to the stack

227
00:09:18,160 --> 00:09:21,920
pointer in a way that changes the stack

228
00:09:20,160 --> 00:09:23,959
pointer it's just add to the stack

229
00:09:21,920 --> 00:09:26,800
pointer and put it in you know some

230
00:09:23,959 --> 00:09:28,600
temporary you know like actually hidden

231
00:09:26,800 --> 00:09:30,839
register that you the assembly

232
00:09:28,600 --> 00:09:33,079
programmer can't see so it places it in

233
00:09:30,839 --> 00:09:34,360
a scratch register calculates the value

234
00:09:33,079 --> 00:09:36,160
and stores that value into the

235
00:09:34,360 --> 00:09:38,720
destination without ever actually

236
00:09:36,160 --> 00:09:40,880
changing SP so this is how I would kind

237
00:09:38,720 --> 00:09:42,880
of uh describe this instruction using

238
00:09:40,880 --> 00:09:46,120
the conventions we use in this class we

239
00:09:42,880 --> 00:09:49,279
have ADI forn we've got the destination

240
00:09:46,120 --> 00:09:51,160
Rd prime Rd prime can only be specified

241
00:09:49,279 --> 00:09:53,920
by three bits so that's going to be some

242
00:09:51,160 --> 00:09:57,040
subset of the 32 possible registers you

243
00:09:53,920 --> 00:09:59,560
can't encode all possible 32 registers

244
00:09:57,040 --> 00:10:03,360
in only three bits so some subset of

245
00:09:59,560 --> 00:10:06,200
registers and take SP plus your nonzero

246
00:10:03,360 --> 00:10:08,920
zero extended 10bit immediate that is

247
00:10:06,200 --> 00:10:10,680
also scaled by4 as we saw the key thing

248
00:10:08,920 --> 00:10:12,480
is this is not actually going to memory

249
00:10:10,680 --> 00:10:14,360
at stack pointer plus 10 so we're not

250
00:10:12,480 --> 00:10:16,519
dereferencing memory there's no

251
00:10:14,360 --> 00:10:18,760
parentheses here to indicate that we're

252
00:10:16,519 --> 00:10:20,720
dereferencing memory just stack pointer

253
00:10:18,760 --> 00:10:23,720
plus immediate goes into the destination

254
00:10:20,720 --> 00:10:26,680
register as I said the Rd prime can only

255
00:10:23,720 --> 00:10:29,240
be the so there's only three bits and

256
00:10:26,680 --> 00:10:31,880
they've chosen to say that the encoded

257
00:10:29,240 --> 00:10:35,720
of those three bits will encode X8

258
00:10:31,880 --> 00:10:38,000
through X15 also known as s0 S1 a0

259
00:10:35,720 --> 00:10:41,200
through a5 and then we saw that the

260
00:10:38,000 --> 00:10:44,200
nonzero unsigned or zero extended

261
00:10:41,200 --> 00:10:46,000
immediate 10 is scaled by four so the

262
00:10:44,200 --> 00:10:48,519
bottom two bits are implicit zeros which

263
00:10:46,000 --> 00:10:50,720
means only eight bits are actually used

264
00:10:48,519 --> 00:10:52,680
to encode this 10 bit immediate all

265
00:10:50,720 --> 00:10:56,480
right with that we have checked off the

266
00:10:52,680 --> 00:10:58,720
C ADI forn non-destructive so add

267
00:10:56,480 --> 00:11:01,040
immediate times 4 so really just an

268
00:10:58,720 --> 00:11:02,800
immediate that's encoded in a way that

269
00:11:01,040 --> 00:11:06,200
must always be a multiple of four

270
00:11:02,800 --> 00:11:07,760
immediate times 4 to the stack pointer

271
00:11:06,200 --> 00:11:09,440
non-destructive so we're not changing

272
00:11:07,760 --> 00:11:12,279
the stack pointer we're changing

273
00:11:09,440 --> 00:11:15,399
whatever the destination prime register

274
00:11:12,279 --> 00:11:15,399
is here

