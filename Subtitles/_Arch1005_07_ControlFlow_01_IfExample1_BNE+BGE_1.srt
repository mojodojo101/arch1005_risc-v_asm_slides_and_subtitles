1
00:00:00,399 --> 00:00:05,520
now it's time to see some examples of

2
00:00:02,600 --> 00:00:08,120
typical C control flow constructs

3
00:00:05,520 --> 00:00:11,400
starting with the if statements so here

4
00:00:08,120 --> 00:00:14,759
in main we have a long a which is 1 B

5
00:00:11,400 --> 00:00:17,359
is 2 then we check if a equals B which

6
00:00:14,759 --> 00:00:19,880
it's not going to be true return one if

7
00:00:17,359 --> 00:00:22,400
a is greater than b which is also not

8
00:00:19,880 --> 00:00:25,279
going to be true then return two and a

9
00:00:22,400 --> 00:00:27,920
is less than b then return three so of

10
00:00:25,279 --> 00:00:30,000
course return three will be returned it

11
00:00:27,920 --> 00:00:31,279
will never possibly return defeated

12
00:00:30,000 --> 00:00:33,520
because of course something must be

13
00:00:31,279 --> 00:00:36,360
either equal to greater than or less

14
00:00:33,520 --> 00:00:38,280
than so this is just dead code but we

15
00:00:36,360 --> 00:00:40,719
are compiling with optimization level

16
00:00:38,280 --> 00:00:43,920
zero so we do expect the compiler to

17
00:00:40,719 --> 00:00:46,000
still insert it compile it and we get

18
00:00:43,920 --> 00:00:48,399
this and if we skim down we can see yes

19
00:00:46,000 --> 00:00:52,000
there is a reference to defeat in there

20
00:00:48,399 --> 00:00:55,640
new assembly instructions b& and BGE

21
00:00:52,000 --> 00:00:58,960
what are those branch not equal branch

22
00:00:55,640 --> 00:01:01,000
if not equal so takes two parameters

23
00:00:58,960 --> 00:01:04,199
source one and source SCE 2 and then an

24
00:01:01,000 --> 00:01:08,720
offset of where to branch so you do if

25
00:01:04,199 --> 00:01:11,560
rs1 not equal rs2 then it branches to

26
00:01:08,720 --> 00:01:14,280
the offset meaning it takes the program

27
00:01:11,560 --> 00:01:17,280
counter and adds the offset which behind

28
00:01:14,280 --> 00:01:20,159
the scenes is a signed immediate 13 so

29
00:01:17,280 --> 00:01:23,079
13 bits and it's scaled by two so the

30
00:01:20,159 --> 00:01:27,320
least significant bit is always zero so

31
00:01:23,079 --> 00:01:31,360
PC plus or minus immediate 13 is the new

32
00:01:27,320 --> 00:01:33,880
pc else then it just falls through so if

33
00:01:31,360 --> 00:01:36,200
they are equal so if this condition is

34
00:01:33,880 --> 00:01:38,200
not true then it's just going to fall

35
00:01:36,200 --> 00:01:40,399
through it's not going to branch and the

36
00:01:38,200 --> 00:01:42,479
next assembly instruction will be at PC

37
00:01:40,399 --> 00:01:45,240
plus4 because we're dealing with a

38
00:01:42,479 --> 00:01:47,560
32-bit non-compressed assembly

39
00:01:45,240 --> 00:01:49,360
instruction so just in practice you

40
00:01:47,560 --> 00:01:51,680
should know that disassemblers will tend

41
00:01:49,360 --> 00:01:53,759
to show what I'm specifying here as

42
00:01:51,680 --> 00:01:56,000
offset they will show you just the

43
00:01:53,759 --> 00:01:58,280
absolute target address not the relative

44
00:01:56,000 --> 00:02:01,399
displacement so for instance here in

45
00:01:58,280 --> 00:02:04,600
this code we have b& it says a a4 a5 so

46
00:02:01,399 --> 00:02:06,479
branch if a4 not equal to a5 and where's

47
00:02:04,600 --> 00:02:10,119
it going to branch to it'll give you the

48
00:02:06,479 --> 00:02:13,440
absolute address of main plus 34 or all

49
00:02:10,119 --> 00:02:15,640
fives 64a which we can see is lower down

50
00:02:13,440 --> 00:02:17,640
in the assembly but behind the scenes

51
00:02:15,640 --> 00:02:21,120
this is not like an absolute addressing

52
00:02:17,640 --> 00:02:24,280
coding it is a relative immediate 13 so

53
00:02:21,120 --> 00:02:27,319
if that's b& branch not equal what is BG

54
00:02:24,280 --> 00:02:30,519
it is branch if greater than or equal so

55
00:02:27,319 --> 00:02:33,879
again two parameters an rs1 and rs2 and

56
00:02:30,519 --> 00:02:37,040
an offset and so the comparison is if

57
00:02:33,879 --> 00:02:39,599
rs1 is greater than or equal rs2 where

58
00:02:37,040 --> 00:02:41,760
the greater than here is treated as a

59
00:02:39,599 --> 00:02:43,200
signed comparison so it's going to take

60
00:02:41,760 --> 00:02:46,280
into account whether they're positive or

61
00:02:43,200 --> 00:02:49,560
negative values so if rs1 greater than

62
00:02:46,280 --> 00:02:51,480
or equal to rs2 then again PC plus

63
00:02:49,560 --> 00:02:54,480
offset where offset is encoded by an

64
00:02:51,480 --> 00:02:57,640
immediate 13 scaled by two and that's

65
00:02:54,480 --> 00:02:59,440
your new pc if not then just fall

66
00:02:57,640 --> 00:03:02,000
through don't branch go to the next

67
00:02:59,440 --> 00:03:03,720
thing which is going to be PC plus4 and

68
00:03:02,000 --> 00:03:05,920
just like with b& you should expect

69
00:03:03,720 --> 00:03:09,159
disassemblers to show the offset as an

70
00:03:05,920 --> 00:03:11,760
absolute target so BGE branch if greater

71
00:03:09,159 --> 00:03:14,360
than or equal is a5 greater than or

72
00:03:11,760 --> 00:03:16,720
equal to a4 if so go to this address if

73
00:03:14,360 --> 00:03:20,080
not fall through to the next one which

74
00:03:16,720 --> 00:03:22,040
is PC plus4 okay so that's all we need

75
00:03:20,080 --> 00:03:23,879
to know to understand this code so it's

76
00:03:22,040 --> 00:03:25,920
time for you to stop step through the

77
00:03:23,879 --> 00:03:27,480
assembly check your understanding make

78
00:03:25,920 --> 00:03:29,120
sure you understand what's going on with

79
00:03:27,480 --> 00:03:31,360
the new instructions I just told you

80
00:03:29,120 --> 00:03:35,599
about and draw a stack diagram if you

81
00:03:31,360 --> 00:03:35,599
feel like it it's always a good habit

