1
00:00:00,399 --> 00:00:05,400
and now we get to my favorite lab in the

2
00:00:02,280 --> 00:00:07,279
whole wed world the binary bomb lab so

3
00:00:05,400 --> 00:00:09,160
this comes from the Carnegie melon

4
00:00:07,279 --> 00:00:11,639
computer architecture class by

5
00:00:09,160 --> 00:00:13,360
professors Bryant and O helerin and I

6
00:00:11,639 --> 00:00:15,480
appreciate them providing the source

7
00:00:13,360 --> 00:00:17,320
code for this lab so that we could port

8
00:00:15,480 --> 00:00:19,119
it between different architectures

9
00:00:17,320 --> 00:00:22,199
originally it's provided in the context

10
00:00:19,119 --> 00:00:24,400
of x86 and the students only get a x86

11
00:00:22,199 --> 00:00:27,480
binary but they provided us the source

12
00:00:24,400 --> 00:00:29,439
so we compiled it for RISC-V they also

13
00:00:27,480 --> 00:00:31,519
wanted me to mention that the class

14
00:00:29,439 --> 00:00:33,559
textbook is available here and you can

15
00:00:31,519 --> 00:00:36,239
thank that textbook for me drawing the

16
00:00:33,559 --> 00:00:38,879
stack diagrams the way I do so what is

17
00:00:36,239 --> 00:00:42,719
the goal of the binary bomb lab you are

18
00:00:38,879 --> 00:00:44,719
given opaque binary blob and you run it

19
00:00:42,719 --> 00:00:47,520
and you have to figure out what input

20
00:00:44,719 --> 00:00:49,120
does it want to not explode so

21
00:00:47,520 --> 00:00:51,559
fundamentally this is a reverse

22
00:00:49,120 --> 00:00:53,440
engineering exercise and when I first

23
00:00:51,559 --> 00:00:55,600
had this class not at Carnegie melon but

24
00:00:53,440 --> 00:00:57,840
at the University of Minnesota this

25
00:00:55,600 --> 00:00:59,960
ultimately got me interested in assembly

26
00:00:57,840 --> 00:01:01,840
and reverse engineering and you know

27
00:00:59,960 --> 00:01:04,799
it's been really foundational for my

28
00:01:01,840 --> 00:01:07,000
learning ever since so what I'm going to

29
00:01:04,799 --> 00:01:09,400
do is I'm going to walk you through the

30
00:01:07,000 --> 00:01:11,520
first phase of the bomb and then I

31
00:01:09,400 --> 00:01:13,960
expect you to do all five remaining

32
00:01:11,520 --> 00:01:15,640
phases on your own when I was first

33
00:01:13,960 --> 00:01:18,759
doing this as part of the University of

34
00:01:15,640 --> 00:01:21,000
Minnesota class I did it with a partner

35
00:01:18,759 --> 00:01:24,520
and between the two of us working on and

36
00:01:21,000 --> 00:01:26,520
off it took us something like two weeks

37
00:01:24,520 --> 00:01:29,040
now when I was first taking this class

38
00:01:26,520 --> 00:01:31,320
at U ofm I was also trying to put myself

39
00:01:29,040 --> 00:01:33,520
through school and pay for it myself by

40
00:01:31,320 --> 00:01:35,600
working night shift at a hotel meaning I

41
00:01:33,520 --> 00:01:37,320
worked overnight and they told us that

42
00:01:35,600 --> 00:01:39,079
that would allow us to do our homework

43
00:01:37,320 --> 00:01:41,320
and things like that but that really

44
00:01:39,079 --> 00:01:44,000
didn't ever work out so my first

45
00:01:41,320 --> 00:01:46,719
semester or my second semester at uh my

46
00:01:44,000 --> 00:01:49,159
freshman year basically I got three C's

47
00:01:46,719 --> 00:01:51,240
and a b and I got a C in the computer

48
00:01:49,159 --> 00:01:52,840
architecture class so although it took

49
00:01:51,240 --> 00:01:55,079
me two weeks to finish this with a

50
00:01:52,840 --> 00:01:56,960
partner the first time I went off and

51
00:01:55,079 --> 00:01:59,280
didn't want to have that C on my record

52
00:01:56,960 --> 00:02:01,280
so I retook the class later on in my

53
00:01:59,280 --> 00:02:03,360
college career and the second time

54
00:02:01,280 --> 00:02:05,159
through it was much much easier and

55
00:02:03,360 --> 00:02:07,000
that's the way it always goes the second

56
00:02:05,159 --> 00:02:08,840
time through is so much easier if you

57
00:02:07,000 --> 00:02:11,640
take this class again in a year I

58
00:02:08,840 --> 00:02:13,000
guarantee it will be ridiculously easy

59
00:02:11,640 --> 00:02:15,239
so the second time through instead of

60
00:02:13,000 --> 00:02:18,560
two weeks it took me about 2 hours all

61
00:02:15,239 --> 00:02:20,640
by myself so this is a lab that can take

62
00:02:18,560 --> 00:02:22,879
quite a lot of time or it could take not

63
00:02:20,640 --> 00:02:25,720
much time at all if you've seen it in

64
00:02:22,879 --> 00:02:27,480
other classes where we use it it may go

65
00:02:25,720 --> 00:02:30,160
relatively fast but don't worry we have

66
00:02:27,480 --> 00:02:31,720
an expert mode for you okay I haven't

67
00:02:30,160 --> 00:02:33,920
decided where exactly we're going to

68
00:02:31,720 --> 00:02:36,160
place this binary bomb in your labs yet

69
00:02:33,920 --> 00:02:38,760
but I've placed it in my home directory

70
00:02:36,160 --> 00:02:41,120
so if I just execute the bomb like that

71
00:02:38,760 --> 00:02:43,360
I see it say welcome to my fish little

72
00:02:41,120 --> 00:02:45,280
bomb you have six phases with which to

73
00:02:43,360 --> 00:02:47,959
blow yourself up have a nice day and

74
00:02:45,280 --> 00:02:49,480
then it's got a blinking cursor if I hit

75
00:02:47,959 --> 00:02:51,360
enter a few times a whole bunch of

76
00:02:49,480 --> 00:02:54,400
nothing happens if I type some

77
00:02:51,360 --> 00:02:57,319
characters and hit enter oh boom then I

78
00:02:54,400 --> 00:02:59,800
got a boom the bomb has blown up that's

79
00:02:57,319 --> 00:03:01,560
not what I want to see so of course

80
00:02:59,800 --> 00:03:05,519
course I could brute force this and type

81
00:03:01,560 --> 00:03:07,239
a single a and then type two a but

82
00:03:05,519 --> 00:03:09,400
that's not the smart way to go about it

83
00:03:07,239 --> 00:03:11,680
right we know how to use a debugger and

84
00:03:09,400 --> 00:03:13,920
so we need to figure out where does it

85
00:03:11,680 --> 00:03:16,760
print out boom how do I avoid that what

86
00:03:13,920 --> 00:03:20,799
sort of input would cause it to not go

87
00:03:16,760 --> 00:03:23,480
boom so I'm going to cat out my x.c

88
00:03:20,799 --> 00:03:25,280
config just to show that right now I'm

89
00:03:23,480 --> 00:03:27,840
going to be printing out the return

90
00:03:25,280 --> 00:03:31,200
address the program counter the frame

91
00:03:27,840 --> 00:03:33,680
pointer s0 the stack pointer and

92
00:03:31,200 --> 00:03:35,920
arguments 0 through 5 that's just what

93
00:03:33,680 --> 00:03:37,920
happens to have been in my XTO config at

94
00:03:35,920 --> 00:03:39,799
this point after fiddling around and all

95
00:03:37,920 --> 00:03:43,599
the other labs and stuff going to print

96
00:03:39,799 --> 00:03:45,519
out 14 giant words 14 64-bit values from

97
00:03:43,599 --> 00:03:47,280
the stack pointer and print out 20

98
00:03:45,519 --> 00:03:49,120
instructions before setting a break on

99
00:03:47,280 --> 00:03:51,959
main and running the

100
00:03:49,120 --> 00:03:54,439
program so let's go ahead and start up

101
00:03:51,959 --> 00:04:01,680
the bomb in the debugger GDB

102
00:03:54,439 --> 00:04:01,680
DX and /x. config and the bomb

103
00:04:04,319 --> 00:04:08,079
when this starts up it'll give me all of

104
00:04:06,000 --> 00:04:10,680
my registers that I want to see and

105
00:04:08,079 --> 00:04:13,720
it'll also give me 20 instructions of

106
00:04:10,680 --> 00:04:16,079
main okay so GDB is showing me my

107
00:04:13,720 --> 00:04:19,040
registers that I want my stack that I

108
00:04:16,079 --> 00:04:21,840
want and also 20 assembly instructions

109
00:04:19,040 --> 00:04:25,240
if I want to see the full main I can do

110
00:04:21,840 --> 00:04:27,520
disas for disassemble and main and

111
00:04:25,240 --> 00:04:30,440
that'll give me the full thing and I can

112
00:04:27,520 --> 00:04:32,199
continue on and so if we just skim again

113
00:04:30,440 --> 00:04:34,039
this is the big secret of reverse

114
00:04:32,199 --> 00:04:35,840
engineering look at at a high level then

115
00:04:34,039 --> 00:04:38,160
go down to a low level then look it at a

116
00:04:35,840 --> 00:04:40,479
high level then go down to a low level

117
00:04:38,160 --> 00:04:43,440
so at the high level our initial skim

118
00:04:40,479 --> 00:04:45,520
tells us we've got calls to F open so

119
00:04:43,440 --> 00:04:47,680
there's going to be some sort of file

120
00:04:45,520 --> 00:04:50,639
opening there's some sort of call to

121
00:04:47,680 --> 00:04:53,600
initialize bomb there's put s which we

122
00:04:50,639 --> 00:04:55,919
know is printing stuff out put s a

123
00:04:53,600 --> 00:04:58,479
function called readline a function

124
00:04:55,919 --> 00:05:01,720
called phase one function called phase

125
00:04:58,479 --> 00:05:04,400
one diffused putf puts so print

126
00:05:01,720 --> 00:05:07,680
something out read line phase two phase

127
00:05:04,400 --> 00:05:09,880
diffused so printing reading phase

128
00:05:07,680 --> 00:05:11,759
diffused printing reading phase diffused

129
00:05:09,880 --> 00:05:13,600
so there's a general pattern going on

130
00:05:11,759 --> 00:05:16,479
here and that looks like that continues

131
00:05:13,600 --> 00:05:18,240
all the way down to phase six but we are

132
00:05:16,479 --> 00:05:20,240
focused on phase one right now and we

133
00:05:18,240 --> 00:05:22,000
want to figure out how do we get from

134
00:05:20,240 --> 00:05:24,360
phase one to phase two first of all or

135
00:05:22,000 --> 00:05:27,199
from more to the point does it explode

136
00:05:24,360 --> 00:05:29,960
in Phase one like if I step over phase

137
00:05:27,199 --> 00:05:32,520
one do I see a boom or or is it

138
00:05:29,960 --> 00:05:34,000
somewhere else that that's occurring so

139
00:05:32,520 --> 00:05:35,919
our first you know question and

140
00:05:34,000 --> 00:05:38,080
hypothesis to ourselves is we will

141
00:05:35,919 --> 00:05:40,680
hypothesize that if we step over phase

142
00:05:38,080 --> 00:05:42,720
one that it'll go boom if we give the

143
00:05:40,680 --> 00:05:45,600
incorrect input we will test that

144
00:05:42,720 --> 00:05:49,759
hypothesis by copying this address

145
00:05:45,600 --> 00:05:51,800
setting a break point and continuing on

146
00:05:49,759 --> 00:05:54,800
okay we continued and we're now at this

147
00:05:51,800 --> 00:05:57,400
input where it seems to say or this

148
00:05:54,800 --> 00:06:00,240
output where it wants some input so I'm

149
00:05:57,400 --> 00:06:03,199
going to type hello and I'm going to hit

150
00:06:00,240 --> 00:06:05,479
enter so just typing hello and hitting

151
00:06:03,199 --> 00:06:06,759
enter it is not yet went boom we are

152
00:06:05,479 --> 00:06:08,960
still in the debugger it is still

153
00:06:06,759 --> 00:06:11,599
continuing on so we're now at phase one

154
00:06:08,960 --> 00:06:14,280
so we can eliminate any idea that the

155
00:06:11,599 --> 00:06:16,680
boom happens before phase one it cannot

156
00:06:14,280 --> 00:06:18,479
happen earlier than phase one next we

157
00:06:16,680 --> 00:06:20,120
want to see does it happen inside of

158
00:06:18,479 --> 00:06:22,039
phase one and to test that we're going

159
00:06:20,120 --> 00:06:25,120
to go ahead and step over it we're going

160
00:06:22,039 --> 00:06:27,560
to use the ni for next instruction that

161
00:06:25,120 --> 00:06:29,840
does not go into that's basically like a

162
00:06:27,560 --> 00:06:32,280
step over that's not a step into so we

163
00:06:29,840 --> 00:06:34,039
will step over this instruction hoping

164
00:06:32,280 --> 00:06:37,680
that we reach the next assembly

165
00:06:34,039 --> 00:06:40,880
instruction so hit ni but no that did

166
00:06:37,680 --> 00:06:43,880
not work Boom the bomb has blown up so

167
00:06:40,880 --> 00:06:45,560
somewhere in phase one is the boom and

168
00:06:43,880 --> 00:06:47,240
we need to find where that is and avoid

169
00:06:45,560 --> 00:06:49,280
it so the next thing we could do before

170
00:06:47,240 --> 00:06:52,599
we restart the process is we could go

171
00:06:49,280 --> 00:06:57,360
ahead and disas phase

172
00:06:52,599 --> 00:06:57,360
one and go ahead and look

173
00:06:57,720 --> 00:07:04,560
at we can go ahead and look at just

174
00:06:59,919 --> 00:07:06,080
phase one so this is the phase one so we

175
00:07:04,560 --> 00:07:07,280
could look at every single assembly

176
00:07:06,080 --> 00:07:09,199
instruction figure out what they all do

177
00:07:07,280 --> 00:07:11,759
right now but let's not do that let's

178
00:07:09,199 --> 00:07:14,520
start with a skim and the starting with

179
00:07:11,759 --> 00:07:18,680
a skim is that we see something called

180
00:07:14,520 --> 00:07:21,680
strings not equal we see some bz we see

181
00:07:18,680 --> 00:07:24,120
an explode bomb so that's kind of

182
00:07:21,680 --> 00:07:26,479
interesting a String's not equal call

183
00:07:24,120 --> 00:07:28,400
and an explode bomb call in here kind of

184
00:07:26,479 --> 00:07:30,919
makes me think that this conditional

185
00:07:28,400 --> 00:07:33,720
control flow the branch if not equal to

186
00:07:30,919 --> 00:07:35,240
zero that's checking the argument zero

187
00:07:33,720 --> 00:07:39,560
which is going to be the return from

188
00:07:35,240 --> 00:07:41,639
strings not equal if bz if this it's

189
00:07:39,560 --> 00:07:43,879
going to branch if this is not equal to

190
00:07:41,639 --> 00:07:46,720
zero where does it branch to phase 1

191
00:07:43,879 --> 00:07:49,199
plus 24 and that is Phase 1 plus 24

192
00:07:46,720 --> 00:07:51,720
branch to the explode bomb so if that's

193
00:07:49,199 --> 00:07:54,000
not equal to zero go to explode bomb

194
00:07:51,720 --> 00:07:55,639
otherwise if it's zero then it's not

195
00:07:54,000 --> 00:07:57,440
going to go to explode bomb and it's

196
00:07:55,639 --> 00:07:58,599
going to continue on in return so that's

197
00:07:57,440 --> 00:08:00,240
what I mean about you know we don't want

198
00:07:58,599 --> 00:08:01,960
to just start by reading every single

199
00:08:00,240 --> 00:08:04,479
assembly instruction let's only read

200
00:08:01,960 --> 00:08:06,639
them if they seem relevant to our goals

201
00:08:04,479 --> 00:08:08,400
and our goals are to avoid the explode

202
00:08:06,639 --> 00:08:12,039
bomb and we can see that we get to the

203
00:08:08,400 --> 00:08:14,840
explode Bomb by the result of a jump a

204
00:08:12,039 --> 00:08:17,080
branch that has to do with um whether or

205
00:08:14,840 --> 00:08:19,199
not this output of the strings not equal

206
00:08:17,080 --> 00:08:21,800
walking backwards if the output is not

207
00:08:19,199 --> 00:08:24,360
equal to zero we go boom so all right

208
00:08:21,800 --> 00:08:26,240
we've got ourselves some Clues we've got

209
00:08:24,360 --> 00:08:27,479
um we've got you know enough to go on

210
00:08:26,240 --> 00:08:29,960
right now that let's go ahead and

211
00:08:27,479 --> 00:08:32,200
restart the program our for run we're

212
00:08:29,960 --> 00:08:36,240
running the program with no command line

213
00:08:32,200 --> 00:08:38,320
arguments and we continue on and we have

214
00:08:36,240 --> 00:08:39,919
previously set a breakpoint on that

215
00:08:38,320 --> 00:08:43,080
phase one so let's just go ahead and

216
00:08:39,919 --> 00:08:45,920
continue all right here's my request for

217
00:08:43,080 --> 00:08:48,080
input going to say hello question mark

218
00:08:45,920 --> 00:08:50,360
and I'm going to go ahead and step into

219
00:08:48,080 --> 00:08:53,080
this phase one SI for step in

220
00:08:50,360 --> 00:08:55,560
instruction we are now inside phase one

221
00:08:53,080 --> 00:08:58,120
but I really want to get to this strings

222
00:08:55,560 --> 00:09:00,480
not equal so I want to see what are the

223
00:08:58,120 --> 00:09:03,240
inputs to a function called strings not

224
00:09:00,480 --> 00:09:05,519
equal and just walking backwards from

225
00:09:03,240 --> 00:09:08,760
that I can see well there's an a1 being

226
00:09:05,519 --> 00:09:11,760
set and so there's no other a z

227
00:09:08,760 --> 00:09:13,360
explicitly said here so maybe it's using

228
00:09:11,760 --> 00:09:15,440
whatever is in a z at the time we come

229
00:09:13,360 --> 00:09:17,880
in we don't know but let's go ahead and

230
00:09:15,440 --> 00:09:19,839
assume that a1 is a argument being

231
00:09:17,880 --> 00:09:21,800
passed to this and that it has two

232
00:09:19,839 --> 00:09:24,040
arguments for doing some sort of strings

233
00:09:21,800 --> 00:09:28,040
not equal comparison so let's go ahead

234
00:09:24,040 --> 00:09:31,440
and step our way right up to that call

235
00:09:28,040 --> 00:09:33,399
the jaw we want to see right before we

236
00:09:31,440 --> 00:09:35,680
call into this thing like we clearly

237
00:09:33,399 --> 00:09:38,480
haven't gone to explode bomb yet our

238
00:09:35,680 --> 00:09:41,640
hypothesis is that these argument the a

239
00:09:38,480 --> 00:09:44,360
z and the a1 are arguments to the

240
00:09:41,640 --> 00:09:46,839
strings not equal and we don't know what

241
00:09:44,360 --> 00:09:48,600
kind of arguments they are so we could

242
00:09:46,839 --> 00:09:52,200
examine them as various things we could

243
00:09:48,600 --> 00:09:56,000
say let's look at it as if it's um 10

244
00:09:52,200 --> 00:09:58,600
bytes or let's call it hex uh

245
00:09:56,000 --> 00:10:01,600
by and let's put that address and

246
00:09:58,600 --> 00:10:03,600
that'll give us plus 10 hexes at that

247
00:10:01,600 --> 00:10:06,240
value first of all we should also say

248
00:10:03,600 --> 00:10:09,800
that they they really look like pointers

249
00:10:06,240 --> 00:10:12,040
roughly into the code area so we can see

250
00:10:09,800 --> 00:10:13,200
they don't start with like seven FFF and

251
00:10:12,040 --> 00:10:15,399
something so they don't seem like

252
00:10:13,200 --> 00:10:17,279
they're pointing to uh the stack or

253
00:10:15,399 --> 00:10:19,959
anything like that they seem to have the

254
00:10:17,279 --> 00:10:22,040
same general address range of all fives

255
00:10:19,959 --> 00:10:23,399
that we see with the code itself so we

256
00:10:22,040 --> 00:10:25,040
don't know what those arguments are but

257
00:10:23,399 --> 00:10:27,519
we do know they're probably not pointing

258
00:10:25,040 --> 00:10:29,240
at the stack so they've if we look at

259
00:10:27,519 --> 00:10:32,160
the memory at those addresses we see

260
00:10:29,240 --> 00:10:35,160
some values and maybe those values look

261
00:10:32,160 --> 00:10:37,399
suspiciously within a small range to you

262
00:10:35,160 --> 00:10:40,920
let's look at the other one let's look

263
00:10:37,399 --> 00:10:43,880
at this thing 10 hex bytes at that

264
00:10:40,920 --> 00:10:46,959
address all right some more values that

265
00:10:43,880 --> 00:10:49,600
also are suspiciously in a small range

266
00:10:46,959 --> 00:10:52,480
in the sixish range well let's go ahead

267
00:10:49,600 --> 00:10:54,720
and just uh call a spade a spade and say

268
00:10:52,480 --> 00:10:56,519
that let's examine it as if it is a

269
00:10:54,720 --> 00:10:59,760
string maybe it's a string because those

270
00:10:56,519 --> 00:11:02,160
look like asy character ranges

271
00:10:59,760 --> 00:11:04,880
and indeed if we examine as a string

272
00:11:02,160 --> 00:11:07,320
this second argument seems to so that

273
00:11:04,880 --> 00:11:09,760
was the argument one second argument is

274
00:11:07,320 --> 00:11:12,279
a string the Moon Unit will be divided

275
00:11:09,760 --> 00:11:14,600
into two divisions and if we did the

276
00:11:12,279 --> 00:11:18,079
same thing and we pointed it at argument

277
00:11:14,600 --> 00:11:21,240
zero we have hello and so that is our

278
00:11:18,079 --> 00:11:23,160
input and this is some other input some

279
00:11:21,240 --> 00:11:26,040
other string some other pointer to a

280
00:11:23,160 --> 00:11:29,040
string that is being passed into Strings

281
00:11:26,040 --> 00:11:32,079
not equal so two apparent pointers to

282
00:11:29,040 --> 00:11:35,320
strings passed into Strings not equal

283
00:11:32,079 --> 00:11:37,040
well I would Hazard a guess that the

284
00:11:35,320 --> 00:11:38,760
result of strings not equal is going to

285
00:11:37,040 --> 00:11:41,480
be based on whether or not these things

286
00:11:38,760 --> 00:11:44,720
are the exact same string so let's go

287
00:11:41,480 --> 00:11:47,519
ahead and ni next instruction step over

288
00:11:44,720 --> 00:11:52,160
this strings not equal we got here where

289
00:11:47,519 --> 00:11:54,839
bz the a z value is one so is one not

290
00:11:52,160 --> 00:11:57,440
equal to zero yes that is true so we're

291
00:11:54,839 --> 00:11:59,519
going to branch step by and we're going

292
00:11:57,440 --> 00:12:01,639
to branch to the explode bomb we don't

293
00:11:59,519 --> 00:12:05,120
want that right we want to avoid the

294
00:12:01,639 --> 00:12:07,920
explode bombs so we must necessarily

295
00:12:05,120 --> 00:12:11,639
make sure that whatever comes back from

296
00:12:07,920 --> 00:12:14,560
the strings not equal is going to be

297
00:12:11,639 --> 00:12:16,920
equal to zero not not equal to zero it

298
00:12:14,560 --> 00:12:19,519
needs to be exactly equal to zero so now

299
00:12:16,920 --> 00:12:22,320
at this point if we were you know a

300
00:12:19,519 --> 00:12:24,720
novice reverse engineer we would say

301
00:12:22,320 --> 00:12:27,360
disassemble that function and let's go

302
00:12:24,720 --> 00:12:30,720
read that full thing and figure out how

303
00:12:27,360 --> 00:12:33,160
it works but I am not an novice reverse

304
00:12:30,720 --> 00:12:35,760
engineer so I try to do things the easy

305
00:12:33,160 --> 00:12:38,040
way and the easy way here says I'm just

306
00:12:35,760 --> 00:12:40,600
going to do a quick test and I'm going

307
00:12:38,040 --> 00:12:42,800
to see what happens if I give the same

308
00:12:40,600 --> 00:12:46,040
sort of input that was being passed into

309
00:12:42,800 --> 00:12:48,120
this uh I'm going to give this this

310
00:12:46,040 --> 00:12:49,720
particular string that I saw used here

311
00:12:48,120 --> 00:12:51,480
that was not my string it's just some

312
00:12:49,720 --> 00:12:53,320
other string I'm going to give that as

313
00:12:51,480 --> 00:12:54,880
the input then I'm just going to let

314
00:12:53,320 --> 00:12:56,800
this entire code run and see what

315
00:12:54,880 --> 00:12:59,000
happens like this is the difference

316
00:12:56,800 --> 00:13:00,720
between pure static analysis if if we

317
00:12:59,000 --> 00:13:02,120
had pure static analysis we would have

318
00:13:00,720 --> 00:13:03,680
to like read everything and try to

319
00:13:02,120 --> 00:13:05,279
figure out what's going on but we're

320
00:13:03,680 --> 00:13:07,720
doing Dynamic analysis here we have a

321
00:13:05,279 --> 00:13:09,959
debugger we can go ahead and just test

322
00:13:07,720 --> 00:13:12,560
things as they really are so let's go

323
00:13:09,959 --> 00:13:15,680
ahead and rerun the program and let's

324
00:13:12,560 --> 00:13:18,440
give this new magic string as the input

325
00:13:15,680 --> 00:13:21,880
continuing on pass our magic string we

326
00:13:18,440 --> 00:13:24,600
saw used elsewhere and step into the

327
00:13:21,880 --> 00:13:26,680
phase one step step step right up to the

328
00:13:24,600 --> 00:13:29,480
point of strings not equal we're going

329
00:13:26,680 --> 00:13:32,480
to look at our arguments we see the

330
00:13:29,480 --> 00:13:35,399
first one examine as a string that is

331
00:13:32,480 --> 00:13:36,560
the Moon Unit and the second one examine

332
00:13:35,399 --> 00:13:38,880
it as a

333
00:13:36,560 --> 00:13:41,160
string that is also the Moon Unit will

334
00:13:38,880 --> 00:13:43,360
be divided into two divisions so let's

335
00:13:41,160 --> 00:13:47,800
just test our Theory next instruction

336
00:13:43,360 --> 00:13:50,120
over it and what is a z a z is zero so

337
00:13:47,800 --> 00:13:54,000
if we step we no longer go to the

338
00:13:50,120 --> 00:13:57,120
explode bomb if we continue we see phase

339
00:13:54,000 --> 00:13:58,560
one diffused how about the next one all

340
00:13:57,120 --> 00:14:00,519
right so that is where I'm going to

341
00:13:58,560 --> 00:14:02,440
leave leave you with my initial walkth

342
00:14:00,519 --> 00:14:04,360
through it's up to you from here to

343
00:14:02,440 --> 00:14:07,720
figure out what inputs are needed for

344
00:14:04,360 --> 00:14:10,480
this binary to pass phase two past phase

345
00:14:07,720 --> 00:14:12,680
three and so forth okay well that was

346
00:14:10,480 --> 00:14:14,399
the first phase of the bomb lab but now

347
00:14:12,680 --> 00:14:17,360
you've got to do all the rest of the

348
00:14:14,399 --> 00:14:19,560
phases of the bomb lab so if you've seen

349
00:14:17,360 --> 00:14:21,399
this before or if you've already learned

350
00:14:19,560 --> 00:14:24,199
some other assembly language before this

351
00:14:21,399 --> 00:14:25,759
which I expect many of you have probably

352
00:14:24,199 --> 00:14:28,800
RISC-V is not your first ever

353
00:14:25,759 --> 00:14:31,199
assembly I recommend you do this in

354
00:14:28,800 --> 00:14:33,399
expert mode and expert mode means take

355
00:14:31,199 --> 00:14:35,279
away those helpful symbols the things

356
00:14:33,399 --> 00:14:37,920
that tell you where phase one is and

357
00:14:35,279 --> 00:14:40,000
tell you where initialize bomb is take

358
00:14:37,920 --> 00:14:42,639
away the symbols by running the strip

359
00:14:40,000 --> 00:14:44,839
command so run strip on the bomb and

360
00:14:42,639 --> 00:14:47,040
then now you won't have any nice helpful

361
00:14:44,839 --> 00:14:48,160
symbols it'll be a little bit harder

362
00:14:47,040 --> 00:14:49,800
although if you've got reverse

363
00:14:48,160 --> 00:14:52,120
engineering experience it won't be that

364
00:14:49,800 --> 00:14:54,560
much harder but it'll certainly take you

365
00:14:52,120 --> 00:14:57,240
more time than if you had symbols and

366
00:14:54,560 --> 00:14:59,639
the final hints for this lab is that you

367
00:14:57,240 --> 00:15:02,440
need to remember to read the fun Man

368
00:14:59,639 --> 00:15:04,839
pages you will be encountering C

369
00:15:02,440 --> 00:15:06,839
functions that perhaps you don't use

370
00:15:04,839 --> 00:15:09,000
every day and you need to go off and

371
00:15:06,839 --> 00:15:11,160
read the man page for those functions so

372
00:15:09,000 --> 00:15:13,440
you understand what are the arguments in

373
00:15:11,160 --> 00:15:15,399
what are the arguments out the other

374
00:15:13,440 --> 00:15:17,199
thing is that this is just general

375
00:15:15,399 --> 00:15:18,519
reverse engineering guidance especially

376
00:15:17,199 --> 00:15:20,720
for those who have not reverse

377
00:15:18,519 --> 00:15:22,839
engineered a lot before when you reverse

378
00:15:20,720 --> 00:15:24,959
engineering it can be very tempting to

379
00:15:22,839 --> 00:15:27,440
try to understand every single assembly

380
00:15:24,959 --> 00:15:29,399
instruction and go one by one and drill

381
00:15:27,440 --> 00:15:31,759
into every single function

382
00:15:29,399 --> 00:15:34,319
it is generally my guidance when doing

383
00:15:31,759 --> 00:15:36,639
reverse engineering to dive down and

384
00:15:34,319 --> 00:15:38,920
then come back up and dive down and then

385
00:15:36,639 --> 00:15:40,720
come back up you basically don't want to

386
00:15:38,920 --> 00:15:43,040
get too caught up in the weeds you have

387
00:15:40,720 --> 00:15:45,040
to think to yourself what is my core

388
00:15:43,040 --> 00:15:47,079
goal here and your core goal in the

389
00:15:45,040 --> 00:15:50,000
binary bomb lab is just make it not

390
00:15:47,079 --> 00:15:53,519
explode so you need to always be focused

391
00:15:50,000 --> 00:15:57,199
on M is what I'm figuring out right now

392
00:15:53,519 --> 00:15:59,839
about the binary is it going to help me

393
00:15:57,199 --> 00:16:01,519
not hit the explode bomb function and to

394
00:15:59,839 --> 00:16:03,480
avoid that and to continue on and be

395
00:16:01,519 --> 00:16:05,319
able to transition into the next phase

396
00:16:03,480 --> 00:16:07,680
so it is reverse engineering to go

397
00:16:05,319 --> 00:16:09,839
backwards and say you know hey I need to

398
00:16:07,680 --> 00:16:12,079
get here in the code where it's not

399
00:16:09,839 --> 00:16:13,959
going to hit the bomb walk backwards and

400
00:16:12,079 --> 00:16:15,959
what inputs do you need for that to be

401
00:16:13,959 --> 00:16:18,600
correct what conditions need to hold

402
00:16:15,959 --> 00:16:20,959
true and how do you achieve those okay

403
00:16:18,600 --> 00:16:23,519
well that's it now I want you to run off

404
00:16:20,959 --> 00:16:27,040
and do the binary bomb it's going to be

405
00:16:23,519 --> 00:16:27,040
fun trust me

