1
00:00:01,000 --> 00:00:04,560
all right this is our first source Code

2
00:00:02,800 --> 00:00:06,759
example where we're going to see the

3
00:00:04,560 --> 00:00:08,960
related assembly and I tried to create

4
00:00:06,759 --> 00:00:11,080
something as simple as humanly possible

5
00:00:08,960 --> 00:00:13,440
so that I introduced as few assembly

6
00:00:11,080 --> 00:00:14,799
instructions that were new as possible

7
00:00:13,440 --> 00:00:17,080
unfortunately we're going to see

8
00:00:14,799 --> 00:00:18,480
something this simple is going to

9
00:00:17,080 --> 00:00:20,279
actually lead to a decent amount of

10
00:00:18,480 --> 00:00:22,439
complexity which is why I said before

11
00:00:20,279 --> 00:00:24,439
that the class starts out a bit complex

12
00:00:22,439 --> 00:00:26,439
but it'll get easier over time so all we

13
00:00:24,439 --> 00:00:28,800
want to do is return from main and

14
00:00:26,439 --> 00:00:30,279
return the hardcoded value of hex 41

15
00:00:28,800 --> 00:00:32,800
this will be your compilation

16
00:00:30,279 --> 00:00:34,840
instructions in order to actually

17
00:00:32,800 --> 00:00:37,640
generate the same assembly you'll note

18
00:00:34,840 --> 00:00:40,760
that it is optimized we are using the 01

19
00:00:37,640 --> 00:00:42,879
optimization here to try to create a

20
00:00:40,760 --> 00:00:45,520
much smaller bit of assembly and this is

21
00:00:42,879 --> 00:00:48,680
what we come up with we have a single Li

22
00:00:45,520 --> 00:00:50,840
instruction and a r assembly instruction

23
00:00:48,680 --> 00:00:53,600
so here's our first our next two

24
00:00:50,840 --> 00:00:56,520
assembly instructions what do they do

25
00:00:53,600 --> 00:00:59,359
well the first one Li stands for load

26
00:00:56,520 --> 00:01:02,440
immediate to register so it takes an

27
00:00:59,359 --> 00:01:05,119
immediate 12bit so a signed immediate

28
00:01:02,440 --> 00:01:06,920
12bit and it loads it into the register

29
00:01:05,119 --> 00:01:09,360
but a key thing about it is because it's

30
00:01:06,920 --> 00:01:11,799
signed it's going to be sign extended so

31
00:01:09,360 --> 00:01:14,759
if you have again three FS what will be

32
00:01:11,799 --> 00:01:17,240
loaded into the register Rd is going to

33
00:01:14,759 --> 00:01:20,000
either be8 FS or 16 FS depending on

34
00:01:17,240 --> 00:01:22,159
whether you're executing 32 or 64-bit

35
00:01:20,000 --> 00:01:24,799
code so again just because I say it's RV

36
00:01:22,159 --> 00:01:27,960
32i does not mean that you can't use

37
00:01:24,799 --> 00:01:29,520
this in 64-bit code you can it's just

38
00:01:27,960 --> 00:01:32,399
the destination register is going to be

39
00:01:29,520 --> 00:01:34,240
6 4 bits large instead of 32 and that

40
00:01:32,399 --> 00:01:36,200
means consequently the sin extended

41
00:01:34,240 --> 00:01:39,360
value is going to be sin extended to 64

42
00:01:36,200 --> 00:01:41,840
instead of 32 so here's a concrete

43
00:01:39,360 --> 00:01:45,000
example that is in the actual uh code

44
00:01:41,840 --> 00:01:48,880
that we just compiled it had Li

45
00:01:45,000 --> 00:01:50,119
a065 so 65 in decimal again GDB

46
00:01:48,880 --> 00:01:53,119
disassembly is always going to be

47
00:01:50,119 --> 00:01:56,280
showing you stuff in decimal 65 is the

48
00:01:53,119 --> 00:01:59,280
hex 41 so 65 is going to be loaded into

49
00:01:56,280 --> 00:02:02,200
a z so the a z register will just be all

50
00:01:59,280 --> 00:02:05,360
zeros and 65 or all zeros and 41 in

51
00:02:02,200 --> 00:02:07,640
heximal now by convention the a0 is not

52
00:02:05,360 --> 00:02:10,280
just the argument zero when passing

53
00:02:07,640 --> 00:02:12,879
arguments to functions but it is also

54
00:02:10,280 --> 00:02:15,040
the ABI calling conventions register

55
00:02:12,879 --> 00:02:18,160
where return values are stored so you're

56
00:02:15,040 --> 00:02:19,840
returning a value in this case hex 41 65

57
00:02:18,160 --> 00:02:22,360
decimal you're returning the value and

58
00:02:19,840 --> 00:02:24,640
that means it should be placed in a zero

59
00:02:22,360 --> 00:02:27,160
which the ABI says is where return value

60
00:02:24,640 --> 00:02:30,000
goope now here's a couple of concrete

61
00:02:27,160 --> 00:02:32,120
examples Li if you had temp zero a Nega

62
00:02:30,000 --> 00:02:33,760
2 this is okay this is a valid assembly

63
00:02:32,120 --> 00:02:36,640
instruction again it's a 12-bit

64
00:02:33,760 --> 00:02:39,599
immediate and you can encode -2 but this

65
00:02:36,640 --> 00:02:42,599
on the other hand is not valid Li of 41

66
00:02:39,599 --> 00:02:44,720
into PC because that destination

67
00:02:42,599 --> 00:02:46,879
register needs to be a general purpose

68
00:02:44,720 --> 00:02:49,680
register not the special purpose PC

69
00:02:46,879 --> 00:02:53,360
register and likewise this Li of hex

70
00:02:49,680 --> 00:02:57,000
4141 4141 into ra is not possible

71
00:02:53,360 --> 00:02:59,840
because you can't encode 4141 4141 into

72
00:02:57,000 --> 00:03:01,840
12 bits now I'm a BigTime Marvel comic

73
00:02:59,840 --> 00:03:04,159
book fan and I know that most people do

74
00:03:01,840 --> 00:03:06,959
not read Marvel Comics but thanks to the

75
00:03:04,159 --> 00:03:08,920
MCU Marvel Cinematic Universe you are

76
00:03:06,959 --> 00:03:11,319
perhaps familiar with a variety of

77
00:03:08,920 --> 00:03:13,440
Marvel characters what is this character

78
00:03:11,319 --> 00:03:16,519
right here well that is Scarlet Witch or

79
00:03:13,440 --> 00:03:18,599
Wanda maximov as seen as starting in

80
00:03:16,519 --> 00:03:21,239
Avengers 2 but she also you know

81
00:03:18,599 --> 00:03:24,840
recently had her own series on Disney

82
00:03:21,239 --> 00:03:27,440
plus uh W Division and so in the W

83
00:03:24,840 --> 00:03:29,239
division TV show she was distraught over

84
00:03:27,440 --> 00:03:31,319
the fact that she you know didn't have

85
00:03:29,239 --> 00:03:33,920
her children and so she was searching

86
00:03:31,319 --> 00:03:35,680
for alternate reality ways to get access

87
00:03:33,920 --> 00:03:38,680
to her children well in the comic books

88
00:03:35,680 --> 00:03:41,000
a similar thing happened in that uh she

89
00:03:38,680 --> 00:03:42,319
basically lost her mind because of the

90
00:03:41,000 --> 00:03:44,360
fact that she found out that the

91
00:03:42,319 --> 00:03:46,920
children she had thought she had with

92
00:03:44,360 --> 00:03:48,599
the robotic Vision were actually just

93
00:03:46,920 --> 00:03:50,159
figments of her imagination and the fact

94
00:03:48,599 --> 00:03:52,640
that her powers turned out to actually

95
00:03:50,159 --> 00:03:55,000
be rewriting reality so she rewrote

96
00:03:52,640 --> 00:03:56,599
reality gave herself uh imaginary

97
00:03:55,000 --> 00:03:58,319
children and when she found out they

98
00:03:56,599 --> 00:04:00,640
were imaginary children she basically

99
00:03:58,319 --> 00:04:02,200
lost her mind and disassembled the

100
00:04:00,640 --> 00:04:04,439
Avengers here's an example of her

101
00:04:02,200 --> 00:04:05,959
disassembling Hawkeye Hawkeye also has a

102
00:04:04,439 --> 00:04:08,879
series

103
00:04:05,959 --> 00:04:11,480
anyways the point is that Scarlet Witch

104
00:04:08,879 --> 00:04:13,840
has chaos magic powers and she has the

105
00:04:11,480 --> 00:04:16,799
ability to completely rewrite the

106
00:04:13,840 --> 00:04:19,280
reality that you and I see now in this

107
00:04:16,799 --> 00:04:22,800
class we are going to be using this GDB

108
00:04:19,280 --> 00:04:25,759
command GDB set disassembler options no

109
00:04:22,800 --> 00:04:28,240
aliases and so in a very famous quote

110
00:04:25,759 --> 00:04:30,520
Scarlet Witch said no more mutants where

111
00:04:28,240 --> 00:04:33,680
she wrote rewrote reality and got rid of

112
00:04:30,520 --> 00:04:35,120
all the mutants except 198 of them but

113
00:04:33,680 --> 00:04:37,759
in this class we're going to use and

114
00:04:35,120 --> 00:04:40,199
invoke her chaos magic powers to say no

115
00:04:37,759 --> 00:04:42,759
more aliases and we're actually going to

116
00:04:40,199 --> 00:04:44,720
wipe away instruction aliases which are

117
00:04:42,759 --> 00:04:46,960
those things that hide the real assembly

118
00:04:44,720 --> 00:04:48,880
instructions from us behind pseudo

119
00:04:46,960 --> 00:04:51,479
instructions so let's go ahead and

120
00:04:48,880 --> 00:04:54,080
invoke her chaos magic now so this is

121
00:04:51,479 --> 00:04:56,759
what the GDB disassembler shows us to

122
00:04:54,080 --> 00:04:59,320
start with but if we rewrite reality we

123
00:04:56,759 --> 00:05:02,240
will see that actually without aliases

124
00:04:59,320 --> 00:05:05,800
the Li is an ADDI and the r is a

125
00:05:02,240 --> 00:05:08,199
seizure so the LI is a lie it's actually

126
00:05:05,800 --> 00:05:11,199
ad I and we've already learned about ADI

127
00:05:08,199 --> 00:05:13,639
when we were learning about Noah so Li

128
00:05:11,199 --> 00:05:17,160
actually pseudo instruction which is

129
00:05:13,639 --> 00:05:19,639
doing ADI destination x0 the hard-coded

130
00:05:17,160 --> 00:05:21,840
zero register and an immediate so you're

131
00:05:19,639 --> 00:05:24,560
not just loading the immediate into Rd

132
00:05:21,840 --> 00:05:27,120
you're adding the immediate to zero and

133
00:05:24,560 --> 00:05:29,039
then placing the result into Rd so

134
00:05:27,120 --> 00:05:31,240
pseudo assembly instruction Li is

135
00:05:29,039 --> 00:05:33,360
actually add I we learned about this

136
00:05:31,240 --> 00:05:36,039
already so that's your reminder it was

137
00:05:33,360 --> 00:05:37,720
used for noop but basically it's just

138
00:05:36,039 --> 00:05:39,600
taking a source adding it to an

139
00:05:37,720 --> 00:05:41,680
immediate sign extending that immediate

140
00:05:39,600 --> 00:05:43,880
before the addition and putting it in

141
00:05:41,680 --> 00:05:46,039
the destination register building up our

142
00:05:43,880 --> 00:05:48,720
little diagram again the LI is a lie

143
00:05:46,039 --> 00:05:51,000
it's actually add I we now know two

144
00:05:48,720 --> 00:05:53,199
pseudo instructions and one real

145
00:05:51,000 --> 00:05:56,240
assembly instruction going back to this

146
00:05:53,199 --> 00:05:58,840
rewriting reality again that R turned

147
00:05:56,240 --> 00:06:00,639
into a seizure so this is going to be

148
00:05:58,840 --> 00:06:02,080
our first compressed assembly

149
00:06:00,639 --> 00:06:04,240
instruction which we will denote with

150
00:06:02,080 --> 00:06:07,160
our nice little yellow turning into

151
00:06:04,240 --> 00:06:08,759
orange representing the increase in heat

152
00:06:07,160 --> 00:06:11,199
caused by the compression of this

153
00:06:08,759 --> 00:06:13,479
assembly instruction so R's not real

154
00:06:11,199 --> 00:06:16,199
it's another Insidious pseudo

155
00:06:13,479 --> 00:06:18,919
instruction hiding in plain sight so

156
00:06:16,199 --> 00:06:21,960
that is a seizure not a r but what is a

157
00:06:18,919 --> 00:06:25,199
seizure I'm glad you asked it is a

158
00:06:21,960 --> 00:06:28,360
compressed jump to register so jump to

159
00:06:25,199 --> 00:06:29,919
register but this small 16bit form you

160
00:06:28,360 --> 00:06:32,240
can have a jump to register without the

161
00:06:29,919 --> 00:06:34,280
16-bit form but this is the 16-bit form

162
00:06:32,240 --> 00:06:36,560
making it smaller coat so this is our

163
00:06:34,280 --> 00:06:38,680
first exposure to the RISC-V compressed

164
00:06:36,560 --> 00:06:42,479
ISA I'm going to call it RVC it can

165
00:06:38,680 --> 00:06:44,720
actually be RV 32c or RV 64c but just to

166
00:06:42,479 --> 00:06:46,560
be generic I'll call it RVC and again as

167
00:06:44,720 --> 00:06:48,520
we've said multiple times already the

168
00:06:46,560 --> 00:06:50,479
basic point is if you have size

169
00:06:48,520 --> 00:06:52,400
optimization size of the code you want

170
00:06:50,479 --> 00:06:54,520
it small then you're going to use this

171
00:06:52,400 --> 00:06:56,879
compressed assembly instructions to

172
00:06:54,520 --> 00:06:59,199
encode things in 16 bits instead of 32

173
00:06:56,879 --> 00:07:01,919
bits again cisk architectures like x86

174
00:06:59,199 --> 00:07:04,080
they have multi-te encoding they have

175
00:07:01,919 --> 00:07:05,919
variable sized encoding and so

176
00:07:04,080 --> 00:07:07,720
consequently you could have 1 two 3 all

177
00:07:05,919 --> 00:07:09,720
the way up to 15 byte assembly

178
00:07:07,720 --> 00:07:12,280
instructions that's how they achieve

179
00:07:09,720 --> 00:07:14,720
smaller code size and RISC architectures

180
00:07:12,280 --> 00:07:16,759
like arm and myips have completely

181
00:07:14,720 --> 00:07:18,360
different 16bit Isis and when I say

182
00:07:16,759 --> 00:07:21,000
completely different I'm making a

183
00:07:18,360 --> 00:07:24,240
distinction in that RVC is ultimately

184
00:07:21,000 --> 00:07:26,840
going to be mapping back to the RV32I

185
00:07:24,240 --> 00:07:28,680
or RV64I so basically although they

186
00:07:26,840 --> 00:07:31,680
have different encoding the assembly

187
00:07:28,680 --> 00:07:33,479
instructions for compressed RISC-V are

188
00:07:31,680 --> 00:07:35,720
supposed to map back to just assembly

189
00:07:33,479 --> 00:07:37,720
instructions you already know whereas

190
00:07:35,720 --> 00:07:39,000
thumb and mips may have completely

191
00:07:37,720 --> 00:07:40,879
different assembly instructions that

192
00:07:39,000 --> 00:07:43,160
don't map back to anything so

193
00:07:40,879 --> 00:07:44,800
necessarily because they don't have a

194
00:07:43,160 --> 00:07:46,960
lot of space to encode these assembly

195
00:07:44,800 --> 00:07:49,039
instructions they're no longer going to

196
00:07:46,960 --> 00:07:51,879
be able to have generality and be able

197
00:07:49,039 --> 00:07:54,080
to encode all possible forms of assembly

198
00:07:51,879 --> 00:07:56,680
instructions so in practice what this

199
00:07:54,080 --> 00:07:59,759
means is that they have chosen these

200
00:07:56,680 --> 00:08:02,960
registers s0 through S1 a0 through a 5

201
00:07:59,759 --> 00:08:07,000
ra and SP you can use those which these

202
00:08:02,960 --> 00:08:09,479
are the uh X8 to X15 x1 which is RA and

203
00:08:07,000 --> 00:08:11,080
x2 which is sp these are the only

204
00:08:09,479 --> 00:08:13,080
general purpose registers that can be

205
00:08:11,080 --> 00:08:17,039
used in compressed assembly instructions

206
00:08:13,080 --> 00:08:19,360
you can't use x3 x4 x5 Etc so

207
00:08:17,039 --> 00:08:20,960
restriction now on the possible uh

208
00:08:19,360 --> 00:08:22,759
registers that are used and we'll teach

209
00:08:20,960 --> 00:08:24,879
you all about that later on in the read

210
00:08:22,759 --> 00:08:26,520
the fund manual section of this class

211
00:08:24,879 --> 00:08:29,280
but for now just know that it's a

212
00:08:26,520 --> 00:08:30,720
cutdown set of possible instruction uh

213
00:08:29,280 --> 00:08:32,320
possible registers now when you're

214
00:08:30,720 --> 00:08:35,560
looking at things in the disassembler

215
00:08:32,320 --> 00:08:37,599
you're going to see a prefix of C dot to

216
00:08:35,560 --> 00:08:41,000
indicate that it's a compressed form of

217
00:08:37,599 --> 00:08:44,159
instruction and as I said before the

218
00:08:41,000 --> 00:08:46,160
values are going to map back one: one to

219
00:08:44,159 --> 00:08:48,800
normal uh instructions from the

220
00:08:46,160 --> 00:08:51,200
non-compressed form so if you see c.

221
00:08:48,800 --> 00:08:53,959
Addie that is going to map back to an

222
00:08:51,200 --> 00:08:55,680
Addie instruction if you see c. Jr

223
00:08:53,959 --> 00:08:57,480
that's going to map back to a Jr

224
00:08:55,680 --> 00:08:59,240
instruction so the key thing is that

225
00:08:57,480 --> 00:09:01,600
most of the time you can just kind of

226
00:08:59,240 --> 00:09:03,079
ignore the C Dot and know okay well

227
00:09:01,600 --> 00:09:04,760
they're using two bytes instead of four

228
00:09:03,079 --> 00:09:06,720
bytes that doesn't really affect like

229
00:09:04,760 --> 00:09:08,160
the logic of what it's doing so most of

230
00:09:06,720 --> 00:09:10,200
the time for reading you can just ignore

231
00:09:08,160 --> 00:09:11,399
the C dot unfortunately for writing if

232
00:09:10,200 --> 00:09:12,920
you're going to try to write something

233
00:09:11,399 --> 00:09:14,839
then you have to be aware you have to

234
00:09:12,920 --> 00:09:16,640
read the fun manual to figure out you

235
00:09:14,839 --> 00:09:19,040
know what possible register values are

236
00:09:16,640 --> 00:09:21,640
there any restrictions can it perhaps

237
00:09:19,040 --> 00:09:23,040
not use 12 bit immediates maybe it only

238
00:09:21,640 --> 00:09:24,839
use six bit immediates or eight bit

239
00:09:23,040 --> 00:09:26,880
immediates Etc so there are more

240
00:09:24,839 --> 00:09:28,120
restrictions on things you don't care

241
00:09:26,880 --> 00:09:29,519
when you see it when you're reading but

242
00:09:28,120 --> 00:09:32,040
if you're writing you definitely have to

243
00:09:29,519 --> 00:09:35,040
care and read the fun manual so we'll

244
00:09:32,040 --> 00:09:37,160
have a couple uh exceptions very soon

245
00:09:35,040 --> 00:09:41,120
where unfortunately although things do

246
00:09:37,160 --> 00:09:44,360
map back directly to actual RV32I or

247
00:09:41,120 --> 00:09:47,600
64i instructions they have mnemonics or

248
00:09:44,360 --> 00:09:49,680
names that don't map back to uh the the

249
00:09:47,600 --> 00:09:51,519
core just one to one back to things so

250
00:09:49,680 --> 00:09:53,480
we'll see some you know this is why I

251
00:09:51,519 --> 00:09:55,959
said this first section gets a little

252
00:09:53,480 --> 00:09:58,320
complex so again R's not real it's a

253
00:09:55,959 --> 00:10:02,040
pseudo instruction and in this case it

254
00:09:58,320 --> 00:10:03,839
was a compressed jump to register ra so

255
00:10:02,040 --> 00:10:06,519
here we have our R pseudo assembly

256
00:10:03,839 --> 00:10:08,680
instruction it points at a compressed

257
00:10:06,519 --> 00:10:10,320
jump to register but a compressed jump

258
00:10:08,680 --> 00:10:12,959
to register implies there is a

259
00:10:10,320 --> 00:10:15,480
non-compressed jump to register so if

260
00:10:12,959 --> 00:10:18,040
that's a cjer what's a jur glad you

261
00:10:15,480 --> 00:10:20,320
asked that's just a jump to register and

262
00:10:18,040 --> 00:10:22,880
that is defined as follows we have a

263
00:10:20,320 --> 00:10:26,000
source register and that source register

264
00:10:22,880 --> 00:10:28,560
is simply set the the PC value is simply

265
00:10:26,000 --> 00:10:30,920
set to the source register value so the

266
00:10:28,560 --> 00:10:32,880
specific example used by a r so if you

267
00:10:30,920 --> 00:10:35,680
have a r pseudo instruction it

268
00:10:32,880 --> 00:10:38,839
specifically needs to be using ra the

269
00:10:35,680 --> 00:10:42,040
return address register as its source

270
00:10:38,839 --> 00:10:44,920
register if it was using you know x3 x4

271
00:10:42,040 --> 00:10:47,399
x5 X6 those are all possible values for

272
00:10:44,920 --> 00:10:49,639
this uh source register but that would

273
00:10:47,399 --> 00:10:51,880
not be interpreted as a r by a

274
00:10:49,639 --> 00:10:54,639
disassembler only when it's using ra

275
00:10:51,880 --> 00:10:57,000
jump to ra will it be treated as a red

276
00:10:54,639 --> 00:11:00,800
and again that's the return address

277
00:10:57,000 --> 00:11:03,480
register which is set to the PC value so

278
00:11:00,800 --> 00:11:06,120
again it's an ABI convention that ra

279
00:11:03,480 --> 00:11:07,920
shall be used as the return address that

280
00:11:06,120 --> 00:11:09,880
is stored when you're calling into a

281
00:11:07,920 --> 00:11:11,880
function so that you can get back to

282
00:11:09,880 --> 00:11:14,600
where you came from the ra is the return

283
00:11:11,880 --> 00:11:17,320
address back to where you came from um

284
00:11:14,600 --> 00:11:19,480
after the call to a function but I want

285
00:11:17,320 --> 00:11:22,160
to be clear that this RS could actually

286
00:11:19,480 --> 00:11:25,440
be any possible register it's just the

287
00:11:22,160 --> 00:11:27,600
ra form which is interpreted as red but

288
00:11:25,440 --> 00:11:29,680
now we have yet another Pretender these

289
00:11:27,600 --> 00:11:32,040
pseudo instructions are popping up

290
00:11:29,680 --> 00:11:34,279
everywhere this is ridiculous and this

291
00:11:32,040 --> 00:11:36,639
is not what I was hoping for when I said

292
00:11:34,279 --> 00:11:38,320
let's make the smallest simplest

293
00:11:36,639 --> 00:11:40,120
possible just return a hardcoded

294
00:11:38,320 --> 00:11:41,839
constant assembly instruction I didn't

295
00:11:40,120 --> 00:11:43,399
want to go through all this complexity I

296
00:11:41,839 --> 00:11:45,480
considered pulling it all out and you

297
00:11:43,399 --> 00:11:47,639
know treating it in a you know separate

298
00:11:45,480 --> 00:11:49,639
section but I decided you know you guys

299
00:11:47,639 --> 00:11:52,440
can handle it so let's go ahead and keep

300
00:11:49,639 --> 00:11:55,399
trucking here the Jr is a pseudo

301
00:11:52,440 --> 00:11:58,000
instruction for a different instruction

302
00:11:55,399 --> 00:12:00,920
called joller so I'm going to pronounce

303
00:11:58,000 --> 00:12:03,160
joller like dollar cuz I have this nice

304
00:12:00,920 --> 00:12:04,880
low resolution picture of a dollar store

305
00:12:03,160 --> 00:12:07,160
and I went to all this trouble of

306
00:12:04,880 --> 00:12:10,399
creating this slightly italic so that it

307
00:12:07,160 --> 00:12:12,959
matches the slightly italic jer jer so

308
00:12:10,399 --> 00:12:14,600
yeah Jer this originally was because I

309
00:12:12,959 --> 00:12:16,800
had uh you know when I was learning RISC

310
00:12:14,600 --> 00:12:17,839
5 I misinterpreted jaw and I thought

311
00:12:16,800 --> 00:12:19,600
that was going to be a pseudo

312
00:12:17,839 --> 00:12:22,600
instruction for call and I was like Jaw

313
00:12:19,600 --> 00:12:25,199
like call jaw like call Jer like dollar

314
00:12:22,600 --> 00:12:26,760
jaw jaw Call Etc but yeah it's pointless

315
00:12:25,199 --> 00:12:29,279
at this point because I found out that

316
00:12:26,760 --> 00:12:33,560
it was not just jaw as call oh well so

317
00:12:29,279 --> 00:12:36,079
joller joller jump and link register

318
00:12:33,560 --> 00:12:38,399
jump and link register what does that do

319
00:12:36,079 --> 00:12:40,959
well we have a source a destination and

320
00:12:38,399 --> 00:12:42,880
an immediate 12 bit take the source

321
00:12:40,959 --> 00:12:45,639
register take the value inside of it

322
00:12:42,880 --> 00:12:48,480
take the immediate 12 sign extend it add

323
00:12:45,639 --> 00:12:50,839
them together then take that value and

324
00:12:48,480 --> 00:12:53,199
place it into the PC register so that

325
00:12:50,839 --> 00:12:55,240
says we are doing a calculation this is

326
00:12:53,199 --> 00:12:58,160
the calculation and it tells us we

327
00:12:55,240 --> 00:12:59,800
should now be jumping to a new location

328
00:12:58,160 --> 00:13:01,440
the program counter is set to a new

329
00:12:59,800 --> 00:13:03,959
location that's going to be the new

330
00:13:01,440 --> 00:13:06,399
location of code to execute because the

331
00:13:03,959 --> 00:13:08,160
program counter always holds the next

332
00:13:06,399 --> 00:13:09,519
assembly instruction to execute now

333
00:13:08,160 --> 00:13:11,160
there's one other little minor thing

334
00:13:09,519 --> 00:13:13,480
that doesn't just fit nicely in this

335
00:13:11,160 --> 00:13:14,920
little math and that is that there is a

336
00:13:13,480 --> 00:13:16,839
side effect of setting the least

337
00:13:14,920 --> 00:13:18,959
significant bit of the final address

338
00:13:16,839 --> 00:13:21,320
take this calculate a final address set

339
00:13:18,959 --> 00:13:24,000
the least significant bit to zero this

340
00:13:21,320 --> 00:13:25,959
is to try to enforce alignment so

341
00:13:24,000 --> 00:13:27,880
basically we know that assembly

342
00:13:25,959 --> 00:13:30,920
instructions are either encoded in 16 or

343
00:13:27,880 --> 00:13:32,480
32 bits that's two or four bytes that

344
00:13:30,920 --> 00:13:34,399
means that all valid assembly

345
00:13:32,480 --> 00:13:37,079
instructions should always be a multiple

346
00:13:34,399 --> 00:13:38,560
of two so every compressed assembly

347
00:13:37,079 --> 00:13:41,079
instruction is going to end on an

348
00:13:38,560 --> 00:13:44,600
address 0 or 2 or 4 or 6 and your

349
00:13:41,079 --> 00:13:46,279
non-compressed ones are 0 4 8 12 Etc but

350
00:13:44,600 --> 00:13:48,240
by setting the least significant bit to

351
00:13:46,279 --> 00:13:50,839
zero that just enforces that every

352
00:13:48,240 --> 00:13:52,800
single possible value placed into PC is

353
00:13:50,839 --> 00:13:54,839
going to be a multiple of two and so

354
00:13:52,800 --> 00:13:56,959
that is to try to avoid instruction

355
00:13:54,839 --> 00:13:59,079
misalignment now there is a secondary

356
00:13:56,959 --> 00:14:01,399
effect this is jump and that is is the

357
00:13:59,079 --> 00:14:04,360
jump but end link register what is the

358
00:14:01,399 --> 00:14:05,920
end link register so this Rd the r

359
00:14:04,360 --> 00:14:07,560
destination you can see we didn't

360
00:14:05,920 --> 00:14:09,279
calculate something and put it into a

361
00:14:07,560 --> 00:14:13,399
destination register the destination for

362
00:14:09,279 --> 00:14:15,680
the calculation is PC so what is Rd Rd

363
00:14:13,399 --> 00:14:18,440
is going to be the address after the

364
00:14:15,680 --> 00:14:19,880
joller instruction AKA whatever you know

365
00:14:18,440 --> 00:14:21,920
the program counter was pointing here

366
00:14:19,880 --> 00:14:23,560
and it said hey execute this joller so

367
00:14:21,920 --> 00:14:25,279
program counter was pointing there the

368
00:14:23,560 --> 00:14:27,399
address after that assuming that we're

369
00:14:25,279 --> 00:14:30,000
using non-compressed things is going to

370
00:14:27,399 --> 00:14:32,839
be that plus4 so basically at the time

371
00:14:30,000 --> 00:14:34,399
that this was executing the PC plus4 is

372
00:14:32,839 --> 00:14:37,519
the next assembly instruction right

373
00:14:34,399 --> 00:14:39,560
after it and so take that pc+ 4 the

374
00:14:37,519 --> 00:14:41,240
instruction the the address after the

375
00:14:39,560 --> 00:14:43,880
instruction and place that into the

376
00:14:41,240 --> 00:14:46,399
destination register so this is

377
00:14:43,880 --> 00:14:48,839
basically going to be again like a link

378
00:14:46,399 --> 00:14:51,480
register this is our way to get back if

379
00:14:48,839 --> 00:14:53,560
the joller is calling into some function

380
00:14:51,480 --> 00:14:55,800
or returning back from function the

381
00:14:53,560 --> 00:14:57,880
destination could be the way to get back

382
00:14:55,800 --> 00:14:59,199
to where you came from so when it's used

383
00:14:57,880 --> 00:15:01,600
for a call you would would want to do

384
00:14:59,199 --> 00:15:03,560
that but if it's used for a r as is the

385
00:15:01,600 --> 00:15:05,639
path that we're currently hunting down

386
00:15:03,560 --> 00:15:08,440
then that would have no point whatsoever

387
00:15:05,639 --> 00:15:10,519
so again the link register this uh

388
00:15:08,440 --> 00:15:12,079
destination of the jawar that's how you

389
00:15:10,519 --> 00:15:13,880
get back to where you come from but you

390
00:15:12,079 --> 00:15:15,759
may or may not want it so we said right

391
00:15:13,880 --> 00:15:19,320
now we're doing you know Rett is

392
00:15:15,759 --> 00:15:22,639
compressed Jr so Rett is compressed Jr

393
00:15:19,320 --> 00:15:25,320
Jr is actually a joller and so if it if

394
00:15:22,639 --> 00:15:28,880
Rett is compressed Jr ra for return

395
00:15:25,320 --> 00:15:30,639
address and joller is actually the x0

396
00:15:28,880 --> 00:15:35,120
register is used for the destination and

397
00:15:30,639 --> 00:15:37,279
ra is used for the source so ra which is

398
00:15:35,120 --> 00:15:41,160
where we came from before main was

399
00:15:37,279 --> 00:15:43,959
called ra the return address plus Z and

400
00:15:41,160 --> 00:15:46,360
so place that into PC so just go back to

401
00:15:43,959 --> 00:15:48,880
the return address and then store into

402
00:15:46,360 --> 00:15:51,199
the link register the address after the

403
00:15:48,880 --> 00:15:53,680
jwar but this is a r we don't want to

404
00:15:51,199 --> 00:15:55,720
come back to after the R so just store

405
00:15:53,680 --> 00:15:59,079
it into x0 and it'll basically be thrown

406
00:15:55,720 --> 00:16:00,560
away because x0 is always hardcoded as Z

407
00:15:59,079 --> 00:16:02,319
can store anything you want there but

408
00:16:00,560 --> 00:16:05,160
it's not going to have any effect x0

409
00:16:02,319 --> 00:16:07,240
shall always be x0 shall always be the

410
00:16:05,160 --> 00:16:08,920
hardcoded value zero rather so again

411
00:16:07,240 --> 00:16:11,639
what's special about Rett is that it's

412
00:16:08,920 --> 00:16:14,639
using the ra and per the ABI calling

413
00:16:11,639 --> 00:16:17,360
convention ra is the return address

414
00:16:14,639 --> 00:16:20,120
where it can return back to ra return

415
00:16:17,360 --> 00:16:22,199
address so here is our summary of what

416
00:16:20,120 --> 00:16:24,959
we learned here super simple code all

417
00:16:22,199 --> 00:16:27,639
it's doing is returning hex 41 we start

418
00:16:24,959 --> 00:16:30,120
out with these pseudo instructions Li

419
00:16:27,639 --> 00:16:32,040
and Rett those are sort of the nice form

420
00:16:30,120 --> 00:16:34,720
of things those are perhaps easier for

421
00:16:32,040 --> 00:16:36,560
humans to remember sometimes Li load

422
00:16:34,720 --> 00:16:39,440
immediate so it's just loading immediate

423
00:16:36,560 --> 00:16:42,000
65 and a z that's easy to understand R

424
00:16:39,440 --> 00:16:44,000
just return back return to the return

425
00:16:42,000 --> 00:16:45,639
address it's hard-coded it doesn't even

426
00:16:44,000 --> 00:16:47,480
bother showing you the return address

427
00:16:45,639 --> 00:16:49,319
just return back to whatever the return

428
00:16:47,480 --> 00:16:51,480
address is set the PC to whatever the

429
00:16:49,319 --> 00:16:53,120
return address is so the LI is a lie

430
00:16:51,480 --> 00:16:54,839
though and the R's not real these are

431
00:16:53,120 --> 00:16:56,560
both pseudo assembly instructions and

432
00:16:54,839 --> 00:16:58,480
the real assembly instructions behind

433
00:16:56,560 --> 00:17:00,279
the scenes that were actually used for

434
00:16:58,480 --> 00:17:04,039
for this compilation for these compile

435
00:17:00,279 --> 00:17:06,039
options is ADD I where we have 65 added

436
00:17:04,039 --> 00:17:08,480
to zero so here's the here's the long

437
00:17:06,039 --> 00:17:11,559
form here's the extended form in English

438
00:17:08,480 --> 00:17:14,760
right add I we have the value 65 which

439
00:17:11,559 --> 00:17:16,760
is hex 41 plus Z the zero register add

440
00:17:14,760 --> 00:17:19,360
those two together and place it into the

441
00:17:16,760 --> 00:17:22,160
a z register and the special reason why

442
00:17:19,360 --> 00:17:25,039
it's choosing to use a z is that per the

443
00:17:22,160 --> 00:17:27,000
ABI calling convention a0 holds the

444
00:17:25,039 --> 00:17:30,160
return value at the time of function

445
00:17:27,000 --> 00:17:32,559
returns so the original source code 41

446
00:17:30,160 --> 00:17:34,400
shall be returned back out of main so

447
00:17:32,559 --> 00:17:36,080
when it returns back out of main 41

448
00:17:34,400 --> 00:17:40,080
better be in the return value the return

449
00:17:36,080 --> 00:17:44,360
value is the a z register so then cedure

450
00:17:40,080 --> 00:17:46,440
ra is jumped to the address stored in ra

451
00:17:44,360 --> 00:17:48,280
which is a special address per the ABI

452
00:17:46,440 --> 00:17:49,960
calling convention it holds the address

453
00:17:48,280 --> 00:17:51,720
after the address of the function that

454
00:17:49,960 --> 00:17:54,559
called this function and what is this

455
00:17:51,720 --> 00:17:57,640
function it's main so someone somewhere

456
00:17:54,559 --> 00:17:59,440
called main and they placed the way to

457
00:17:57,640 --> 00:18:02,080
get back out of of main when you return

458
00:17:59,440 --> 00:18:04,280
back out of main they placed that value

459
00:18:02,080 --> 00:18:06,880
into the ra because they're abiding by

460
00:18:04,280 --> 00:18:09,039
the ABI calling convention so basically

461
00:18:06,880 --> 00:18:10,760
it's going to return back to the ra

462
00:18:09,039 --> 00:18:13,400
value it's going to jump to the ra value

463
00:18:10,760 --> 00:18:15,320
set the PC to the ra value but also

464
00:18:13,400 --> 00:18:17,600
don't bother with storing the address

465
00:18:15,320 --> 00:18:19,480
after this instruction don't do the

466
00:18:17,600 --> 00:18:23,400
linkage because we don't need to come

467
00:18:19,480 --> 00:18:26,400
back here it is a jump register with ra

468
00:18:23,400 --> 00:18:28,440
but jump register specifically doesn't

469
00:18:26,400 --> 00:18:30,559
link anything so it's a pseudo

470
00:18:28,440 --> 00:18:33,720
instruction jump register for joller

471
00:18:30,559 --> 00:18:36,400
which is jump and link register but

472
00:18:33,720 --> 00:18:38,159
specifically use uh Jr when you don't

473
00:18:36,400 --> 00:18:39,600
want to bother linking you are just

474
00:18:38,159 --> 00:18:41,039
going to set the link register to the

475
00:18:39,600 --> 00:18:42,799
zero register so it doesn't bother

476
00:18:41,039 --> 00:18:44,960
storing the address afterwards because

477
00:18:42,799 --> 00:18:46,880
we're never coming back here again okay

478
00:18:44,960 --> 00:18:49,960
with that we picked up another assembly

479
00:18:46,880 --> 00:18:53,760
instruction joller and building up from

480
00:18:49,960 --> 00:18:55,360
here we had our seizure pointed at JR

481
00:18:53,760 --> 00:18:57,919
this is a pseudo assembly instruction

482
00:18:55,360 --> 00:18:59,039
though we found out it turns out that Jr

483
00:18:57,919 --> 00:19:01,440
is actually

484
00:18:59,039 --> 00:19:03,440
jwar behind the scenes and then the only

485
00:19:01,440 --> 00:19:06,520
thing I want to say here is that if you

486
00:19:03,440 --> 00:19:09,240
were compiling without compressed uh

487
00:19:06,520 --> 00:19:12,440
instructions enabled then your R would

488
00:19:09,240 --> 00:19:14,720
probably use a Jr or jwar instead so

489
00:19:12,440 --> 00:19:16,720
Rett could actually be a Jr not a

490
00:19:14,720 --> 00:19:18,360
compressed Jr just depends on whether or

491
00:19:16,720 --> 00:19:22,440
not something is being compiled with

492
00:19:18,360 --> 00:19:22,440
compressed instruction support or not

