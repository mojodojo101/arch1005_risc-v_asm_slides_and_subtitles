1
00:00:00,080 --> 00:00:03,439
while we're on hello world I thought we

2
00:00:01,959 --> 00:00:05,160
would explore a couple of the

3
00:00:03,439 --> 00:00:07,359
differences at different optimization

4
00:00:05,160 --> 00:00:09,760
levels we just looked at optimization

5
00:00:07,359 --> 00:00:11,840
level zero and here is optimization

6
00:00:09,760 --> 00:00:13,559
level one so concretely what are the

7
00:00:11,840 --> 00:00:16,279
differences well it helps to stick it

8
00:00:13,559 --> 00:00:18,320
into a differ so we're putting it into a

9
00:00:16,279 --> 00:00:20,039
program to show us the diffs and so

10
00:00:18,320 --> 00:00:21,880
these things are highlighted as

11
00:00:20,039 --> 00:00:24,160
disappearing and going into nothing over

12
00:00:21,880 --> 00:00:26,359
here and this is highlighted as a change

13
00:00:24,160 --> 00:00:29,480
rather than a deletion so what are our

14
00:00:26,359 --> 00:00:31,920
first deletions well this is the storage

15
00:00:29,480 --> 00:00:33,360
of the frame pointer to the stack and

16
00:00:31,920 --> 00:00:34,960
the movement of the frame pointer and

17
00:00:33,360 --> 00:00:37,480
this is the reloading of the frame

18
00:00:34,960 --> 00:00:39,559
pointer so the optimization level one

19
00:00:37,480 --> 00:00:41,840
does not use the frame pointers and it

20
00:00:39,559 --> 00:00:43,559
just gets rid of that code straight up

21
00:00:41,840 --> 00:00:45,399
additionally we see it removing that

22
00:00:43,559 --> 00:00:47,039
stray KN that we don't really know why

23
00:00:45,399 --> 00:00:48,719
it's there but it doesn't need to be

24
00:00:47,039 --> 00:00:51,440
there and so it is removed at

25
00:00:48,719 --> 00:00:52,760
optimization level one now I did look up

26
00:00:51,440 --> 00:00:54,280
why that knop was there because I

27
00:00:52,760 --> 00:00:56,000
thought maybe it was being used like

28
00:00:54,280 --> 00:01:00,079
some other architectures and I found

29
00:00:56,000 --> 00:01:03,320
this post from 2019 by Jim Wilson a GNU

30
00:01:00,079 --> 00:01:05,080
tool chain developer since 1987 so he

31
00:01:03,320 --> 00:01:07,280
gives some good description of what's

32
00:01:05,080 --> 00:01:09,960
going on here and he says the knops

33
00:01:07,280 --> 00:01:12,159
emitted by GCC at optimization level

34
00:01:09,960 --> 00:01:14,320
zero are more of a historical artifact

35
00:01:12,159 --> 00:01:17,240
of implementation these knops are not

36
00:01:14,320 --> 00:01:19,560
emitted at 01 we just saw that the knops

37
00:01:17,240 --> 00:01:21,759
may be emitted if we have a source line

38
00:01:19,560 --> 00:01:23,439
that no code needs to be emitted for we

39
00:01:21,759 --> 00:01:27,040
emit a knop so that we have some place

40
00:01:23,439 --> 00:01:30,520
to attach the line number info with- g-g

41
00:01:27,040 --> 00:01:32,600
is like the- GDB which we use in AR code

42
00:01:30,520 --> 00:01:35,399
in order to tell it to create symbols

43
00:01:32,600 --> 00:01:37,360
for GDB the knops may be emitted if we

44
00:01:35,399 --> 00:01:39,840
have a node in the control flow graph

45
00:01:37,360 --> 00:01:41,520
that has no code in it the knop is

46
00:01:39,840 --> 00:01:43,560
emitted so that the simplifying the

47
00:01:41,520 --> 00:01:45,759
control flow graph won't cause the basic

48
00:01:43,560 --> 00:01:48,399
block to accidentally disappear so again

49
00:01:45,759 --> 00:01:51,399
I sort of mentioned before that you know

50
00:01:48,399 --> 00:01:53,880
rote compilation of very simple code is

51
00:01:51,399 --> 00:01:56,200
going to have to do with the changing of

52
00:01:53,880 --> 00:01:58,320
abstract syntax trees and graphs that

53
00:01:56,200 --> 00:02:00,119
compilers do behind the scenes and they

54
00:01:58,320 --> 00:02:01,560
just try to have you know at simple

55
00:02:00,119 --> 00:02:03,439
levels of optimization at no

56
00:02:01,560 --> 00:02:05,719
optimization they just try to do one

57
00:02:03,439 --> 00:02:07,320
toone replacement and as it says here

58
00:02:05,719 --> 00:02:09,360
this stems from the design decision that

59
00:02:07,320 --> 00:02:11,000
the code emitted at optimization zero

60
00:02:09,360 --> 00:02:12,440
should be simple quick and easy to

61
00:02:11,000 --> 00:02:14,680
generate and it should be easily

62
00:02:12,440 --> 00:02:17,000
debuggable and that adding DG should

63
00:02:14,680 --> 00:02:18,680
never change the code emitted by GCC so

64
00:02:17,000 --> 00:02:20,599
that is our little mini mystery about

65
00:02:18,680 --> 00:02:23,519
the knop and it's just to say that it is

66
00:02:20,599 --> 00:02:25,560
not being used for like a delay slot as

67
00:02:23,519 --> 00:02:28,920
you may find on other architectures the

68
00:02:25,560 --> 00:02:30,319
KN is just there because GCC has various

69
00:02:28,920 --> 00:02:32,239
historical artifacts effect of its

70
00:02:30,319 --> 00:02:34,519
implementation which are now being

71
00:02:32,239 --> 00:02:36,200
applied to RISC-V as they added it and

72
00:02:34,519 --> 00:02:38,440
then there's one last change between

73
00:02:36,200 --> 00:02:41,200
optimization level 0 and 1 and that is

74
00:02:38,440 --> 00:02:43,840
the change in this ad eyes constant so

75
00:02:41,200 --> 00:02:45,599
it was 32 here and it's 28 there and

76
00:02:43,840 --> 00:02:49,319
that really just boils down to the fact

77
00:02:45,599 --> 00:02:50,920
that this ADI is a we PC zero which

78
00:02:49,319 --> 00:02:53,480
means it's getting the PC and then it's

79
00:02:50,920 --> 00:02:55,760
adding a constant to it to go find some

80
00:02:53,480 --> 00:02:58,319
string in this case the string for hello

81
00:02:55,760 --> 00:03:01,400
world and that string is going to be

82
00:02:58,319 --> 00:03:03,760
stored after the code and so it's just

83
00:03:01,400 --> 00:03:06,000
calculating a pointer to the string and

84
00:03:03,760 --> 00:03:10,319
because the code got smaller the pointer

85
00:03:06,000 --> 00:03:10,319
to the string is less distance away

