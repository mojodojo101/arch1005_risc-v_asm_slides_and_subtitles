1
00:00:00,719 --> 00:00:05,520
continuing our exploration of compound

2
00:00:03,679 --> 00:00:08,840
variable types we're now going to

3
00:00:05,520 --> 00:00:12,240
introduce structs or structures so I

4
00:00:08,840 --> 00:00:15,320
typed f a new mystruct of type my

5
00:00:12,240 --> 00:00:18,960
structor T which shall contain a single

6
00:00:15,320 --> 00:00:21,960
short six integers all in an array and a

7
00:00:18,960 --> 00:00:25,240
single long long so that structure is

8
00:00:21,960 --> 00:00:28,679
declared my struct foo and then f.a which

9
00:00:25,240 --> 00:00:31,279
is the short is set to babe f. C is set

10
00:00:28,679 --> 00:00:35,399
to Balboa bled blood because it's always

11
00:00:31,279 --> 00:00:37,719
like I say to my wife Babe b blood blood

12
00:00:35,399 --> 00:00:40,760
anyways food. a is Babe which was a

13
00:00:37,719 --> 00:00:42,960
short it is now being assigned to an INT

14
00:00:40,760 --> 00:00:44,960
B of one that's going to lead to some

15
00:00:42,960 --> 00:00:48,440
sign extension because it is a signed

16
00:00:44,960 --> 00:00:50,480
short and a signed int so that 16bit

17
00:00:48,440 --> 00:00:52,879
value is sign extended up to a 32-bit

18
00:00:50,480 --> 00:00:55,960
value then we do math on the 32-bit

19
00:00:52,879 --> 00:00:57,600
Value plus the 64-bit value so again

20
00:00:55,960 --> 00:01:00,800
there's going to be some sign extension

21
00:00:57,600 --> 00:01:04,280
going on and storing it into a smaller

22
00:01:00,800 --> 00:01:07,520
variable the integer so 64-bit Value

23
00:01:04,280 --> 00:01:09,680
plus 32-bit value into a 32-bit storage

24
00:01:07,520 --> 00:01:12,119
and ultimately returning that storage

25
00:01:09,680 --> 00:01:14,840
once again compiled without the stack

26
00:01:12,119 --> 00:01:17,799
protector to simplify the code slightly

27
00:01:14,840 --> 00:01:20,280
so here's what we get and we get a bunch

28
00:01:17,799 --> 00:01:23,159
of new assembly instructions so let's

29
00:01:20,280 --> 00:01:26,560
check them out first we have store

30
00:01:23,159 --> 00:01:28,799
halfword so halfword again 16bit values

31
00:01:26,560 --> 00:01:31,240
that makes sense we had a 16 bit value

32
00:01:28,799 --> 00:01:33,640
in this code that short a so to write

33
00:01:31,240 --> 00:01:36,960
the value babee to a we're going to

34
00:01:33,640 --> 00:01:40,520
store a half word so this is going to be

35
00:01:36,960 --> 00:01:42,439
a store which stores from left to right

36
00:01:40,520 --> 00:01:44,479
we've got a calculation of a memory

37
00:01:42,439 --> 00:01:47,560
address and that is the parentheses

38
00:01:44,479 --> 00:01:51,119
saying memory address and it is rs1

39
00:01:47,560 --> 00:01:52,479
address plus sign extended immediate 12

40
00:01:51,119 --> 00:01:56,119
that is the memory address where we

41
00:01:52,479 --> 00:01:59,119
shall store the halfword sized lower 16

42
00:01:56,119 --> 00:02:01,079
bits from rs2 and again I'm a broken

43
00:01:59,119 --> 00:02:03,960
record here thanks to the miracle of

44
00:02:01,079 --> 00:02:07,119
copy and paste unlike most instructions

45
00:02:03,960 --> 00:02:09,759
stores are read from left to right this

46
00:02:07,119 --> 00:02:11,920
arrow goes that way for everything else

47
00:02:09,759 --> 00:02:14,160
it goes the opposite direction and the

48
00:02:11,920 --> 00:02:16,800
parentheses indicate the parentheses

49
00:02:14,160 --> 00:02:18,800
specifically in the disassembled output

50
00:02:16,800 --> 00:02:20,920
indicate that something is being used as

51
00:02:18,800 --> 00:02:22,879
a memory address and there is going to

52
00:02:20,920 --> 00:02:24,800
be a dereferencing of memory for either

53
00:02:22,879 --> 00:02:26,599
reading or writing on the flip side

54
00:02:24,800 --> 00:02:29,200
there is load half word so we're going

55
00:02:26,599 --> 00:02:33,200
to get something back out from memory so

56
00:02:29,200 --> 00:02:36,400
this is red from right to left so rs1

57
00:02:33,200 --> 00:02:39,239
plus immediate 12 pluck a half word 16

58
00:02:36,400 --> 00:02:41,680
bits out of that and store it into this

59
00:02:39,239 --> 00:02:42,920
register now this is going to sign

60
00:02:41,680 --> 00:02:46,200
extend it to

61
00:02:42,920 --> 00:02:48,920
32bits and if we're in 32-bit code then

62
00:02:46,200 --> 00:02:50,800
you're done but if you are in 64-bit

63
00:02:48,920 --> 00:02:53,840
code then it will further sign extended

64
00:02:50,800 --> 00:02:56,720
up to the full 64-bit size of your

65
00:02:53,840 --> 00:02:58,640
64-bit register broken record once again

66
00:02:56,720 --> 00:03:00,760
this instruction is read from right to

67
00:02:58,640 --> 00:03:03,040
left and because there's parentheses we

68
00:03:00,760 --> 00:03:05,879
know that it is using memory then we

69
00:03:03,040 --> 00:03:09,040
picked up another instruction add W for

70
00:03:05,879 --> 00:03:11,760
add word register to word register so

71
00:03:09,040 --> 00:03:14,280
two parameters a source one and a source

72
00:03:11,760 --> 00:03:16,440
two so add these two together and place

73
00:03:14,280 --> 00:03:20,120
the result in the destination but the

74
00:03:16,440 --> 00:03:22,440
key thing here is this is an RV64I only

75
00:03:20,120 --> 00:03:25,879
assembly instruction so it is dealing

76
00:03:22,440 --> 00:03:27,400
with word registers and normally RV64I

77
00:03:25,879 --> 00:03:29,720
is dealing with double word sized

78
00:03:27,400 --> 00:03:31,599
registers they're 64 bits but this is

79
00:03:29,720 --> 00:03:34,080
instruction is saying specifically I'm

80
00:03:31,599 --> 00:03:36,799
in 64-bit mode but I want to deal in

81
00:03:34,080 --> 00:03:40,360
32-bit registers so consequently it's

82
00:03:36,799 --> 00:03:43,760
going to take the bottom 32 bits of rs1

83
00:03:40,360 --> 00:03:47,120
and the bottom 32 bits of rs2 and add

84
00:03:43,760 --> 00:03:49,439
them together yielding a 32-bit result

85
00:03:47,120 --> 00:03:51,280
and then it will sign extend that 64

86
00:03:49,439 --> 00:03:52,720
bits and store the result into the

87
00:03:51,280 --> 00:03:54,519
destination register and this

88
00:03:52,720 --> 00:03:56,480
instruction isn't going to care if there

89
00:03:54,519 --> 00:03:58,519
was some sort of overflow it's just

90
00:03:56,480 --> 00:04:00,840
going to add them together and there's

91
00:03:58,519 --> 00:04:03,000
no indication anywhere that the result

92
00:04:00,840 --> 00:04:06,599
may have overflown and then there's the

93
00:04:03,000 --> 00:04:08,959
sex instruction sign extend word in

94
00:04:06,599 --> 00:04:13,400
register and then there's the Oho

95
00:04:08,959 --> 00:04:16,120
conspicuous seex instruction so S.W

96
00:04:13,400 --> 00:04:19,079
specifically is sign extend word in

97
00:04:16,120 --> 00:04:22,440
register to double word register so this

98
00:04:19,079 --> 00:04:25,360
is an RV64I instruction and it is

99
00:04:22,440 --> 00:04:27,960
saying Hey I want to treat rs1 as a

100
00:04:25,360 --> 00:04:30,720
32-bit word register and I want that

101
00:04:27,960 --> 00:04:33,759
sign extended and just placed into the

102
00:04:30,720 --> 00:04:36,280
dword double word register destination

103
00:04:33,759 --> 00:04:39,520
but this is scandalous isn't it RISC

104
00:04:36,280 --> 00:04:41,600
five encouraging sexting what about the

105
00:04:39,520 --> 00:04:44,759
children won't somebody please think of

106
00:04:41,600 --> 00:04:46,560
the children well it's just that UNH

107
00:04:44,759 --> 00:04:48,560
wholesome influence of those pseudo

108
00:04:46,560 --> 00:04:50,680
instructions at it again and so as a

109
00:04:48,560 --> 00:04:53,039
reminder I've never actually seen this

110
00:04:50,680 --> 00:04:54,800
vtv show so I have no context for what

111
00:04:53,039 --> 00:04:56,400
this picture is but I can tell you it's

112
00:04:54,800 --> 00:04:58,800
extra inappropriate when we add that

113
00:04:56,400 --> 00:05:00,880
little jiggle so let's go ahead and get

114
00:04:58,800 --> 00:05:02,800
rid of that UNH wholesome pseudo

115
00:05:00,880 --> 00:05:05,680
instruction through the use of chaos

116
00:05:02,800 --> 00:05:08,759
magic rewrite reality and there we go

117
00:05:05,680 --> 00:05:11,400
nice clean assembly traditional Family

118
00:05:08,759 --> 00:05:14,600
Values no more Sexting in our assembly

119
00:05:11,400 --> 00:05:16,960
but we do pick up some compressed Lou

120
00:05:14,600 --> 00:05:20,080
some compressed ad IW which we haven't

121
00:05:16,960 --> 00:05:21,840
seen ad IW yet oh wait there it is AD IW

122
00:05:20,080 --> 00:05:24,280
down a couple assembly instructions

123
00:05:21,840 --> 00:05:26,080
later and a compressed at w we just

124
00:05:24,280 --> 00:05:27,680
literally learned about ad W and now

125
00:05:26,080 --> 00:05:29,840
we're picking up the compressed form

126
00:05:27,680 --> 00:05:32,520
great hope you remember it well anyway

127
00:05:29,840 --> 00:05:36,240
the sexting is not sexting it's a pseudo

128
00:05:32,520 --> 00:05:39,560
instruction which is actually ad IW and

129
00:05:36,240 --> 00:05:43,039
this zero for the immediate so what is

130
00:05:39,560 --> 00:05:45,120
AD IW ad IW is ADD immediate to word

131
00:05:43,039 --> 00:05:47,680
it's another one of these RV64I

132
00:05:45,120 --> 00:05:49,400
instructions where the W suffix is

133
00:05:47,680 --> 00:05:52,600
specifically telling us we want to deal

134
00:05:49,400 --> 00:05:54,479
in word-sized registers so we've got a

135
00:05:52,600 --> 00:05:56,199
source we've got an immediate we've got

136
00:05:54,479 --> 00:06:00,240
a destination so this is going to take

137
00:05:56,199 --> 00:06:02,800
the bottom 32bits word size of the rs1

138
00:06:00,240 --> 00:06:05,000
source register and then take the 12-bit

139
00:06:02,800 --> 00:06:07,919
immediate and sign extended to only 32

140
00:06:05,000 --> 00:06:10,160
bits add them together yield a 32bit

141
00:06:07,919 --> 00:06:11,840
result and then sign extend that

142
00:06:10,160 --> 00:06:13,560
ultimately and place it into the

143
00:06:11,840 --> 00:06:16,280
destination register because we're still

144
00:06:13,560 --> 00:06:20,240
in RV 64 mode and it is a 64-bit

145
00:06:16,280 --> 00:06:22,880
register so this again like the ad W is

146
00:06:20,240 --> 00:06:24,960
going to ignore any sort of overflow

147
00:06:22,880 --> 00:06:26,520
that occurs as a result of this addition

148
00:06:24,960 --> 00:06:28,440
but the key thing is that we can see

149
00:06:26,520 --> 00:06:30,160
that if SE which behind the scenes is

150
00:06:28,440 --> 00:06:33,039
really an Adi

151
00:06:30,160 --> 00:06:34,919
destination source and a zero it's

152
00:06:33,039 --> 00:06:37,599
really just taking advantage of the fact

153
00:06:34,919 --> 00:06:39,800
that the ad IW is performing this sign

154
00:06:37,599 --> 00:06:40,960
extension after the addition and then

155
00:06:39,800 --> 00:06:43,120
it's saying you know what I'm going to

156
00:06:40,960 --> 00:06:46,599
add zero I'm going to just treat this as

157
00:06:43,120 --> 00:06:50,120
a word register 32 bits add zero and

158
00:06:46,599 --> 00:06:52,360
then the adiw will do a 32 to 64-bit

159
00:06:50,120 --> 00:06:54,880
sign extension for me so sexting is

160
00:06:52,360 --> 00:06:57,319
really just adiw when we got rid of the

161
00:06:54,880 --> 00:07:00,280
instruction aliases we did also see a

162
00:06:57,319 --> 00:07:02,599
compressed at IW and this is the same

163
00:07:00,280 --> 00:07:05,160
ultimate result but it is of course

164
00:07:02,599 --> 00:07:07,120
compressed it has less bits and so while

165
00:07:05,160 --> 00:07:09,280
it'll be written like destination

166
00:07:07,120 --> 00:07:11,759
register and an immediate that immediate

167
00:07:09,280 --> 00:07:14,280
is actually only a six-bit immediate and

168
00:07:11,759 --> 00:07:16,479
the destination is both a source and a

169
00:07:14,280 --> 00:07:20,840
destination so basically you're going to

170
00:07:16,479 --> 00:07:23,639
do Rd plus sign extended immediate 6 and

171
00:07:20,840 --> 00:07:26,240
store the result into Rd now there is a

172
00:07:23,639 --> 00:07:28,400
caveat here that Rd may not be the zero

173
00:07:26,240 --> 00:07:30,240
register because that's reserved and

174
00:07:28,400 --> 00:07:32,879
then moving back back to before we

175
00:07:30,240 --> 00:07:34,400
learned about sex we had the add W

176
00:07:32,879 --> 00:07:36,280
instruction we just learned about it

177
00:07:34,400 --> 00:07:39,440
this is now the compressed add word

178
00:07:36,280 --> 00:07:41,879
register to word register so ad IW

179
00:07:39,440 --> 00:07:43,919
versus add W they're all just additions

180
00:07:41,879 --> 00:07:45,759
on word registers one of them is

181
00:07:43,919 --> 00:07:47,520
register to register ad and one of them

182
00:07:45,759 --> 00:07:50,039
is an immediate to register ad this is

183
00:07:47,520 --> 00:07:52,400
the register to register so it is giving

184
00:07:50,039 --> 00:07:54,639
us a destination and a source they are

185
00:07:52,400 --> 00:07:57,560
destination prime source prime that

186
00:07:54,639 --> 00:08:00,960
expands to the uncompressed form of

187
00:07:57,560 --> 00:08:03,319
destination plus source two because this

188
00:08:00,960 --> 00:08:05,440
is also implicitly source one source one

189
00:08:03,319 --> 00:08:07,360
source two add them together place them

190
00:08:05,440 --> 00:08:09,919
into the destination register but this

191
00:08:07,360 --> 00:08:13,479
is an add W so treat those things like

192
00:08:09,919 --> 00:08:16,199
they are 32-bit values so r d prime is

193
00:08:13,479 --> 00:08:18,800
the bottom 32 of whatever the initial Rd

194
00:08:16,199 --> 00:08:21,680
prime value is plus the bottom 32 bits

195
00:08:18,800 --> 00:08:24,680
of rs2 prime get a 32-bit result sign

196
00:08:21,680 --> 00:08:26,599
extended to 64 bits a quick reminder on

197
00:08:24,680 --> 00:08:28,960
LUI from before LUI had a

198
00:08:26,599 --> 00:08:31,800
destination register and a upper IM

199
00:08:28,960 --> 00:08:33,399
mediate to 20 bits and it set bits 31

200
00:08:31,800 --> 00:08:36,839
through 12 so that we could ultimately

201
00:08:33,399 --> 00:08:39,120
create things like 32-bit constants well

202
00:08:36,839 --> 00:08:41,240
there is a compressed LUI that popped

203
00:08:39,120 --> 00:08:45,320
up in this code and the encoding is like

204
00:08:41,240 --> 00:08:47,120
this got a nonzero upper immediate six

205
00:08:45,320 --> 00:08:48,399
and so it gets a little more complicated

206
00:08:47,120 --> 00:08:52,160
because it's not just setting the upper

207
00:08:48,399 --> 00:08:54,240
20 bits it's setting bits 17 through 12

208
00:08:52,160 --> 00:08:57,120
with our six bits there and then it

209
00:08:54,240 --> 00:08:59,560
takes and it s extends bit 17 to the

210
00:08:57,120 --> 00:09:01,680
rest of the upper bits and it Z zeros

211
00:08:59,560 --> 00:09:04,480
out the bottom 12 bits just like the

212
00:09:01,680 --> 00:09:06,279
LUI did before so I think um well let

213
00:09:04,480 --> 00:09:08,120
me just say quick that like the there is

214
00:09:06,279 --> 00:09:10,920
a further caveat that the destination

215
00:09:08,120 --> 00:09:13,279
register cannot be x0 or x2 but I think

216
00:09:10,920 --> 00:09:15,560
this human readable text makes it a

217
00:09:13,279 --> 00:09:18,600
little clear compressed LUI loads the

218
00:09:15,560 --> 00:09:21,720
nonzero 6bit immediate field into bits

219
00:09:18,600 --> 00:09:24,399
17 to 12 of the destination register

220
00:09:21,720 --> 00:09:27,440
clears the bottom 12 bits and sign

221
00:09:24,399 --> 00:09:29,560
extends bit 17 into all higher bits of

222
00:09:27,440 --> 00:09:31,680
the destination all higher bit of the

223
00:09:29,560 --> 00:09:34,279
destination being a nice generic way to

224
00:09:31,680 --> 00:09:36,040
say that although this is an rv32

225
00:09:34,279 --> 00:09:38,480
compressed instruction if it was

226
00:09:36,040 --> 00:09:40,839
executed as it is here in the context of

227
00:09:38,480 --> 00:09:43,079
64-bit code all higher bits would mean

228
00:09:40,839 --> 00:09:46,240
it will sign extend that bit 17 all the

229
00:09:43,079 --> 00:09:48,560
way up to bit 63 the maximum size for

230
00:09:46,240 --> 00:09:49,920
the 64-bit destination register and so

231
00:09:48,560 --> 00:09:51,720
with that we've learned way more

232
00:09:49,920 --> 00:09:53,600
assembly instructions than we want to so

233
00:09:51,720 --> 00:09:56,640
it's time for you once again to go off

234
00:09:53,600 --> 00:10:00,000
and draw a stack diagram figure out how

235
00:09:56,640 --> 00:10:01,880
that my struct T is stor on the stack

236
00:10:00,000 --> 00:10:04,079
what stored where what order is it

237
00:10:01,880 --> 00:10:07,200
reordered by the compiler let's figure

238
00:10:04,079 --> 00:10:09,640
it out by reading the code so go ahead

239
00:10:07,200 --> 00:10:12,760
and step through the code you can see it

240
00:10:09,640 --> 00:10:14,279
with your instruction aliases the sex or

241
00:10:12,760 --> 00:10:17,040
you can see it without the instruction

242
00:10:14,279 --> 00:10:20,839
aliases the ADDI whatever is more

243
00:10:17,040 --> 00:10:20,839
convenient for you

