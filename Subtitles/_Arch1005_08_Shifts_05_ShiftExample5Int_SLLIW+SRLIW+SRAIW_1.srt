1
00:00:00,160 --> 00:00:04,440
all right Pop Quiz Hot Shot you've got

2
00:00:02,560 --> 00:00:07,520
shifts that use immediates that are

3
00:00:04,440 --> 00:00:09,480
known to the compiler and you've got INS

4
00:00:07,520 --> 00:00:10,480
What kind of shifts do you think are

5
00:00:09,480 --> 00:00:13,200
going to be

6
00:00:10,480 --> 00:00:16,560
generated if you answered shifts with

7
00:00:13,200 --> 00:00:22,080
both an i and a w you answered correctly

8
00:00:16,560 --> 00:00:24,160
we've got sirly w s sray w and silly W

9
00:00:22,080 --> 00:00:27,119
yes it's the return of the silly that we

10
00:00:24,160 --> 00:00:29,519
love with a W suffix so I'm not going to

11
00:00:27,119 --> 00:00:31,679
even bother with the build slide here

12
00:00:29,519 --> 00:00:34,079
just going to say it's word sized so

13
00:00:31,679 --> 00:00:36,040
it's RV64I which means it's always

14
00:00:34,079 --> 00:00:39,440
going to be a 5 bit shift because we're

15
00:00:36,040 --> 00:00:41,280
treating things as 32bit registers and

16
00:00:39,440 --> 00:00:43,719
it's a shift left logical with an

17
00:00:41,280 --> 00:00:45,960
immediate an i for an immediate rather

18
00:00:43,719 --> 00:00:48,199
than pulling it out of a register so

19
00:00:45,960 --> 00:00:53,559
concretely looking at our example we

20
00:00:48,199 --> 00:00:57,039
have silly w we got T1 t0 and 12 so if

21
00:00:53,559 --> 00:01:01,879
we shift it over by hard-coded 12 then

22
00:00:57,039 --> 00:01:05,159
we're going to get Zer 6 7 7 8 8 and

23
00:01:01,879 --> 00:01:07,720
three Zer so shifted by 12 and

24
00:01:05,159 --> 00:01:11,400
ultimately sign extended same as before

25
00:01:07,720 --> 00:01:13,680
with the C w one more example for good

26
00:01:11,400 --> 00:01:15,320
measure showing that sign extension in

27
00:01:13,680 --> 00:01:18,640
action we're going to shift it by a

28
00:01:15,320 --> 00:01:20,880
hardcoded 13 instead of 12 and once

29
00:01:18,640 --> 00:01:24,280
again that means that our upper bit of

30
00:01:20,880 --> 00:01:26,720
that six goes away from being a 01 to

31
00:01:24,280 --> 00:01:29,640
being just a one and we're going to use

32
00:01:26,720 --> 00:01:30,439
the one to sign extend and we get Fs C e

33
00:01:29,640 --> 00:01:33,200
f

34
00:01:30,439 --> 00:01:35,200
F10 and again you can try this out when

35
00:01:33,200 --> 00:01:37,720
you get to Stepping Through the Code by

36
00:01:35,200 --> 00:01:40,479
passing in your own shift values so that

37
00:01:37,720 --> 00:01:44,360
is the shift left and of course the sign

38
00:01:40,479 --> 00:01:46,200
extension all right sirly W same thing

39
00:01:44,360 --> 00:01:48,040
concretely let's look at it we know

40
00:01:46,200 --> 00:01:49,960
we're going right we know it's logical

41
00:01:48,040 --> 00:01:52,399
so it's just going to always be a zero

42
00:01:49,960 --> 00:01:54,479
EXT uh zero filling in and it's an

43
00:01:52,399 --> 00:01:56,799
immediate which is our hardcoded 12 and

44
00:01:54,479 --> 00:02:00,039
it's w so we're only going to consider

45
00:01:56,799 --> 00:02:02,840
these bottom 32 bits so what do we get

46
00:02:00,039 --> 00:02:04,759
we get the zeros right here in the

47
00:02:02,840 --> 00:02:06,600
middle now so this is what's different

48
00:02:04,759 --> 00:02:08,560
from the left shift we're pulling the

49
00:02:06,600 --> 00:02:11,520
stuff out of the middle and so we move

50
00:02:08,560 --> 00:02:13,520
it down by 12 to the right by 12 and

51
00:02:11,520 --> 00:02:15,200
then it's a logical shift so we fill in

52
00:02:13,520 --> 00:02:17,840
zeros and of course that's zero

53
00:02:15,200 --> 00:02:19,720
extension for the other 32 upper bits

54
00:02:17,840 --> 00:02:24,360
because it can never be anything but

55
00:02:19,720 --> 00:02:26,959
zeros and our last thing the sray W same

56
00:02:24,360 --> 00:02:28,560
thing we go ahead and we're now here I'm

57
00:02:26,959 --> 00:02:30,640
going to show you the two examples that

58
00:02:28,560 --> 00:02:33,200
I didn't show you in the previous videos

59
00:02:30,640 --> 00:02:38,080
so here I am starting with 1 1 22 3 3 4

60
00:02:33,200 --> 00:02:40,040
4 55 and we shift it by 12 so here that

61
00:02:38,080 --> 00:02:43,159
five gets shifted down along with

62
00:02:40,040 --> 00:02:45,200
everything else and zeros gets filled in

63
00:02:43,159 --> 00:02:48,000
for the upper bits because the upper

64
00:02:45,200 --> 00:02:50,959
most significant bit bit 31 of this

65
00:02:48,000 --> 00:02:52,959
32-bit register was set to zero so that

66
00:02:50,959 --> 00:02:55,879
is what is used for the fill in of the

67
00:02:52,959 --> 00:02:58,480
shift sign extension means we've get all

68
00:02:55,879 --> 00:03:01,680
zeros in the upper 32 bits now back to

69
00:02:58,480 --> 00:03:03,760
that example where instead I use a

70
00:03:01,680 --> 00:03:08,000
11223344

71
00:03:03,760 --> 00:03:10,239
a5 just to force the upper bit to be one

72
00:03:08,000 --> 00:03:13,360
and they all shift down by 12 and the

73
00:03:10,239 --> 00:03:14,959
upper one is filled in for that so

74
00:03:13,360 --> 00:03:16,120
nothing should be too surprising here

75
00:03:14,959 --> 00:03:18,120
we're really just dealing with

76
00:03:16,120 --> 00:03:21,640
permutations that we've already seen

77
00:03:18,120 --> 00:03:23,519
multiple times so one last time into the

78
00:03:21,640 --> 00:03:25,400
breach this is going to be your last

79
00:03:23,519 --> 00:03:27,040
time stepping through the assembly and

80
00:03:25,400 --> 00:03:32,760
checking your understanding of these

81
00:03:27,040 --> 00:03:32,760
last of the 18 variants of shifts

